liveBet.controller('realTimeBettingController7g', function ($scope, $http, $filter) {
    $scope.bettingResult = [];
    $scope.oldBettingData = [];
    $scope.confirmData = [];
    $scope.loading = true;
    $scope.selectedData = [];
    $scope.timePeriod = 20000;
    $scope.totalStake = 0;
    $scope.possibleWin = 0;
    $scope.confirmSuccess = false;
    $scope.confirmFail = false;
    let getApi;
    $scope.isChanged = false;
    $scope.slipOddTotal = 0;

    $scope.changeNow = false;

    $scope.timeFormate = function (timestap) {
        timestap = parseInt(timestap);
        let h = padZero(new Date(timestap).getHours());
        let m = padZero(new Date(timestap).getMinutes());
        return h + ":" + m;
    }

    function padZero(n) {
        if (n < 10) return '0' + n;
        return n;
    }

    getBetData();

    $scope.$on('activeGame',function(event,opt){
        for(let item in opt)
        {
            if(item === 'tennis')
            {
                getBetData();
            }
        }
    });


    function getApiResponse() {
        if($scope.$parent.tab_contents.tennis)
        {
            getApi = setInterval(function () {
                getBetData();
            }, $scope.timePeriod);
        }

    }

    $scope.clearBetSlip = function () {
        if ($scope.selectedData.length === 0) {
            alert("You haven't selected a slip yet");
        } else {
            $scope.selectedData.forEach(function (item) {
                $('#' + item.type +
                    item.id).removeClass('active-bet-juice');
            });
            $scope.selectedData = [];
            calculateReport();
        }
    };

    function getBetData() {
        $http.get('/live-tennis').then(function (response) {
           // console.log(response);
            if (response.data) {
                console.log(response.data);
                $scope.$parent.loading = false;
                $scope.oldBettingData = $scope.bettingResult;
                $scope.bettingResult = response.data;
                //console.log(response.data);
                betRefresh();
                $scope.loading = false;
                $scope.$parent.sports_avaiable.tennis = ($scope.bettingResult.length == 0) ? false:true;
                //console.log('old',$scope.oldBettingData);
               if ($scope.bettingResult.length < 5) {
                    clearInterval(getApi);
                    $scope.timePeriod = 3000;
                    getApiResponse();
                } else if ($scope.bettingResult.length >= 5 && $scope.bettingResult.length
                    < 10) {
                    clearInterval(getApi);
                    $scope.timePeriod = 5000;
                    getApiResponse();
                } else if ($scope.bettingResult.length >= 10
                    && $scope.bettingResult.length < 25) {
                    clearInterval(getApi);
                    $scope.timePeriod = 10000;
                    getApiResponse();
                } else if ($scope.bettingResult.length >= 25 && $scope.bettingResult.length < 35) {
                    clearInterval(getApi);
                    $scope.timePeriod = 15000;
                    getApiResponse();
                } else if ($scope.bettingResult.length >= 35) {
                    clearInterval(getApi);
                    $scope.timePeriod = 20000;
                    getApiResponse();
                }
            }
        });

    }

// SET selected value to latest value.
    function betRefresh() {
        let newTotal = 0;
        $scope.selectedData.forEach((item) => {
            let replaced = false;
            $scope.bettingResult.forEach((dataitem) => {
                dataitem.results.forEach(result => {
                    if (item.id === result.id) {
                        switch (item.type) {
                            case 'odd_first':
                                if (result.winner_set) {
                                    item.score = result.winner_set.home_odd;
                                    replaced = true;
                                }
                                break;
                            case 'odd_second':
                                if (result.winner_set) {
                                    item.score = result.winner_set.away_odd;
                                    replaced = true;
                                }
                                break;
                            case 'cur_first':
                                if (result.current_set) {
                                    item.score = result.current_set.home_odd;
                                    replaced = true;
                                }
                                break;
                            case 'cur_second':
                                if (result.current_set) {
                                    item.score = result.current_set.away_odd;
                                    replaced = true;
                                }
                                break;
                        }
                        if ($scope.changeNow) {
                            newTotal += item.score;
                        }


                    }
                });

            });
            if (!replaced)
                $scope.selectedData.splice($scope.selectedData.indexOf(item), 1);
            calculateReport();
        });
        if ($scope.changeNow) {
            if (newTotal !== $scope.slipOddTotal)
                $scope.isChanged = true;
        }

    }

// Increase, Decrease Icon && Add active class
    $scope.compareODD = function (id, type) {
        let oldFirstVal = 0;
        let curFirstVal = 0;
        let oldCur = 0;
        let curCur = 0;
        let oldNext = 0;
        let curNext = 0;
        let selected = false;
        let oldresult = [];
        let newresult = [];
        if ($scope.oldBettingData.length < 1) return;
        if (type === 'odd_first') {
            $scope.selectedData.forEach((item) => {
                if (item.id == id && item.type == type) {
                    $('#odd_first' + id).addClass('active');
                    selected = true;
                }
            });
            if (!selected)
                $('#odd_first' + id).removeClass('active');

            $scope.oldBettingData.forEach((item) => {
                item.results.forEach(ite => {
                    if (ite.id === id) {
                        oldresult = ite;
                    }
                });

            });

            oldFirstVal = oldresult.winner_set.home_odd;
            $scope.bettingResult.forEach((item) => {
                item.results.forEach(ite => {
                    if (ite.id === id) {
                        newresult = ite;
                    }
                });
            });
            $("#betSlip_" + id + "_" + type + "_decrease").hide();
            $("#betSlip_" + id + "_" + type + "_increase").hide();
            curFirstVal = newresult.winner_set.home_odd;
            if (oldFirstVal > curFirstVal) {
                $('#first_' + id).css('color', 'red');
                $('#first_' + id + '_decrease').show();
                $('#first_' + id + '_decrease').closest('li').addClass('__blink');

                $("#betSlip_" + id + "_" + type + "_decrease").show();
                betChanges(id, type);
            } else if (oldFirstVal < curFirstVal) {
                $('#first_' + id).css('color', 'green');
                $('#first_' + id + '_increase').show();
                $('#first_' + id + '_increase').closest('li').addClass('__blink');

                $("#betSlip_" + id + "_" + type + "_increase").show();
                betChanges(id,
                    type);
            }
        } else if (type === 'odd_second') {
            $scope.selectedData.forEach((item) => {

                if (item.id == id && item.type == type) {
                    $('#odd_second' + id).addClass('active');
                    selected = true;
                }


            });
            if (!selected) $('#odd_second' + id).removeClass('active');

            angular.forEach($scope.oldBettingData, function (item) {
                item.results.forEach(ite => {
                    if (ite.id === id) {
                       // console.log(ite,'control check');
                        oldFirstVal = ite.winner_set.away_odd;
                    }
                });

            });

            angular.forEach($scope.bettingResult, function (item) {
                item.results.forEach(ite => {
                    if (ite.id === id) {
                        curFirstVal = ite.winner_set.away_odd;
                    }
                });

            });
            $("#betSlip_" + id + "_" + type + "_decrease").hide();
            $("#betSlip_" + id + "_" + type + "_increase").hide();
            if (oldFirstVal > curFirstVal) {
                $('#second_' + id).css('color', 'red');
                $('#second_' + id + '_decrease').show();
                $('#second_' + id + '_decrease').closest('li').addClass('__blink');

                $("#betSlip_" + id + "_" + type + "_decrease").show();
                betChanges(id, type);
            } else if (oldFirstVal < curFirstVal) {
                $('#second_' + id).css('color', 'green');
                $('#second_'
                    + id + '_increase').show();
                    $('#second_' + id + '_increase').closest('li').addClass('__blink');

                $("#betSlip_" + id + "_" + type + "_increase").show();
                betChanges(id, type);
            }
        } else if (type === 'cur_first') {
            $scope.selectedData.forEach((item) => {
                if (item.id == id && item.type == type) {
                    $('#cur_first' + id).addClass('active');
                    selected = true;
                }
            });

            if (!selected) $('#cur_first' + id).removeClass('active');

            angular.forEach($scope.oldBettingData, function (item) {
                item.results.forEach(ite => {
                    if (ite.id === id) {
                        oldCur = ite.current_set.home_odd;
                    }
                });

            });

            angular.forEach($scope.bettingResult, function (item) {
                item.results.forEach(ite => {
                    if (ite.id === id) {
                        curCur = ite.current_set.home_odd;
                    }
                });

            });
            $("#betSlip_" + id + "_" + type + "_decrease").hide();
            $("#betSlip_" + id + "_" + type + "_increase").hide();
            if (oldCur > curCur) {
                $('#third_' + id).css('color', 'red');
                $('#third_' + id + '_decrease').show();
                $('#third_' + id + '_decrease').closest('li').addClass('__blink');

                $("#betSlip_" + id + "_" + type + "_decrease").show();
                betChanges(id, type);
            } else if (oldCur < curCur) {
                $('#third_' + id).css('color', 'green');
                $('#third_' + id
                    + '_increase').show();
                    $('#third_' + id + '_increase').closest('li').addClass('__blink');

                $("#betSlip_" + id + "_" + type + "_increase").show();
                betChanges(id, type);
            }
        } else if (type === 'cur_second') {
            $scope.selectedData.forEach((item) => {
                if (item.id === id && item.type === type) {
                    $('#cur_second' + id).addClass('active');
                    selected = true;
                }
            });
            if (!selected) $('#cur_second' + id).removeClass('active');
            angular.forEach($scope.oldBettingData, function (item) {
                item.results.forEach(ite => {
                    if (ite.id === id) {
                        oldCur = ite.current_set.away_odd;
                    }
                });

            });

            angular.forEach($scope.bettingResult, function (item) {
                item.results.forEach(ite => {
                    if (ite.id === id) {
                        curCur = ite.current_set.away_odd;
                    }
                });

            });

            $("#betSlip_" + id + "_" + type + "_decrease").hide();
            $("#betSlip_" + id + "_" + type + "_increase").hide();
            if (oldCur > curCur) {
                $('#fourth_' + id).css('color', 'red');
                $('#fourth_' + id + '_decrease').show();
                $('#fourth_' + id + '_decrease').closest('li').addClass('__blink');

                $("#betSlip_" + id + "_" + type + "_decrease").show();
                betChanges(id, type);
            } else if (oldCur < curCur) {
                $('#fourth_' + id).css('color', 'green');
                $('#fourth_' +
                    id + '_increase').show();
                    $('#fourth_' + id + '_increase').closest('li').addClass('__blink');

                $("#betSlip_" + id + "_" + type + "_increase").show();
                betChanges(id, type);
            }
        }

        function betChanges(id, type) {
            $scope.selectedData.forEach((item) => {
                if (item.id === id && item.type === type) {

                    if (item.score >= 0) item.win = Math.round(item.score * item.risk) / 100;
                    else item.win = Math.round((10000 / item.score) * item.risk) * (-1) / 100;

                    if (item.score >= 0) item.risk = Math.round((10000 / item.score) * item.win) / 100;
                    else item.risk = Math.round(item.win * item.score) * (-1) / 100;

                }
            });
            calculateReport();
        };

    };

// Add To Cart ( BET Slip function )
    $scope.addtocart = function (id, type) {
        let selected = false;
        let result = [];

        $scope.selectedData.forEach((item) => {
            if (item.id === id && item.type === type) {
                $scope.selectedData.splice($scope.selectedData.indexOf(item), 1);
                $('#' + type + id).removeClass('active-bet-juice');
                selected = true;
            }
        });

        if (!selected) {

            $scope.bettingResult.forEach((data) => {
                data.results.forEach(item => {
                    if (item.id === id) {
                        result = item;
                    }

                });

            });

            let date = new Date(result.time * 1000);

            let betObject = {
                id: id,
                type: type,
                win: "",
                risk: "",
                score: 0,
                teamName: '',
                wager: '',
                displayName: '',
                isHome: 0,
                tennis_wager:'',
                column: '',
                isAway: 0,
                teamHomeName: result.home.name,
                teamAwayName: result.away.name,
                dateTime: result.datetime,
                event_date: result.time,
                event_name: result.league.name,
                bet_extra_info: {},
            };
            let slug;
            switch (type) {
                case 'odd_first':
                    betObject.isHome = 1;
                    betObject.teamName = result.home.name;
                    betObject.wager = 'Winner';
                    betObject.tennis_wager = '';
                    betObject.score = result.winner_set.home_odd;
                    betObject.column = '2';
                    slug = "home";
                    break;
                case 'odd_second':
                    betObject.teamName = result.away.name;
                    betObject.wager = 'Winner';
                    betObject.tennis_wager = '';
                    betObject.score = result.winner_set.away_odd;
                    betObject.column = '2';
                    slug = "away";
                    betObject.isAway = 1;
                    break;
                case 'cur_first':
                    betObject.isHome = 1;
                    betObject.teamName = result.home.name;
                    betObject.wager = 'Current Set ('+result.period+')';
                    betObject.tennis_wager = result.period;
                    betObject.score = result.current_set.home_odd;
                    slug = "home";
                    betObject.column = '1';
                    break;
                case 'cur_second':
                    betObject.teamName = result.away.name;
                    betObject.wager = 'Current Set ('+result.period+')';
                    betObject.tennis_wager = result.period;
                    betObject.score = result.current_set.away_odd;
                    slug = "away";
                    betObject.isAway = 1;
                    betObject.column = '1';
                    break;
                case 'next_set_home':
                    betObject.teamName = result.home.name;
                    betObject.wager = 'Next Set ('+result.period+')';
                    betObject.tennis_wager = result.period;
                    betObject.score = result.current_set_two.home_odd;
                    slug = "home";
                    betObject.isHome = 1;
                    betObject.column = '3';
                    break;
                case 'next_set_away':
                    betObject.teamName = result.away.name;
                    betObject.wager = 'Next Set ('+result.period+')';
                    betObject.tennis_wager = result.period;
                    betObject.score = result.current_set_two.away_odd;
                    slug = "away";
                    betObject.isAway = 1;
                    betObject.column = '3';
                    break;
            }
            betObject.displayName = betObject.teamName;
            betObject.bet_extra_info = {
                bet_on_team_name: betObject.teamName,
                other_team_name: (betObject.teamName === result.home.name) ? result.away.name :
                    result.home.name,
                betting_slug: slug,
                betting_wager: betObject.wager
            };
            $scope.slipOddTotal += betObject.score;
            $scope.selectedData.push(betObject);
            $('#' + type + id).addClass('active-bet-juice');
        }
        calculateReport();
        console.log($scope.selectedData);
    };

// Remove Card Item
//i have adddddd
    $scope.removeCardItem = function (id, type) {
        $scope.selectedData.forEach((item) => {
            if (item.id === id && item.type === type) {
                $scope.selectedData.splice($scope.selectedData.indexOf(item), 1);
                $('#' + type + id).removeClass('active-bet-juice');
            }
        });
        calculateReport();
    };

    $scope.betChange = function (id, type, model) {
        $scope.selectedData.forEach((item) => {
            if (item.id === id && item.type === type) {
                if (model === 'tennis_risk_') {
                    if (item.score >= 0) item.win = Math.round(item.score * item.risk) / 100;
                    else item.win = Math.round((10000 / item.score) * item.risk) * (-1) / 100;
                } else {
                    if (item.score >= 0) item.risk = Math.round((10000 / item.score) * item.win) / 100;
                    else item.risk = Math.round(item.win * item.score) * (-1) / 100;
                }
            }
        });
        calculateReport();
    };

    function calculateReport() {
        $scope.possibleWin = 0;
        $scope.totalStake = 0;
        $scope.selectedData.forEach((item) => {
            $scope.possibleWin += parseFloat(item.win);
            $scope.totalStake += parseFloat(item.risk);
        })
    }

    $scope.ConfirmPassBetSlip = function () {
        let pass = $("#bet_confirm_passwordTennis").val();
        if (pass.length === 0) {
            alert("password fields can't be empty")
        }
        else {
            $http.post("/check-user-password",{
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    pass: pass
                }).then( function(response)  {
                       let passRess = response.data;
                     if (passRess.status) {
                        $("#confirm_passwordTennis").addClass('d-none');
                        if (!$scope.isChanged) {
                            $scope.selectedData.forEach((item) => {
                                $scope.bettingResult.forEach((dataset) => {
                                    dataset.results.forEach(apiData => {
                                        if (item.id === apiData.id) {
                                            let carditem = {};
                                            carditem.sport_league = JSON.stringify(['13', item.event_name]);
                                            carditem.is_away = item.isAway;
                                            carditem.is_home = item.isHome;
                                            carditem.o_and_u = '';
                                            carditem.column  = item.column;
                                            carditem.teamname = item.teamName;
                                            carditem.team_id = item.teamName === apiData.home.name ? apiData.home.id:apiData.away.id;
                                            carditem.even_id = item.id;
                                            carditem.sport_id = '13';
                                            carditem.scores = item.score;
                                            carditem.event_date = item.event_date;
                                            let scorre = (item.score>0)?'+'+item.score:item.score;
                                            if (carditem.column !== '2')
                                            {
                                               carditem.original_money     =  "("+item.tennis_wager+") "+scorre ;
                                            }
                                            else
                                            {
                                               carditem.original_money     =  scorre;
                                            }
                                            carditem.risk_stake_slip = parseFloat(item.risk);
                                            carditem.risk_win_slip = parseFloat(item.win);
                                            carditem.bet_type = 'live';
                                            carditem.bet_extra_info = JSON.stringify(item.bet_extra_info);
                                            $scope.confirmData.push(carditem);
                                        }
                                    });

                                });
                            });
                            $http.post("/save-live-data", {
                                    "_token": $('meta[name="csrf-token"]').attr('content'),
                                    items: $scope.confirmData,
                                    status: "oka"
                                }).then( function(resp) {
                                     let res = resp.data;
                                    if (res.status) {
                                        $scope.changeNow = false;
                                        $scope.isChanged = false;
                                        $scope.slipOddTotal = 0;
                                        $('#header_available_balance').html(res.user.available_balance);
                                        $('#header_pending_amount').html(res.user.pending_amount);
                                        $scope.selectedData.forEach((item) => {
                                            $('#' + item.type + item.id).removeClass('active-bet-juice');
                                        });

                                            $scope.selectedData = [];
                                            $scope.confirmData = [];
                                            $scope.confirmSuccess = true;

                                        setTimeout(function () {
                                         $scope.confirmSuccess = false;
                                        }, 3000);
                                    } else {
                                        $scope.confirmFail = true;
                                        setTimeout(function () {
                                            $scope.confirmFail = false;
                                        }, 5000);
                                    }
                            });
                        } else {
                            $("#bet_notice_tennis").removeClass('d-none');
                            $scope.isChanged = false;
                            $scope.changeNow = false;
                        }
                    } else {
                        $("#mesageTennis").text("password didn't match, try again !");
                    }
                });
            }
    }

    $scope.confirmBet = function () {
        if ($scope.selectedData.length !== 0) {
            $scope.changeNow = true;
            $("#confirm_passwordTennis").removeClass('d-none');
        } else {
            alert("No Item is selected");
        }
    }

    $scope.passwordCloseIcon = function () {
        $(".popup-box").addClass("d-none");
    }

    setTimeout(function(){
        $('.__ng_loaded').fadeIn();
    }, 1000);

});
