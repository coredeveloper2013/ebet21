
    liveBet.controller('realTimeBettingController6f', function ($scope, $http, $filter) {
        $scope.bettingResult = [];
        $scope.oldBettingData = [];
        $scope.confirmData = [];
        $scope.loading = true;
        $scope.selectedData = [];
        $scope.timePeriod = 20000;
        $scope.totalStake = 0;
        $scope.possibleWin = 0;
        $scope.confirmSuccess = false;
        $scope.confirmFail = false;
        $scope.isChanged = false;
        $scope.slipOddTotal = 0;
        let getApi;

        $scope.changeNow = false;

        getBetData();

        $scope.$on('activeGame',function(event,opt){
            for(let item in opt)
            {
                if(item === 'volleyball')
                {
                    getBetData();
                }
            }
        });

        function getApiResponse() {
            if($scope.$parent.tab_contents.volleyball)
             {
                getApi = setInterval(function () {
                    getBetData();
                }, $scope.timePeriod);
             }
        }

        $scope.timeFormate = function (timestap) {
            timestap = parseInt(timestap);
            let h = padZero(new Date(timestap).getHours());
            let m = padZero(new Date(timestap).getMinutes());
            return h + ":" + m;
        }

        function padZero(n) {
            if (n < 10) return '0' + n;
            return n;
        }

        function ObjectLength_Modern(object) {
            return Object.keys(object).length;
        }

        function ObjectLength_Legacy(object) {
            var length = 0;
            for (var key in object) {
                if (object.hasOwnProperty(key)) {
                    ++length;
                }
            }
            return length;
        }

        let ObjectLength = Object.keys ? ObjectLength_Modern : ObjectLength_Legacy;

        function getBetData() {

            $http.get('/live-vollyball').then(function (response) {
                if (response.data) {
                    $scope.$parent.loading = false;
                    $scope.oldBettingData = $scope.bettingResult;
                    $scope.bettingResult = response.data;
                    //console.log(response.data);
                    betRefresh();
                    $scope.loading = false;
                    if ($scope.bettingResult.length < 5) {
                        clearInterval(getApi);
                        $scope.timePeriod = 3000;
                        getApiResponse();
                    } else if ($scope.bettingResult.length >= 5 && $scope.bettingResult.length < 10) {
                        clearInterval(getApi);
                        $scope.timePeriod = 5000;
                        getApiResponse();
                    } else if ($scope.bettingResult.length >= 10 && $scope.bettingResult.length < 25) {
                        clearInterval(getApi);
                        $scope.timePeriod = 10000;
                        getApiResponse();
                    } else if ($scope.bettingResult.length >= 25 && $scope.bettingResult.length < 35) {
                        clearInterval(getApi);
                        $scope.timePeriod = 15000;
                        getApiResponse();
                    } else if ($scope.bettingResult.length >= 35) {
                        clearInterval(getApi);
                        $scope.timePeriod = 20000;
                        getApiResponse();
                    }
                }
            });
        }

        // SET selected value to latest value.
        function betRefresh() {
            let newTotal = 0;
            $scope.selectedData.forEach((item) => {
                let replaced = false;
                $scope.bettingResult.forEach((resu) => {
                    resu.results.forEach(result => {
                        if (item.id === result.id) {
                            switch (item.type) {
                                case 'odd_first':
                                    if (result.odd.first) {
                                        item.score = result.odd.first;
                                        replaced = true;
                                    }
                                    break;
                                case 'odd_second':
                                    if (result.odd.second) {
                                        item.score = result.odd.second;
                                        replaced = true;
                                    }
                                    break;
                                case 'next_first':
                                    if (result.next.first) {
                                        item.score = result.next.first;
                                        replaced = true;
                                    }
                                    break;
                                case 'next_second':
                                    if (result.next.second) {
                                        item.score = result.next.second;
                                        replaced = true;
                                    }
                                    break;
                                case 'match_over':
                                    if (result.match.over.odd) {
                                        item.score = result.match.over.odd;
                                        replaced = true;
                                    }
                                    break;
                                case 'match_under':
                                    if (result.match.under.odd) {
                                        item.score = result.match.under.odd;
                                        replaced = true;
                                    }
                                    break;

                            }
                            if ($scope.changeNow) {
                                newTotal += item.score;
                            }

                        }
                    });

                });
                if (!replaced) $scope.selectedData.splice($scope.selectedData.indexOf(item), 1);
                calculateReport();
            });
            if ($scope.changeNow) {
                if (newTotal !== $scope.slipOddTotal) {
                    $scope.isChanged = true;

                }
            }

            // console.log($scope.slipOddTotal,newTotal);

        }

        // Increase, Decrease Icon && Add active class
        $scope.compareODD = function (id, type) {

            let oldFirstVal = 0;
            let curFirstVal = 0;
            let selected = false;

            if ($scope.oldBettingData.length < 1) return;

            if (type === 'odd_first') {

                $scope.selectedData.forEach((item) => {
                    if (item.id == id && item.type == type) {
                        $('#odd_first' + id).addClass('active');
                        selected = true;
                    }
                });

                if (!selected) $('#odd_first' + id).removeClass('active');

                angular.forEach($scope.oldBettingData, function (ite) {
                    ite.results.forEach(item => {
                        if (item.id === id) {
                            oldFirstVal = item.odd.first;
                        }

                    });
                });

                angular.forEach($scope.bettingResult, function (ite) {
                    ite.results.forEach(item => {
                        if (item.id === id) {
                            curFirstVal = item.odd.first;
                        }
                    });
                });

                $("#betSlip_" + id + "_" + type + "_decrease").hide();
                $("#betSlip_" + id + "_" + type + "_increase").hide();
                if (oldFirstVal > curFirstVal) {
                    $('#odd_first' + id).css('color', 'red');
                    $('#odd_first' + id + '_decrease').show();
                    $("#betSlip_" + id + "_" + type + "_decrease").show();
                    betChanges(id, type);
                } else if (oldFirstVal < curFirstVal) {
                    $('#odd_first' + id).css('color', 'green');
                    $('#odd_first' + id + '_increase').show();
                    $("#betSlip_" + id + "_" + type + "_increase").show();
                    betChanges(id, type);
                }
            } else if (type === 'odd_second') {
                $scope.selectedData.forEach((item) => {
                    if (item.id === id && item.type === type) {
                        $('#odd_second' + id).addClass('active');
                        selected = true;
                    }
                });

                if (!selected) $('#odd_second' + id).removeClass('active');

                angular.forEach($scope.oldBettingData, function (ite) {
                    ite.results.forEach(item => {
                        if (item.id === id) {
                            oldFirstVal = item.odd.second;
                        }
                    });
                });

                angular.forEach($scope.bettingResult, function (ite) {
                    ite.results.forEach(item => {
                        if (item.id === id) {
                            curFirstVal = item.odd.second;
                        }
                    });
                });
                $("#betSlip_" + id + "_" + type + "_decrease").hide();
                $("#betSlip_" + id + "_" + type + "_increase").hide();
                if (oldFirstVal > curFirstVal) {
                    $('#odd_second' + id).css('color', 'red');
                    $('#odd_second' + id + '_decrease').show();
                    $("#betSlip_" + id + "_" + type + "_decrease").show();
                    betChanges(id, type);
                } else if (oldFirstVal < curFirstVal) {
                    $('#odd_second' + id).css('color', 'green');
                    $('#odd_second' + id + '_increase').show();
                    $("#betSlip_" + id + "_" + type + "_increase").show();
                    betChanges(id, type);
                }
            } else if (type === 'next_first') {
                $scope.selectedData.forEach((item) => {
                    if (item.id == id && item.type == type) {
                        $('#next_first' + id).addClass('active');
                        selected = true;
                    }
                });

                if (!selected) $('#next_first' + id).removeClass('active');

                angular.forEach($scope.oldBettingData, function (ite) {
                    ite.results.forEach(item => {
                        if (item.id === id) {
                            oldFirstVal = item.next.first;
                        }
                    });
                });

                angular.forEach($scope.bettingResult, function (ite) {
                    ite.results.forEach(item => {
                        if (item.id === id) {
                            curFirstVal = item.next.first;
                        }
                    });
                });
                $("#betSlip_" + id + "_" + type + "_decrease").hide();
                $("#betSlip_" + id + "_" + type + "_increase").hide();
                if (oldFirstVal > curFirstVal) {
                    $('#next_first' + id).css('color', 'red');
                    $('#next_first' + id + '_decrease').show();
                    $("#betSlip_" + id + "_" + type + "_decrease").show();
                    betChanges(id, type);
                } else if (oldFirstVal < curFirstVal) {
                    $('#next_first' + id).css('color', 'green');
                    $('#next_first' + id + '_increase').show();
                    $("#betSlip_" + id + "_" + type + "_increase").show();
                    betChanges(id, type);
                }
            } else if (type === 'next_second') {
                $scope.selectedData.forEach((item) => {
                    if (item.id == id && item.type == type) {
                        $('#next_second' + id).addClass('active');
                        selected = true;
                    }
                });

                if (!selected) $('#next_second' + id).removeClass('active');

                angular.forEach($scope.oldBettingData, function (ite) {
                    ite.results.forEach(item => {
                        if (item.id === id) {
                            oldFirstVal = item.next.first;
                        }
                    });
                });

                angular.forEach($scope.bettingResult, function (ite) {
                    ite.results.forEach(item => {
                        if (item.id === id) {
                            curFirstVal = item.next.first;
                        }
                    });
                });
                $("#betSlip_" + id + "_" + type + "_decrease").hide();
                $("#betSlip_" + id + "_" + type + "_increase").hide();
                if (oldFirstVal > curFirstVal) {
                    $('#next_second' + id).css('color', 'red');
                    $('#next_second' + id + '_decrease').show();
                    $("#betSlip_" + id + "_" + type + "_decrease").show();
                    betChanges(id, type);
                } else if (oldFirstVal < curFirstVal) {
                    $('#next_second' + id).css('color', 'green');
                    $('#next_second' + id + '_increase').show();
                    $("#betSlip_" + id + "_" + type + "_increase").show();
                    betChanges(id, type);
                }
            } else if (type === 'match_over') {
                $scope.selectedData.forEach((item) => {
                    if (item.id == id && item.type == type) {
                        $('#match_over' + id).addClass('active');
                        selected = true;
                    }
                });

                if (!selected) $('#match_over' + id).removeClass('active');

                angular.forEach($scope.oldBettingData, function (ite) {
                    ite.results.forEach(item => {
                        if (item.id === id) {
                            oldFirstVal = item.match.over.odd;
                        }
                    });
                });

                angular.forEach($scope.bettingResult, function (ite) {
                    ite.results.forEach(item => {
                        if (item.id === id) {
                            curFirstVal = item.match.over.odd;
                        }
                    });
                });
                $("#betSlip_" + id + "_" + type + "_decrease").hide();
                $("#betSlip_" + id + "_" + type + "_increase").hide();
                if (oldFirstVal > curFirstVal) {
                    $('#match_over' + id).css('color', 'red');
                    $('#match_over' + id + '_decrease').show();
                    $("#betSlip_" + id + "_" + type + "_decrease").show();
                    betChanges(id, type);
                } else if (oldFirstVal < curFirstVal) {
                    $('#match_over' + id).css('color', 'green');
                    $('#match_over' + id + '_increase').show();
                    $("#betSlip_" + id + "_" + type + "_increase").show();
                    betChanges(id, type);
                }
            } else if (type === 'match_under') {
                $scope.selectedData.forEach((item) => {
                    if (item.id == id && item.type == type) {
                        $('#match_under' + id).addClass('active');
                        selected = true;
                    }
                });

                if (!selected) $('#match_under' + id).removeClass('active');

                angular.forEach($scope.oldBettingData, function (ite) {
                    ite.results.forEach(item => {
                        if (item.id === id) {
                            oldFirstVal = item.match.under.odd;
                        }
                    });
                });

                angular.forEach($scope.bettingResult, function (ite) {
                    ite.results.forEach(item => {
                        if (item.id === id) {
                            curFirstVal = item.match.under.odd;
                        }
                    });
                });
                $("#betSlip_" + id + "_" + type + "_decrease").hide();
                $("#betSlip_" + id + "_" + type + "_increase").hide();
                if (oldFirstVal > curFirstVal) {
                    $('#match_under' + id).css('color', 'red');
                    $('#match_under' + id + '_decrease').show();
                    $("#betSlip_" + id + "_" + type + "_decrease").show();
                    betChanges(id, type);
                } else if (oldFirstVal < curFirstVal) {
                    $('#match_under' + id).css('color', 'green');
                    $('#match_under' + id + '_increase').show();
                    $("#betSlip_" + id + "_" + type + "_increase").show();
                    betChanges(id, type);
                }
            }

        };

        function betChanges(id, type) {

            $scope.selectedData.forEach((item) => {
                if (item.id === id && item.type === type) {

                    if (item.score >= 0) item.win = Math.round(item.score * item.risk) / 100;
                    else item.win = Math.round((10000 / item.score) * item.risk) * (-1) / 100;

                    if (item.score >= 0) item.risk = Math.round((10000 / item.score) * item.win) / 100;
                    else item.risk = Math.round(item.win * item.score) * (-1) / 100;

                }
            });
            calculateReport();
        };

        // Add To Cart ( BET Slip function )
        $scope.addtocart = function (id, type) {
            let selected = false;
            let result = [];
            let league = null;

            $scope.selectedData.forEach((item) => {
                if (item.id === id && item.type === type) {
                    $scope.selectedData.splice($scope.selectedData.indexOf(item), 1);
                    $('#' + type + id).removeClass('active');
                    selected = true;
                }
            });

            if (!selected) {

                $scope.bettingResult.forEach((dat) => {
                    let tst = dat;
                    dat.results.forEach(data => {
                        if (data.id === id) {
                            result = data;
                            league = tst;
                        }
                    });

                });

                let date = new Date(result.time * 1000);


                let betObject = {
                    id: id,
                    type: type,
                    win: '',
                    risk: '',
                    score: 0,
                    teamName: '',
                    displayName: '',
                    wager: '',
                    isHome: null,
                    isAway: null,
                    spread_handicap: null,
                    teamHomeName: result.home.name,
                    teamAwayName: result.away.name,
                    dateTime: result.datetime,
                    event_date: result.time,
                    event_name: result.league.name,
                };
                let slug;
                switch (type) {
                    case 'odd_first':
                        betObject.isHome = 1;
                        betObject.teamName = result.home.name;
                        betObject.displayName = result.home.name;
                        betObject.wager = 'Run Line';
                        betObject.score = result.odd.first;
                        betObject.spread_handicap = result.odd.first_val;
                        slug = "home";
                        break;
                    case 'odd_second':
                        betObject.isAway = 1;
                        betObject.teamName = result.away.name;
                        betObject.displayName = result.away.name;
                        betObject.wager = 'Run Line';
                        betObject.score = result.odd.second;
                        betObject.spread_handicap = result.odd.second_val;
                        slug = "away";
                        break;
                    case 'match_over':
                        betObject.isHome = 1;
                        betObject.teamName = result.home.name;
                        betObject.displayName = result.home.name;
                        betObject.wager = 'Money Line';
                        betObject.score = result.match.over.odd;
                        slug = "home";
                        break;
                    case 'match_under':
                        betObject.isAway = 1;
                        betObject.teamName = result.away.name;
                        betObject.displayName = result.away.name;
                        betObject.wager = 'Money Line';
                        betObject.score = result.match.under.odd;
                        slug = "away";
                        break;
                    case 'next_first':
                        betObject.isHome = 1;
                        betObject.teamName = result.home.name;
                        betObject.displayName = 'Over ' + result.next.first_val;
                        betObject.wager = 'Total';
                        betObject.score = result.next.first;
                        slug = "home";
                        break;
                    case 'next_second':
                        betObject.isAway = 1;
                        betObject.teamName = result.away.name;
                        betObject.displayName = 'Under ' + result.next.second_val;
                        betObject.wager = 'Total';
                        betObject.score = result.next.second;
                        slug = "away";
                        break;

                }
                betObject.bet_extra_info = {
                    bet_on_team_name: betObject.teamName,
                    other_team_name: (betObject.teamName === result.home.name) ? result.away.name : result.home.name,
                    betting_slug: slug,
                    betting_wager: betObject.wager
                };

                $scope.slipOddTotal += betObject.score;
                $scope.selectedData.push(betObject);
                $('#' + type + id).addClass('active');
            }
            calculateReport();
        };

        // Remove Card Item
        $scope.removeCardItem = function (id, type) {
            $scope.selectedData.forEach((item) => {
                if (item.id == id && item.type == type) {
                    $scope.selectedData.splice($scope.selectedData.indexOf(item), 1);
                    $('#' + type + id).removeClass('active')
                }
            });
            calculateReport();
        };
        $scope.clearBetSlip = function () {

            if ($scope.selectedData.length === 0) {
                alert("You haven't selected a slip yet");
            } else {
                $scope.selectedData.forEach(function (item) {
                    $('#' + item.type + item.id).removeClass('active');
                });
                $scope.selectedData = [];
                calculateReport();
            }

        };
        $scope.betChange = function (id, type, model) {
            $scope.selectedData.forEach((item) => {
                if (item.id == id && item.type == type) {
                    if (model === 'volley_risk_') {
                        if (item.score >= 0) item.win = Math.round(item.score * item.risk) / 100;
                        else item.win = Math.round((10000 / item.score) * item.risk) * (-1) / 100;
                    } else {
                        if (item.score >= 0) item.risk = Math.round((10000 / item.score) * item.win) / 100;
                        else item.risk = Math.round(item.win * item.score) * (-1) / 100;
                    }
                }
            });
            calculateReport();
        };

        function calculateReport() {
            $scope.possibleWin = 0;
            $scope.totalStake = 0;
            $scope.selectedData.forEach((item) => {
                $scope.possibleWin += parseFloat(item.win);
                $scope.totalStake += parseFloat(item.risk);
            })
        }

        $scope.confirmBet = function () {
            if ($scope.selectedData.length !== 0) {
                $scope.changeNow = true;
                $("#confirm_passwordVolleyball").removeClass('d-none');
                $("#submit_confirm_passwordVolleyball").off().click(function (e) {
                    let pass = $("#bet_confirm_passwordVolleyball").val();
                    if (pass.length === 0) {
                        alert("password fields can't be empty")
                    } else {
                        $.ajax({
                            url: "/check-user-password",
                            type: "POST",
                            data: {
                                "_token": $('meta[name="csrf-token"]').attr('content'),
                                pass: pass
                            },
                            success: function (response) {
                                if (response.status) {
                                    $("#confirm_passwordVolleyball").addClass('d-none');

                                    if (!$scope.isChanged) {
                                        $scope.selectedData.forEach((item) => {
                                            $scope.bettingResult.forEach((ad) => {
                                                ad.results.forEach(apiData => {
                                                    if (item.id === apiData.id) {
                                                        let carditem = {};
                                                        carditem.sport_league = JSON.stringify(['91', item.event_name]);
                                                        carditem.is_away = item.isAway;
                                                        carditem.is_home = item.isHome;
                                                        carditem.o_and_u = '';
                                                        carditem.teamname = item.teamName;
                                                        carditem.team_id = item.teamName === apiData.home.name ? apiData.home.id : apiData.away.id;
                                                        carditem.even_id = item.id;
                                                        carditem.scores = item.score;
                                                        carditem.sport_id = '91';
                                                        carditem.event_date = item.event_date;
                                                        carditem.original_money = item.spread_handicap !== null ? "(" + item.spread_handicap + ") " + item.score : item.score;
                                                        carditem.risk_stake_slip = parseFloat(item.risk);
                                                        carditem.risk_win_slip = parseFloat(item.win);
                                                        carditem.bet_type = 'live';
                                                        carditem.event_name = item.event_name;
                                                        carditem.bet_extra_info = JSON.stringify(item.bet_extra_info);
                                                        $scope.confirmData.push(carditem);
                                                    }
                                                });
                                            });
                                        });
                                        $.ajax({
                                            url: "/save-live-data",
                                            type: "POST",
                                            data: {
                                                "_token": $('meta[name="csrf-token"]').attr('content'),
                                                items: $scope.confirmData,
                                                status: "oka"
                                            },
                                            success: function (resp) {
                                                //  console.log(res);
                                                // let resp = JSON.parse(res);
                                                if (resp.status) {

                                                    $scope.isChanged = false;
                                                    $scope.slipOddTotal = 0;
                                                    $scope.changeNow = false;

                                                    $scope.selectedData = [];
                                                    $scope.confirmData = [];

                                                    $('#header_available_balance').html(resp.user.available_balance);
                                                    $('#header_pending_amount').html(resp.user.pending_amount);
                                                    $scope.selectedData.forEach((item) => {
                                                        // $scope.selectedData.splice($scope.selectedData.indexOf(item), 1);
                                                        $('#' + item.type + item.id).removeClass('active');
                                                    });
                                                    $scope.selectedData = [];
                                                    $scope.totalStake = 0;
                                                    $scope.possibleWin = 0;

                                                    $scope.confirmSuccess = true;
                                                    setTimeout(function () {
                                                        $scope.confirmSuccess = false;
                                                    }, 3000);
                                                } else {
                                                    $scope.confirmFail = true;
                                                    setTimeout(function () {
                                                        $scope.confirmFail = false;
                                                    }, 5000);
                                                }
                                            }
                                        });
                                    } else {
                                        $("#bet_noticeVolleyball").removeClass('d-none');
                                        $scope.isChanged = false;
                                        $scope.changeNow = false;
                                    }

                                } else {
                                    $("#mesageVolleyball").text("password didn't match, try again !");
                                }
                            }
                        })
                    }
                });

            } else {
                alert("No Item is selected");
            }
        }
        $scope.passwordCloseIcon = function () {
            $(".popup-box").addClass("d-none");
        }
    });
