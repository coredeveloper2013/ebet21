<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>Site Maintenance</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <style>
        body { text-align: center; padding: 150px; }
        h1 { font-size: 50px; }
        body { font: 20px Helvetica, sans-serif; color: #333; }
        article { display: block; text-align: left; width: 650px; margin: 0 auto; }
        a { color: #dc8100; text-decoration: none; }
        a:hover { color: #333; text-decoration: none; }
    </style>
</head>
<body>
<article>
    <h1>We&rsquo;ll be back soon!</h1>
    <div>
        <p style="text-align: justify">Sorry for the inconvenience but we&rsquo;re performing some maintenance at the moment. If you need to you can always <a href="mailto:#">contact us</a>, otherwise we&rsquo;ll be back online shortly!</p>
        <p style="text-align: justify">&mdash; The Team</p>
    </div>
</article>


<script type="text/javascript" src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
<script>
    function checkMaintain() {
        $.ajax({
            url: "{{ route('maintenance.check') }}",
            type: 'get',
            dataType: 'json',
            success: function (res) {
                console.log(res);
                if (res.success == false) {
                    window.location.href = "{{ url('login') }}"
                }
            }
        });
    }

    $(document).ready(function () {
        setTimeout(function () {
            checkMaintain();
        }, 100);

        setInterval(function () {
            checkMaintain();
        }, 60000);
    });
</script>

</body>
</html>

