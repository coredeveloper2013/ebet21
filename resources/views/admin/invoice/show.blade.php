@extends('layouts.admin_layout')


@section('title')
    Admin / Invoices Details
@endsection

@section('body-title')
    Invoices Details : <span>{{ $agentInfo->username }}</span>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active">Invoices Details</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example1" data-page-length='50' class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>S No.</th>
                                <th>Month/Invoice</th>
                                <th>Week1</th>
                                <th>Week2</th>
                                <th>Week3</th>
                                <th>Week4</th>
                                <th>Week5</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($realArray))
                                @foreach($realArray[2019] as $key => $user)
                                <tr>
                                    @php
                                    $week1=$week2=$week3=$week4=$week5=0;
                                    @endphp
                                    @foreach ($user as $weeks)
                                        @php
                                          $agent_rate = ($weeks['rate'] == 0)?1:$weeks['rate'];
                                          $user_count = $weeks['user_counts'];
                                             if ($weeks['week_number'] == 1) {
                                                $week1 =  ($agent_rate*$user_count);
                                             }
                                             else if($weeks['week_number'] == 2)
                                             {
                                                $week2 =  ($agent_rate*$user_count);
                                             }
                                             else if($weeks['week_number'] == 3)
                                             {
                                                $week3 =  ($agent_rate*$user_count);
                                             }
                                             else if($weeks['week_number'] == 4)
                                             {
                                                $week4 =  ($agent_rate*$user_count);
                                             }
                                             else if($weeks['week_number'] == 5)
                                             {
                                                $week5 =  ($agent_rate*$user_count);
                                             }
                                        @endphp
                                    @endforeach
                                    <td>{{ ++$key }}</td>
                                    <td> <strong>{{ isset($user[0])?$user[0]['month_name']:"Nope" }}</strong> #Invoice</td>
                                    <td>{{ $week1 }}</td>
                                    <td>{{ $week2 }}</td>
                                    <td>{{ $week3 }}</td>
                                    <td>{{ $week4 }}</td>
                                    <td>{{ $week5 }}</td>
                                    <td>{{ ($week1 + $week2 + $week3 + $week4) }}</td>
                                </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('page_js')

@endsection



