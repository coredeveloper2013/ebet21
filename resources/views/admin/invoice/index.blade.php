@extends('layouts.admin_layout')


@section('title')
    Admin / Invoices
@endsection

@section('body-title')
    Invoices
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active">Invoices</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example1" data-page-length='50' class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>S No.</th>
                                <th>Username</th>
                                <th>Name</th>
                                @if(Auth::user()->role == 'admin')
                                <th>History</th>
                                @endif
                                <th class="text-center">Week 1<br>{{ $week4->start.' - '.$week4->end }}</th>
                                <th class="text-center">Week 2<br>{{ $week3->start.' - '.$week3->end }}</th>
                                <th class="text-center">Week 3<br>{{ $week2->start.' - '.$week2->end }}</th>
                                <th class="text-center">Week 4<br>{{ $week1->start.' - '.$week1->end }}</th>
                                <th>Total</th>
                                <th>Status</th>
                                @if(Auth::user()->role == 'admin')
                                <th>&nbsp;</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($data))
                                @foreach($data as $key => $user)
                                @php($total = ($user->total1+$user->total2+$user->total3+$user->total4))
                                @php($paid = false)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->name }}</td>
                                    @if(Auth::user()->role == 'admin')
                                    <td><a href="{{ route('invoice.show', $user->id) }}">History</a></td>
                                    @endif
                                    <td class="text-right">
                                        <a href="{{ route('invoice.show', $user->id) }}?week=4">
                                            {{ $user->total4 }}
                                            @if($user->total4>0)
                                                <br><small>user: {{ $user->count4 }} / rate: {{ ($user->total4/$user->count4) }}</small>
                                                {{-- @if($user->is_paid4>0)
                                                    <span class="text-success">&nbsp;(paid)</span>
                                                    @php($paid = true)
                                                @endif --}}
                                            @endif
                                        </a>
                                    </td>

                                    <td class="text-right">
                                        <a href="{{ route('invoice.show', $user->id) }}?week=3">
                                            {{ $user->total3 }}
                                            @if($user->total3>0)
                                                <br><small>user: {{ $user->count3 }} / rate: {{ ($user->total3/$user->count3) }}</small>
                                                {{-- @if($user->is_paid3>0)
                                                    <span class="text-success">&nbsp;(paid)</span>
                                                    @php($paid = true)
                                                @endif --}}
                                            @endif
                                        </a>
                                    </td>

                                    <td class="text-right">
                                        <a href="{{ route('invoice.show', $user->id) }}?week=2">
                                            {{ $user->total2 }}
                                            @if($user->total2>0)
                                                <br><small>user: {{ $user->count2 }} / rate: {{ ($user->total2/$user->count2) }}</small>
                                                {{-- @if($user->is_paid2>0)
                                                    <span class="text-success">&nbsp;(paid)</span>
                                                    @php($paid = true)
                                                @endif --}}
                                            @endif
                                        </a>
                                    </td>

                                    <td class="text-right">
                                        <a href="{{ route('invoice.show', $user->id) }}?week=1">
                                            {{ $user->total1 }}
                                            @if($user->total1>0)
                                                <br><small>user: {{ $user->count1 }} / rate: {{ ($user->total1/$user->count1) }}</small>
                                                {{-- @if($user->is_paid1>0)
                                                    <span class="text-success">&nbsp;(paid)</span>
                                                    @php($paid = true)
                                                @endif --}}
                                            @endif
                                        </a>
                                    </td>

                                    <td>
                                        <a href="{{ route('invoice.show', $user->id) }}?allweek=1">{{ number_format($total, 2) }}</a>
                                    </td>

                                    <td>{!! ($total>0 && $paid)?'<span class="text-success">Paid</span>':'<span class="text-danger">Unpaid</span>' !!}</td>

                                    @if(Auth::user()->role == 'admin')
                                    <td>
                                        <form  method="post" action="{{ route('invoice.store') }}">
                                            @csrf
                                            {{-- {{ ($total>0 && $paid)?'disabled':'' }} {{ ($total>0 && $paid)?'checked':'' }} --}}
                                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                                            <input type="hidden" name="amount" value="{{ $total }}">
                                            <button class="btn btn-info" {{($total>0 && $paid)?'':'disabled' }}>Paid</button>
                                        </form>
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                            @endif

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('page_js')

@endsection



