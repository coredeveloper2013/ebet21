@extends('layouts.admin_layout')


@section('title')
    Admin / Straight league approval
@endsection

@section('page_css')
    <style>
        .grid-item {
            width: calc(50% - 25px);
            padding: 5px;
            background: #fff;
            box-shadow: 0 0 3px #d0d0d0;
            margin-bottom: 10px;
            float: left;
        }
        .table{
            margin: 0!important;
        }

        .data-text{
            margin-top: 6%;
            color: rgba(0,0,0,0.32);
            text-shadow: 0 0 2px rgba(0,0,0,0.5);
        }

        @media only screen and (max-width: 600px) {
            .grid-item {
                width: 100%;
            }
        }
    </style>
@endsection

@section('body-title')
    Straight league approval
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active">Straight league approval</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            @if(!empty($data))
                <form role="form" class="pb-3" method="post" action="{{ route('wager-disable.store') }}">
                    @csrf
                    <div class="col-md-2 mb-3">
                        <button type="submit" class="btn btn-block btn-outline-success">Submit</button>
                    </div>
                    <div class="grid">
                        @foreach($data['first_column']+$data['second_column']+$data['third_column'] as $sport_key => $sport)

                            <div class="grid-item">
                                <div class="table-responsive">
                                    <table class="table table-hover" id="straightTbl">
                                        <thead class="bg-teal">
                                        <tr>
                                            <th>
                                                <div class="custom-control custom-checkbox">
                                                    <input
                                                    class="custom-control-input"
                                                    onclick="straightGet(this)"
                                                    type="checkbox" name="sport_id[]"
                                                    id="{{ $sport['game_name'] }}" {{ !empty(getWagerDisableCheck($sport_key)) ? 'checked':'' }}
                                                    value="{{ $sport_key }}">
                                                    <input
                                                    type="hidden"
                                                    name="sport_name[{{ $sport_key }}]"
                                                    value="{{ $sport['game_name'] }}">
                                                    <label for="{{ $sport['game_name'] }}" class="custom-control-label">{{ $sport['game_name'] }}</label>
                                                </div>
                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @if(!empty($sport))
                                            @php
                                                $sports_league = [];
                                                if (isset($sport['game_data'])){
                                                    /* if($sport_key == 6 || $sport_key == 10 || $sport_key == 133)
                                                       dd($sport['game_data']); */
                                                    foreach ($sport['game_data'] as $k => $v) {
                                                            $sports_league[$k] = $v;
                                                    }
                                                }
                                                $u_sport_league = array_map("unserialize", array_unique(array_map("serialize", $sports_league)));
                                                asort($u_sport_league);
                                            @endphp

                                            @if(!empty($u_sport_league))
                                                @foreach($u_sport_league as $key => $val)
                                                    <tr id="addremove{{$val}}">
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input
                                                                class="custom-control-input"
                                                                onclick="straightGet(this)"
                                                                name="league_id[]" {{ !empty(getWagerDisableCheck($sport_key.'_'.$key)) ? 'checked':'' }}
                                                                type="checkbox" id="{{ $sport_key.'_'.$key }}" value="{{ $sport_key.'_'.$key }}">
                                                                <input
                                                                    type="hidden"
                                                                    name="league_name[{{$sport_key.'_'.$key}}]"
                                                                    value="{{$val}}">
                                                                <label for="{{$sport_key.'_'.$key }}" class="custom-control-label">{{ $val  }}</label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td class="text-center">
                                                        <strong class="text-danger">League not available</strong>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="col-md-2 mt-3">
                        <button type="submit" class="btn btn-block btn-outline-success">Submit</button>
                    </div>

                </form>
            @else
                <section class="content">
                    <div class="error-page" style="width: 100%!important;">
                        <div class="row">
                            <div class="col-md-5 text-right">
                                <h2 style="font-size: 80px;" class="text-danger">API</h2>
                            </div>
                            <div class="col-md-6" style="margin-top: 20px;">
                                <div class="error-content">
                                    <h3><i class="fas fa-exclamation-triangle text-danger"></i> Oops! Something went wrong.</h3>
                                    <p>
                                        Response unavailable
                                        you may <a href="{{ url('admin/dashboard') }}">return to dashboard</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="text-center data-text">
                        <h1 style="    font-size: 55px;">Data unavailable</h1>
                    </div>
                    <!-- /.error-page -->

                </section>
            @endif
        </div><!-- /.container-fluid -->
    </section>
@endsection

@section('page_js')
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
    <script>
        $('.grid').masonry({
            itemSelector: '.grid-item',
            columnWidth: 30
        });
    </script>


    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function straightGet(elem){
            var sport_league = $(elem).val();
            $.ajax({
                type: "GET",
                url: "{!! url('admin/wager-disable-delete') !!}/" + sport_league,
                data: {sport_league:sport_league},
                success: function(data){
                    // console.log(data);
                }
            });
        }

    </script>
@endsection



