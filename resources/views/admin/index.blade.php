@extends('layouts.admin_layout')


@section('title')
    Ebet21::Dasboard panel
@endsection

@section('body-title')
    Dashboard
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active">Admin Dashboard</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row justify-content-center">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                        <h3>{{ $ad->currentWeekActiveUsers() }}</h3>
                            <p>Active Users</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-users"></i>
                        </div>
                    <a href="{{ url('admin/active-user') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3>{{ $ad->todayRepresent() }}</h3>
                            <p>Today</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-calendar-alt"></i>
                        </div>
                    <a href="{{ url('admin/today-represent-lists') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3>{{ $ad->weeklyRepresent() }}</h3>
                            <p>Weekly Figure</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                    <a href="{{ url('admin/weekly-represent-lists') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->

                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                        <h3 class="text-white">{{ $ad->deletedWager() }}</h3>

                            <p class="text-white">Deleted Wagers</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-trash"></i>
                        </div>
                    <a href="{{ url('admin/deleted-wager-lists') }}" class="small-box-footer "><span class="text-white">More info</span> <i class="fas fa-arrow-circle-right text-white"></i></a>
                    </div>
                </div>

                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-blue">
                        <div class="inner">
                        <h3>{{ $ad->weeklyWager() }}</h3>
                            <p>Agents Weekly Balance</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-chart-area"></i>
                        </div>
                    <a href="{{url('admin/weekly-agents-wagers')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-teal">
                        <div class="inner">
                            <h3>{{ $ad->allWager() }}</h3>
                            <p>Agents Total Outstanding Balance</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-list"></i>
                        </div>
                    <a href="{{ url('admin/total-agents-wagers') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <!-- Left col -->
                <section class="col-lg-7 connectedSortable">
                    <!-- Custom tabs (Charts with tabs)-->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="fas fa-chart-pie mr-1"></i>
                                Active User Count
                            </h3>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content p-0">
                                <!-- Morris chart - Sales -->
                                <div class="chart tab-pane active" id="revenue-chart"
                                     style="position: relative; height: 300px;">
                                    <canvas id="revenue-chart-canvas" height="300" style="height: 300px;"></canvas>
                                </div>
                                <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px; display:none;">
                                    <canvas id="sales-chart-canvas" height="300" style="height: 300px;"></canvas>
                                </div>
                            </div>
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Notification Form</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form">
                            <div class="card-body">
                                <div class="text-justify">
                                    <p>
                                        <strong>Last Message :</strong>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, ipsam itaque laudantium nemo officia perspiciatis quisquam quos reiciendis rem, soluta velit, vero. Accusantium ad et nam praesentium repudiandae sed unde.
                                    </p>
                                </div>
                                <div class="form-group">
                                    <label>Send notification to all users</label>
                                    <textarea class="form-control" id="exampleInputEmail1" placeholder="Enter your message"></textarea>
                                </div>

                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>





                </section>


                <!-- /.Left col -->
                <!-- right col (We are only adding the ID to make the widgets sortable)-->
                <section class="col-lg-5 connectedSortable">

                    <!-- Map card -->
                    <div class="card bg-gradient-primary" style="display:none;">
                        <div class="card-header border-0">
                            <h3 class="card-title">
                                <i class="fas fa-map-marker-alt mr-1"></i>
                                Visitors
                            </h3>
                            <!-- card tools -->
                            <div class="card-tools">
                                <button type="button"
                                        class="btn btn-primary btn-sm daterange"
                                        data-toggle="tooltip"
                                        title="Date range">
                                    <i class="far fa-calendar-alt"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-primary btn-sm"
                                        data-card-widget="collapse"
                                        data-toggle="tooltip"
                                        title="Collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <div class="card-body">
                            <div id="world-map" style="height: 250px; width: 100%;"></div>
                        </div>
                        <!-- /.card-body-->
                        <div class="card-footer bg-transparent">
                            <div class="row">
                                <div class="col-4 text-center">
                                    <div id="sparkline-1"></div>
                                    <div class="text-white">Visitors</div>
                                </div>
                                <!-- ./col -->
                                <div class="col-4 text-center">
                                    <div id="sparkline-2"></div>
                                    <div class="text-white">Online</div>
                                </div>
                                <!-- ./col -->
                                <div class="col-4 text-center">
                                    <div id="sparkline-3"></div>
                                    <div class="text-white">Sales</div>
                                </div>
                                <!-- ./col -->
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                    <!-- /.card -->

                    <!-- solid sales graph -->
                    <div class="card bg-gradient-info">
                        <div class="card-header border-0">
                            <h3 class="card-title">
                                <i class="fas fa-th mr-1"></i>
                                Sales Graph
                            </h3>

                            <div class="card-tools">
                                <button type="button" class="btn bg-info btn-sm" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn bg-info btn-sm" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <canvas class="chart" id="line-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer bg-transparent" style="display:none;">
                            <div class="row">
                                <div class="col-4 text-center">
                                    <input type="text" class="knob" data-readonly="true" value="20" data-width="60" data-height="60"
                                           data-fgColor="#39CCCC">

                                    <div class="text-white">Mail-Orders</div>
                                </div>
                                <!-- ./col -->
                                <div class="col-4 text-center">
                                    <input type="text" class="knob" data-readonly="true" value="50" data-width="60" data-height="60"
                                           data-fgColor="#39CCCC">

                                    <div class="text-white">Online</div>
                                </div>
                                <!-- ./col -->
                                <div class="col-4 text-center">
                                    <input type="text" class="knob" data-readonly="true" value="30" data-width="60" data-height="60"
                                           data-fgColor="#39CCCC">

                                    <div class="text-white">In-Store</div>
                                </div>
                                <!-- ./col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card-footer -->
                    </div>
                    <!-- /.card -->

                    <!-- Calendar -->
                    <div class="card bg-gradient-success" style="display:none;">
                        <div class="card-header border-0">

                            <h3 class="card-title">
                                <i class="far fa-calendar-alt"></i>
                                Calendar
                            </h3>
                            <!-- tools card -->
                            <div class="card-tools">
                                <!-- button with a dropdown -->
                                <div class="btn-group">
                                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                                        <i class="fas fa-bars"></i></button>
                                    <div class="dropdown-menu float-right" role="menu">
                                        <a href="#" class="dropdown-item">Add new event</a>
                                        <a href="#" class="dropdown-item">Clear events</a>
                                        <div class="dropdown-divider"></div>
                                        <a href="#" class="dropdown-item">View calendar</a>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-success btn-sm" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                            <!-- /. tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body pt-0">
                            <!--The calendar -->
                            <div id="calendar" style="width: 100%"></div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">User Login Details</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr class="HeaderColumnsName">
                                        <th>Date Time</th>
                                        <th>IP Address</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>10/09/2019 17:25:31</td>
                                        <td>103.60.175.94</td>
                                    </tr>
                                    <tr>
                                        <td>10/09/2019 13:56:47</td>
                                        <td>66.25.127.83</td>
                                    </tr>
                                    <tr>
                                        <td>10/09/2019 13:41:05</td>
                                        <td>66.25.127.83</td>
                                    </tr>
                                    <tr>
                                        <td>10/09/2019 12:49:51</td>
                                        <td>103.60.175.94</td>
                                    </tr>
                                    <tr>
                                        <td>10/08/2019 02:19:32</td>
                                        <td>66.25.127.83</td>
                                    </tr>
                                    <tr>
                                        <td>10/08/2019 02:17:46</td>
                                        <td>108.69.105.2</td>
                                    </tr>
                                    <tr>
                                        <td>10/08/2019 01:43:38</td>
                                        <td>66.25.127.83</td>
                                    </tr>
                                    <tr>
                                        <td>10/08/2019 01:32:21</td>
                                        <td>108.69.105.2</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            <ul class="pagination pagination-sm m-0 float-right">
                                <li class="page-item"><a class="page-link" href="#">«</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">»</a></li>
                            </ul>
                        </div>
                    </div>
                </section>
                <!-- right col -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('page_js')
    <script>
        $(function () {
            'use strict'

            // Make the dashboard widgets sortable Using jquery UI
            $('.connectedSortable').sortable({
                placeholder         : 'sort-highlight',
                connectWith         : '.connectedSortable',
                handle              : '.card-header, .nav-tabs',
                forcePlaceholderSize: true,
                zIndex              : 999999
            })
            $('.connectedSortable .card-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move')

            // jQuery UI sortable for the todo list
            $('.todo-list').sortable({
            placeholder         : 'sort-highlight',
            handle              : '.handle',
            forcePlaceholderSize: true,
            zIndex              : 999999
            })

            // bootstrap WYSIHTML5 - text editor
            $('.textarea').summernote()

            $('.daterange').daterangepicker({
            ranges   : {
                'Today'       : [moment(), moment()],
                'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment().subtract(29, 'days'),
            endDate  : moment()
            }, function (start, end) {
            window.alert('You chose: ' + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
            })

            /* jQueryKnob */
            $('.knob').knob()

            // jvectormap data
            var visitorsData = {
            'US': 398, //USA
            'SA': 400, //Saudi Arabia
            'CA': 1000, //Canada
            'DE': 500, //Germany
            'FR': 760, //France
            'CN': 300, //China
            'AU': 700, //Australia
            'BR': 600, //Brazil
            'IN': 800, //India
            'GB': 320, //Great Britain
            'RU': 3000 //Russia
            }
            // World map by jvectormap
            $('#world-map').vectorMap({
            map              : 'usa_en',
            backgroundColor  : 'transparent',
            regionStyle      : {
                initial: {
                fill            : 'rgba(255, 255, 255, 0.7)',
                'fill-opacity'  : 1,
                stroke          : 'rgba(0,0,0,.2)',
                'stroke-width'  : 1,
                'stroke-opacity': 1
                }
            },
            series           : {
                regions: [{
                values           : visitorsData,
                scale            : ['#ffffff', '#0154ad'],
                normalizeFunction: 'polynomial'
                }]
            },
            onRegionLabelShow: function (e, el, code) {
                if (typeof visitorsData[code] != 'undefined')
                el.html(el.html() + ': ' + visitorsData[code] + ' new visitors')
            }
            })

            // Sparkline charts
            var sparkline1 = new Sparkline($("#sparkline-1")[0], {width: 80, height: 50, lineColor: '#92c1dc', endColor: '#ebf4f9'});
            var sparkline2 = new Sparkline($("#sparkline-2")[0], {width: 80, height: 50, lineColor: '#92c1dc', endColor: '#ebf4f9'});
            var sparkline3 = new Sparkline($("#sparkline-3")[0], {width: 80, height: 50, lineColor: '#92c1dc', endColor: '#ebf4f9'});

            sparkline1.draw([1000, 1200, 920, 927, 931, 1027, 819, 930, 1021]);
            sparkline2.draw([515, 519, 520, 522, 652, 810, 370, 627, 319, 630, 921]);
            sparkline3.draw([15, 19, 20, 22, 33, 27, 31, 27, 19, 30, 21]);

            // The Calender
            $('#calendar').datetimepicker({
            format: 'L',
            inline: true
            })

            // SLIMSCROLL FOR CHAT WIDGET
            $('#chat-box').overlayScrollbars({
            height: '250px'
            })

            /* Chart.js Charts */
            // Sales chart
            var salesChartCanvas = document.getElementById('revenue-chart-canvas').getContext('2d');
            //$('#revenue-chart').get(0).getContext('2d');

            var salesChartData = {
            labels  : [{!! "'".implode("', '", $ad->weeklyActiveUsers()['week'])."'" !!}],
            datasets: [
                {
                label               : 'Digital Goods',
                backgroundColor     : 'rgba(60,141,188,0.9)',
                borderColor         : 'rgba(60,141,188,0.8)',
                pointRadius          : false,
                pointColor          : '#3b8bba',
                pointStrokeColor    : 'rgba(60,141,188,1)',
                pointHighlightFill  : '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data                : [{{ implode(', ', $ad->weeklyActiveUsers()['count']) }}]
                },
            ]
            }

            var salesChartOptions = {
            maintainAspectRatio : false,
            responsive : true,
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                gridLines : {
                    display : false,
                }
                }],
                yAxes: [{
                gridLines : {
                    display : false,
                }
                }]
            }
            }

            // This will get the first returned node in the jQuery collection.
            var salesChart = new Chart(salesChartCanvas, {
                type: 'line',
                data: salesChartData,
                options: salesChartOptions
            }
            )

            // Donut Chart
            var pieChartCanvas = $('#sales-chart-canvas').get(0).getContext('2d')
            var pieData        = {
            labels: [
                'Instore Sales',
                'Download Sales',
                'Mail-Order Sales',
            ],
            datasets: [
                {
                data: [30,12,20],
                backgroundColor : ['#f56954', '#00a65a', '#f39c12'],
                }
            ]
            }
            var pieOptions = {
            legend: {
                display: false
            },
            maintainAspectRatio : false,
            responsive : true,
            }
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            var pieChart = new Chart(pieChartCanvas, {
            type: 'doughnut',
            data: pieData,
            options: pieOptions
            });

            // Sales graph chart
            var salesGraphChartCanvas = $('#line-chart').get(0).getContext('2d');
            //$('#revenue-chart').get(0).getContext('2d');

            var salesGraphChartData = {
            // labels  : ['2011 Q1', '2011 Q2', '2011 Q3', '2011 Q4', '2012 Q1', '2012 Q2', '2012 Q3', '2012 Q4', '2013 Q1', '2013 Q2'],
            labels  : [{!! "'".implode("', '", $ad->salesGraphDashboard()[0])."'" !!}],
            datasets: [
                {
                label               : 'Digital Goods',
                fill                : false,
                borderWidth         : 2,
                lineTension         : 0,
                spanGaps : true,
                borderColor         : '#efefef',
                pointRadius         : 3,
                pointHoverRadius    : 7,
                pointColor          : '#efefef',
                pointBackgroundColor: '#efefef',
                // data                : [2666, 2778, 4912, 3767, 6810, 5670, 4820, 15073, 10687, 8432]
                data                : [{{ implode(', ', $ad->salesGraphDashboard()[1]) }}]
                }
            ]
            }

            var salesGraphChartOptions = {
            maintainAspectRatio : false,
            responsive : true,
            legend: {
                display: false,
            },
            scales: {
                xAxes: [{
                ticks : {
                    fontColor: '#efefef',
                },
                gridLines : {
                    display : false,
                    color: '#efefef',
                    drawBorder: false,
                }
                }],
                yAxes: [{
                ticks : {
                    stepSize: 500,
                    fontColor: '#efefef',
                },
                gridLines : {
                    display : true,
                    color: '#efefef',
                    drawBorder: false,
                }
                }]
            }
            }

            // This will get the first returned node in the jQuery collection.
            var salesGraphChart = new Chart(salesGraphChartCanvas, {
                type: 'line',
                data: salesGraphChartData,
                options: salesGraphChartOptions
            }
            )

            })
    </script>
@endsection





