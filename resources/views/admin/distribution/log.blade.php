@extends('layouts.admin_layout')


@section('title')
    Admin / Distribution Log
@endsection

@section('body-title')
    Distribution Log
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active">Distribution Log</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card">

                <div class="card-body">
                    <div class="table-responsive">
                        <table data-page-length='50' class="table myTbl table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Week</th>
                                <th>Rate</th>
                                <th>Week Total</th>
                                <th>Agent Total</th>
                                <th>Active Player</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($dataArray))
                                @foreach($dataArray as $distribution)
                                    <tr>
                                        <td>{{ _getDate(strtotime($distribution->start_week)) }}</td>
                                        <td>{{ $distribution->rate }}</td>
                                        <td>{{ $distribution->total_amount }}</td>
                                        <td>{{ $distribution->total_amount_new }}</td>
                                        <td>{{ $distribution->total_player }}</td>
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>

                        </table>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <!-- /.modal -->
@endsection

@section('page_js')

@endsection



