@extends('layouts.admin_layout')


@section('title')
    Admin / {{ $data->name }} / Edit
@endsection

@section('body-title')
    User Edit
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('user.index') }}">User</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <table style="width:100%; background:#fff; padding:20px; height:50px;">
                <tbody>
                <tr>
                    <td style="text-align:center"><b> {{ $data->username }}</b></td>
                    <td style="text-align:center"><b>Balance : {{ $data->available_balance }} </b></td>
                    <td style="text-align:center"><b>Free Balance : {{ $data->free_pay_balance }} </b></td>
                    <td style="text-align:center"><b>Agent : admin</b></td>
                    <td style="text-align:center"><b>Pending : 00.00</b></td>
                </tr>
                </tbody>
            </table>

            <div class="row mt-5">
                <div class="col-12 col-sm-12 col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            @include('admin.users.user_navbar')
                        </div>

                        <form  method="post" action="{!! route('user.update', $data->id)!!}">
                            @method('PATCH')
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="">User Name</label>
                                    <input type="text" class="form-control" id="username" name="username" value="{{ $data->username }}" placeholder="Enter username">
                                    @if ($errors->has('username'))
                                        <small class="text-danger">{{ $errors->first('username') }}</small>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="">User Full Name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ $data->name }}" placeholder="Enter User Full Name">
                                    @if ($errors->has('name'))
                                        <small class="text-danger">{{ $errors->first('name') }}</small>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="">User Email</label>
                                    <input type="email" class="form-control" id="email" name="email" value="{{ $data->email }}" placeholder="Enter email">
                                    {{-- @if ($errors->has('email'))
                                        <small class="text-danger">{{ $errors->first('email') }}</small>
                                    @endif --}}
                                </div>

                                <div class="form-group">
                                    <label for="">User Referred By</label>
                                    <input type="text" class="form-control" id="referred_by" name="referred_by" value="{{ $data->referred_by }}" placeholder="Enter Referred By">
                                    @if ($errors->has('referred_by'))
                                        <small class="text-danger">{{ $errors->first('referred_by') }}</small>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="">Account Status</label>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="custom-control custom-radio">
                                                <input class="custom-control-input" type="radio" {{ $data->status == 1 ? 'checked':'' }} value="1" id="open" name="account_status">
                                                <label for="open" class="custom-control-label">Open</label>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="custom-control custom-radio">
                                                <input class="custom-control-input" type="radio" {{ $data->status == 0 ? 'checked':'' }} value="0" id="close" name="account_status">
                                                <label for="close" class="custom-control-label">Close</label>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($errors->has('account_status'))
                                        <small class="text-danger">{{ $errors->first('account_status') }}</small>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="">Casino Status</label>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="custom-control custom-radio">
                                                <input class="custom-control-input" {{ $data->is_casino == 1 ? 'checked':'' }} value="1" type="radio" id="enable" name="casino_status">
                                                <label for="enable" class="custom-control-label">Enable </label>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="custom-control custom-radio">
                                                <input class="custom-control-input" {{ $data->is_casino == 0 ? 'checked':'' }} value="0" type="radio" id="disable" name="casino_status">
                                                <label for="disable" class="custom-control-label">Disable</label>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($errors->has('casino_status'))
                                        <small class="text-danger">{{ $errors->first('casino_status') }}</small>
                                    @endif
                                </div>

                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                        <!-- /.card -->
                    </div>


                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">Reset Password</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="{{ url('admin/reset-password', $data->id) }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Enter Password</label>
                                    <input type="password" id="password" name="password" value="{{ old('password') }}" class="form-control" placeholder="Password" required>
                                    @if ($errors->has('password'))
                                        <small class="text-danger">{{ $errors->first('password') }}</small>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Re-enter Password</label>
                                    <input type="password" id="password_confirmation" value="{{ old('password_confirmation') }}" name="password_confirmation" class="form-control" placeholder="Retype password" required>
                                </div>

                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div><!-- /.container-fluid -->
    </section>

    <!-- /.modal -->
@endsection

@section('page_js')

@endsection



