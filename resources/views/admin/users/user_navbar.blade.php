<ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
    <li class="nav-item">
        <a href="{{ url('admin/user/'.$data->id) }}/edit" class="nav-link {{ Request::is('admin/user/*/edit') ? 'active':'' }}">Edit Profile</a>
    </li>
    <li class="nav-item">
        <a href="{{ url('admin/agentassign/transaction/'.$data->id) }}"  class="nav-link {{ Request::is('admin/agentassign/transaction*') ? 'active':'' }}">Transaction</a>
    </li>
    <li class="nav-item">
        <a href="{{ url('admin/agentassign/limits/'.$data->id) }}" class="nav-link {{ Request::is('admin/agentassign/limits*') ? 'active':'' }}">Limits</a>
    </li>
    <li class="nav-item">
        <a href="{{ url('admin/agentassign/pending/'.$data->id) }}" class="nav-link {{ Request::is('admin/agentassign/pending*') ? 'active':'' }}">Pending</a>
    </li>
    <li class="nav-item">
        <a href="{{ url('admin/agentassign/wager/'.$data->id) }}" class="nav-link {{ Request::is('admin/agentassign/wager*') ? 'active':'' }}">Wager</a>
    </li>
    <li class="nav-item">
        <a href="{{ url('admin/agentassign/notification/'.$data->id) }}" class="nav-link {{ Request::is('admin/agentassign/notification*') ? 'active':'' }}">Notification</a>
    </li>
    <li class="nav-item">
        <a href="{{ url('admin/agentassign/internet-log/'.$data->id) }}" class="nav-link {{ Request::is('admin/agentassign/internet-log*') ? 'active':'' }}">Internet Log</a>
    </li>
    <li class="nav-item">
        <a href="{{ url('admin/agentassign/delete-account/'.$data->id) }}" class="nav-link {{ Request::is('admin/agentassign/delete-account*') ? 'active':'' }}"> Delete Account</a>
    </li>
</ul>
