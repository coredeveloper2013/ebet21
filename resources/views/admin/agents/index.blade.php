@extends('layouts.admin_layout')


@section('title')
    Admin / Agents
@endsection

@section('body-title')
    Agents
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active">Agents</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="button" data-toggle="modal" data-target="#modal-lg" class="btn btn-outline-secondary btn-sm">Create New
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example1" data-page-length='50' class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>S No.</th>
                                <th>Username</th>
                                <th>Name</th>
                                <th>Active User</th>
                                <th>Weekly Balance</th>
                                <th>Total Outstanding Balance</th>
                                <th>Last Login</th>
                                <th>Registered</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($data))
                                @foreach($data as $key => $user)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>
                                        <a href="{{ url('admin/agent/'.$user->id) }}/edit">{{ $user->username }}</a>
                                    </td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ !empty($user->allAssignUser) ? count($user->allAssignUser):0 }}</td>
                                    <td>{{ $user->total_amount }}</td>
                                    <td>{{ $user->agent_total }}</td>
                                    <td>{{ !empty($user->lastLogin) ? _getDate(strtotime($user->lastLogin->created_at)).' '. _getTime(strtotime($user->lastLogin->created_at)):'' }}</td>
                                    <td>{{ _getDate(strtotime($user->registered)).' '. _getTime(strtotime($user->registered))}}</td>
                                    <td>

                                            <div class="dropdown">
                                                    <button
                                                    class="btn btn-outline-secondary dropdown-toggle pt-0 pb-0"
                                                    type="button" id="dropdownMenuButton"
                                                    data-toggle="dropdown"
                                                    aria-haspopup="true"
                                                    aria-expanded="false">
                                                            <i class="fa fa-cog" aria-hidden="true"></i>
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                      <a class="dropdown-item" href="{{ url('admin/assign-user/create/'. $user->id) }}">Assign Users</a>
                                                      <a class="dropdown-item" href="{{ route('invoice.show', $user->id) }}">Invoices</a>
                                                    </div>
                                                  </div>

                                    </td>
                                </tr>
                                @endforeach
                            @endif

                            </tbody>

                        </table>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-teal">
                    <h4 class="modal-title">Create Agent User</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form  method="post" action="{{ route('agent.store') }}">
                    @csrf
                    <div class="modal-body">
                        <div class="body">
                            <div class="form-group">
                                <input type="text" id="name" name="name" value="{{ old('name') }}" class="form-control" placeholder="Full name" required>
                                @if ($errors->has('name'))
                                    <small class="text-danger">{{ $errors->first('name') }}</small>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="E-mail">
                                @if ($errors->has('email'))
                                    <small class="text-danger">{{ $errors->first('email') }}</small>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="text" id="referred_by" name="referred_by" value="{{ old('referred_by') }}" class="form-control" placeholder="Referred By">
                                @if ($errors->has('referred_by'))
                                    <small class="text-danger">{{ $errors->first('referred_by') }}</small>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="text" id="username" name="username" value="{{ old('username') }}" class="form-control" placeholder="Username">
                                @if ($errors->has('username'))
                                    <small class="text-danger">{{ $errors->first('username') }}</small>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="password" id="password" name="password" value="{{ old('password') }}" class="form-control" placeholder="Password" required>
                                @if ($errors->has('password'))
                                    <small class="text-danger">{{ $errors->first('password') }}</small>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="password" id="password_confirmation" value="{{ old('password_confirmation') }}" name="password_confirmation" class="form-control" placeholder="Retype password" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Sign Up</button>
                    </div>
                </form>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection

@section('page_js')

@endsection



