@extends('layouts.admin_layout')


@section('title')
    Admin / Wagers
@endsection

@section('body-title')
    Wagers
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item active">Wagers</li>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example1" data-page-length='10' class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>S No.</th>
                                <th>Username</th>
                                <th>Date Placed</th>
                                <th>Description</th>
                                <th>Risk/Win</th>
                                <th>Ticket ID</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if (!empty($dateRecords))
                                     @php
                                        $iteration = 0;
                                     @endphp
                                    @foreach ($dateRecords as $parlayRecords)
                                        @foreach($parlayRecords as $key => $val)
                                        @php
                                        $iteration++;
                                      @endphp
                                            <tr class="tr-tap-board">
                                            <td>{{$iteration}}</td>
                                                <td data-th="Date Time Accepted " class="table-hide-640">
                                                   {{__getUsername($val->user_id)->username}}
                                                </td>
                                                <td data-th="Ticket #" class="table-hide-640">

                                                    {{ _getDate(strtotime($val->created_at)) }}<br/>{{ _getTime(strtotime($val->created_at)) }}
                                                </td>
                                                <td data-th="Game">
                                                    <div class="table-show-640 tr-tap-sticker">
                                                        <div class="pull-left tr-tap-sticker-inline">{{ _getDate(strtotime($val->created_at)) }} &nbsp;&nbsp;{{ _getTime(strtotime($val->created_at)) }}</div>
                                                        <div class="pull-right tr-tap-sticker-inline">{{ $val->ticket_id }}</div>
                                                    </div>
                                                      {{-- parlay bets pending result --}}
                                                    @if(isset($val->detail))
                                                        @foreach($val->detail as $detail)
                                                        @php ($sportLeague = json_decode($detail->sport_league, true))
                                                        @php( $extraInfo   = json_decode($detail->bet_extra_info))
                                                        @php($type=json_decode($detail->type))
                                                        {{ sportsName($sportLeague[0])."-".$sportLeague[1] }}
                                                        <br>
                                                        {{ isset($extraInfo->bet_on_team_name)?$extraInfo->bet_on_team_name:""}} VS {{isset($extraInfo->other_team_name)?$extraInfo->other_team_name:"" }}
                                                        <br>
                                                        @if (isset($extraInfo->event_date) &&  isset($extraInfo->event_time))
                                                        {{ _getDate(strtotime($extraInfo->event_date)) }} ({{ _getTime(strtotime($extraInfo->event_time)) }})
                                                        @else
                                                        {{ _getDate(strtotime($val->created_at)) }} ({{ _getTime(strtotime($val->created_at)) }})
                                                        @endif

                                                        <br>
                                                        <strong style="font-weight:bold;">
                                                            @if($type==1)
                                                                <strong style="font-weight: bold;"> Spread</strong>
                                                            @elseif($type==2)
                                                                <strong style="font-weight: bold;"> Money Line</strong>
                                                            @elseif($type==123)
                                                                <strong style="font-weight: bold;"> Money Line</strong>
                                                            @elseif($type==3)
                                                                <strong style="font-weight: bold;"> Total</strong>
                                                            @endif
                                                        </strong>
                                                        <strong style="font-weight: bold;">@if($type!==2)({{ $detail->points }})@endif</strong>
                                                        <strong style="font-weight: bold;">{{ ((int)$detail->betting_condition)>0 ? "+".$detail->betting_condition:$detail->betting_condition }} </strong>
                                                            @if($type!=3 && $type!=123)
                                                                for <strong style="font-weight:bold">{{ isset($extraInfo->bet_on_team_name)?$extraInfo->bet_on_team_name:"" }}</strong>
                                                            @endif


                                                            <br>
                                                            <br>
                                                        @endforeach
                                                    {{-- Horse bet pending  --}}
                                                    @elseif(isset($val->horsedetail))
                                                        @foreach($val->horsedetail as $horsedetail)
                                                            @php ($race_name =$horsedetail->race_name)
                                                            @php ($horse_name=$horsedetail->horse_name)
                                                            @php($tournament_name=$horsedetail->tournament_name)
                                                            @php($race_date=$horsedetail->race_date)
                                                            <strong style="font-weight:bold">{{$tournament_name}}</strong><span>_</span><strong style="font-weight:bold">{{$race_name}}</strong><br>
                                                            <strong>{{$race_date}}</strong><br>
                                                            <strong style="font-weight:bold">{{$horsedetail->race_type}}</strong>
                                                            <span>_</span>
                                                            <strong style="font-weight:bold">{{$horsedetail->horse_number}}</strong>
                                                            <strong style="font-weight:bold">{{$horsedetail->horse_name}}</strong>
                                                        @endforeach
                                                    {{-- OTHER bet pending  --}}
                                                    @else
                                                        @php ($sportLeague = json_decode(isset($val->sport_league)?$val->sport_league:'{}', true))
                                                        @php ($extraInfo = json_decode(isset($val->bet_extra_info)?$val->bet_extra_info:'{}'))
                                                        @if(isset($sportLeague[0]) && isset($sportLeague[1]))
                                                        {{ sportsName($sportLeague[0])."-".$sportLeague[1] }}
                                                        @endisset
                                                        <br>
                                                        @if(!empty($extraInfo))
                                                            @if (isset($extraInfo->bet_on_team_name) || isset($extraInfo->other_team_name))
                                                                {{ $extraInfo->bet_on_team_name}}<b> VS </b> {{ $extraInfo->other_team_name }}<br>
                                                            @endif
                                                            @if (isset($extraInfo->event_date) &&  isset($extraInfo->event_time))
                                                                {{ _getDate(strtotime($extraInfo->event_date)) }} ({{ _getTime(strtotime($extraInfo->event_time)) }})
                                                            @else
                                                                {{ _getDate(strtotime($val->created_at)) }} ({{ _getTime(strtotime($val->created_at)) }})
                                                            @endif

                                                        @endif
                                                        <br>
                                                        <?php
                                                        $wager = '';
                                                        $original_condition ='';
                                                        $wager_type = isset($val->betting_wager_type)?$val->betting_wager_type:"";
                                                        $show_for = true;
                                                        include $_SERVER['DOCUMENT_ROOT']. '/../app/wagers.php';
                                                        ?>
                                                        <strong style="font-weight: bold;">
                                                            {{$wager}}
                                                            @if (!empty($original_condition))
                                                            {{$original_condition}}
                                                            @else
                                                             @if (isset($val->betting_condition_original))
                                                             {{ $val->betting_condition_original}}
                                                             @endif
                                                            @endif
                                                        </strong>
                                                        @if ($show_for)
                                                        for <strong style="font-weight:bold;"> {{ isset($extraInfo->bet_on_team_name)?$extraInfo->bet_on_team_name:"" }} </strong>
                                                        @endif
                                                    @endif
                                                </td>

                                                <td >
                                                    {{ $val->risk_amount }} /  {{ $val->parlay_win_amount }}
                                                </td>
                                                <td >
                                                    {{ $val->ticket_id }}
                                                </td>

                                            </tr>
                                        @endforeach
                                    @endforeach
                                @endif
                                </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <!-- /.modal -->
@endsection

@section('page_js')

@endsection



