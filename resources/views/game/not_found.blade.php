@extends('game.game_layout')

@section('game-head')
    <head>
        <title>Game not allowed</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, minimal-ui" />
        <meta name="msapplication-tap-highlight" content="no"/>
    </head>
@endsection

@section('game-content')
    <h1 style="text-align:center; height:100%; color:brown; border-bottom:1px solid brown; padding:100px;">This game not allowed!</h1>
@endsection
