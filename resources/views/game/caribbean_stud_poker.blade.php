@extends('game.game_layout')

@section('game-head')
    <head>
        <title>Caribbean Stud Poker</title>
        <title></title>
        <link rel="stylesheet" href="{{ asset('game/caribbean_stud_poker') }}/css/reset.css" type="text/css">
        <link rel="stylesheet" href="{{ asset('game/caribbean_stud_poker') }}/css/main.css" type="text/css">
        <link rel="stylesheet" href="{{ asset('game/caribbean_stud_poker') }}/css/orientation_utils.css" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0,minimal-ui" />
        <meta name="msapplication-tap-highlight" content="no"/>

        <script type="text/javascript" src="{{ asset('game/caribbean_stud_poker') }}/js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="{{ asset('game/caribbean_stud_poker') }}/js/createjs-2013.12.12.min.js"></script>
        <script type="text/javascript" src="{{ asset('game/caribbean_stud_poker') }}/js/main.js"></script>

    </head>
@endsection

@section('game-content')

    <div style="position: fixed; background-color: transparent; top: 0px; left: 0px; width: 100%; height: 100%"></div>
    <script>
        $(document).ready(function(){
            var oMain = new CMain({
                win_occurrence: 40,          //WIN OCCURRENCE PERCENTAGE. VALUES BETWEEN 0-100
                min_bet: Number("{{ $creditLimits->casino_min_amount }}"),                //MIN BET PLAYABLE BY USER. DEFAULT IS 0.1$
                max_bet: Number("{{ $creditLimits->casino_max_amount }}"),                //MAX BET PLAYABLE BY USER.
                money: Number("{{ Auth::user()->available }}"),                    //STARING CREDIT FOR THE USER
                game_cash: 100,             //GAME CASH AVAILABLE WHEN GAME STARTS
                payout:[
                    100,                 //MULTIPLIER FOR ROYAL FLUSH
                    50,                 //MULTIPLIER FOR STRAIGHT FLUSH
                    20,                 //MULTIPLIER FOR FOUR OF A KIND
                    7,                  //MULTIPLIER FOR FULL HOUSE
                    5,                  //MULTIPLIER FOR FLUSH
                    4,                  //MULTIPLIER FOR STRAIGHT
                    3,                  //MULTIPLIER FOR THREE OF A KIND
                    2,                  //MULTIPLIER FOR TWO PAIR
                    1                  //MULTIPLIER FOR ONE PAIR OR LESS
                ],
                time_show_hand: 1500,        //TIME (IN MILLISECONDS) SHOWING LAST HAND
                show_credits:true,           //SET THIS VALUE TO FALSE IF YOU DON'T TO SHOW CREDITS BUTTON
                fullscreen:true,             //SET THIS TO FALSE IF YOU DON'T WANT TO SHOW FULLSCREEN BUTTON
                check_orientation:true,      //SET TO FALSE IF YOU DON'T WANT TO SHOW ORIENTATION ALERT ON MOBILE DEVICES
                //////////////////////////////////////////////////////////////////////////////////////////
                ad_show_counter: 10           //NUMBER OF HANDS PLAYED BEFORE AD SHOWN
                //
                //// THIS FUNCTIONALITY IS ACTIVATED ONLY WITH CTL ARCADE PLUGIN.///////////////////////////
                /////////////////// YOU CAN GET IT AT: /////////////////////////////////////////////////////////
                // http://codecanyon.net/item/ctl-arcade-wordpress-plugin/13856421 ///////////
            });



            $(oMain).on("recharge", function(evt) {
                alert("recharge");
            });

            $(oMain).on("start_session", function(evt) {
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeStartSession();
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            $(oMain).on("end_session", function(evt) {
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeEndSession();
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            $(oMain).on("bet_placed", function (evt, iTotBet) {
                document.getElementById("place_bid").value =  iTotBet;
            });

            $(oMain).on("save_score", function(evt,iMoney) {
                document.getElementById("win_bid").value =  iMoney;
                makebeteasy();
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeSaveScore({score:iMoney});
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            $(oMain).on("show_interlevel_ad", function(evt) {
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeShowInterlevelAD();
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            $(oMain).on("share_event", function(evt, iScore) {
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeShareEvent({   img: TEXT_SHARE_IMAGE,
                        title: TEXT_SHARE_TITLE,
                        msg: TEXT_SHARE_MSG1 + iScore + TEXT_SHARE_MSG2,
                        msg_share: TEXT_SHARE_SHARE1 + iScore + TEXT_SHARE_SHARE1});
                }
            });

            if (isIOS()) {
                setTimeout(function () {
                    sizeHandler();
                }, 200);
            } else {
                sizeHandler();
            }
        });

    </script>

    <canvas id="canvas" class='ani_hack' width="1700" height="768"> </canvas>
    <div data-orientation="landscape" class="orientation-msg-container"><p class="orientation-msg-text">Please rotate your device</p></div>
    <div id="block_game" style="position: fixed; background-color: transparent; top: 0px; left: 0px; width: 100%; height: 100%; display:none"></div>
@endsection

@section('game-script')
    <input type="hidden" id="place_bid"  value="0">
    <input type="hidden" id="win_bid"  value="0">

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function makebeteasy()
        {
            var place_bid = document.getElementById("place_bid").value ;
            var win_bid = document.getElementById("win_bid").value ;
            $.ajax({
                type: "POST",
                url: "{{ route('casino.store') }}",
                data: {place_bid:place_bid,win_bid:win_bid,game_id:"{{ $game_id }}"},
                success: function(data){
                }
            })
        }
    </script>
@endsection
