@extends('game.game_layout')

@section('game-head')
	<meta charset="utf-8">
	<title>BlackJack</title>
    <script type="text/javascript" src="{{ asset('game/bingo') }}/js/jquery-2.0.3.min.js"></script>
	<script type="text/javascript" src="{{ asset('game/blackjack_21') }}/js/createjs.js"></script>
	<style type="text/css">
		* {
			margin: 0;
			padding: 0;
		}
		html, body {
			background-color: #000;
			overflow: hidden;
		}
	</style>
	<link rel="shortcut icon" sizes="64x64" href="icon.png" />
@endsection

@section('game-content')
    {{-- <div style="position: fixed; background-color: transparent; top: 0px; left: 0px; width: 100%; height: 100%"></div> --}}
    <canvas id="canvas" width="1280" height="720"></canvas>
    <script src="{{ asset('game/blackjack_21') }}/js/screenfull.js"></script>
    <script src="{{ asset('game/blackjack_21') }}/js/main.js"></script>
    <script>
        $(document).ready(function(){
            var oMain = new CMain({
                Cash : Number("{{ floor(Auth::user()->available) }}"),
                reloadAmount : Number("{{ floor(Auth::user()->available) }}"),
                saveMoney: false,
            });
        });
    </script>

    <div data-orientation="landscape" class="orientation-msg-container"><p class="orientation-msg-text">Please rotate your device</p></div>
    <div id="block_game" style="position: fixed; background-color: transparent; top: 0px; left: 0px; width: 100%; height: 100%; display:none"></div>
@endsection

@section('game-script')
    <input type="hidden" id="place_bid"  value="0">
    <input type="hidden" id="win_bid"  value="0">

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function makebeteasy()
        {
            var place_bid = document.getElementById("place_bid").value ;
            var win_bid = document.getElementById("win_bid").value ;
            $.ajax({
                type: "POST",
                url: "{{ route('casino.store') }}",
                data: {place_bid:place_bid,win_bid:win_bid,game_id:"{{ $game_id }}"},
                success: function(data){
                }
            })
        }
    </script>
@endsection
