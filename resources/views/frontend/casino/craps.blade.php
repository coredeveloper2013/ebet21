@extends('layouts.frontend_layout')

@section('title')
    Craps
@endsection

@section('content')
    <section id="body_warapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <div class="col-lg12">
                        <div class="video_game_code_here">
                            <h2>Craps</h2>
                            <iframe style="width: 100%; height: 500px" src="{{ url('game/craps/start') }}"></iframe>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="single_game_img">
                                <img src="{{ asset('assets/img/Casinio/craps.png') }}" alt="" />
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="single_game_details">
                                <p>No matter what time of day, the outdoor basketball courts are always brimming with fast-pace action in Streetball Star. You can score 243 unique ways simply by landing matching icons on consecutive reels. Beyond that, you can win bonus cash through an urban-style Spray Reels feature and a free spins mode. A stacked wild also dominates space on the reels, resulting in more wins. Think you’ve got what it takes to start a win streak in streetball? Hit the court and show them what you’re made of.</p>
                                <p>Features Stacked Wild Stacked wilds take up three spots on a reel and substitute for any icon other than scatters. </p>
                                <p>Free Spins Mode Land at least three free spin scatters side by side on the reels to trigger the game’s free spins mode. Up to 15 free games are available every time.</p>
                                <p>Spray Reels Feature After winning a payout, the winning icons get a dose of spray paint. New icons will appear in their place, giving you a chance to score another win. Additional Information 5 reels.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
