<div ng-controller="realTimeBettingController6f">

    <div class="popup-box d-none" id="confirm_passwordVolleyball">
        <div class="popup-box-body">
            <div class="close-icon">
                <span data-ng-click="passwordCloseIcon()">X</span>
            </div>
            <div>
                <h1>Enter Your Password</h1>
            </div>
            <div>
                <input type="password" class="form-control" id="bet_confirm_passwordVolleyball">
                <p id="mesageVolleyball" style="color: red"></p>
            </div>
            <div>
                <button class="btn" id="submit_confirm_passwordVolleyball">Submit</button>
            </div>
        </div>
    </div>
    <div class="popup-box d-none" id="bet_noticeVolleyball">
        <div class="popup-box-body">
            <div class="close-icon">
                <span data-ng-click="passwordCloseIcon()">X</span>
            </div>
            <div>
                <h4>Bets data has been changed, please review your bets</h4>
                <p>when review is done, hit the confirm button again</p>
            </div>
            <div>
                <button class="btn" id="Okaybutton" data-ng-click="passwordCloseIcon()">Okay</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-9 col-xl-9 col-lg-9 padding-0">
            <article id="myaccount">

                <div class="Live-bet-event-content">
                    <div class="Live-bet-event-content-each" ng-repeat="league in bettingResult track by $index">
                        <div class="Live-bet-league-header">
                            <div class="_league_header_title">@{{league.even_name}}</div>
                            <div class="mobile-w-375">
                                <div class="_league_header_spread">Spread</div>
                                <div class="_league_header_mline">Money <span>Line</span> </div>
                                <div class="_league_header_total">Total</div>
                          </div>
                        </div>
                        <div class="Live-bet-league-each" ng-repeat="item in league.results">
                            <div class="Live-bet-event-each">
                                <div class="_event_sections_mobile">
                                    SET @{{item.set_number}}
                                </div>
                                <div class="_league_header_title">
                                    <div class="_event_sections">
                                        SET @{{item.set_number}}
                                    </div>
                                    <div class="_event_titles">
                                        <ul>
                                            <li>
                                                <span class="spn-title">@{{item.home.name}}</span>
                                                <span class="spn-values">
                                                    <span ng-repeat="val in item.ss.split(',')">@{{ val.split('-')[0] }}</span>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="spn-title">@{{item.away.name}}</span>
                                                <span class="spn-values">
                                                    <span ng-repeat="val in item.ss.split(',')">@{{ val.split('-')[1] }}</span>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="_league_header_spread">
                                    <ul>
                                        <li ng-if="item.odd.first" ng-style="compareODD(item.id, 'odd_first')"
                                            id="odd_first@{{item.id}}" data-ng-click="addtocart(item.id, 'odd_first')">
                                            <span class="mobile-w-100">
                                                <span class="mobile-w-100" style="color: #929292">@{{ item.odd.first_val }}</span>
                                                <span class="mobile-w-100">
                                                    <i class="fa fa-arrow-down" id="odd_first@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                                    <i class="fa fa-arrow-up" id="odd_first@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                                    <span>(</span><span ng-if="item.odd.first > 0" style="margin-right: -2px">+</span> @{{ item.odd.first }})
                                                </span>
                                            </span>
                                        </li>
                                        <li ng-if="!item.odd.first" class="no-event">OTB</li>
                                        <li ng-if="item.odd.second"
                                            ng-style="compareODD(item.id, 'odd_second')"
                                            id="odd_second@{{item.id}}"
                                            data-ng-click="addtocart(item.id, 'odd_second')">
                                            <span class="mobile-w-100">
                                                <span class="mobile-w-100" style="color: #929292">@{{ item.odd.first_val.includes("-") ?  item.odd.second_val.replace("-","+") : item.odd.second_val.replace("+","-") }}</span>
                                                <span class="mobile-w-100">
                                                    <i class="fa fa-arrow-down" id="odd_second@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                                    <i class="fa fa-arrow-up" id="odd_second@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                                    <span>(</span><span ng-if="item.odd.second > 0" style="margin-right: -2px">+</span> @{{ item.odd.second }})
                                                </span>
                                            </span>
                                        </li>
                                        <li ng-if="!item.odd.second" class="no-event">OTB</li>
                                    </ul>
                                </div>
                                <div class="_league_header_mline">
                                    <ul>
                                        <li  ng-if="item.match.over.odd"
                                             ng-style="compareODD(item.id, 'match_over')"
                                             id="match_over@{{item.id}}" data-ng-click="addtocart(item.id, 'match_over')">
                                            <span class="mobile-w-100">
                                                <i class="fa fa-arrow-down" id="match_over@{{item.id}}_decrease"
                                                   style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                                <i class="fa fa-arrow-up" id="match_over@{{item.id}}_increase"
                                                   style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                                <span ng-if="item.match.over.odd > 0" style="margin-right: -2px">+</span> @{{ item.match.over.odd }}
                                            </span>
                                        </li>
                                        <li class="no-event" ng-if="!item.match.over.odd">OTB</li>
                                        <li ng-if="item.match.under.odd"
                                            ng-style="compareODD(item.id, 'match_under')"
                                            id="match_under@{{item.id}}" data-ng-click="addtocart(item.id, 'match_under')">
                                            <span class="mobile-w-100">
                                                <i class="fa fa-arrow-down" id="match_under@{{item.id}}_decrease"
                                                   style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                                <i class="fa fa-arrow-up" id="match_under@{{item.id}}_increase"
                                                   style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                                <span ng-if="item.match.under.odd > 0" style="margin-right: -2px">+</span> @{{ item.match.under.odd }}
                                            </span>
                                        </li>
                                        <li class="no-event" ng-if="!item.match.under.odd">OTB</li>
                                    </ul>
                                </div>
                                <div class="_league_header_total">
                                    <ul>
                                        <li ng-if="item.next.first"
                                            ng-style="compareODD(item.id, 'next_first')"
                                            id="next_first@{{item.id}}" data-ng-click="addtocart(item.id, 'next_first')">
                                            <span class="mobile-w-100">
                                                <span class="mobile-w-100" style="color: #929292">@{{ item.next.first_pretype.substr(0,1) }}@{{ item.next.first_val }}</span>
                                                <span class="mobile-w-100">
                                                    <i class="fa fa-arrow-down" id="next_first@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                                    <i class="fa fa-arrow-up" id="next_first@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                                    <span>(</span><span ng-if="item.next.first > 0" style="margin-right: -2px">+</span> @{{ item.next.first }})
                                                </span>
                                            </span>

                                        </li>
                                        <li class="no-event" ng-if="!item.next.first">OTB</li>
                                        <li ng-if="item.next.second"
                                            ng-style="compareODD(item.id, 'next_second')"
                                            id="next_second@{{item.id}}" data-ng-click="addtocart(item.id, 'next_second')">
                                            <span class="mobile-w-100">
                                                <span class="mobile-w-100" style="color: #929292">(@{{ item.next.second_pretype.substr(0,1) }} @{{ item.next.second_val }}</span>
                                                <span class="mobile-w-100">
                                                    <i class="fa fa-arrow-down" id="next_second@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                                    <i class="fa fa-arrow-up" id="next_second@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                                    <span>(</span><span ng-if="item.next.second > 0" style="margin-right: -2px">+</span> @{{ item.next.second }})
                                                </span>
                                            </span>
                                        </li>
                                        <li class="mediumtd" ng-if="!item.next.second">OTB</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <div ng-if="loading" class="preloading">
                <img src="assets/img/live/spinner.gif" alt="loading..."/>
            </div>
        </div>
        <div class="col-md-3 col-xl-3 col-lg-3 padding-0">
            <div class="bg-overlay" id="popup-optin-3">

                <div class="subscribe-optin" style="margin-top: 8px;">
                    <a href="#" class="optin-close">&times;</a>
                    <div class="selip custom-slip">
                        <div class="slip_cart">
                            <h3> BET SLIP <span id="slip_count" >@{{ selectedData.length }}</span> </h3>
                        </div>
                        <!-- scroll bet slip  scroll_bet_slip-->
                        <div id="dev-slip-container">
                            <div class="bet_slip_add straight_bet_count" ng-if="selectedData.length > 0"
                                 ng-repeat="item in selectedData">
                                <div class="add_title">
                                    <table class="bet_slip_title">
                                        <tr>
                                            <td>@{{ item.displayName }}</td>
                                            <td> <div>
                                                    <span style="float:right;font-size:15px;color:rgba(0,0,0,0.75);">
                                        <i class="fa fa-arrow-down" id="betSlip_@{{item.id}}_@{{item.type}}_decrease"
                                           style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                        <i class="fa fa-arrow-up" id="betSlip_@{{item.id}}_@{{item.type}}_increase"
                                           style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                        <span ng-if="item.spread_handicap != null">( @{{item.spread_handicap}} )</span>
                                        <span ng-if="item.score > 0">+</span>
                                        @{{ item.score }}
                                                         <a class="closecard" data-ng-click="removeCardItem(item.id, item.type)" style="color: #fff !important;"> &times; </a>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="bet_slip_details">
                                        <p> @{{ item.wager }}  </p>
                                        <h3><strong> @{{ item.teamHomeName }} @ @{{ item.teamAwayName }} </strong></h3>
                                        <h3> @{{ item.dateTime | date: 'yyyy-MM-dd' }}</h3>
                                    </div>
                                    <table class="bet_slip_rat">
                                        <tr>
                                            <td>
                                                <input type="text" class="custom-bet-input risk_stake_slip form-control"
                                                       id="volley_risk_@{{ item.type }}_@{{ item.id }}"
                                                       ng-model="item.risk"
                                                       ng-change="betChange(item.id, item.type, 'volley_risk_')"
                                                       placeholder="Risk">
                                            </td>
                                            <td>
                                                <input type="text" class="custom-bet-input risk_win_slip form-control"
                                                       id="volley_win_@{{ item.type }}_@{{ item.id }}"
                                                       ng-model="item.win" ng-change="betChange(item.id, item.type, 'volley_win_')"
                                                       placeholder="Win">
                                            </td>
                                        </tr>
                                    </table>
                                    <p class="messgae hide"></p>
                                    <div class="confirm-success-alert" ng-if="confirmSuccess">
                                        Confirm Success Message
                                    </div>
                                    <div class="confirm-fail-alert" ng-if="confirmFail">
                                        Your Balance doesn't enough
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="live_bet_slip_empty"  ng-if="selectedData.length == 0">
                            <h3>Bet Slip is Empty</h3>
                            <p>Not sure where to start?</p>
                            <a href="#">Learn how to place a bet</a>
                        </div>
                        <div class="slip-message-box"><span id="cartMessage"></span></div>
                        <div class="bet_slip_details">
                            <div class="slip_price_table">
                                <div class="slip_checkbox" id="freePlayContainer">
                                    <input type="checkbox" name="freeplay" value="1" id="freeplay"> Free Play <br>
                                </div>
                                <table>
                                    <tr>
                                        <td>Total bets</td>
                                        <td><span>@{{ selectedData.length }}</span></td>
                                    </tr>
                                    <tr>
                                        <td>Total Stake</td>
                                        <td><span>@{{ totalStake }}</span></td>
                                    </tr>
                                    <tr>
                                        <td>Possible Winning</td>
                                        <td> <span>@{{ possibleWin }}</span> </td>
                                    </tr>
                                </table>
                                <p class="messgae hide" id="messagebox">

                                </p>
                            </div>
                            <div class="confirm">
                                <button type="button" class="confirm" id="confirmbutton"
                                        data-ng-click="confirmBet()">
                                    Confirm
                                </button>
                                <p style="text-align:center;"><a class="clearsection" id="clearBetSlip"
                                                                 data-ng-click="clearBetSlip()"> Clear all selection</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <a href="#popup-optin-3" class="popup-live">SLIP <span id="badge_count" >@{{selectedData.length}}</span></a>

        </div>
    </div>

</div>
