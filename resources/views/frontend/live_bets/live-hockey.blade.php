<div ng-controller="realTimeBettingController3cvc">
    <div class="popup-box d-none" id="confirm_passwordHockey">
       <div class="popup-box-body">
          <div class="close-icon">
             <span data-ng-click="passwordCloseIcon()">X</span>
          </div>
          <div>
             <h1>Enter Your Password</h1>
          </div>
          <div>
             <input type="password" class="form-control" id="bet_confirm_passwordHockey">
             <p id="mesageFootball" style="color: red"></p>
          </div>
          <div>
             <button class="btn" id="submit_confirm_passwordHockey">Submit</button>
          </div>
       </div>
    </div>
    <div class="popup-box d-none" id="bet_noticeFootball">
       <div class="popup-box-body">
          <div class="close-icon">
             <span data-ng-click="passwordCloseIcon()">X</span>
          </div>
          <div>
             <h4>Bets data has been changed, please review your bets</h4>
             <p>when review is done, hit the confirm button again</p>
          </div>
          <div>
             <button class="btn" id="Okaybutton" data-ng-click="passwordCloseIcon()">Okay</button>
          </div>
       </div>
    </div>
    <div class="row">
        <div class="col-md-9 col-xl-9 col-lg-9 padding-0">
            <article id="myaccount" class="col span_12">
                <div class="Live-bet-event-content">
                    <div class="Live-bet-event-content-each" ng-repeat="league in bettingResult track by $index">
                        <div class="Live-bet-league-header">
                            <div class="_league_header_title">@{{league.even_name}}</div>
                            <div class="mobile-w-375">
                                <div class="_league_header_spread">Spread</div>
                                <div class="_league_header_mline">Money <span>Line</span></div>
                                <div class="_league_header_total">Total</div>
                            </div>
                        </div>
                        <div class="Live-bet-league-each" ng-repeat="item in league.results track by $index">
                            <div class="Live-bet-event-each">
                                <div class="_event_sections_mobile">
                                    @{{item.hr}}<br> @{{ item.time }}
                                </div>
                                <div class="_league_header_title">
                                    <div class="_event_sections gm-sec-3">
                                    @{{item.hr}}
                                    <br>
                                    @{{ item.time }}
                                    </div>
                                    <div class="_event_titles gm-sec-3">
                                    <ul>
                                        <li>
                                            <span class="spn-title">@{{item.home.name}}</span>
                                            <span class="spn-values no-letter-spacing">
                                            <span ng-repeat="val in item.ss.home">@{{ val }}</span>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="spn-title">@{{item.away.name}}</span>
                                            <span class="spn-values no-letter-spacing">
                                            <span ng-repeat="val in item.ss.away">@{{ val }}</span>
                                            </span>
                                        </li>
                                        <li class="text-center"><span class="spn-title">Draw</span></li>
                                    </ul>
                                    </div>
                                </div>
                                <div class="_league_header_spread">
                                    <ul>
                                        <li ng-if="item.spread_line.home_odd"
                                            ng-style="compareODD(item.id, 'spread_home')"
                                            id="spread_home@{{item.id}}"
                                            data-ng-click="addtocart(item.id, 'spread_home')">
                                            <i class="fa fa-arrow-down"  id="spread_home@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                            <i class="fa fa-arrow-up" id="spread_home@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                            <span class="mobile-w-100">

                                                <span class="mobile-w-100" style="color: #929292">@{{item.spread_line.home.handicap}}</span>
                                                <span class="mobile-w-100">
                                                    <span>(</span><span ng-if="item.spread_line.home_odd > 0"style="margin-right: -2px">+</span>@{{item.spread_line.home_odd}})
                                                </span>
                                            </span>
                                        </li>
                                        <li ng-if="!item.spread_line.home_odd" class="no-event">OTB</li>
                                        <li ng-if="item.spread_line.away_odd"
                                            ng-style="compareODD(item.id, 'spread_away')"
                                            id="spread_away@{{item.id}}"
                                            data-ng-click="addtocart(item.id, 'spread_away')">
                                            <i class="fa fa-arrow-down"  id="spread_away@{{item.id}}_decrease"  style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                            <i class="fa fa-arrow-up" id="spread_away@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                            <span class="mobile-w-100">
                                                <span class="mobile-w-100" style="color: #929292">
                                                @{{item.spread_line.away.handicap}}
                                                </span>
                                                <span class="mobile-w-100">
                                                    <span>(</span><span ng-if="item.spread_line.away_odd>0"style="margin-right: -2px">+</span>@{{item.spread_line.away_odd}})
                                                </span>
                                            </span>
                                        </li>
                                        <li ng-if="!item.spread_line.away_odd" class="no-event">OTB</li>
                                        <li class="no-event"> <a>&nbsp; </a> </li>
                                    </ul>
                                </div>
                                <div class="_league_header_mline">
                                    <ul>
                                        <li ng-if="item.money_line.home_odd"
                                            ng-style="compareODD(item.id, 'money_home')"
                                            id="money_home@{{item.id}}"
                                            data-ng-click="addtocart(item.id, 'money_home')">
                                            <span class="mobile-w-100">
                                                <i class="fa fa-arrow-down" id="money_home@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                                <i class="fa fa-arrow-up" id="money_home@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                                <span ng-if="item.money_line.home_odd > 0" style="margin-right: -2px">+</span>
                                                @{{item.money_line.home_odd}}
                                            </span>
                                        </li>
                                        <li class="no-event" ng-if="!item.money_line.home_odd">OTB</li>
                                        <li ng-if="item.money_line.away_odd"
                                            ng-style="compareODD(item.id, 'money_away')"
                                            id="money_away@{{item.id}}"
                                            data-ng-click="addtocart(item.id, 'money_away')">
                                            <span class="mobile-w-100">
                                                <i class="fa fa-arrow-down" id="money_away@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                                <i class="fa fa-arrow-up" id="money_away@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>

                                                <span ng-if="item.money_line.away_odd > 0" style="margin-right: -2px">+</span>
                                                @{{item.money_line.away_odd}}
                                            </span>
                                        </li>
                                        <li class="no-event" ng-if="!item.money_line.away_odd">OTB</li>
                                        <li ng-if="item.draw"
                                        ng-style="compareODD(item.id, 'draw_line')"
                                        id="draw_line@{{item.id}}"
                                        data-ng-click="addtocart(item.id, 'draw_line')">
                                        <i class="fa fa-arrow-down" id="draw_line@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                        <i class="fa fa-arrow-up" id="draw_line@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                                <span ng-if="item.draw > 0" style="margin-right: -2px">+ </span>
                                                @{{item.draw}}
                                        </li>
                                        <li class="no-event" ng-if="!item.draw"> OTB</li>
                                    </ul>
                                </div>
                                <div class="_league_header_total">
                                    <ul>
                                    <li ng-if="item.over.home_odd"
                                        ng-style="compareODD(item.id, 'total_home')"
                                        id="total_home@{{item.id}}"
                                        data-ng-click="addtocart(item.id, 'total_home')">
                                        <i class="fa fa-arrow-down" id="total_home@{{item.id}}_decrease" style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                        <i class="fa fa-arrow-up" id="total_home@{{item.id}}_increase"  style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                        <span class="mobile-w-100">
                                            <span class="mobile-w-100" style="color: #929292">
                                            @{{item.over.handicap}}
                                            </span>
                                            <span>(</span><span class="mobile-w-100"><span ng-if="item.over.home_odd > 0"style="margin-right: -2px">+</span>@{{item.over.home_odd}})
                                            </span>
                                        </span>
                                    </li>
                                    <li class="no-event" ng-if="!item.over.home_odd">OTB</li>
                                    <li ng-if="item.under.away_odd"
                                        ng-style="compareODD(item.id, 'total_away')"
                                        id="total_away@{{item.id}}"
                                        data-ng-click="addtocart(item.id, 'total_away')">
                                        <i class="fa fa-arrow-down" id="total_away@{{item.id}}_decrease"  style="font-size:12px; color:red; display: none; margin-right: 5px"></i>
                                        <i class="fa fa-arrow-up" id="total_away@{{item.id}}_increase" style="font-size:12px; color:green; display: none; margin-right: 5px"></i>
                                        <span class="mobile-w-100">
                                            <span class="mobile-w-100" style="color: #929292">@{{item.under.handicap}}</span>
                                            <span>(</span><span class="mobile-w-100"><span ng-if="item.under.away_odd > 0" style="margin-right: -2px">+</span>@{{item.under.away_odd}})
                                            </span>
                                        </span>
                                    </li>
                                    <li class="mediumtd" ng-if="!item.under.away_odd">OTB</li>
                                    <li class="no-event"> <a>&nbsp; </a> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <div ng-if="loading" class="preloading">
                <img src="assets/img/live/spinner.gif" alt="loading..."/>
            </div>
        </div>
        <div class="col-md-3 col-xl-3 col-lg-3 padding-0">
            <div class="bg-overlay" id="popup-optin-6">
                <div class="subscribe-optin" style="margin-top: 8px;">
                    <a href="#" class="optin-close">&times;</a>
                    @php
                            $slip = [
                                'name' => 'hockey'
                             ];
                        @endphp
                        @include('frontend.live_bets.live-slip',$slip)
                </div>
            </div>
        </div>
    </div>
    <a href="#popup-optin-6" class="popup-live">SLIP <span id="badge_count">@{{selectedData.length}}</span></a>
 </div>
