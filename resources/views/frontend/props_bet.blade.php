@extends('layouts.frontend_layout')

@section('title')

@endsection

@section('content')



<!-- main body area start -->
<section id="body_warapper">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-12 col-xs-12 col-lg-9 col-xl-9">
                @php
                    $prop_data = json_decode(json_encode($prop_bet_details['prop_data']), true);
                    $event_data = $prop_bet_details['event_info'];
                    $event_data->date = _getDate($event_data->time);
                    $event_data->time = _getTime($event_data->time);
                    $home_name = $event_data->home_name;
                    $away_name = $event_data->away_name;
                @endphp
                <!-- Upcoming Events area start -->
                <div class="col-12" id="upcoming_events">
                    <div class="row">
                        <div class="col-lg-6 col-xs-6">
                            <div class="upcoming_title">
                                <h2> {{$home_name}} @ {{ $away_name }}</h2>
                                {{--   <p>{{_getHeadlineDate($event_data->time)}}</p> --}}
                            </div>
                        </div>
                        <div class="col-lg-6 col-xs-12">
                            <div class="go_conintue -mobile-devices-previous-button">
                                <a href="{{ url('/straight_odds') }}">Previous</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Upcoming Events area end -->
                @if ($prop_bet_details['prop_data']->sport_id==16)
                    {{--team total baseball--}}
                     @if(!empty($prop_data['team_totals']))
                         <!-- 1st half -->
                             {{--mobile-view--}}
                             <table class="table prop-table prop-td-show-767">
                                 <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">Team Totals</h3>
                                 <thead>
                                 <tr>
                                     <th>Team Name</th>
                                     <th>Team Total 1</th>
                                     <th>Team Total 2</th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                 <tr class="text-center">
                                     <td>
                                         {{ $home_name }}
                                     </td>
                                     <td data-th="Spread" class="">
                                         @if (empty($prop_data['team_totals'][0]['odds']))
                                             <a>OTB</a>
                                         @else
                                             @php
                                                 $prop_data['team_totals'][0]['odds'] =
                                                 _toAmericanDecimal($prop_data['team_totals'][0]['odds']);
                                                 $prop_data['team_totals'][0]['team_name'] = "Team Totals";
                                                 $prop_data['team_totals'][0]['sub_type_name'] = $prop_data['team_totals'][0]['name'];
                                                 $unique_id = $event_data->id."team-totals-77558-01-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['team_totals'][0]['sub_type_name']);
                                                 $prop_data['team_totals'][0]['Unique_id'] = $unique_id;
                                                 $prop_data['team_totals'][0]['betting_wager'] = $prop_data['team_totals'][0]['name'];
                                                 $prop_data['team_totals'][0]['row_line'] = 'home';
                                                 $prop_data['team_totals'][0]['handicap'] = $prop_data['team_totals'][0]['handicap'];
                                             @endphp
                                             <a id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['team_totals'][0])}}','prop_bet')">
                                                 ({{ $prop_data['team_totals'][0]['name'] }})
                                                 {{_preccedSign($prop_data['team_totals'][0]['odds'])}} </a>
                                         @endif

                                     </td>
                                     <td data-th="Money Line" class=" ">
                                         <div class="">
                                             @if (empty($prop_data['team_totals'][1]['odds']))
                                                 <a>OTB</a>
                                             @else
                                                 @php
                                                     $prop_data['team_totals'][1]['odds'] =
                                                     _toAmericanDecimal($prop_data['team_totals'][1]['odds']);
                                                     $prop_data['team_totals'][1]['team_name'] = "Team Totals";
                                                     $prop_data['team_totals'][1]['sub_type_name'] = $prop_data['team_totals'][1]['name'];
                                                     $unique_id = $event_data->id."team-totals-77500-02-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['team_totals'][1]['sub_type_name']);
                                                     $prop_data['team_totals'][1]['Unique_id'] = $unique_id;
                                                     $prop_data['team_totals'][1]['betting_wager'] = $prop_data['team_totals'][1]['name'];
                                                     $prop_data['team_totals'][1]['row_line'] = 'home';
                                                     $prop_data['team_totals'][1]['handicap'] = $prop_data['team_totals'][1]['handicap'];
                                                 @endphp
                                                 <a id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['team_totals'][1])}}','prop_bet')">
                                                     ({{ $prop_data['team_totals'][1]['name'] }})
                                                     {{_preccedSign($prop_data['team_totals'][1]['odds'])}} </a>
                                             @endif

                                         </div>
                                     </td>
                                 </tr>
                                 <tr class="text-center">
                                     <td>
                                         {{ $away_name  }}
                                     </td>
                                     <td data-th="Spread" class="">
                                         @if (empty($prop_data['team_totals'][2]['odds']))
                                             <a href="#">OTB</a>
                                         @else
                                             @php
                                                 $prop_data['team_totals'][2]['odds'] =
                                                 _toAmericanDecimal($prop_data['team_totals'][2]['odds']);
                                                 $prop_data['team_totals'][2]['team_name'] = "Team Totals";
                                                 $prop_data['team_totals'][2]['sub_type_name'] = $prop_data['team_totals'][2]['name'];
                                                 $unique_id = $event_data->id."team-totals-77300-03-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['team_totals'][2]['sub_type_name']);
                                                 $prop_data['team_totals'][2]['Unique_id'] = $unique_id;
                                                 $prop_data['team_totals'][2]['betting_wager'] = $prop_data['team_totals'][2]['name'];
                                                 $prop_data['team_totals'][2]['row_line'] = 'home';
                                                 $prop_data['team_totals'][2]['handicap'] = $prop_data['team_totals'][2]['handicap'];
                                             @endphp
                                             <a id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['team_totals'][2])}}','prop_bet')">
                                                 ({{ $prop_data['team_totals'][2]['name'] }})
                                                 {{_preccedSign($prop_data['team_totals'][2]['odds'])}} </a>
                                         @endif

                                     </td>
                                     <td data-th="Total" class="">
                                         @if(empty($prop_data['team_totals'][3]['odds']))
                                             <a>OTB</a>
                                         @else
                                             @php
                                                 $prop_data['team_totals'][3]['odds'] =
                                                 _toAmericanDecimal($prop_data['team_totals'][3]['odds']);
                                                 $prop_data['team_totals'][3]['team_name'] = "Team Totals";
                                                 $prop_data['team_totals'][3]['sub_type_name'] = $prop_data['team_totals'][3]['name'];
                                                 $unique_id = $event_data->id."team-totals-77000-04-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['team_totals'][3]['sub_type_name']);
                                                 $prop_data['team_totals'][3]['Unique_id'] = $unique_id;
                                                 $prop_data['team_totals'][3]['betting_wager'] = $prop_data['team_totals'][3]['name'];
                                                 $prop_data['team_totals'][3]['row_line'] = 'home';
                                                 $prop_data['team_totals'][3]['handicap'] = $prop_data['team_totals'][3]['handicap'];
                                             @endphp
                                             <a id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['team_totals'][3])}}','prop_bet')">
                                                 ({{ $prop_data['team_totals'][3]['name'] }})
                                                 {{_preccedSign($prop_data['team_totals'][3]['odds'])}} </a>
                                         @endif
                                     </td>
                                 </tr>
                                 </tbody>
                             </table>
                             {{--mobile-view end--}}
                             <table class="rwd-table newtables prop-td-hide-767">
                                 <h3 class="tabletitle prop-td-hide-767">Team Totals</h3>
                                 <tbody>
                                 <tr>
                                     <th>Team Name</th>
                                     <th>Team Total 1</th>
                                     <th>Team Total 2</th>
                                 </tr>
                                 <!-- table single column & row list -->
                                 <tr class="mobilecss wordltable">
                                     <td data-th="Player name" class="">
                                         <!-- Each table heading name &  data-th wile be same -->
                                         <div class="newtabls">
                                             <p>{{ $home_name }}</p>
                                         </div>
                                     </td>
                                     <td data-th="Spread" class="">
                                         @if (empty($prop_data['team_totals'][0]['odds']))
                                             <a>OTB</a>
                                         @else
                                             @php
                                                 $prop_data['team_totals'][0]['odds'] =
                                                 _toAmericanDecimal($prop_data['team_totals'][0]['odds']);
                                                 $prop_data['team_totals'][0]['team_name'] = "Team Totals";
                                                 $prop_data['team_totals'][0]['sub_type_name'] = $prop_data['team_totals'][0]['name'];
                                                 $unique_id = $event_data->id."team-totals-77558-01-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['team_totals'][0]['sub_type_name']);
                                                 $prop_data['team_totals'][0]['Unique_id'] = $unique_id;
                                                 $prop_data['team_totals'][0]['betting_wager'] = $prop_data['team_totals'][0]['name'];
                                                 $prop_data['team_totals'][0]['row_line'] = 'home';
                                                 $prop_data['team_totals'][0]['handicap'] = $prop_data['team_totals'][0]['handicap'];
                                             @endphp
                                             <a id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['team_totals'][0])}}','prop_bet')">
                                                 ({{ $prop_data['team_totals'][0]['name'] }})
                                                 {{_preccedSign($prop_data['team_totals'][0]['odds'])}} </a>
                                         @endif

                                     </td>
                                     <td data-th="Money Line" class=" ">
                                         <div class="">
                                             @if (empty($prop_data['team_totals'][1]['odds']))
                                                 <a>OTB</a>
                                             @else
                                                 @php
                                                     $prop_data['team_totals'][1]['odds'] =
                                                     _toAmericanDecimal($prop_data['team_totals'][1]['odds']);
                                                     $prop_data['team_totals'][1]['team_name'] = "Team Totals";
                                                     $prop_data['team_totals'][1]['sub_type_name'] = $prop_data['team_totals'][1]['name'];
                                                     $unique_id = $event_data->id."team-totals-77500-02-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['team_totals'][1]['sub_type_name']);
                                                     $prop_data['team_totals'][1]['Unique_id'] = $unique_id;
                                                     $prop_data['team_totals'][1]['betting_wager'] = $prop_data['team_totals'][1]['name'];
                                                     $prop_data['team_totals'][1]['row_line'] = 'home';
                                                     $prop_data['team_totals'][1]['handicap'] = $prop_data['team_totals'][1]['handicap'];
                                                 @endphp
                                                 <a id="{{$unique_id}}"
                                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['team_totals'][1])}}','prop_bet')">
                                                     ({{ $prop_data['team_totals'][1]['name'] }})
                                                     {{_preccedSign($prop_data['team_totals'][1]['odds'])}} </a>
                                             @endif

                                         </div>
                                     </td>
                                 </tr>
                                 <tr class="mobilecss wordltable">
                                     <td data-th="Player name" class="">
                                         <!-- Each table heading name &  data-th wile be same -->
                                         <div class="newtabls">
                                             <p>{{ $away_name  }}</p>
                                         </div>
                                     </td>
                                     <td data-th="Spread" class="">
                                         @if (empty($prop_data['team_totals'][2]['odds']))
                                             <a href="#">OTB</a>
                                         @else
                                             @php
                                                 $prop_data['team_totals'][2]['odds'] =
                                                 _toAmericanDecimal($prop_data['team_totals'][2]['odds']);
                                                 $prop_data['team_totals'][2]['team_name'] = "Team Totals";
                                                 $prop_data['team_totals'][2]['sub_type_name'] = $prop_data['team_totals'][2]['name'];
                                                 $unique_id = $event_data->id."team-totals-77300-03-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['team_totals'][2]['sub_type_name']);
                                                 $prop_data['team_totals'][2]['Unique_id'] = $unique_id;
                                                 $prop_data['team_totals'][2]['betting_wager'] = $prop_data['team_totals'][2]['name'];
                                                 $prop_data['team_totals'][2]['row_line'] = 'home';
                                                 $prop_data['team_totals'][2]['handicap'] = $prop_data['team_totals'][2]['handicap'];
                                             @endphp
                                             <a id="{{$unique_id}}"
                                                onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['team_totals'][2])}}','prop_bet')">
                                                 ({{ $prop_data['team_totals'][2]['name'] }})
                                                 {{_preccedSign($prop_data['team_totals'][2]['odds'])}} </a>
                                         @endif

                                     </td>
                                     <td data-th="Total" class="">
                                            @if(empty($prop_data['team_totals'][3]['odds']))
                                             <a>OTB</a>
                                                @else
                                             @php
                                                 $prop_data['team_totals'][3]['odds'] =
                                                 _toAmericanDecimal($prop_data['team_totals'][3]['odds']);
                                                 $prop_data['team_totals'][3]['team_name'] = "Team Totals";
                                                 $prop_data['team_totals'][3]['sub_type_name'] = $prop_data['team_totals'][3]['name'];
                                                 $unique_id = $event_data->id."team-totals-77000-04-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['team_totals'][3]['sub_type_name']);
                                                 $prop_data['team_totals'][3]['Unique_id'] = $unique_id;
                                                 $prop_data['team_totals'][3]['betting_wager'] = $prop_data['team_totals'][3]['name'];
                                                 $prop_data['team_totals'][3]['row_line'] = 'home';
                                                 $prop_data['team_totals'][3]['handicap'] = $prop_data['team_totals'][3]['handicap'];
                                             @endphp
                                         <a id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['team_totals'][3])}}','prop_bet')">
                                             ({{ $prop_data['team_totals'][3]['name'] }})
                                             {{_preccedSign($prop_data['team_totals'][3]['odds'])}} </a>
                                                @endif
                                     </td>
                                 </tr>

                                 </tbody>
                             </table>
                             <!-- 1st half -->
                     @endif

                @if (!empty($prop_data['a_run_in_the_1st_inning']))

                <!-- First Team to Score -->
                    {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">A run in the first inning</h3>
                        <thead>
                        <tr>
                            <th>{{ $prop_data['a_run_in_the_1st_inning'][0]['name'] }}</th>
                            <th>{{ $prop_data['a_run_in_the_1st_inning'][1]['name'] }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="text-center">
                            <td data-th="{{ $prop_data['a_run_in_the_1st_inning'][0]['name'] }}" class=" ">
                                @if (empty($prop_data['a_run_in_the_1st_inning'][0]['odds']))
                                    <div class="">
                                        <a href="#"> OTB </a>
                                    </div>
                                @else
                                    <div class="">
                                        @php
                                            $prop_data['a_run_in_the_1st_inning'][0]['odds'] =
                                            _toAmericanDecimal($prop_data['a_run_in_the_1st_inning'][0]['odds']);
                                            $prop_data['a_run_in_the_1st_inning'][0]['team_name'] = "A run in the first inning";
                                            $prop_data['a_run_in_the_1st_inning'][0]['sub_type_name'] =
                                            '('.$prop_data['a_run_in_the_1st_inning'][0]['name'].')';
                                            $unique_id = $event_data->id."a-run-in-the-1st-inning-99665-01-".str_replace(['.', '_', ' ', '--','(',')'], '-',$prop_data['a_run_in_the_1st_inning'][0]['sub_type_name']);
                                            $prop_data['a_run_in_the_1st_inning'][0]['Unique_id'] = $unique_id;
                                            $prop_data['a_run_in_the_1st_inning'][0]['betting_wager'] = $prop_data['a_run_in_the_1st_inning'][0]['name'];
                                            $prop_data['a_run_in_the_1st_inning'][0]['row_line'] = 'home';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['a_run_in_the_1st_inning'][0])}}','prop_bet')">
                                            {{ _preccedSign($prop_data['a_run_in_the_1st_inning'][0]['odds'])}}
                                        </a>
                                    </div>
                                @endif

                            </td>
                            <td data-th="{{ $prop_data['a_run_in_the_1st_inning'][1]['name'] }}" class="">
                                @if (empty($prop_data['a_run_in_the_1st_inning'][1]['odds']))
                                    <a href="#"> OTB </a>
                                @else
                                    @php
                                        $prop_data['a_run_in_the_1st_inning'][1]['odds'] =
                                        _toAmericanDecimal($prop_data['a_run_in_the_1st_inning'][1]['odds']);
                                        $prop_data['a_run_in_the_1st_inning'][1]['team_name'] = "A run in the first inning";
                                        $prop_data['a_run_in_the_1st_inning'][1]['sub_type_name'] =
                                        '('.$prop_data['a_run_in_the_1st_inning'][1]['name'].')';
                                        $unique_id = $event_data->id."a-run-in-the-1st-inning-45455-02-".str_replace(['.', '_', ' ', '--','(',')'], '-',$prop_data['a_run_in_the_1st_inning'][1]['sub_type_name']);
                                        $prop_data['a_run_in_the_1st_inning'][1]['Unique_id'] = $unique_id;
                                        $prop_data['a_run_in_the_1st_inning'][1]['betting_wager'] =  $prop_data['a_run_in_the_1st_inning'][1]['name'];
                                        $prop_data['a_run_in_the_1st_inning'][1]['row_line'] = 'away';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['a_run_in_the_1st_inning'][1])}}','prop_bet')">
                                        {{_preccedSign($prop_data['a_run_in_the_1st_inning'][1]['odds'])}}
                                    </a>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    {{--mobile-view end--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-show-767">A run in the first inning</h3>
                    <tbody class="rays">
                        <!-- table heading name -->
                        <tr>
                            <th>{{ $prop_data['a_run_in_the_1st_inning'][0]['name'] }}</th>
                            <th>{{ $prop_data['a_run_in_the_1st_inning'][1]['name'] }}</th>
                        </tr>
                        <!-- table single column & row list -->
                        <tr class="mobilecss wordltable">
                            <td data-th="{{ $prop_data['a_run_in_the_1st_inning'][0]['name'] }}" class=" ">
                                @if (empty($prop_data['a_run_in_the_1st_inning'][0]['odds']))
                                <div class="">
                                    <a href="#"> OTB </a>
                                </div>
                                @else
                                <div class="">
                                    @php
                                    $prop_data['a_run_in_the_1st_inning'][0]['odds'] =
                                    _toAmericanDecimal($prop_data['a_run_in_the_1st_inning'][0]['odds']);
                                    $prop_data['a_run_in_the_1st_inning'][0]['team_name'] = "A run in the first inning";
                                    $prop_data['a_run_in_the_1st_inning'][0]['sub_type_name'] =
                                    '('.$prop_data['a_run_in_the_1st_inning'][0]['name'].')';
                                    $unique_id = $event_data->id."a-run-in-the-1st-inning-99665-01-".str_replace(['.', '_', ' ', '--','(',')'], '-',$prop_data['a_run_in_the_1st_inning'][0]['sub_type_name']);
                                    $prop_data['a_run_in_the_1st_inning'][0]['Unique_id'] = $unique_id;
                                    $prop_data['a_run_in_the_1st_inning'][0]['betting_wager'] = $prop_data['a_run_in_the_1st_inning'][0]['name'];
                                    $prop_data['a_run_in_the_1st_inning'][0]['row_line'] = 'home';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                        onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['a_run_in_the_1st_inning'][0])}}','prop_bet')">
                                        {{ _preccedSign($prop_data['a_run_in_the_1st_inning'][0]['odds'])}}
                                    </a>
                                </div>
                                @endif

                            </td>
                            <td data-th="{{ $prop_data['a_run_in_the_1st_inning'][1]['name'] }}" class="">
                                @if (empty($prop_data['a_run_in_the_1st_inning'][1]['odds']))
                                <a href="#"> OTB </a>
                                @else
                                @php
                                $prop_data['a_run_in_the_1st_inning'][1]['odds'] =
                                _toAmericanDecimal($prop_data['a_run_in_the_1st_inning'][1]['odds']);
                                $prop_data['a_run_in_the_1st_inning'][1]['team_name'] = "A run in the first inning";
                                $prop_data['a_run_in_the_1st_inning'][1]['sub_type_name'] =
                                '('.$prop_data['a_run_in_the_1st_inning'][1]['name'].')';
                                $unique_id = $event_data->id."a-run-in-the-1st-inning-45455-02-".str_replace(['.', '_', ' ', '--','(',')'], '-',$prop_data['a_run_in_the_1st_inning'][1]['sub_type_name']);
                                $prop_data['a_run_in_the_1st_inning'][1]['Unique_id'] = $unique_id;
                                $prop_data['a_run_in_the_1st_inning'][1]['betting_wager'] =  $prop_data['a_run_in_the_1st_inning'][1]['name'];
                                $prop_data['a_run_in_the_1st_inning'][1]['row_line'] = 'away';
                                @endphp
                                <a id="{{$unique_id}}"
                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['a_run_in_the_1st_inning'][1])}}','prop_bet')">
                                    {{_preccedSign($prop_data['a_run_in_the_1st_inning'][1]['odds'])}}
                                </a>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>

                @endif

                @if (!empty($prop_data['first_team_to_score']))
                <!-- First Team to Score -->
                    {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">Team Totals</h3>
                        <thead>
                        <tr>
                            <th>{{ $prop_data['first_team_to_score'][0]['name'] }}</th>
                            <th>{{ $prop_data['first_team_to_score'][1]['name'] }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="text-center">
                            <td data-th="{{ $prop_data['first_team_to_score'][0]['name'] }}" class=" ">
                                @if (empty($prop_data['first_team_to_score'][0]['odds']))
                                    <div class="">
                                        <a href="#"> OTB </a>
                                    </div>
                                @else
                                    <div class="">
                                        @php
                                            $prop_data['first_team_to_score'][0]['odds'] =
                                            _toAmericanDecimal($prop_data['first_team_to_score'][0]['odds']);
                                            $prop_data['first_team_to_score'][0]['team_name'] = "First Team to Score";
                                            $prop_data['first_team_to_score'][0]['sub_type_name'] =
                                            $prop_data['first_team_to_score'][0]['name'];
                                            $unique_id = $event_data->id."first-team-to-score-84849-01-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['first_team_to_score'][0]['sub_type_name']);
                                            $prop_data['first_team_to_score'][0]['Unique_id'] = $unique_id;
                                            $prop_data['first_team_to_score'][0]['betting_wager'] = $prop_data['first_team_to_score'][0]['name'];
                                            $prop_data['first_team_to_score'][0]['row_line'] = 'home';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_team_to_score'][0])}}','prop_bet')">
                                            {{_preccedSign($prop_data['first_team_to_score'][0]['odds'])}}
                                        </a>
                                    </div>
                                @endif

                            </td>
                            <td data-th="{{ $prop_data['first_team_to_score'][1]['name'] }}" class="">
                                @if (empty($prop_data['first_team_to_score'][1]['odds']))
                                    <a href="#"> OTB </a>
                                @else
                                    @php
                                        $prop_data['first_team_to_score'][1]['odds'] =
                                        _toAmericanDecimal($prop_data['first_team_to_score'][1]['odds']);
                                        $prop_data['first_team_to_score'][1]['team_name'] = "First Team to Score";
                                        $prop_data['first_team_to_score'][1]['sub_type_name'] =
                                        $prop_data['first_team_to_score'][1]['name'];
                                        $unique_id = $event_data->id."first-team-to-score-85269-02-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['first_team_to_score'][1]['sub_type_name']);
                                        $prop_data['first_team_to_score'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_team_to_score'][1]['betting_wager'] = $prop_data['first_team_to_score'][1]['name'];
                                        $prop_data['first_team_to_score'][1]['row_line'] = 'away';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_team_to_score'][1])}}','prop_bet')">
                                        {{_preccedSign($prop_data['first_team_to_score'][1]['odds'])}}
                                    </a>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    {{--mobile-view end--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767">Team Totals</h3>
                    <tbody class="rays">
                        <!-- table heading name -->

                        <tr>
                            <th>{{ $prop_data['first_team_to_score'][0]['name'] }}</th>
                            <th>{{ $prop_data['first_team_to_score'][1]['name'] }}</th>
                        </tr>

                        <!-- table single column & row list -->
                        <tr class="mobilecss wordltable">
                            <td data-th="{{ $prop_data['first_team_to_score'][0]['name'] }}" class=" ">
                                @if (empty($prop_data['first_team_to_score'][0]['odds']))
                                <div class="">
                                    <a href="#"> OTB </a>
                                </div>
                                @else
                                <div class="">
                                    @php
                                    $prop_data['first_team_to_score'][0]['odds'] =
                                    _toAmericanDecimal($prop_data['first_team_to_score'][0]['odds']);
                                    $prop_data['first_team_to_score'][0]['team_name'] = "First Team to Score";
                                    $prop_data['first_team_to_score'][0]['sub_type_name'] =
                                    $prop_data['first_team_to_score'][0]['name'];
                                    $unique_id = $event_data->id."first-team-to-score-84849-01-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['first_team_to_score'][0]['sub_type_name']);
                                    $prop_data['first_team_to_score'][0]['Unique_id'] = $unique_id;
                                    $prop_data['first_team_to_score'][0]['betting_wager'] = $prop_data['first_team_to_score'][0]['name'];
                                    $prop_data['first_team_to_score'][0]['row_line'] = 'home';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                        onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_team_to_score'][0])}}','prop_bet')">
                                        {{_preccedSign($prop_data['first_team_to_score'][0]['odds'])}}
                                    </a>
                                </div>
                                @endif

                            </td>
                            <td data-th="{{ $prop_data['first_team_to_score'][1]['name'] }}" class="">
                                @if (empty($prop_data['first_team_to_score'][1]['odds']))
                                <a href="#"> OTB </a>
                                @else
                                @php
                                $prop_data['first_team_to_score'][1]['odds'] =
                                _toAmericanDecimal($prop_data['first_team_to_score'][1]['odds']);
                                $prop_data['first_team_to_score'][1]['team_name'] = "First Team to Score";
                                $prop_data['first_team_to_score'][1]['sub_type_name'] =
                                $prop_data['first_team_to_score'][1]['name'];
                                $unique_id = $event_data->id."first-team-to-score-85269-02-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['first_team_to_score'][1]['sub_type_name']);
                                $prop_data['first_team_to_score'][1]['Unique_id'] = $unique_id;
                                $prop_data['first_team_to_score'][1]['betting_wager'] = $prop_data['first_team_to_score'][1]['name'];
                                $prop_data['first_team_to_score'][1]['row_line'] = 'away';
                                @endphp
                                <a id="{{$unique_id}}"
                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_team_to_score'][1])}}','prop_bet')">
                                    {{_preccedSign($prop_data['first_team_to_score'][1]['odds'])}}
                                </a>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>

                @endif


                @if (!empty($prop_data['five_innings_line']))
                <!-- 5 Innings -->
                    {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">5 Innings</h3>
                        <thead>
                        <tr>
                            <th>{{ $prop_data['five_innings_line'][0]['name'] }}</th>
                            <th>{{ $prop_data['five_innings_line'][1]['name'] }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="text-center">
                            <td data-th="{{ $prop_data['five_innings_line'][0]['name'] }}" class=" ">
                                @if (empty($prop_data['five_innings_line'][0]['odds']))
                                    <div class="">
                                        <a href="#"> OTB </a>
                                    </div>
                                @else
                                    <div class="">
                                        @php
                                            $prop_data['five_innings_line'][0]['odds'] =
                                            _toAmericanDecimal($prop_data['five_innings_line'][0]['odds']);
                                            $prop_data['five_innings_line'][0]['team_name'] = "5 Innings";
                                            $prop_data['five_innings_line'][0]['sub_type_name'] =
                                            $prop_data['five_innings_line'][0]['name'];
                                            $unique_id = $event_data->id."five-innings-line-86569-01-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['five_innings_line'][0]['sub_type_name']);
                                            $prop_data['five_innings_line'][0]['Unique_id'] = $unique_id;
                                            $prop_data['five_innings_line'][0]['betting_wager'] = $prop_data['five_innings_line'][0]['name'];
                                            $prop_data['five_innings_line'][0]['row_line'] = 'home';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['five_innings_line'][0])}}','prop_bet')">
                                            {{_preccedSign($prop_data['five_innings_line'][0]['odds'])}}
                                        </a>
                                    </div>
                                @endif

                            </td>
                            <td data-th="{{ $prop_data['five_innings_line'][1]['name'] }}" class="">
                                @if (empty($prop_data['five_innings_line'][1]['odds']))
                                    <a href="#"> OTB </a>
                                @else
                                    @php
                                        $prop_data['five_innings_line'][1]['odds'] =
                                        _toAmericanDecimal($prop_data['five_innings_line'][1]['odds']);
                                        $prop_data['five_innings_line'][1]['team_name'] = "5 Innings";
                                        $prop_data['five_innings_line'][1]['sub_type_name'] =
                                        $prop_data['five_innings_line'][1]['name'];
                                        $unique_id = $event_data->id."five-innings-line-86319-01-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['five_innings_line'][1]['sub_type_name']);
                                        $prop_data['five_innings_line'][1]['Unique_id'] = $unique_id;
                                        $prop_data['five_innings_line'][0]['betting_wager'] = $prop_data['five_innings_line'][1]['name'];
                                        $prop_data['five_innings_line'][0]['row_line'] = 'away';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['five_innings_line'][1])}}','prop_bet')">
                                        {{_preccedSign($prop_data['five_innings_line'][1]['odds'])}}
                                    </a>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    {{--mobile-view end--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767">5 Innings</h3>
                    <tbody class="rays">
                        <!-- table heading name -->

                        <tr>
                            <th>{{ $prop_data['five_innings_line'][0]['name'] }}</th>
                            <th>{{ $prop_data['five_innings_line'][1]['name'] }}</th>
                        </tr>

                        <!-- table single column & row list -->
                        <tr class="mobilecss wordltable">
                            <td data-th="{{ $prop_data['five_innings_line'][0]['name'] }}" class=" ">
                                @if (empty($prop_data['five_innings_line'][0]['odds']))
                                <div class="">
                                    <a href="#"> OTB </a>
                                </div>
                                @else
                                <div class="">
                                    @php
                                    $prop_data['five_innings_line'][0]['odds'] =
                                    _toAmericanDecimal($prop_data['five_innings_line'][0]['odds']);
                                    $prop_data['five_innings_line'][0]['team_name'] = "5 Innings";
                                    $prop_data['five_innings_line'][0]['sub_type_name'] =
                                    $prop_data['five_innings_line'][0]['name'];
                                    $unique_id = $event_data->id."five-innings-line-86569-01-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['five_innings_line'][0]['sub_type_name']);
                                    $prop_data['five_innings_line'][0]['Unique_id'] = $unique_id;
                                    $prop_data['five_innings_line'][0]['betting_wager'] = $prop_data['five_innings_line'][0]['name'];
                                    $prop_data['five_innings_line'][0]['row_line'] = 'home';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                        onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['five_innings_line'][0])}}','prop_bet')">
                                        {{_preccedSign($prop_data['five_innings_line'][0]['odds'])}}
                                    </a>
                                </div>
                                @endif

                            </td>
                            <td data-th="{{ $prop_data['five_innings_line'][1]['name'] }}" class="">
                                @if (empty($prop_data['five_innings_line'][1]['odds']))
                                <a href="#"> OTB </a>
                                @else
                                @php
                                $prop_data['five_innings_line'][1]['odds'] =
                                _toAmericanDecimal($prop_data['five_innings_line'][1]['odds']);
                                $prop_data['five_innings_line'][1]['team_name'] = "5 Innings";
                                $prop_data['five_innings_line'][1]['sub_type_name'] =
                                $prop_data['five_innings_line'][1]['name'];
                                $unique_id = $event_data->id."five-innings-line-86319-01-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['five_innings_line'][1]['sub_type_name']);
                                $prop_data['five_innings_line'][1]['Unique_id'] = $unique_id;
                                $prop_data['five_innings_line'][0]['betting_wager'] = $prop_data['five_innings_line'][1]['name'];
                                $prop_data['five_innings_line'][0]['row_line'] = 'away';
                                @endphp
                                <a id="{{$unique_id}}"
                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['five_innings_line'][1])}}','prop_bet')">
                                    {{_preccedSign($prop_data['five_innings_line'][1]['odds'])}}
                                </a>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
                @endif


                @if (!empty($prop_data['alternative_run_line']))
                <!-- Alternate Run Line -->
                    {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">Alternate Run Line</h3>
                        <thead>
                        <tr>
                            <th>{{ $home_name }}</th>
                            <th>{{ $away_name }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $alternativeRunLine = [];
                            foreach ($prop_data['alternative_run_line'] as $val) {
                            $alternativeRunLine[$val['header']][] = [
                            'id' => $val['id'],
                            'header' => $val['header'],
                            'odds' => $val['odds'],
                            'name' => $val['name'],
                            'handicap' => $val['handicap'],
                            ];
                            }
                            $rows = ($alternativeRunLine[$home_name] >
                            $alternativeRunLine[$away_name]) ?
                            $alternativeRunLine[$home_name] :
                            $alternativeRunLine[$away_name];
                        @endphp
                        @foreach ($rows as $k=> $bal)
                            <!-- table single column & row list -->
                            <tr class=" ">
                                <td data-th="{{$home_name}}" class=" ">
                                    @if (empty($alternativeRunLine[$home_name][$k]['odds']))
                                        <div class="">
                                            <a href="#"> OTB </a>
                                        </div>
                                    @else
                                        @php
                                            $alternativeRunLine[$home_name][$k]['odds'] =
                                            _toAmericanDecimal($alternativeRunLine[$home_name][$k]['odds']);
                                            $alternativeRunLine[$home_name][$k]['team_name'] = "Alternate Run Line (Tennis)";
                                            $alternativeRunLine[$home_name][$k]['sub_type_name'] = $home_name;
                                            $unique_id = $event_data->id."alternate-run-line-69852-".$k."-".str_replace(['.', '_', ' ', '--'], '-',$alternativeRunLine[$home_name][$k]['sub_type_name']);
                                            $alternativeRunLine[$home_name][$k]['Unique_id'] = $unique_id;
                                            $alternativeRunLine[$home_name][$k]['betting_wager'] = $home_name;
                                            $alternativeRunLine[$home_name][$k]['row_line'] = 'home';
                                            $alternativeRunLine[$home_name][$k]['handicap'] = $alternativeRunLine[$home_name][$k]['handicap'];
                                        @endphp
                                        <div class="">
                                            <a id="{{$unique_id}}"
                                               onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($alternativeRunLine[$home_name][$k])}}','prop_bet')">
                                                <?= " (" . $alternativeRunLine[$home_name][$k]['handicap'] . ") " . _preccedSign($alternativeRunLine[$home_name][$k]['odds']) ?>
                                            </a>
                                        </div>
                                    @endif
                                </td>
                                <td data-th="{{$away_name}}" class="">
                                    @if (empty($alternativeRunLine[$away_name][$k]['odds']))
                                        <a href="#"> OTB </a>
                                    @else
                                        @php
                                            $alternativeRunLine[$away_name][$k]['odds'] =
                                            _toAmericanDecimal($alternativeRunLine[$away_name][$k]['odds']);
                                            $alternativeRunLine[$away_name][$k]['team_name'] = "Alternate Run Line (Tennis)";
                                            $alternativeRunLine[$away_name][$k]['sub_type_name'] = $away_name;
                                            $unique_id = $event_data->id."alternate-run-line-49572-".$k."-".str_replace(['.', '_', ' ', '--'], '-',$alternativeRunLine[$away_name][$k]['sub_type_name']);
                                            $alternativeRunLine[$away_name][$k]['Unique_id'] = $unique_id;
                                            $alternativeRunLine[$away_name][$k]['betting_wager'] = $away_name;
                                            $alternativeRunLine[$away_name][$k]['row_line'] = 'away';
                                            $alternativeRunLine[$away_name][$k]['handicap'] = $alternativeRunLine[$away_name][$k]['handicap'];
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($alternativeRunLine[$away_name][$k])}}','prop_bet')">
                                            <?= " (" . $alternativeRunLine[$away_name][$k]['handicap'] . ") " . _preccedSign($alternativeRunLine[$away_name][$k]['odds']) ?></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{--mobile-view end--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767">Alternate Run Line</h3>
                    <tbody class="rays">
                        <!-- table heading name -->
                        <tr>
                            <th>{{ $home_name }}</th>
                            <th>{{ $away_name }}</th>
                        </tr>

                        @php
                        $alternativeRunLine = [];
                        foreach ($prop_data['alternative_run_line'] as $val) {
                        $alternativeRunLine[$val['header']][] = [
                        'id' => $val['id'],
                        'header' => $val['header'],
                        'odds' => $val['odds'],
                        'name' => $val['name'],
                        'handicap' => $val['handicap'],
                        ];
                        }
                        $rows = ($alternativeRunLine[$home_name] >
                        $alternativeRunLine[$away_name]) ?
                        $alternativeRunLine[$home_name] :
                        $alternativeRunLine[$away_name];
                        @endphp
                        @foreach ($rows as $k=> $bal)
                        <!-- table single column & row list -->
                        <tr class="mobilecss wordltable">
                            <td data-th="{{$home_name}}" class=" ">
                                @if (empty($alternativeRunLine[$home_name][$k]['odds']))
                                <div class="">
                                    <a href="#"> OTB </a>
                                </div>
                                @else
                                @php
                                $alternativeRunLine[$home_name][$k]['odds'] =
                                _toAmericanDecimal($alternativeRunLine[$home_name][$k]['odds']);
                                $alternativeRunLine[$home_name][$k]['team_name'] = "Alternate Run Line (Tennis)";
                                $alternativeRunLine[$home_name][$k]['sub_type_name'] = $home_name;
                                $unique_id = $event_data->id."alternate-run-line-69852-".$k."-".str_replace(['.', '_', ' ', '--'], '-',$alternativeRunLine[$home_name][$k]['sub_type_name']);
                                $alternativeRunLine[$home_name][$k]['Unique_id'] = $unique_id;
                                $alternativeRunLine[$home_name][$k]['betting_wager'] = $home_name;
                                $alternativeRunLine[$home_name][$k]['row_line'] = 'home';
                                $alternativeRunLine[$home_name][$k]['handicap'] = $alternativeRunLine[$home_name][$k]['handicap'];
                                @endphp
                                <div class="">
                                    <a id="{{$unique_id}}"
                                        onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($alternativeRunLine[$home_name][$k])}}','prop_bet')">
                                        <?= " (" . $alternativeRunLine[$home_name][$k]['handicap'] . ") " . _preccedSign($alternativeRunLine[$home_name][$k]['odds']) ?>
                                    </a>
                                </div>
                                @endif
                            </td>
                            <td data-th="{{$away_name}}" class="">
                                @if (empty($alternativeRunLine[$away_name][$k]['odds']))
                                <a href="#"> OTB </a>
                                @else
                                @php
                                $alternativeRunLine[$away_name][$k]['odds'] =
                                _toAmericanDecimal($alternativeRunLine[$away_name][$k]['odds']);
                                $alternativeRunLine[$away_name][$k]['team_name'] = "Alternate Run Line (Tennis)";
                                $alternativeRunLine[$away_name][$k]['sub_type_name'] = $away_name;
                                $unique_id = $event_data->id."alternate-run-line-49572-".$k."-".str_replace(['.', '_', ' ', '--'], '-',$alternativeRunLine[$away_name][$k]['sub_type_name']);
                                $alternativeRunLine[$away_name][$k]['Unique_id'] = $unique_id;
                                $alternativeRunLine[$away_name][$k]['betting_wager'] = $away_name;
                                $alternativeRunLine[$away_name][$k]['row_line'] = 'away';
                                $alternativeRunLine[$away_name][$k]['handicap'] = $alternativeRunLine[$away_name][$k]['handicap'];
                                @endphp
                                <a id="{{$unique_id}}"
                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($alternativeRunLine[$away_name][$k])}}','prop_bet')">
                                    <?= " (" . $alternativeRunLine[$away_name][$k]['handicap'] . ") " . _preccedSign($alternativeRunLine[$away_name][$k]['odds']) ?></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>

                @endif

                @if (!empty($prop_data['winning_margins']))
                <!-- Winning Margins -->
                    {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">Winning Margins</h3>
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>{{$home_name}}</th>
                            <th>{{$away_name}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $winningMargins = [];
                            foreach ($prop_data['winning_margins'] as $margin) {
                            $winningMargins[$margin['header']][] = [
                            'id' => $margin['id'],
                            'header' => $margin['header'],
                            'odds' => $margin['odds'],
                            'name' => $margin['name'],
                            ];
                            }
                            $winningRows = ($winningMargins[$home_name] >
                            $winningMargins[$away_name]) ?
                            $winningMargins[$home_name] : $winningMargins[$away_name];
                        @endphp

                        @foreach ($winningRows as $k => $winningVal)
                            <!-- table single column & row list -->
                            <tr class=" ">
                                <td data-th="Name" class=" ">
                                    <div class="">
                                        <a > {{$winningMargins[$home_name][$k]['name']}}
                                        </a>
                                    </div>
                                </td>
                                <td data-th="{{$home_name}}" class=" ">
                                    @if (empty($winningMargins[$home_name][$k]['odds']))
                                        <div class="">
                                            <a href="#"> OTB </a>
                                        </div>
                                    @else
                                        @php
                                            $winningMargins[$home_name][$k]['odds'] =
                                            _toAmericanDecimal($winningMargins[$home_name][$k]['odds']);
                                            $winningMargins[$home_name][$k]['team_name'] = "Winning Margins (".$winningMargins[$home_name][$k]['name'].")";
                                            $winningMargins[$home_name][$k]['sub_type_name'] = $home_name;
                                            $unique_id = $event_data->id."winning-margins-59742-".$k."-".str_replace(['.', '_', ' ', '--'], '-',$winningMargins[$home_name][$k]['sub_type_name']);
                                            $winningMargins[$home_name][$k]['Unique_id'] = $unique_id;
                                            $winningMargins[$home_name][$k]['betting_wager'] = $home_name;
                                            $winningMargins[$home_name][$k]['row_line'] = 'home';
                                        @endphp
                                        <div class="">
                                            <a id="{{$unique_id}}"
                                               onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($winningMargins[$home_name][$k])}}','prop_bet')">
                                                {{_preccedSign($winningMargins[$home_name][$k]['odds'])}}
                                            </a>

                                        </div>
                                    @endif
                                </td>
                                <td data-th="{{$away_name}}" class="">
                                    @if (empty($winningMargins[$away_name][$k]['odds']))
                                        <a href="#"> OTB </a>
                                    @else
                                        @php
                                            $winningMargins[$away_name][$k]['odds'] =
                                            _toAmericanDecimal($winningMargins[$away_name][$k]['odds']);
                                            $winningMargins[$away_name][$k]['team_name'] = "Winning Margins (".$winningMargins[$away_name][$k]['name'].")";
                                            $winningMargins[$away_name][$k]['sub_type_name'] = $away_name;
                                            $unique_id = $event_data->id."winning-margins-61432-".$k."-".str_replace(['.', '_', ' ', '--'], '-',$winningMargins[$away_name][$k]['sub_type_name']);
                                            $winningMargins[$away_name][$k]['Unique_id'] = $unique_id;
                                            $winningMargins[$away_name][$k]['betting_wager'] = $away_name;
                                            $winningMargins[$away_name][$k]['row_line'] = 'away';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($winningMargins[$away_name][$k])}}','prop_bet')">
                                            {{_preccedSign($winningMargins[$away_name][$k]['odds'])}}</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{--mobile-view end--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767">Winning Margins</h3>
                    <tbody class="rays">
                        <!-- table heading name -->
                        <tr>
                            <th>Name</th>
                            <th>{{$home_name}}</th>
                            <th>{{$away_name}}</th>
                        </tr>

                        @php
                        $winningMargins = [];
                        foreach ($prop_data['winning_margins'] as $margin) {
                        $winningMargins[$margin['header']][] = [
                        'id' => $margin['id'],
                        'header' => $margin['header'],
                        'odds' => $margin['odds'],
                        'name' => $margin['name'],
                        ];
                        }
                        $winningRows = ($winningMargins[$home_name] >
                        $winningMargins[$away_name]) ?
                        $winningMargins[$home_name] : $winningMargins[$away_name];
                        @endphp

                        @foreach ($winningRows as $k => $winningVal)
                        <!-- table single column & row list -->
                        <tr class="mobilecss wordltable">
                            <td data-th="Name" class=" ">
                                <div class="">
                                    <a > {{$winningMargins[$home_name][$k]['name']}}
                                    </a>
                                </div>
                            </td>
                            <td data-th="{{$home_name}}" class=" ">
                                @if (empty($winningMargins[$home_name][$k]['odds']))
                                <div class="">
                                    <a href="#"> OTB </a>
                                </div>
                                @else
                                @php
                                $winningMargins[$home_name][$k]['odds'] =
                                _toAmericanDecimal($winningMargins[$home_name][$k]['odds']);
                                $winningMargins[$home_name][$k]['team_name'] = "Winning Margins (".$winningMargins[$home_name][$k]['name'].")";
                                $winningMargins[$home_name][$k]['sub_type_name'] = $home_name;
                                $unique_id = $event_data->id."winning-margins-59742-".$k."-".str_replace(['.', '_', ' ', '--'], '-',$winningMargins[$home_name][$k]['sub_type_name']);
                                $winningMargins[$home_name][$k]['Unique_id'] = $unique_id;
                                $winningMargins[$home_name][$k]['betting_wager'] = $home_name;
                                $winningMargins[$home_name][$k]['row_line'] = 'home';
                                @endphp
                                <div class="">
                                    <a id="{{$unique_id}}"
                                        onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($winningMargins[$home_name][$k])}}','prop_bet')">
                                        {{_preccedSign($winningMargins[$home_name][$k]['odds'])}}
                                    </a>

                                </div>
                                @endif
                            </td>
                            <td data-th="{{$away_name}}" class="">
                                @if (empty($winningMargins[$away_name][$k]['odds']))
                                <a href="#"> OTB </a>
                                @else
                                @php
                                $winningMargins[$away_name][$k]['odds'] =
                                _toAmericanDecimal($winningMargins[$away_name][$k]['odds']);
                                $winningMargins[$away_name][$k]['team_name'] = "Winning Margins (".$winningMargins[$away_name][$k]['name'].")";
                                $winningMargins[$away_name][$k]['sub_type_name'] = $away_name;
                                $unique_id = $event_data->id."winning-margins-61432-".$k."-".str_replace(['.', '_', ' ', '--'], '-',$winningMargins[$away_name][$k]['sub_type_name']);
                                $winningMargins[$away_name][$k]['Unique_id'] = $unique_id;
                                $winningMargins[$away_name][$k]['betting_wager'] = $away_name;
                                $winningMargins[$away_name][$k]['row_line'] = 'away';
                                @endphp
                                <a id="{{$unique_id}}"
                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($winningMargins[$away_name][$k])}}','prop_bet')">
                                    {{_preccedSign($winningMargins[$away_name][$k]['odds'])}}</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif

                @elseif($prop_bet_details['prop_data']->sport_id == 1)

                @if (!empty($prop_data['half_time_full_time']))
                <!-- Half Time Results -->
                    {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">Half Time Results</h3>
                        <thead>
                        @php
                            $half_time_results_0 = isset($prop_data['half_time_full_time'][0]['opp'])?$prop_data['half_time_full_time'][0]['opp']:"";
                            $half_time_results_1 = isset($prop_data['half_time_full_time'][1]['opp'])?$prop_data['half_time_full_time'][1]['opp']:"";
                            $half_time_results_2 = isset($prop_data['half_time_full_time'][2]['opp'])?$prop_data['half_time_full_time'][2]['opp']:"";
                        @endphp
                        <tr>
                            <th>{{ $home_name . " (" . $half_time_results_0 . ")"}}
                            </th>
                            <th>Draw {{ "(". $half_time_results_1 . ")" }}</th>
                            <th><?php echo $away_name . " (" .  $half_time_results_2 . ")" ?>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="text-center">
                            <td>
                                @if (empty($prop_data['half_time_full_time'][0]['odds']))
                                    <div class="">
                                        <a href="#"> OTB </a>
                                    </div>
                                @else
                                    <div class="">
                                        @php
                                            $prop_data['half_time_full_time'][0]['odds'] =
                                            _toAmericanDecimal($prop_data['half_time_full_time'][0]['odds']);
                                            $prop_data['half_time_full_time'][0]['team_name'] = "Half Time Results";
                                            $prop_data['half_time_full_time'][0]['sub_type_name'] =
                                            $home_name.'('.$half_time_results_0.')';
                                            $unique_id = $event_data->id."half-time-results-15487-01-".$half_time_results_0;
                                            $prop_data['half_time_full_time'][0]['Unique_id'] = $unique_id;
                                            $prop_data['half_time_full_time'][0]['betting_wager'] = $home_name . " (" . $half_time_results_0 . ")";
                                            $prop_data['half_time_full_time'][0]['row_line'] = 'home';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['half_time_full_time'][0])}}','prop_bet')">
                                            {{_preccedSign($prop_data['half_time_full_time'][0]['odds'])}}
                                        </a>
                                    </div>
                                @endif
                            </td>
                            <td>
                                @if (empty($prop_data['half_time_full_time'][1]['odds']))
                                    <a href="#"> OTB </a>
                                @else
                                    @php
                                        $prop_data['half_time_full_time'][1]['odds'] =
                                        _toAmericanDecimal($prop_data['half_time_full_time'][1]['odds']);
                                        $prop_data['half_time_full_time'][1]['team_name'] = "Half Time Results";
                                        $prop_data['half_time_full_time'][1]['sub_type_name'] =
                                        'Draw ('.$half_time_results_1.')';
                                        $unique_id =
                                        $event_data->id."half-time-results-96875-02-".$half_time_results_1;
                                        $prop_data['half_time_full_time'][1]['Unique_id'] = $unique_id;
                                        $prop_data['half_time_full_time'][1]['betting_wager'] = "Draw (". $half_time_results_1 . ")";
                                        $prop_data['half_time_full_time'][1]['row_line'] = 'draw';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['half_time_full_time'][1]) }}','prop_bet')">
                                        {{_preccedSign($prop_data['half_time_full_time'][1]['odds'])}}
                                    </a>
                                @endif
                            </td>
                            <td>
                                @if (empty($prop_data['half_time_full_time'][2]['odds']))

                                    <a href="#"> OTB </a>

                                @else
                                    @php
                                        $prop_data['half_time_full_time'][2]['team_name'] = "Half Time Results";
                                        $prop_data['half_time_full_time'][2]['sub_type_name'] =
                                        $away_name.'('.$half_time_results_2.')';
                                        $prop_data['half_time_full_time'][2]['odds'] =
                                        _toAmericanDecimal($prop_data['half_time_full_time'][2]['odds']);
                                        $unique_id = $event_data->id."half-time-results-55886-03-".$half_time_results_2;
                                        $prop_data['half_time_full_time'][2]['Unique_id'] = $unique_id;
                                        $prop_data['half_time_full_time'][2]['betting_wager'] = $away_name . " (" . $half_time_results_2 . ")";
                                        $prop_data['half_time_full_time'][2]['row_line'] = 'away';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode( $event_data ) }},'{{ json_encode( $prop_data['half_time_full_time'][2]) }}','prop_bet')">
                                        {{ _preccedSign($prop_data['half_time_full_time'][2]['odds'])}}
                                    </a>
                                @endif

                            </td>
                        </tr>
                        </tbody>
                    </table>
                    {{--mobile-view end--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767"> Half Time Results</h3>
                    <tbody class="rays">
                        <!-- table heading name -->
                        @php
                            $half_time_results_0 = isset($prop_data['half_time_full_time'][0]['opp'])?$prop_data['half_time_full_time'][0]['opp']:"";
                            $half_time_results_1 = isset($prop_data['half_time_full_time'][1]['opp'])?$prop_data['half_time_full_time'][1]['opp']:"";
                            $half_time_results_2 = isset($prop_data['half_time_full_time'][2]['opp'])?$prop_data['half_time_full_time'][2]['opp']:"";
                        @endphp
                        <tr>
                            <th>{{ $home_name . " (" . $half_time_results_0 . ")"}}
                            </th>
                            <th>Draw {{ "(". $half_time_results_1 . ")" }}</th>
                            <th><?php echo $away_name . " (" .  $half_time_results_2 . ")" ?>
                            </th>
                        </tr>
                        <!-- table single column & row list -->
                        <tr class="mobilecss wordltable">

                            <td data-th="Hubert Hurkacz" class=" ">
                                @if (empty($prop_data['half_time_full_time'][0]['odds']))
                                <div class="">
                                    <a href="#"> OTB </a>
                                </div>
                                @else
                                <div class="">
                                    @php
                                    $prop_data['half_time_full_time'][0]['odds'] =
                                    _toAmericanDecimal($prop_data['half_time_full_time'][0]['odds']);
                                    $prop_data['half_time_full_time'][0]['team_name'] = "Half Time Results";
                                    $prop_data['half_time_full_time'][0]['sub_type_name'] =
                                    $home_name.'('.$half_time_results_0.')';
                                    $unique_id = $event_data->id."half-time-results-15487-01-".$half_time_results_0;
                                    $prop_data['half_time_full_time'][0]['Unique_id'] = $unique_id;
                                    $prop_data['half_time_full_time'][0]['betting_wager'] = $home_name . " (" . $half_time_results_0 . ")";
                                    $prop_data['half_time_full_time'][0]['row_line'] = 'home';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                        onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['half_time_full_time'][0])}}','prop_bet')">
                                        {{_preccedSign($prop_data['half_time_full_time'][0]['odds'])}}
                                    </a>
                                </div>
                                @endif
                            </td>
                            <td data-th="Stefanos Tsitsipas" class="">
                                @if (empty($prop_data['half_time_full_time'][1]['odds']))
                                <a href="#"> OTB </a>
                                @else
                                @php
                                $prop_data['half_time_full_time'][1]['odds'] =
                                _toAmericanDecimal($prop_data['half_time_full_time'][1]['odds']);
                                $prop_data['half_time_full_time'][1]['team_name'] = "Half Time Results";
                                $prop_data['half_time_full_time'][1]['sub_type_name'] =
                                'Draw ('.$half_time_results_1.')';
                                $unique_id =
                                $event_data->id."half-time-results-96875-02-".$half_time_results_1;
                                $prop_data['half_time_full_time'][1]['Unique_id'] = $unique_id;
                                $prop_data['half_time_full_time'][1]['betting_wager'] = "Draw (". $half_time_results_1 . ")";
                                $prop_data['half_time_full_time'][1]['row_line'] = 'draw';
                                @endphp
                                <a id="{{$unique_id}}"
                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['half_time_full_time'][1]) }}','prop_bet')">
                                    {{_preccedSign($prop_data['half_time_full_time'][1]['odds'])}}
                                </a>
                                @endif
                            </td>
                            <td data-th="Stefanos Tsitsipas" class="">
                                @if (empty($prop_data['half_time_full_time'][2]['odds']))

                                <a href="#"> OTB </a>

                                @else
                                @php
                                $prop_data['half_time_full_time'][2]['team_name'] = "Half Time Results";
                                $prop_data['half_time_full_time'][2]['sub_type_name'] =
                                $away_name.'('.$half_time_results_2.')';
                                $prop_data['half_time_full_time'][2]['odds'] =
                                _toAmericanDecimal($prop_data['half_time_full_time'][2]['odds']);
                                $unique_id = $event_data->id."half-time-results-55886-03-".$half_time_results_2;
                                $prop_data['half_time_full_time'][2]['Unique_id'] = $unique_id;
                                $prop_data['half_time_full_time'][2]['betting_wager'] = $away_name . " (" . $half_time_results_2 . ")";
                                $prop_data['half_time_full_time'][2]['row_line'] = 'away';
                                @endphp
                                <a id="{{$unique_id}}"
                                    onclick="Betslip.addToCart({{ json_encode( $event_data ) }},'{{ json_encode( $prop_data['half_time_full_time'][2]) }}','prop_bet')">
                                    {{ _preccedSign($prop_data['half_time_full_time'][2]['odds'])}}
                                </a>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
                @endif



                @if (!empty($prop_data['both_teams_to_score']))
                <!--Both Teams To Score -->
                    {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">Both Teams To Score</h3>
                        <thead>
                        <tr>
                            <th>Yes</th>
                            <th>No</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="text-center">
                            @for ($i = 0; $i < count($prop_data['both_teams_to_score']); $i++)
                                <td data-th="Yes" class=" ">
                                    @if (empty($prop_data['both_teams_to_score'][$i]['odds']))
                                        <div class="">
                                            <a href="#"> OTB </a>
                                        </div>
                                    @else
                                        <div class="">
                                            @php
                                                $prop_data['both_teams_to_score'][$i]['odds'] =
                                                _toAmericanDecimal($prop_data['both_teams_to_score'][$i]['odds']);
                                                $prop_data['both_teams_to_score'][$i]['team_name'] = "Both Teams To Score";

                                                $prop_data['both_teams_to_score'][$i]['sub_type_name'] = ($i%2==0) ? "Yes" : "No";
                                                $unique_id = $event_data->id."both-teams-to-score-789654-".$i."-".$prop_data['both_teams_to_score'][$i]['sub_type_name'];
                                                $prop_data['both_teams_to_score'][$i]['Unique_id'] = $unique_id;
                                                $prop_data['both_teams_to_score'][$i]['betting_wager'] = ($i%2==0) ? "Yes" : "No";
                                                $prop_data['both_teams_to_score'][$i]['row_line'] = ($i%2==0) ?'home' : 'away';
                                            @endphp
                                            <a id="{{$unique_id}}"
                                               onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['both_teams_to_score'][$i])}}','prop_bet')">
                                                {{_preccedSign($prop_data['both_teams_to_score'][$i]['odds'])}}
                                            </a>
                                        </div>
                                    @endif
                                </td>
                                @endfor
                        </tr>
                        </tbody>
                    </table>
                    {{--mobile-view end--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767"> Both Teams To Score </h3>
                    <tbody class="rays">
                        <!-- table heading name -->
                        <tr>
                            <th>Yes</th>
                            <th>No</th>
                        </tr>
                        <!-- table single column & row list -->
                        <tr class="mobilecss wordltable">
                            @for ($i = 0; $i < count($prop_data['both_teams_to_score']); $i++)
                                <td data-th="Yes" class=" ">
                                @if (empty($prop_data['both_teams_to_score'][$i]['odds']))
                                <div class="">
                                    <a href="#"> OTB </a>
                                </div>
                                @else
                                <div class="">
                                    @php
                                    $prop_data['both_teams_to_score'][$i]['odds'] =
                                    _toAmericanDecimal($prop_data['both_teams_to_score'][$i]['odds']);
                                    $prop_data['both_teams_to_score'][$i]['team_name'] = "Both Teams To Score";

                                    $prop_data['both_teams_to_score'][$i]['sub_type_name'] = ($i%2==0) ? "Yes" : "No";
                                    $unique_id = $event_data->id."both-teams-to-score-789654-".$i."-".$prop_data['both_teams_to_score'][$i]['sub_type_name'];
                                    $prop_data['both_teams_to_score'][$i]['Unique_id'] = $unique_id;
                                    $prop_data['both_teams_to_score'][$i]['betting_wager'] = ($i%2==0) ? "Yes" : "No";
                                    $prop_data['both_teams_to_score'][$i]['row_line'] = ($i%2==0) ?'home' : 'away';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                        onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['both_teams_to_score'][$i])}}','prop_bet')">
                                        {{_preccedSign($prop_data['both_teams_to_score'][$i]['odds'])}}
                                    </a>
                                </div>
                                @endif
                                </td>
                            @endfor
                        </tr>
                    </tbody>
                </table>
                @endif


                @if (!empty($prop_data['winning_margin']))
                <!-- Winning Margin -->
                    {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">Winning Margin</h3>
                        <thead>
                        <tr>
                            <th></th>
                            <th>{{ $home_name}}
                                (Home)</th>
                            <th>{{ $away_name}}
                                (Away)</th>
                        </tr>
                        </thead>
                        <tbody>
                        @for ($i = 0; $i < count($prop_data['winning_margin']) / 2; $i +=2) <tr
                                class="mobilecss wordltable">
                            <td data-th="" class="">
                                <a> {{ $prop_data['winning_margin'][$i]['goals'] }} </a>
                            </td>
                            @if ($prop_data['winning_margin'][$i]['header'] == 'Home')
                                <td data-th="{{ $home_name}}
                                        (Home)" class=" ">
                                    @if (empty($prop_data['winning_margin'][$i]['odds']))
                                        <div class="">
                                            <a> OTB </a>
                                        </div>
                                    @else
                                        <div class="">
                                            @php
                                                $prop_data['winning_margin'][$i]['odds'] =
                                                _toAmericanDecimal($prop_data['winning_margin'][$i]['odds']);
                                                $prop_data['winning_margin'][$i]['team_name'] = "Winning Margin";
                                                $prop_data['winning_margin'][$i]['sub_type_name'] = $home_name."(Home)";
                                                $unique_id = $event_data->id."-winning-margin-09090-".$i."-".str_replace(['.', '_', ' ', '--','(',')'], '-',$prop_data['winning_margin'][$i]['sub_type_name']);
                                                $prop_data['winning_margin'][$i]['Unique_id'] = $unique_id;
                                                $prop_data['winning_margin'][$i]['betting_wager'] = $home_name."(Home)";
                                                $prop_data['winning_margin'][$i]['row_line'] = 'home';
                                            @endphp
                                            <a id="{{$unique_id}}"
                                               onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['winning_margin'][$i])}}','prop_bet')">
                                                {{_preccedSign($prop_data['winning_margin'][$i]['odds'])}}
                                            </a>
                                        </div>
                                    @endif
                                </td>
                            @endif
                            @if ($prop_data['winning_margin'][$i + 1]['header'] == 'Away')
                                <td data-th="{{ $away_name}}
                                        (Away)" class="">
                                    @if (empty($prop_data['winning_margin'][$i + 1]['odds']))
                                        <a> OTB </a>
                                    @else
                                        @php
                                            $prop_data['winning_margin'][$i + 1]['odds'] =
                                            _toAmericanDecimal($prop_data['winning_margin'][$i + 1]['odds']);
                                            $prop_data['winning_margin'][$i + 1]['team_name'] = "Winning Margin";
                                            $prop_data['winning_margin'][$i + 1]['sub_type_name'] = $away_name."(Away)";
                                            $unique_id = $event_data->id."winning-margin-09590-".($i+1)."-".str_replace(['.', '_', ' ', '--','(',')'], '-',$prop_data['winning_margin'][$i +
                                            1]['sub_type_name']);
                                            $prop_data['winning_margin'][$i + 1]['Unique_id'] = $unique_id;
                                            $prop_data['winning_margin'][$i + 1]['betting_wager'] = $away_name."(Away)";
                                            $prop_data['winning_margin'][$i + 1]['row_line'] = 'home';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['winning_margin'][$i + 1])}}','prop_bet')">
                                            {{_preccedSign($prop_data['winning_margin'][$i + 1]['odds'])}} </a>
                                    @endif
                                </td>
                            @endif
                        </tr>
                        @endfor
                        </tbody>
                    </table>
                    {{--mobile-view end--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767"> Winning Margin</h3>
                    <tbody class="rays">
                        <!-- table heading name -->
                        <tr>
                            <th></th>
                            <th>{{ $home_name}}
                                (Home)</th>
                            <th>{{ $away_name}}
                                (Away)</th>
                        </tr>

                        @for ($i = 0; $i < count($prop_data['winning_margin']) / 2; $i +=2) <tr
                            class="mobilecss wordltable">
                            <td data-th="" class="">
                                <a> {{ $prop_data['winning_margin'][$i]['goals'] }} </a>
                            </td>
                            @if ($prop_data['winning_margin'][$i]['header'] == 'Home')
                            <td data-th="{{ $home_name}}
                                (Home)" class=" ">
                                @if (empty($prop_data['winning_margin'][$i]['odds']))
                                <div class="">
                                    <a> OTB </a>
                                </div>
                                @else
                                <div class="">
                                    @php
                                    $prop_data['winning_margin'][$i]['odds'] =
                                    _toAmericanDecimal($prop_data['winning_margin'][$i]['odds']);
                                    $prop_data['winning_margin'][$i]['team_name'] = "Winning Margin";
                                    $prop_data['winning_margin'][$i]['sub_type_name'] = $home_name."(Home)";
                                    $unique_id = $event_data->id."-winning-margin-09090-".$i."-".str_replace(['.', '_', ' ', '--','(',')'], '-',$prop_data['winning_margin'][$i]['sub_type_name']);
                                    $prop_data['winning_margin'][$i]['Unique_id'] = $unique_id;
                                    $prop_data['winning_margin'][$i]['betting_wager'] = $home_name."(Home)";
                                    $prop_data['winning_margin'][$i]['row_line'] = 'home';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                        onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['winning_margin'][$i])}}','prop_bet')">
                                        {{_preccedSign($prop_data['winning_margin'][$i]['odds'])}}
                                    </a>
                                </div>
                                @endif
                            </td>
                            @endif
                            @if ($prop_data['winning_margin'][$i + 1]['header'] == 'Away')
                            <td data-th="{{ $away_name}}
                                (Away)" class="">
                                @if (empty($prop_data['winning_margin'][$i + 1]['odds']))
                                <a> OTB </a>
                                @else
                                @php
                                $prop_data['winning_margin'][$i + 1]['odds'] =
                                _toAmericanDecimal($prop_data['winning_margin'][$i + 1]['odds']);
                                $prop_data['winning_margin'][$i + 1]['team_name'] = "Winning Margin";
                                $prop_data['winning_margin'][$i + 1]['sub_type_name'] = $away_name."(Away)";
                                $unique_id = $event_data->id."winning-margin-09590-".($i+1)."-".str_replace(['.', '_', ' ', '--','(',')'], '-',$prop_data['winning_margin'][$i +
                                1]['sub_type_name']);
                                $prop_data['winning_margin'][$i + 1]['Unique_id'] = $unique_id;
                                $prop_data['winning_margin'][$i + 1]['betting_wager'] = $away_name."(Away)";
                                $prop_data['winning_margin'][$i + 1]['row_line'] = 'home';
                                @endphp
                                <a id="{{$unique_id}}"
                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['winning_margin'][$i + 1])}}','prop_bet')">
                                    {{_preccedSign($prop_data['winning_margin'][$i + 1]['odds'])}} </a>
                                @endif
                            </td>
                            @endif
                            </tr>
                            @endfor
                    </tbody>
                </table>
                @endif



                @if (!empty($prop_data['alternative_handicap_result']))
                <!-- Alternative Handicap Result -->
                    {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">Alternative Handicap Result</h3>
                        <thead>
                        <tr>
                            <th></th>
                            <th>{{ $home_name}}
                                (Home)</th>
                            <th>{{ $away_name}}
                                (Away)</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $x = -1;
                        @endphp
                        @for($i=0; $i < count($prop_data['alternative_handicap_result']) / 3; $i ++)
                            <tr class="mobilecss wordltable">


                                <td data-th="{{$home_name}}
                                        (Home)" class=" ">
                                    @if (empty($prop_data['alternative_handicap_result'][$x+1]['odds']))
                                        <div class="">
                                            <a> OTB </a>
                                        </div>
                                    @else
                                        <div class="">
                                            @php
                                                $prop_data['alternative_handicap_result'][$x+1]['odds'] =
                                                _toAmericanDecimal($prop_data['alternative_handicap_result'][$x+1]['odds']);
                                                $prop_data['alternative_handicap_result'][$x+1]['team_name'] = "Alternative Handicap Result";
                                                $prop_data['alternative_handicap_result'][$x+1]['sub_type_name'] = $home_name.'(Home)';
                                                $unique_id = $event_data->id."-alternative-handicap-result-04508-".($x+1)."-".str_replace(['.', '_', ' ', '--','(',')'], '-',$prop_data['alternative_handicap_result'][$x+1]['sub_type_name']);
                                                $prop_data['alternative_handicap_result'][$x+1]['Unique_id'] = $unique_id;
                                                $prop_data['alternative_handicap_result'][$i+1]['betting_wager'] = $home_name.'(Home)';
                                                $prop_data['alternative_handicap_result'][$i+1]['row_line'] = 'home';
                                            @endphp
                                            <a id="{{$unique_id}}"
                                               onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['alternative_handicap_result'][$x+1])}}','prop_bet')">
                                                {{_preccedSign($prop_data['alternative_handicap_result'][$x + 1]['odds'])}}
                                            </a>
                                        </div>
                                    @endif
                                </td>

                                <td data-th="Draw" class="">
                                    @if(empty($prop_data['alternative_handicap_result'][$x + 2]['odds']))
                                        <a href="">OTB </a>
                                    @else
                                        @php
                                            $prop_data['alternative_handicap_result'][$x+2]['odds'] =
                                            _toAmericanDecimal($prop_data['alternative_handicap_result'][$x+2]['odds']);
                                            $prop_data['alternative_handicap_result'][$x+2]['team_name'] = "Alternative Handicap Result";
                                            $prop_data['alternative_handicap_result'][$x+2]['sub_type_name'] ='Draw';
                                            $unique_id = $event_data->id."alternative-handicap-result-06509-".($x+2)."-".str_replace(['.', '_', ' ', '--','(',')'], '-',$prop_data['alternative_handicap_result'][$x+2]['sub_type_name']);
                                            $prop_data['alternative_handicap_result'][$x+2]['Unique_id'] = $unique_id;
                                            $prop_data['alternative_handicap_result'][$i+2]['betting_wager'] = 'Draw';
                                            $prop_data['alternative_handicap_result'][$i+2]['row_line'] = 'draw';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['alternative_handicap_result'][$x+2])}}','prop_bet')">
                                            {{_preccedSign($prop_data['alternative_handicap_result'][$x + 2]['odds'])}} </a>
                                    @endif
                                </td>
                                <td data-th="{{ $away_name}}
                                        (Away)" class="">
                                    @if(empty($prop_data['alternative_handicap_result'][$x + 3]['odds']))
                                        <a>OTB </a>
                                    @else
                                        @php
                                            $prop_data['alternative_handicap_result'][$x+3]['odds'] =
                                            _toAmericanDecimal($prop_data['alternative_handicap_result'][$x+3]['odds']);
                                            $prop_data['alternative_handicap_result'][$x+3]['team_name'] = "Alternative Handicap Result";
                                            $prop_data['alternative_handicap_result'][$x+3]['sub_type_name'] =$away_name.'(Away)';
                                            $unique_id = $event_data->id."-alternative-handicap-result-09609-".($x+3)."-".str_replace(['.', '_', ' ', '--','(',')'], '-',$prop_data['alternative_handicap_result'][$x+3]['sub_type_name']);
                                            $prop_data['alternative_handicap_result'][$x+3]['Unique_id'] = $unique_id;
                                            $prop_data['alternative_handicap_result'][$i+3]['betting_wager'] = $away_name.'(Away)';
                                            $prop_data['alternative_handicap_result'][$i+3]['row_line'] = 'away';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['alternative_handicap_result'][$x+3])}}','prop_bet')">
                                            {{_preccedSign($prop_data['alternative_handicap_result'][$x + 3]['odds'])}} </a>
                                    @endif

                                </td>

                            </tr>
                            @php
                                $x = ($x + 3);
                            @endphp
                        @endfor
                        </tbody>
                    </table>
                    {{--mobile-view end--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767"> Alternative Handicap Result </h3>
                    <tbody class="rays">
                        <!-- table heading name -->
                        <tr>
                            <th>{{ $home_name}}
                                (Home)</th>
                            <th>Draw</th>
                            <th>{{ $away_name}}
                                (Away)</th>
                        </tr>
                        @php
                        $x = -1;
                        @endphp
                        @for($i=0; $i < count($prop_data['alternative_handicap_result']) / 3; $i ++)
                            <tr class="mobilecss wordltable">


                            <td data-th="{{$home_name}}
                                        (Home)" class=" ">
                                @if (empty($prop_data['alternative_handicap_result'][$x+1]['odds']))
                                <div class="">
                                    <a> OTB </a>
                                </div>
                                @else
                                <div class="">
                                    @php
                                    $prop_data['alternative_handicap_result'][$x+1]['odds'] =
                                    _toAmericanDecimal($prop_data['alternative_handicap_result'][$x+1]['odds']);
                                    $prop_data['alternative_handicap_result'][$x+1]['team_name'] = "Alternative Handicap Result";
                                    $prop_data['alternative_handicap_result'][$x+1]['sub_type_name'] = $home_name.'(Home)';
                                    $unique_id = $event_data->id."-alternative-handicap-result-04508-".($x+1)."-".str_replace(['.', '_', ' ', '--','(',')'], '-',$prop_data['alternative_handicap_result'][$x+1]['sub_type_name']);
                                    $prop_data['alternative_handicap_result'][$x+1]['Unique_id'] = $unique_id;
                                    $prop_data['alternative_handicap_result'][$i+1]['betting_wager'] = $home_name.'(Home)';
                                    $prop_data['alternative_handicap_result'][$i+1]['row_line'] = 'home';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                        onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['alternative_handicap_result'][$x+1])}}','prop_bet')">
                                        {{_preccedSign($prop_data['alternative_handicap_result'][$x + 1]['odds'])}}
                                    </a>
                                </div>
                                @endif
                            </td>

                            <td data-th="Draw" class="">
                                @if(empty($prop_data['alternative_handicap_result'][$x + 2]['odds']))
                                <a href="">OTB </a>
                                @else
                                @php
                                $prop_data['alternative_handicap_result'][$x+2]['odds'] =
                                _toAmericanDecimal($prop_data['alternative_handicap_result'][$x+2]['odds']);
                                $prop_data['alternative_handicap_result'][$x+2]['team_name'] = "Alternative Handicap Result";
                                $prop_data['alternative_handicap_result'][$x+2]['sub_type_name'] ='Draw';
                                $unique_id = $event_data->id."alternative-handicap-result-06509-".($x+2)."-".str_replace(['.', '_', ' ', '--','(',')'], '-',$prop_data['alternative_handicap_result'][$x+2]['sub_type_name']);
                                $prop_data['alternative_handicap_result'][$x+2]['Unique_id'] = $unique_id;
                                $prop_data['alternative_handicap_result'][$i+2]['betting_wager'] = 'Draw';
                                $prop_data['alternative_handicap_result'][$i+2]['row_line'] = 'draw';
                                @endphp
                                <a id="{{$unique_id}}"
                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['alternative_handicap_result'][$x+2])}}','prop_bet')">
                                    {{_preccedSign($prop_data['alternative_handicap_result'][$x + 2]['odds'])}} </a>
                                @endif
                            </td>
                            <td data-th="{{ $away_name}}
                                        (Away)" class="">
                                @if(empty($prop_data['alternative_handicap_result'][$x + 3]['odds']))
                                <a>OTB </a>
                                @else
                                @php
                                $prop_data['alternative_handicap_result'][$x+3]['odds'] =
                                _toAmericanDecimal($prop_data['alternative_handicap_result'][$x+3]['odds']);
                                $prop_data['alternative_handicap_result'][$x+3]['team_name'] = "Alternative Handicap Result";
                                $prop_data['alternative_handicap_result'][$x+3]['sub_type_name'] =$away_name.'(Away)';
                                $unique_id = $event_data->id."-alternative-handicap-result-09609-".($x+3)."-".str_replace(['.', '_', ' ', '--','(',')'], '-',$prop_data['alternative_handicap_result'][$x+3]['sub_type_name']);
                                $prop_data['alternative_handicap_result'][$x+3]['Unique_id'] = $unique_id;
                                $prop_data['alternative_handicap_result'][$i+3]['betting_wager'] = $away_name.'(Away)';
                                $prop_data['alternative_handicap_result'][$i+3]['row_line'] = 'away';
                                @endphp
                                <a id="{{$unique_id}}"
                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['alternative_handicap_result'][$x+3])}}','prop_bet')">
                                    {{_preccedSign($prop_data['alternative_handicap_result'][$x + 3]['odds'])}} </a>
                                @endif

                            </td>

                            </tr>
                            @php
                            $x = ($x + 3);
                            @endphp
                            @endfor
                    </tbody>
                </table>
                @endif

                @if (!empty($prop_data['goals_odd_even']))
                <!-- Goals Odd Even -->
                    {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">Goals Odd Even </h3>
                        <thead>
                        <tr>
                            <th>Even</th>
                            <th>Odd</th>
                        </tr>
                        </thead>
                        <tbody>
                        @for($i=0; $i < count($prop_data['goals_odd_even']) / 2; $i +=2) <tr
                                class="mobilecss wordltable">
                            <td data-th="Even" class=" ">
                                @if (empty($prop_data['goals_odd_even'][$i]['odds']))
                                    <div class="">
                                        <a href="#"> OTB </a>
                                    </div>
                                @else
                                    <div class="">
                                        @php
                                            $prop_data['goals_odd_even'][$i]['odds'] =
                                            _toAmericanDecimal($prop_data['goals_odd_even'][$i]['odds']);
                                            $prop_data['goals_odd_even'][$i]['team_name'] = "Goals Odd Even";
                                            $prop_data['goals_odd_even'][$i]['sub_type_name'] = 'Even';
                                            $unique_id = $event_data->id."goals-odd-even-06608-".$i."-".$prop_data['goals_odd_even'][$i]['sub_type_name'];
                                            $prop_data['goals_odd_even'][$i]['Unique_id'] = $unique_id;
                                            $prop_data['goals_odd_even'][$i]['betting_wager'] = 'Even';
                                            $prop_data['goals_odd_even'][$i]['row_line'] = 'home';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['goals_odd_even'][$i])}}','prop_bet')">
                                            {{_preccedSign($prop_data['goals_odd_even'][$i]['odds'])}}
                                        </a>
                                    </div>
                                @endif
                            </td>
                            <td data-th="Odd" class="">
                                @if(empty($prop_data['goals_odd_even'][$i + 1]['odds']))
                                    <a href="">OTB </a>
                                @else
                                    @php
                                        $prop_data['goals_odd_even'][$i + 1]['odds'] =
                                        _toAmericanDecimal($prop_data['goals_odd_even'][$i + 1]['odds']);
                                        $prop_data['goals_odd_even'][$i + 1]['team_name'] = "Goals Odd Even";
                                        $prop_data['goals_odd_even'][$i + 1]['sub_type_name'] = 'Odd';
                                        $unique_id = $event_data->id."goals-odd-even-08808-".($i+1)."-".$prop_data['goals_odd_even'][$i +
                                        1]['sub_type_name'];
                                        $prop_data['goals_odd_even'][$i + 1]['Unique_id'] = $unique_id;
                                        $prop_data['goals_odd_even'][$i + 1]['betting_wager'] = 'Odd';
                                        $prop_data['goals_odd_even'][$i + 1]['row_line'] = 'away';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['goals_odd_even'][$i+1])}}','prop_bet')">
                                        {{_preccedSign($prop_data['goals_odd_even'][$i + 1]['odds'])}} </a>
                                @endif
                            </td>
                        </tr>
                        @endfor
                        </tbody>
                    </table>
                    {{--mobile-view end--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767"> Goals Odd Even </h3>
                    <tbody class="rays">
                        <!-- table heading name -->
                        <tr>
                            <th>Even</th>
                            <th>Odd</th>
                        </tr>

                        @for($i=0; $i < count($prop_data['goals_odd_even']) / 2; $i +=2) <tr
                            class="mobilecss wordltable">
                            <td data-th="Even" class=" ">
                                @if (empty($prop_data['goals_odd_even'][$i]['odds']))
                                <div class="">
                                    <a href="#"> OTB </a>
                                </div>
                                @else
                                <div class="">
                                    @php
                                    $prop_data['goals_odd_even'][$i]['odds'] =
                                    _toAmericanDecimal($prop_data['goals_odd_even'][$i]['odds']);
                                    $prop_data['goals_odd_even'][$i]['team_name'] = "Goals Odd Even";
                                    $prop_data['goals_odd_even'][$i]['sub_type_name'] = 'Even';
                                    $unique_id = $event_data->id."goals-odd-even-06608-".$i."-".$prop_data['goals_odd_even'][$i]['sub_type_name'];
                                    $prop_data['goals_odd_even'][$i]['Unique_id'] = $unique_id;
                                    $prop_data['goals_odd_even'][$i]['betting_wager'] = 'Even';
                                    $prop_data['goals_odd_even'][$i]['row_line'] = 'home';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                        onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['goals_odd_even'][$i])}}','prop_bet')">
                                        {{_preccedSign($prop_data['goals_odd_even'][$i]['odds'])}}
                                    </a>
                                </div>
                                @endif
                            </td>
                            <td data-th="Odd" class="">
                                @if(empty($prop_data['goals_odd_even'][$i + 1]['odds']))
                                <a href="">OTB </a>
                                @else
                                @php
                                $prop_data['goals_odd_even'][$i + 1]['odds'] =
                                _toAmericanDecimal($prop_data['goals_odd_even'][$i + 1]['odds']);
                                $prop_data['goals_odd_even'][$i + 1]['team_name'] = "Goals Odd Even";
                                $prop_data['goals_odd_even'][$i + 1]['sub_type_name'] = 'Odd';
                                $unique_id = $event_data->id."goals-odd-even-08808-".($i+1)."-".$prop_data['goals_odd_even'][$i +
                                1]['sub_type_name'];
                                $prop_data['goals_odd_even'][$i + 1]['Unique_id'] = $unique_id;
                                $prop_data['goals_odd_even'][$i + 1]['betting_wager'] = 'Odd';
                                $prop_data['goals_odd_even'][$i + 1]['row_line'] = 'away';
                                @endphp
                                <a id="{{$unique_id}}"
                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['goals_odd_even'][$i+1])}}','prop_bet')">
                                    {{_preccedSign($prop_data['goals_odd_even'][$i + 1]['odds'])}} </a>
                                @endif
                            </td>
                            </tr>
                            @endfor
                    </tbody>
                </table>
                @endif


                @elseif($prop_bet_details['prop_data']->sport_id == 13)

                @if (!empty($prop_data['set_betting']))
                <!-- Set Betting -->
                    {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">Set Betting </h3>
                        <thead>
                        <tr>
                            <th> {{ $home_name }} </th>
                            <th> {{ $away_name }} </th>
                        </tr>
                        </thead>
                        <tbody>
                        @for ($i = 0; $i < count($prop_data['set_betting']) / 2; $i++) <!-- table single column & row
                            list-->
                        <tr class="mobilecss wordltable">
                            <td data-th="{{ $home_name }}" class=" ">
                                @if (empty( $prop_data['set_betting'][$i]['odds']))
                                    <div class="">
                                        <a href="#"> OTB </a>
                                    </div>
                                @else
                                    <div class="">
                                        @php
                                            $prop_data['set_betting'][$i]['odds'] =
                                            _toAmericanDecimal($prop_data['set_betting'][$i]['odds']);
                                            $prop_data['set_betting'][$i]['team_name'] = "Set Betting";
                                            $prop_data['set_betting'][$i]['sub_type_name'] = $home_name;
                                            $unique_id = $event_data->id."set-betting-90735-".$i."-".str_replace(['.', '_', ' ', '--'], '-',$home_name);
                                            $prop_data['set_betting'][$i]['Unique_id'] = $unique_id;
                                            $prop_data['set_betting'][$i]['betting_wager'] = $home_name;
                                            $prop_data['set_betting'][$i]['row_line'] = 'home';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['set_betting'][$i])}}','prop_bet')">
                                            {{_preccedSign($prop_data['set_betting'][$i]['odds'])}}
                                        </a>
                                    </div>
                                @endif
                            </td>
                            <td data-th="{{ $away_name }}" class="">
                                @if (empty($set_betting[$i + 1]['odds']))
                                    <a href="#"> OTB </a>
                                @else
                                    @php
                                        $prop_data['set_betting'][$i+1]['odds'] =
                                        _toAmericanDecimal($prop_data['set_betting'][$i+1]['odds']);
                                        $prop_data['set_betting'][$i+1]['team_name'] = "Set Betting";
                                        $prop_data['set_betting'][$i+1]['sub_type_name'] = $away_name;
                                        $unique_id = $event_data->id."-set-betting-70394-".($i+1)."-".str_replace(['.', '_', ' ', '--'], '-',$away_name);
                                        $prop_data['set_betting'][$i+1]['Unique_id'] = $unique_id;
                                        $prop_data['set_betting'][$i+1]['betting_wager'] = $away_name;
                                        $prop_data['set_betting'][$i+1]['row_line'] = 'away';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['set_betting'][$i+1])}}','prop_bet')">
                                        {{_preccedSign($set_betting[$i + 1]['odds'])}} </a>
                                @endif
                            </td>
                        </tr>
                        @endfor
                        </tbody>
                    </table>
                    {{--mobile-view end--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767"> Set Betting</h3>
                    <tbody class="rays">
                        <!-- table heading name -->
                        <tr>
                            <th> {{ $home_name }} </th>
                            <th> {{ $away_name }} </th>
                        </tr>
                        @for ($i = 0; $i < count($prop_data['set_betting']) / 2; $i++) <!-- table single column & row
                            list-->
                            <tr class="mobilecss wordltable">
                                <td data-th="{{ $home_name }}" class=" ">
                                    @if (empty( $prop_data['set_betting'][$i]['odds']))
                                    <div class="">
                                        <a href="#"> OTB </a>
                                    </div>
                                    @else
                                    <div class="">
                                        @php
                                        $prop_data['set_betting'][$i]['odds'] =
                                        _toAmericanDecimal($prop_data['set_betting'][$i]['odds']);
                                        $prop_data['set_betting'][$i]['team_name'] = "Set Betting";
                                        $prop_data['set_betting'][$i]['sub_type_name'] = $home_name;
                                        $unique_id = $event_data->id."set-betting-90735-".$i."-".str_replace(['.', '_', ' ', '--'], '-',$home_name);
                                        $prop_data['set_betting'][$i]['Unique_id'] = $unique_id;
                                        $prop_data['set_betting'][$i]['betting_wager'] = $home_name;
                                        $prop_data['set_betting'][$i]['row_line'] = 'home';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                            onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['set_betting'][$i])}}','prop_bet')">
                                            {{_preccedSign($prop_data['set_betting'][$i]['odds'])}}
                                        </a>
                                    </div>
                                    @endif
                                </td>
                                <td data-th="{{ $away_name }}" class="">
                                    @if (empty($set_betting[$i + 1]['odds']))
                                    <a href="#"> OTB </a>
                                    @else
                                    @php
                                    $prop_data['set_betting'][$i+1]['odds'] =
                                    _toAmericanDecimal($prop_data['set_betting'][$i+1]['odds']);
                                    $prop_data['set_betting'][$i+1]['team_name'] = "Set Betting";
                                    $prop_data['set_betting'][$i+1]['sub_type_name'] = $away_name;
                                    $unique_id = $event_data->id."-set-betting-70394-".($i+1)."-".str_replace(['.', '_', ' ', '--'], '-',$away_name);
                                    $prop_data['set_betting'][$i+1]['Unique_id'] = $unique_id;
                                    $prop_data['set_betting'][$i+1]['betting_wager'] = $away_name;
                                    $prop_data['set_betting'][$i+1]['row_line'] = 'away';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                        onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['set_betting'][$i+1])}}','prop_bet')">
                                        {{_preccedSign($set_betting[$i + 1]['odds'])}} </a>
                                    @endif
                                </td>
                            </tr>
                            @endfor
                    </tbody>
                </table>
                @endif

                @if (!empty($prop_data['first_set_winner']))
                <!-- First Set Winner -->
                    {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767"> First Set Winner </h3>
                        <thead>
                        <tr>
                            <th> {{ $home_name }} </th>
                            <th> {{ $away_name }} </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td data-th="{{ $home_name }}" class=" ">
                                @if (empty( $prop_data['first_set_winner'][0]['odds']))
                                    <div class="">
                                        <a href="#"> OTB </a>
                                    </div>
                                @else
                                    <div class="">
                                        @php
                                            $prop_data['first_set_winner'][0]['odds'] =  _toAmericanDecimal($prop_data['first_set_winner'][0]['odds']);
                                            $prop_data['first_set_winner'][0]['team_name'] = "First Set Winner";
                                            $prop_data['first_set_winner'][0]['sub_type_name'] = $home_name;
                                            $unique_id = $event_data->id."first-set-winner-76952-00-".str_replace(['.', '_', ' ', '--'], '-',$home_name);
                                            $prop_data['first_set_winner'][0]['Unique_id'] = $unique_id;
                                            $prop_data['first_set_winner'][0]['betting_wager'] = $home_name;
                                            $prop_data['first_set_winner'][0]['row_line'] = 'home';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_set_winner'][0])}}','prop_bet')">
                                            {{_preccedSign($prop_data['first_set_winner'][0]['odds'])}}
                                        </a>
                                    </div>
                                @endif
                            </td>
                            <td data-th="{{ $away_name }}" class="">
                                @if (empty($prop_data['first_set_winner'][1]['odds']))
                                    <a href="#"> OTB </a>
                                @else
                                    @php
                                        $prop_data['first_set_winner'][1]['odds'] =
                                        _toAmericanDecimal($prop_data['first_set_winner'][1]['odds']);
                                        $prop_data['first_set_winner'][1]['team_name'] = "First Set Winner";
                                        $prop_data['first_set_winner'][1]['sub_type_name'] = $away_name;
                                        $unique_id = $event_data->id."first-set-winner-76933-01-".str_replace(['.', '_', ' ', '--'], '-',$away_name);
                                        $prop_data['first_set_winner'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_set_winner'][1]['betting_wager'] = $away_name;
                                        $prop_data['first_set_winner'][1]['row_line'] = 'away';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_set_winner'][1])}}','prop_bet')">
                                        {{_preccedSign($prop_data['first_set_winner'][1]['odds'])}} </a>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    {{--mobile-view end--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767"> First Set Winner </h3>
                    <tbody class="rays">
                        <!-- table heading name -->
                        <tr>
                            <th> {{ $home_name }} </th>
                            <th> {{ $away_name }} </th>
                        </tr>
                        <!-- table single column & row list-->
                        <tr class="mobilecss wordltable">
                            <td data-th="{{ $home_name }}" class=" ">
                                @if (empty( $prop_data['first_set_winner'][0]['odds']))
                                <div class="">
                                    <a href="#"> OTB </a>
                                </div>
                                @else
                                <div class="">
                                    @php
                                    $prop_data['first_set_winner'][0]['odds'] =  _toAmericanDecimal($prop_data['first_set_winner'][0]['odds']);
                                    $prop_data['first_set_winner'][0]['team_name'] = "First Set Winner";
                                    $prop_data['first_set_winner'][0]['sub_type_name'] = $home_name;
                                    $unique_id = $event_data->id."first-set-winner-76952-00-".str_replace(['.', '_', ' ', '--'], '-',$home_name);
                                    $prop_data['first_set_winner'][0]['Unique_id'] = $unique_id;
                                    $prop_data['first_set_winner'][0]['betting_wager'] = $home_name;
                                    $prop_data['first_set_winner'][0]['row_line'] = 'home';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                        onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_set_winner'][0])}}','prop_bet')">
                                        {{_preccedSign($prop_data['first_set_winner'][0]['odds'])}}
                                    </a>
                                </div>
                                @endif
                            </td>
                            <td data-th="{{ $away_name }}" class="">
                                @if (empty($prop_data['first_set_winner'][1]['odds']))
                                <a href="#"> OTB </a>
                                @else
                                @php
                                $prop_data['first_set_winner'][1]['odds'] =
                                _toAmericanDecimal($prop_data['first_set_winner'][1]['odds']);
                                $prop_data['first_set_winner'][1]['team_name'] = "First Set Winner";
                                $prop_data['first_set_winner'][1]['sub_type_name'] = $away_name;
                                $unique_id = $event_data->id."first-set-winner-76933-01-".str_replace(['.', '_', ' ', '--'], '-',$away_name);
                                $prop_data['first_set_winner'][1]['Unique_id'] = $unique_id;
                                $prop_data['first_set_winner'][1]['betting_wager'] = $away_name;
                                $prop_data['first_set_winner'][1]['row_line'] = 'away';
                                @endphp
                                <a id="{{$unique_id}}"
                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_set_winner'][1])}}','prop_bet')">
                                    {{_preccedSign($prop_data['first_set_winner'][1]['odds'])}} </a>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
                @endif


                @if (!empty($prop_data['first_break_of_serve']))
                <!-- First Break of Serve -->
                    {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767"> First Break of Serve</h3>
                        <thead>
                        <tr>
                            <th>{{ $prop_data['first_break_of_serve'][0]['name'] }}</th>
                            <th>{{ $prop_data['first_break_of_serve'][1]['name'] }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td data-th="{{ $prop_data['first_break_of_serve'][0]['name'] }}" class=" ">
                                @if (empty($prop_data['first_break_of_serve'][0]['odds']))
                                    <div class="">
                                        <a href="#"> OTB </a>
                                    </div>
                                @else
                                    <div class="">
                                        @php
                                            $prop_data['first_break_of_serve'][0]['odds'] =
                                            _toAmericanDecimal($prop_data['first_break_of_serve'][0]['odds']);
                                            $prop_data['first_break_of_serve'][0]['team_name'] = "First Break of Serve";
                                            $prop_data['first_break_of_serve'][0]['sub_type_name'] = $prop_data['first_break_of_serve'][0]['name'];
                                            $unique_id = $event_data->id."first-break-of-serve-22211-00-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['first_break_of_serve'][0]['sub_type_name']);
                                            $prop_data['first_break_of_serve'][0]['Unique_id'] = $unique_id;
                                            $prop_data['first_break_of_serve'][0]['betting_wager'] = $prop_data['first_break_of_serve'][0]['name'];
                                            $prop_data['first_break_of_serve'][0]['row_line'] = 'home';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_break_of_serve'][0])}}','prop_bet')">
                                            {{ _preccedSign($prop_data['first_break_of_serve'][0]['odds'])}}
                                        </a>
                                    </div>
                                @endif
                            </td>
                            <td data-th="{{ $prop_data['first_break_of_serve'][1]['name'] }}" class="">
                                @if (empty($prop_data['first_break_of_serve'][1]['odds']))
                                    <a href="#"> OTB </a>
                                @else
                                    @php
                                        $prop_data['first_break_of_serve'][1]['odds'] =
                                        _toAmericanDecimal($prop_data['first_break_of_serve'][1]['odds']);
                                        $prop_data['first_break_of_serve'][1]['team_name'] = "First Break of Serve";
                                        $prop_data['first_break_of_serve'][1]['sub_type_name'] = $prop_data['first_break_of_serve'][1]['name'];
                                        $unique_id = $event_data->id."-first-break-of-serve-22211-00-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['first_break_of_serve'][1]['sub_type_name']);
                                        $prop_data['first_break_of_serve'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_break_of_serve'][1]['betting_wager'] = $prop_data['first_break_of_serve'][1]['name'];
                                        $prop_data['first_break_of_serve'][1]['row_line'] = 'away';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_break_of_serve'][1])}}','prop_bet')">
                                        {{_preccedSign($prop_data['first_break_of_serve'][1]['odds'])}}
                                    </a>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    {{--mobile-view end--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767"> First Break of Serve </h3>
                    <tbody class="rays">
                        <!-- table heading name -->

                        <tr>
                            <th>{{ $prop_data['first_break_of_serve'][0]['name'] }}</th>
                            <th>{{ $prop_data['first_break_of_serve'][1]['name'] }}</th>
                        </tr>
                        <!-- table single column & row list -->
                        <tr class="mobilecss wordltable">
                            <td data-th="{{ $prop_data['first_break_of_serve'][0]['name'] }}" class=" ">
                                @if (empty($prop_data['first_break_of_serve'][0]['odds']))
                                <div class="">
                                    <a href="#"> OTB </a>
                                </div>
                                @else
                                <div class="">
                                    @php
                                    $prop_data['first_break_of_serve'][0]['odds'] =
                                    _toAmericanDecimal($prop_data['first_break_of_serve'][0]['odds']);
                                    $prop_data['first_break_of_serve'][0]['team_name'] = "First Break of Serve";
                                    $prop_data['first_break_of_serve'][0]['sub_type_name'] = $prop_data['first_break_of_serve'][0]['name'];
                                    $unique_id = $event_data->id."first-break-of-serve-22211-00-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['first_break_of_serve'][0]['sub_type_name']);
                                    $prop_data['first_break_of_serve'][0]['Unique_id'] = $unique_id;
                                    $prop_data['first_break_of_serve'][0]['betting_wager'] = $prop_data['first_break_of_serve'][0]['name'];
                                    $prop_data['first_break_of_serve'][0]['row_line'] = 'home';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                        onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_break_of_serve'][0])}}','prop_bet')">
                                        {{ _preccedSign($prop_data['first_break_of_serve'][0]['odds'])}}
                                    </a>
                                </div>
                                @endif
                            </td>
                            <td data-th="{{ $prop_data['first_break_of_serve'][1]['name'] }}" class="">
                                @if (empty($prop_data['first_break_of_serve'][1]['odds']))
                                <a href="#"> OTB </a>
                                @else
                                @php
                                $prop_data['first_break_of_serve'][1]['odds'] =
                                _toAmericanDecimal($prop_data['first_break_of_serve'][1]['odds']);
                                $prop_data['first_break_of_serve'][1]['team_name'] = "First Break of Serve";
                                $prop_data['first_break_of_serve'][1]['sub_type_name'] = $prop_data['first_break_of_serve'][1]['name'];
                                $unique_id = $event_data->id."-first-break-of-serve-22211-00-".str_replace(['.', '_', ' ', '--'], '-',$prop_data['first_break_of_serve'][1]['sub_type_name']);
                                $prop_data['first_break_of_serve'][1]['Unique_id'] = $unique_id;
                                $prop_data['first_break_of_serve'][1]['betting_wager'] = $prop_data['first_break_of_serve'][1]['name'];
                                $prop_data['first_break_of_serve'][1]['row_line'] = 'away';
                                @endphp
                                <a id="{{$unique_id}}"
                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_break_of_serve'][1])}}','prop_bet')">
                                    {{_preccedSign($prop_data['first_break_of_serve'][1]['odds'])}}
                                </a>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
                @endif

                @elseif($prop_bet_details['prop_data']->sport_id == 12)
                @if (!empty($prop_data['first_half_point_spread_2_way']) &&
                !empty($prop_data['first_half_money_line_2_way']) &&
                !empty($prop_data['first_half_total_2_way']))
                <!-- 1st half -->
                {{--mobile-view--}}
                <table class="table prop-table prop-td-show-767">
                    <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">1st half</h3>
                    <thead>
                    <tr>
                        <th>Player Name</th>
                        <th>Spread</th>
                        <th>Money Line</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="text-center">
                        <td>{{ $home_name }}</td>
                        <td>
                            @if (empty($prop_data['first_half_point_spread_2_way'][0]['odds']))
                                <a>OTB</a>
                            @else
                                @php
                                    $prop_data['first_half_point_spread_2_way'][0]['odds'] =
                                    _toAmericanDecimal($prop_data['first_half_point_spread_2_way'][0]['odds']);
                                    $prop_data['first_half_point_spread_2_way'][0]['team_name'] = "1st half";
                                    $prop_data['first_half_point_spread_2_way'][0]['sub_type_name'] ="Spread";
                                    $unique_id = $event_data->id."1st-half-77799-00-".$prop_data['first_half_point_spread_2_way'][0]['sub_type_name'];
                                    $prop_data['first_half_point_spread_2_way'][0]['Unique_id'] = $unique_id;
                                    $prop_data['first_half_point_spread_2_way'][0]['betting_wager'] = "Spread";
                                    $prop_data['first_half_point_spread_2_way'][0]['row_line'] = 'home';
                                    $prop_data['first_half_point_spread_2_way'][0]['handicap'] = $prop_data['first_half_point_spread_2_way'][0]['handicap'];
                                @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_point_spread_2_way'][0])}}','prop_bet')">
                                    ({{ $prop_data['first_half_point_spread_2_way'][0]['handicap'] }})
                                    {{_preccedSign($prop_data['first_half_point_spread_2_way'][0]['odds'])}} </a>
                            @endif
                        </td>
                        <td>
                            @if (empty($prop_data['first_half_money_line_2_way'][0]['odds']))
                                <a>OTB</a>
                            @else
                                @php
                                    $prop_data['first_half_money_line_2_way'][0]['odds'] =
                                    _toAmericanDecimal($prop_data['first_half_money_line_2_way'][0]['odds']);
                                    $prop_data['first_half_money_line_2_way'][0]['team_name'] = "1st half";
                                    $prop_data['first_half_money_line_2_way'][0]['sub_type_name'] ="Money Line";
                                    $unique_id = $event_data->id."1st-half-99977-00-money-line";
                                    $prop_data['first_half_money_line_2_way'][0]['Unique_id'] = $unique_id;
                                    $prop_data['first_half_money_line_2_way'][0]['betting_wager'] = "Money Line";
                                    $prop_data['first_half_money_line_2_way'][0]['row_line'] = 'home';
                                @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_money_line_2_way'][0])}}','prop_bet')">
                                    {{ $prop_data['first_half_money_line_2_way'][0]['odds'] }}
                                </a>
                            @endif

                        </td>
                        <td>
                            @if (empty($prop_data['first_half_money_line_2_way'][0]['odds']))
                                <a>OTB</a>
                            @else
                                @php
                                    $prop_data['first_half_money_line_2_way'][0]['odds'] =
                                    _toAmericanDecimal($prop_data['first_half_money_line_2_way'][0]['odds']);
                                    $prop_data['first_half_money_line_2_way'][0]['team_name'] = "1st half";
                                    $prop_data['first_half_money_line_2_way'][0]['sub_type_name'] ="Money Line";
                                    $unique_id = $event_data->id."1st-half-99977-00-money-line";
                                    $prop_data['first_half_money_line_2_way'][0]['Unique_id'] = $unique_id;
                                    $prop_data['first_half_money_line_2_way'][0]['betting_wager'] = "Money Line";
                                    $prop_data['first_half_money_line_2_way'][0]['row_line'] = 'home';
                                @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_money_line_2_way'][0])}}','prop_bet')">
                                    {{ $prop_data['first_half_money_line_2_way'][0]['odds'] }}
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{ $away_name  }}
                        </td>
                        <td>
                            @if (empty($prop_data['first_half_point_spread_2_way'][1]['odds']))
                                <a href="#">OTB</a>
                            @else
                                @php
                                    $prop_data['first_half_point_spread_2_way'][1]['odds'] =
                                    _toAmericanDecimal($prop_data['first_half_point_spread_2_way'][1]['odds']);
                                    $prop_data['first_half_point_spread_2_way'][1]['team_name'] = "1st half";
                                    $prop_data['first_half_point_spread_2_way'][1]['sub_type_name'] ="Spread";
                                    $unique_id = $event_data->id."1st-half-78899-11-".$prop_data['first_half_point_spread_2_way'][1]['sub_type_name'];
                                    $prop_data['first_half_point_spread_2_way'][1]['Unique_id'] = $unique_id;
                                    $prop_data['first_half_point_spread_2_way'][1]['betting_wager'] = "Spread";
                                    $prop_data['first_half_point_spread_2_way'][1]['row_line'] = 'away';
                                    $prop_data['first_half_point_spread_2_way'][1]['handicap'] = $prop_data['first_half_point_spread_2_way'][1]['handicap'];
                                @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_point_spread_2_way'][1])}}','prop_bet')">
                                    ({{ $prop_data['first_half_point_spread_2_way'][1]['handicap'] }})
                                    {{ _preccedSign($prop_data['first_half_point_spread_2_way'][1]['odds'])}} </a>
                            @endif

                        </td>
                        <td>
                            @if (empty($prop_data['first_half_money_line_2_way'][1]['odds']))
                                <div><a href="#">OTB</a></div>
                            @else
                                <div class="">
                                    @php
                                        $prop_data['first_half_money_line_2_way'][1]['odds'] =
                                        _toAmericanDecimal($prop_data['first_half_money_line_2_way'][1]['odds']);
                                        $prop_data['first_half_money_line_2_way'][1]['team_name'] = "1st half";
                                        $prop_data['first_half_money_line_2_way'][1]['sub_type_name'] ="Money Line";
                                        $unique_id = $event_data->id."1st-half-96677-11-money-line";
                                        $prop_data['first_half_money_line_2_way'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_half_money_line_2_way'][1]['betting_wager'] = "Money Line";
                                        $prop_data['first_half_money_line_2_way'][1]['row_line'] = 'away';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_money_line_2_way'][1])}}','prop_bet')">
                                        {{_preccedSign($prop_data['first_half_money_line_2_way'][1]['odds'])}}
                                    </a>
                                </div>
                            @endif

                        </td>
                        <td>
                            @php
                                $prop_data['first_half_total_2_way'][1]['odds'] =
                                _toAmericanDecimal($prop_data['first_half_total_2_way'][1]['odds']);
                                $prop_data['first_half_total_2_way'][1]['team_name'] = "1st half";
                                $prop_data['first_half_total_2_way'][1]['sub_type_name'] ="Total";
                                $unique_id = $event_data->id."1st-half-92277-11-".$prop_data['first_half_total_2_way'][1]['sub_type_name'];
                                $prop_data['first_half_total_2_way'][1]['Unique_id'] = $unique_id;
                                $prop_data['first_half_total_2_way'][1]['betting_wager'] = "Total";
                                $prop_data['first_half_total_2_way'][1]['row_line'] = 'away';
                                $prop_data['first_half_total_2_way'][1]['handicap'] = $prop_data['first_half_total_2_way'][1]['handicap'];
                            @endphp
                            {{-- $prop_data['first_half_total_2_way'][1]['name'] .--}}
                            <a id="{{$unique_id}}"
                               onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_total_2_way'][1])}}','prop_bet')">
                                ({{  $prop_data['first_half_total_2_way'][1]['name'] }}
                                ){{ _preccedSign($prop_data['first_half_total_2_way'][1]['odds'])}}
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
                    {{--mobile-view--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767">1st half</h3>
                    <tbody>
                        <!-- table heading name -->

                        <tr class="prop-td-hide-767">
                            <th> </th> <!-- Each table heading name &  data-th wile be same -->
                            <th>Spread</th>
                            <th>Money Line</th>
                            <th>Total</th>
                        </tr>
                        <!-- table single column & row list -->
                        <tr class="mobilecss wordltable prop-td-hide-767">

                            <td data-th="Player name" class="prop-td-hide-767">
                                <!-- Each table heading name &  data-th wile be same -->

                                <div class="newtabls">
                                    <p>{{ $home_name }}</p>
                                </div>
                            </td>
                            <td data-th="Spread" class="prop-td-hide-767">
                                @if (empty($prop_data['first_half_point_spread_2_way'][0]['odds']))
                                <a>OTB</a>
                                @else
                                @php
                                $prop_data['first_half_point_spread_2_way'][0]['odds'] =
                                _toAmericanDecimal($prop_data['first_half_point_spread_2_way'][0]['odds']);
                                $prop_data['first_half_point_spread_2_way'][0]['team_name'] = "1st half";
                                $prop_data['first_half_point_spread_2_way'][0]['sub_type_name'] ="Spread";
                                $unique_id = $event_data->id."1st-half-77799-00-".$prop_data['first_half_point_spread_2_way'][0]['sub_type_name'];
                                $prop_data['first_half_point_spread_2_way'][0]['Unique_id'] = $unique_id;
                                $prop_data['first_half_point_spread_2_way'][0]['betting_wager'] = "Spread";
                                $prop_data['first_half_point_spread_2_way'][0]['row_line'] = 'home';
                                $prop_data['first_half_point_spread_2_way'][0]['handicap'] = $prop_data['first_half_point_spread_2_way'][0]['handicap'];
                                @endphp
                                <a id="{{$unique_id}}"
                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_point_spread_2_way'][0])}}','prop_bet')">
                                    ({{ $prop_data['first_half_point_spread_2_way'][0]['handicap'] }})
                                    {{_preccedSign($prop_data['first_half_point_spread_2_way'][0]['odds'])}} </a>
                                @endif

                            </td>
                            <td data-th="Money Line" class="prop-td-hide-767 ">
                                <div class="">
                                    @if (empty($prop_data['first_half_money_line_2_way'][0]['odds']))
                                    <a>OTB</a>
                                    @else
                                    @php
                                    $prop_data['first_half_money_line_2_way'][0]['odds'] =
                                    _toAmericanDecimal($prop_data['first_half_money_line_2_way'][0]['odds']);
                                    $prop_data['first_half_money_line_2_way'][0]['team_name'] = "1st half";
                                    $prop_data['first_half_money_line_2_way'][0]['sub_type_name'] ="Money Line";
                                    $unique_id = $event_data->id."1st-half-99977-00-money-line";
                                    $prop_data['first_half_money_line_2_way'][0]['Unique_id'] = $unique_id;
                                    $prop_data['first_half_money_line_2_way'][0]['betting_wager'] = "Money Line";
                                    $prop_data['first_half_money_line_2_way'][0]['row_line'] = 'home';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                        onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_money_line_2_way'][0])}}','prop_bet')">
                                        {{ $prop_data['first_half_money_line_2_way'][0]['odds'] }}
                                    </a>
                                    @endif

                                </div>
                            </td>
                            <td data-th="Total" class="prop-td-hide-767">
                                @if (empty($prop_data['first_half_total_2_way'][0]['odds']))
                                <a href="#">OTB</a>
                                @else
                                @php
                                $prop_data['first_half_total_2_way'][0]['odds'] =
                                _toAmericanDecimal($prop_data['first_half_total_2_way'][0]['odds']);
                                $prop_data['first_half_total_2_way'][0]['team_name'] = "1st half";
                                $prop_data['first_half_total_2_way'][0]['sub_type_name'] ="Total";
                                $unique_id = $event_data->id."1st-half-88877-00-".$prop_data['first_half_total_2_way'][0]['sub_type_name'];
                                $prop_data['first_half_total_2_way'][0]['Unique_id'] = $unique_id;
                                $prop_data['first_half_total_2_way'][0]['betting_wager'] = "Total";
                                $prop_data['first_half_total_2_way'][0]['row_line'] = 'home';
                                $prop_data['first_half_total_2_way'][0]['handicap'] = $prop_data['first_half_total_2_way'][0]['handicap'];
                                @endphp
                            {{--  $prop_data['first_half_total_2_way'][0]['name']--}}
                                <a id="{{$unique_id}}"
                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_total_2_way'][0])}}','prop_bet')">
                                    ({{  $prop_data['first_half_total_2_way'][0]['name'] }}
                                    ){{ _preccedSign($prop_data['first_half_total_2_way'][0]['odds'])}}
                                </a>
                                @endif
                            </td>

                        </tr>

                        <tr class="mobilecss wordltable prop-td-hide-767">

                            <td data-th="Player name" class="prop-td-hide-767">
                                <!-- Each table heading name &  data-th wile be same -->
                                <div class="newtabls">
                                    <p>{{ $away_name  }}</p>
                                </div>
                            </td>
                            <td data-th="Spread" class="prop-td-hide-767 ">
                                @if (empty($prop_data['first_half_point_spread_2_way'][1]['odds']))
                                <a href="#">OTB</a>
                                @else
                                @php
                                $prop_data['first_half_point_spread_2_way'][1]['odds'] =
                                _toAmericanDecimal($prop_data['first_half_point_spread_2_way'][1]['odds']);
                                $prop_data['first_half_point_spread_2_way'][1]['team_name'] = "1st half";
                                $prop_data['first_half_point_spread_2_way'][1]['sub_type_name'] ="Spread";
                                $unique_id = $event_data->id."1st-half-78899-11-".$prop_data['first_half_point_spread_2_way'][1]['sub_type_name'];
                                $prop_data['first_half_point_spread_2_way'][1]['Unique_id'] = $unique_id;
                                $prop_data['first_half_point_spread_2_way'][1]['betting_wager'] = "Spread";
                                $prop_data['first_half_point_spread_2_way'][1]['row_line'] = 'away';
                                $prop_data['first_half_point_spread_2_way'][1]['handicap'] = $prop_data['first_half_point_spread_2_way'][1]['handicap'];
                                @endphp
                                <a id="{{$unique_id}}"
                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_point_spread_2_way'][1])}}','prop_bet')">
                                    ({{ $prop_data['first_half_point_spread_2_way'][1]['handicap'] }})
                                    {{ _preccedSign($prop_data['first_half_point_spread_2_way'][1]['odds'])}} </a>
                                @endif

                            </td>
                            <td data-th="Money Line" class="prop-td-hide-767 ">
                                @if (empty($prop_data['first_half_money_line_2_way'][1]['odds']))
                                <div><a href="#">OTB</a></div>
                                @else
                                <div class="">
                                    @php
                                    $prop_data['first_half_money_line_2_way'][1]['odds'] =
                                    _toAmericanDecimal($prop_data['first_half_money_line_2_way'][1]['odds']);
                                    $prop_data['first_half_money_line_2_way'][1]['team_name'] = "1st half";
                                    $prop_data['first_half_money_line_2_way'][1]['sub_type_name'] ="Money Line";
                                    $unique_id = $event_data->id."1st-half-96677-11-money-line";
                                    $prop_data['first_half_money_line_2_way'][1]['Unique_id'] = $unique_id;
                                    $prop_data['first_half_money_line_2_way'][1]['betting_wager'] = "Money Line";
                                    $prop_data['first_half_money_line_2_way'][1]['row_line'] = 'away';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                        onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_money_line_2_way'][1])}}','prop_bet')">
                                        {{_preccedSign($prop_data['first_half_money_line_2_way'][1]['odds'])}}
                                    </a>
                                </div>
                                @endif

                            </td>
                            <td data-th="Total" class="prop-td-hide-767">
                                @php
                                $prop_data['first_half_total_2_way'][1]['odds'] =
                                _toAmericanDecimal($prop_data['first_half_total_2_way'][1]['odds']);
                                $prop_data['first_half_total_2_way'][1]['team_name'] = "1st half";
                                $prop_data['first_half_total_2_way'][1]['sub_type_name'] ="Total";
                                $unique_id = $event_data->id."1st-half-92277-11-".$prop_data['first_half_total_2_way'][1]['sub_type_name'];
                                $prop_data['first_half_total_2_way'][1]['Unique_id'] = $unique_id;
                                $prop_data['first_half_total_2_way'][1]['betting_wager'] = "Total";
                                $prop_data['first_half_total_2_way'][1]['row_line'] = 'away';
                                $prop_data['first_half_total_2_way'][1]['handicap'] = $prop_data['first_half_total_2_way'][1]['handicap'];
                                @endphp
                                    {{-- $prop_data['first_half_total_2_way'][1]['name'] .--}}
                                <a id="{{$unique_id}}"
                                    onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_total_2_way'][1])}}','prop_bet')">
                                    ({{  $prop_data['first_half_total_2_way'][1]['name'] }}
                                    ){{ _preccedSign($prop_data['first_half_total_2_way'][1]['odds'])}}
                                </a>
                            </td>
                        </tr>

                    </tbody>
                </table>
                <!-- 1st half -->
                @endif

                @if (!empty($prop_data['second_half_point_spread_2_way']) &&
                !empty($prop_data['second_half_money_line_2_way']) &&
                !empty($prop_data['second_half_total_2_way']))
                <!-- 2nd half -->
                    {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">2nd half</h3>
                        <thead>
                        <tr>
                            <th>Player Name</th>
                            <th>Spread</th>
                            <th>Money Line</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="text-center">
                            <td>{{ $home_name }}</td>
                            <td>
                                @if (empty($prop_data['second_half_point_spread_2_way'][0]['odds']))
                                    <a href="#">OTB</a>
                                @else
                                    @php
                                        $prop_data['second_half_point_spread_2_way'][0]['odds'] =
                                        _toAmericanDecimal($prop_data['second_half_point_spread_2_way'][0]['odds']);
                                        $prop_data['second_half_point_spread_2_way'][0]['team_name'] = "2nd half";
                                        $prop_data['second_half_point_spread_2_way'][0]['sub_type_name'] ="Spread";
                                        $unique_id = $event_data->id."2st-half-22577-00-".$prop_data['second_half_point_spread_2_way'][0]['sub_type_name'];
                                        $prop_data['second_half_point_spread_2_way'][0]['Unique_id'] = $unique_id;
                                        $prop_data['second_half_point_spread_2_way'][0]['betting_wager'] = "Spread";
                                        $prop_data['second_half_point_spread_2_way'][0]['row_line'] = 'home';
                                        $prop_data['second_half_point_spread_2_way'][0]['handicap'] = $prop_data['second_half_point_spread_2_way'][0]['handicap'];
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['second_half_point_spread_2_way'][0])}}','prop_bet')">
                                        ({{ $prop_data['second_half_point_spread_2_way'][0]['handicap'] }})
                                        {{ _preccedSign($prop_data['second_half_point_spread_2_way'][0]['odds'])}} </a>
                                @endif
                            </td>
                            <td>
                                @if (empty($prop_data['second_half_money_line_2_way'][0]['odds']))
                                    <div class="">
                                        <a href="#">
                                            OTB
                                        </a>
                                    </div>
                                @else
                                    <div class="">
                                        @php
                                            $prop_data['second_half_money_line_2_way'][0]['odds'] =  _toAmericanDecimal($prop_data['second_half_money_line_2_way'][0]['odds']);
                                            $prop_data['second_half_money_line_2_way'][0]['team_name'] = "2nd half";
                                            $prop_data['second_half_money_line_2_way'][0]['sub_type_name'] ="Money Line";
                                            $unique_id = $event_data->id."2st-half-33655-00-money-line";
                                            $prop_data['second_half_money_line_2_way'][0]['Unique_id'] = $unique_id;
                                            $prop_data['second_half_money_line_2_way'][0]['betting_wager'] = "Money Line";
                                            $prop_data['second_half_money_line_2_way'][0]['row_line'] = 'home';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['second_half_money_line_2_way'][0])}}','prop_bet')">
                                            {{ $prop_data['second_half_money_line_2_way'][0]['odds'] }}
                                        </a>
                                    </div>
                                @endif

                            </td>
                            <td>
                                @if (empty($prop_data['second_half_total_2_way'][0]['odds']))
                                    <a href="#">
                                        OTB
                                    </a>
                                @else
                                    @php
                                        $prop_data['second_half_total_2_way'][0]['odds'] =  _toAmericanDecimal($prop_data['second_half_total_2_way'][0]['odds']);
                                        $prop_data['second_half_total_2_way'][0]['team_name'] = "2nd half";
                                        $prop_data['second_half_total_2_way'][0]['sub_type_name'] ="Total";
                                        $unique_id = $event_data->id."2st-half-33299-00-total";
                                        $prop_data['second_half_total_2_way'][0]['Unique_id'] = $unique_id;
                                        $prop_data['second_half_total_2_way'][0]['betting_wager'] = "Total";
                                        $prop_data['second_half_total_2_way'][0]['row_line'] = 'home';
                                        $prop_data['second_half_total_2_way'][0]['handicap'] = $prop_data['second_half_total_2_way'][0]['handicap'];
                                    @endphp
                                    {{-- $prop_data['second_half_total_2_way'][0]['name']--}}
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['second_half_total_2_way'][0])}}','prop_bet')">
                                        ({{ $prop_data['second_half_total_2_way'][0]['name'] }}
                                        ){{ _preccedSign($prop_data['second_half_total_2_way'][0]['odds'])}}
                                    </a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ $away_name  }}
                            </td>
                            <td>
                                @if (empty($prop_data['second_half_point_spread_2_way'][1]['odds']))
                                    <a href="#"> OTB</a>
                                @else
                                    @php
                                        $prop_data['second_half_point_spread_2_way'][1]['odds'] =
                                        _toAmericanDecimal($prop_data['second_half_point_spread_2_way'][1]['odds']);
                                        $prop_data['second_half_point_spread_2_way'][1]['team_name'] = "2nd half";
                                        $prop_data['second_half_point_spread_2_way'][1]['sub_type_name'] ="Spread";
                                        $unique_id = $event_data->id."2st-half-22500-01-".$prop_data['second_half_point_spread_2_way'][1]['sub_type_name'];
                                        $prop_data['second_half_point_spread_2_way'][1]['Unique_id'] = $unique_id;
                                        $prop_data['second_half_point_spread_2_way'][1]['betting_wager'] = "Spread";
                                        $prop_data['second_half_point_spread_2_way'][1]['row_line'] = 'away';
                                        $prop_data['second_half_point_spread_2_way'][1]['handicap'] = $prop_data['second_half_point_spread_2_way'][1]['handicap'];
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['second_half_point_spread_2_way'][1])}}','prop_bet')">
                                        ({{ $prop_data['second_half_point_spread_2_way'][1]['handicap'] }})
                                        {{ _preccedSign($prop_data['second_half_point_spread_2_way'][1]['odds'])}} </a>
                                @endif

                            </td>
                            <td>
                                @if (empty($prop_data['second_half_money_line_2_way'][1]['odds']))
                                    <div class="">
                                        <a href="#">
                                            OTB
                                        </a>
                                    </div>
                                @else
                                    <div class="">
                                        @php
                                            $prop_data['second_half_money_line_2_way'][1]['odds'] =  _toAmericanDecimal($prop_data['second_half_money_line_2_way'][1]['odds']);
                                            $prop_data['second_half_money_line_2_way'][1]['team_name'] = "2nd half";
                                            $prop_data['second_half_money_line_2_way'][1]['sub_type_name'] ="Money Line";
                                            $unique_id = $event_data->id."2st-half-33600-01-money-line";
                                            $prop_data['second_half_money_line_2_way'][1]['Unique_id'] = $unique_id;
                                            $prop_data['second_half_money_line_2_way'][1]['betting_wager'] = "Money Line";
                                            $prop_data['second_half_money_line_2_way'][1]['row_line'] = 'away';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['second_half_money_line_2_way'][1])}}','prop_bet')">
                                            {{_preccedSign($prop_data['second_half_money_line_2_way'][1]['odds'])}}
                                        </a>
                                    </div>
                                @endif

                            </td>
                            <td>
                                @if (empty($prop_data['second_half_total_2_way'][1]['odds']))
                                    <a href="#">
                                        OTB
                                    </a>
                                @else
                                    @php
                                        $prop_data['second_half_total_2_way'][1]['odds'] =  _toAmericanDecimal($prop_data['second_half_total_2_way'][1]['odds']);
                                        $prop_data['second_half_total_2_way'][1]['team_name'] = "2nd half";
                                        $prop_data['second_half_total_2_way'][1]['sub_type_name'] ="Total";
                                        $unique_id = $event_data->id."2st-half-33200-01-total";
                                        $prop_data['second_half_total_2_way'][1]['Unique_id'] = $unique_id;
                                        $prop_data['second_half_total_2_way'][1]['betting_wager'] = "Total";
                                        $prop_data['second_half_total_2_way'][1]['row_line'] = 'away';
                                        $prop_data['second_half_total_2_way'][1]['handicap'] = $prop_data['second_half_total_2_way'][1]['handicap'];
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['second_half_total_2_way'][1])}}','prop_bet')">
                                        ({{ $prop_data['second_half_total_2_way'][1]['name'] }}
                                        ){{_preccedSign($prop_data['second_half_total_2_way'][1]['odds'])}}
                                    </a>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    {{--mobile-view--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767">2nd half</h3>
                    <tbody>
                        <!-- table heading name -->
                        <tr>
                            <th> </th> <!-- Each table heading name &  data-th wile be same -->
                            <th>Spread</th>
                            <th>Money Line</th>
                            <th>Total</th>
                        </tr>
                        <!-- table single column & row list -->
                        <tr class="mobilecss wordltable">

                            <td data-th="Player name" class="">
                                <!-- Each table heading name &  data-th wile be same -->

                                <div class="newtabls">
                                    <p>{{ $home_name }}</p>
                                </div>
                            </td>
                            <td data-th="Spread" class="">
                                @if (empty($prop_data['second_half_point_spread_2_way'][0]['odds']))
                                <a href="#">OTB</a>
                                @else
                                @php
                                $prop_data['second_half_point_spread_2_way'][0]['odds'] =
                                _toAmericanDecimal($prop_data['second_half_point_spread_2_way'][0]['odds']);
                                $prop_data['second_half_point_spread_2_way'][0]['team_name'] = "2nd half";
                                $prop_data['second_half_point_spread_2_way'][0]['sub_type_name'] ="Spread";
                                $unique_id = $event_data->id."2st-half-22577-00-".$prop_data['second_half_point_spread_2_way'][0]['sub_type_name'];
                                $prop_data['second_half_point_spread_2_way'][0]['Unique_id'] = $unique_id;
                                $prop_data['second_half_point_spread_2_way'][0]['betting_wager'] = "Spread";
                                $prop_data['second_half_point_spread_2_way'][0]['row_line'] = 'home';
                                $prop_data['second_half_point_spread_2_way'][0]['handicap'] = $prop_data['second_half_point_spread_2_way'][0]['handicap'];
                                @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['second_half_point_spread_2_way'][0])}}','prop_bet')">
                                    ({{ $prop_data['second_half_point_spread_2_way'][0]['handicap'] }})
                                    {{ _preccedSign($prop_data['second_half_point_spread_2_way'][0]['odds'])}} </a>
                                @endif

                            </td>
                            <td data-th="Money Line" class=" ">
                                @if (empty($prop_data['second_half_money_line_2_way'][0]['odds']))
                                <div class="">
                                    <a href="#">
                                        OTB
                                    </a>
                                </div>
                                @else
                                <div class="">
                                    @php
                                        $prop_data['second_half_money_line_2_way'][0]['odds'] =  _toAmericanDecimal($prop_data['second_half_money_line_2_way'][0]['odds']);
                                        $prop_data['second_half_money_line_2_way'][0]['team_name'] = "2nd half";
                                        $prop_data['second_half_money_line_2_way'][0]['sub_type_name'] ="Money Line";
                                        $unique_id = $event_data->id."2st-half-33655-00-money-line";
                                        $prop_data['second_half_money_line_2_way'][0]['Unique_id'] = $unique_id;
                                        $prop_data['second_half_money_line_2_way'][0]['betting_wager'] = "Money Line";
                                        $prop_data['second_half_money_line_2_way'][0]['row_line'] = 'home';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['second_half_money_line_2_way'][0])}}','prop_bet')">
                                        {{ $prop_data['second_half_money_line_2_way'][0]['odds'] }}
                                    </a>
                                </div>
                                @endif

                            </td>
                            <td data-th="Total" class="">
                                @if (empty($prop_data['second_half_total_2_way'][0]['odds']))
                                <a href="#">
                                    OTB
                                </a>
                                @else
                                    @php
                                        $prop_data['second_half_total_2_way'][0]['odds'] =  _toAmericanDecimal($prop_data['second_half_total_2_way'][0]['odds']);
                                        $prop_data['second_half_total_2_way'][0]['team_name'] = "2nd half";
                                        $prop_data['second_half_total_2_way'][0]['sub_type_name'] ="Total";
                                        $unique_id = $event_data->id."2st-half-33299-00-total";
                                        $prop_data['second_half_total_2_way'][0]['Unique_id'] = $unique_id;
                                        $prop_data['second_half_total_2_way'][0]['betting_wager'] = "Total";
                                        $prop_data['second_half_total_2_way'][0]['row_line'] = 'home';
                                        $prop_data['second_half_total_2_way'][0]['handicap'] = $prop_data['second_half_total_2_way'][0]['handicap'];
                                    @endphp
                                        {{-- $prop_data['second_half_total_2_way'][0]['name']--}}
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['second_half_total_2_way'][0])}}','prop_bet')">
                                    ({{ $prop_data['second_half_total_2_way'][0]['name'] }}
                                    ){{ _preccedSign($prop_data['second_half_total_2_way'][0]['odds'])}}
                                </a>
                                @endif

                            </td>
                        </tr>
                        <tr class="mobilecss wordltable">

                            <td data-th="Player name" class="">
                                <!-- Each table heading name &  data-th wile be same -->

                                <div class="newtabls">
                                    <p>{{ $away_name }}</p>
                                </div>
                            </td>
                            <td data-th="Spread" class="">
                                @if (empty($prop_data['second_half_point_spread_2_way'][1]['odds']))
                                <a href="#"> OTB</a>
                                @else
                                    @php
                                        $prop_data['second_half_point_spread_2_way'][1]['odds'] =
                                        _toAmericanDecimal($prop_data['second_half_point_spread_2_way'][1]['odds']);
                                        $prop_data['second_half_point_spread_2_way'][1]['team_name'] = "2nd half";
                                        $prop_data['second_half_point_spread_2_way'][1]['sub_type_name'] ="Spread";
                                        $unique_id = $event_data->id."2st-half-22500-01-".$prop_data['second_half_point_spread_2_way'][1]['sub_type_name'];
                                        $prop_data['second_half_point_spread_2_way'][1]['Unique_id'] = $unique_id;
                                        $prop_data['second_half_point_spread_2_way'][1]['betting_wager'] = "Spread";
                                        $prop_data['second_half_point_spread_2_way'][1]['row_line'] = 'away';
                                        $prop_data['second_half_point_spread_2_way'][1]['handicap'] = $prop_data['second_half_point_spread_2_way'][1]['handicap'];
                                    @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['second_half_point_spread_2_way'][1])}}','prop_bet')">
                                     ({{ $prop_data['second_half_point_spread_2_way'][1]['handicap'] }})
                                    {{ _preccedSign($prop_data['second_half_point_spread_2_way'][1]['odds'])}} </a>
                                @endif

                            </td>
                            <td data-th="Money Line" class=" ">
                                @if (empty($prop_data['second_half_money_line_2_way'][1]['odds']))
                                <div class="">
                                    <a href="#">
                                        OTB
                                    </a>
                                </div>
                                @else
                                <div class="">
                                    @php
                                        $prop_data['second_half_money_line_2_way'][1]['odds'] =  _toAmericanDecimal($prop_data['second_half_money_line_2_way'][1]['odds']);
                                        $prop_data['second_half_money_line_2_way'][1]['team_name'] = "2nd half";
                                        $prop_data['second_half_money_line_2_way'][1]['sub_type_name'] ="Money Line";
                                        $unique_id = $event_data->id."2st-half-33600-01-money-line";
                                        $prop_data['second_half_money_line_2_way'][1]['Unique_id'] = $unique_id;
                                        $prop_data['second_half_money_line_2_way'][1]['betting_wager'] = "Money Line";
                                        $prop_data['second_half_money_line_2_way'][1]['row_line'] = 'away';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['second_half_money_line_2_way'][1])}}','prop_bet')">
                                        {{_preccedSign($prop_data['second_half_money_line_2_way'][1]['odds'])}}
                                    </a>
                                </div>
                                @endif

                            </td>
                            <td data-th="Total" class="">
                                @if (empty($prop_data['second_half_total_2_way'][1]['odds']))
                                <a href="#">
                                    OTB
                                </a>
                                @else
                                    @php
                                        $prop_data['second_half_total_2_way'][1]['odds'] =  _toAmericanDecimal($prop_data['second_half_total_2_way'][1]['odds']);
                                        $prop_data['second_half_total_2_way'][1]['team_name'] = "2nd half";
                                        $prop_data['second_half_total_2_way'][1]['sub_type_name'] ="Total";
                                        $unique_id = $event_data->id."2st-half-33200-01-total";
                                        $prop_data['second_half_total_2_way'][1]['Unique_id'] = $unique_id;
                                        $prop_data['second_half_total_2_way'][1]['betting_wager'] = "Total";
                                        $prop_data['second_half_total_2_way'][1]['row_line'] = 'away';
                                        $prop_data['second_half_total_2_way'][1]['handicap'] = $prop_data['second_half_total_2_way'][1]['handicap'];
                                    @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['second_half_total_2_way'][1])}}','prop_bet')">
                                    ({{ $prop_data['second_half_total_2_way'][1]['name'] }}
                                    ){{_preccedSign($prop_data['second_half_total_2_way'][1]['odds'])}}
                                </a>
                                @endif

                            </td>
                        </tr>
                    </tbody>
                </table>
                <!--2nd half-->
                @endif

                @if (!empty($prop_data['first_quarter_point_spread_2_way']) &&
                !empty($prop_data['first_quarter_money_line_2_way']) &&
                !empty($prop_data['first_quarter_total_2_way']))
                <!-- 1st Quarter -->
                {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">1st Quarter</h3>
                        <thead>
                        <tr>
                            <th>Player Name</th>
                            <th>Spread</th>
                            <th>Money Line</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="text-center">
                            <td>{{ $home_name }}</td>
                            <td>
                                @if (empty($prop_data['first_quarter_point_spread_2_way'][0]['odds']))
                                    <a href="#"> OTB </a>
                                @else
                                    @php
                                        $prop_data['first_quarter_point_spread_2_way'][0]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_point_spread_2_way'][0]['odds']);
                                        $prop_data['first_quarter_point_spread_2_way'][0]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_point_spread_2_way'][0]['sub_type_name'] ="Spread";
                                        $unique_id = $event_data->id."1st-quarter-66455-00-spread";
                                        $prop_data['first_quarter_point_spread_2_way'][0]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_point_spread_2_way'][0]['betting_wager'] = "Spread";
                                        $prop_data['first_quarter_point_spread_2_way'][0]['row_line'] = 'home';
                                        $prop_data['first_quarter_point_spread_2_way'][0]['handicap'] = $prop_data['first_quarter_point_spread_2_way'][0]['handicap'];
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_point_spread_2_way'][0])}}','prop_bet')">
                                        ({{ $prop_data['first_quarter_point_spread_2_way'][0]['handicap'] }})
                                        {{ _preccedSign($prop_data['first_quarter_point_spread_2_way'][0]['odds'])}} </a>
                                @endif
                            </td>
                            <td>
                                @if (empty($prop_data['first_quarter_money_line_2_way'][0]['odds']))
                                    <div class="">
                                        <a href="#">
                                            OTB
                                        </a>
                                    </div>
                                @else
                                    <div class="">
                                        @php
                                            $prop_data['first_quarter_money_line_2_way'][0]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_money_line_2_way'][0]['odds']);
                                            $prop_data['first_quarter_money_line_2_way'][0]['team_name'] = "1st Quarter";
                                            $prop_data['first_quarter_money_line_2_way'][0]['sub_type_name'] ="Money Line";
                                            $unique_id = $event_data->id."1st-quarter-66577-00-money-line";
                                            $prop_data['first_quarter_money_line_2_way'][0]['Unique_id'] = $unique_id;
                                            $prop_data['first_quarter_money_line_2_way'][0]['betting_wager'] = "Money Line";
                                            $prop_data['first_quarter_money_line_2_way'][0]['row_line'] = 'home';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_money_line_2_way'][0])}}','prop_bet')">
                                            {{_preccedSign($prop_data['first_quarter_money_line_2_way'][0]['odds'])}}
                                        </a>
                                    </div>
                                @endif

                            </td>
                            <td>
                                @if (empty($prop_data['first_quarter_total_2_way'][0]['odds']))
                                    <a href="#">
                                        OTB
                                    </a>
                                @else
                                    @php
                                        $prop_data['first_quarter_total_2_way'][0]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_total_2_way'][0]['odds']);
                                        $prop_data['first_quarter_total_2_way'][0]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_total_2_way'][0]['sub_type_name'] ="Total";
                                        $unique_id = $event_data->id."1st-quarter-66563-00-total";
                                        $prop_data['first_quarter_total_2_way'][0]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_total_2_way'][0]['betting_wager'] = "Total";
                                        $prop_data['first_quarter_total_2_way'][0]['row_line'] = 'home';
                                        $prop_data['first_quarter_total_2_way'][0]['handicap'] =  $prop_data['first_quarter_total_2_way'][0]['handicap'];
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_total_2_way'][0])}}','prop_bet')">
                                        ({{ $prop_data['first_quarter_total_2_way'][0]['name'] }}
                                        ){{_preccedSign($prop_data['first_quarter_total_2_way'][0]['odds'])}}
                                    </a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ $away_name  }}
                            </td>
                            <td>
                                @if (empty($prop_data['first_quarter_point_spread_2_way'][1]['odds']))
                                    <a href="#">OTB </a>
                                @else
                                    @php
                                        $prop_data['first_quarter_point_spread_2_way'][1]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_point_spread_2_way'][1]['odds']);
                                        $prop_data['first_quarter_point_spread_2_way'][1]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_point_spread_2_way'][1]['sub_type_name'] ="Spread";
                                        $unique_id = $event_data->id."1st-quarter-66411-00-spread";
                                        $prop_data['first_quarter_point_spread_2_way'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_point_spread_2_way'][1]['betting_wager'] = "Spread";
                                        $prop_data['first_quarter_point_spread_2_way'][1]['row_line'] = 'away';
                                        $prop_data['first_quarter_point_spread_2_way'][1]['handicap'] = $prop_data['first_quarter_point_spread_2_way'][1]['handicap'];
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_point_spread_2_way'][1])}}','prop_bet')">
                                        ({{ $prop_data['first_quarter_point_spread_2_way'][1]['handicap'] }})
                                        {{_preccedSign($prop_data['first_quarter_point_spread_2_way'][1]['odds'])}} </a>
                                @endif

                            </td>
                            <td>
                                @if (empty($prop_data['first_quarter_money_line_2_way'][1]['odds']))
                                    <div class="">
                                        <a href="#">
                                            OTB
                                        </a>
                                    </div>
                                @else
                                    <div class="">
                                        @php
                                            $prop_data['first_quarter_money_line_2_way'][1]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_money_line_2_way'][1]['odds']);
                                            $prop_data['first_quarter_money_line_2_way'][1]['team_name'] = "1st Quarter";
                                            $prop_data['first_quarter_money_line_2_way'][1]['sub_type_name'] ="Money Line";
                                            $unique_id = $event_data->id."1st-quarter-66599-01-money-line";
                                            $prop_data['first_quarter_money_line_2_way'][1]['Unique_id'] = $unique_id;
                                            $prop_data['first_quarter_money_line_2_way'][1]['betting_wager'] = "Money Line";
                                            $prop_data['first_quarter_money_line_2_way'][1]['row_line'] = 'away';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_money_line_2_way'][1])}}','prop_bet')">
                                            {{_preccedSign($prop_data['first_quarter_money_line_2_way'][1]['odds'])}}
                                        </a>
                                    </div>
                                @endif

                            </td>
                            <td>
                                @if (empty($prop_data['first_quarter_total_2_way'][1]['odds']))
                                    <a href="#">
                                        OTB
                                    </a>
                                @else
                                    @php
                                        $prop_data['first_quarter_total_2_way'][1]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_total_2_way'][1]['odds']);
                                        $prop_data['first_quarter_total_2_way'][1]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_total_2_way'][1]['sub_type_name'] ="Total";
                                        $unique_id = $event_data->id."1st-quarter-66536-01-total";
                                        $prop_data['first_quarter_total_2_way'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_total_2_way'][1]['betting_wager'] = "Total";
                                        $prop_data['first_quarter_total_2_way'][1]['row_line'] = 'away';
                                        $prop_data['first_quarter_total_2_way'][1]['handicap'] =  $prop_data['first_quarter_total_2_way'][1]['handicap'];
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_total_2_way'][1])}}','prop_bet')">
                                        ({{ $prop_data['first_quarter_total_2_way'][1]['name'] }}
                                        ){{ _preccedSign($prop_data['first_quarter_total_2_way'][1]['odds'])}}
                                    </a>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                {{--mobile-view--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767">1st Quarter</h3>
                    <tbody>
                        <!-- table heading name -->

                        <tr>
                            <th> </th> <!-- Each table heading name &  data-th wile be same -->
                            <th>Spread</th>
                            <th>Money Line</th>
                            <th>Total</th>
                        </tr>
                        <!-- table single column & row list -->
                        <tr class="mobilecss wordltable">

                            <td data-th="Player name" class="">
                                <!-- Each table heading name &  data-th wile be same -->

                                <div class="newtabls">
                                    <p>{{ $home_name }}</p>
                                </div>
                            </td>
                            <td data-th="Spread" class="">
                                @if (empty($prop_data['first_quarter_point_spread_2_way'][0]['odds']))
                                <a href="#"> OTB </a>
                                @else
                                    @php
                                        $prop_data['first_quarter_point_spread_2_way'][0]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_point_spread_2_way'][0]['odds']);
                                        $prop_data['first_quarter_point_spread_2_way'][0]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_point_spread_2_way'][0]['sub_type_name'] ="Spread";
                                        $unique_id = $event_data->id."1st-quarter-66455-00-spread";
                                        $prop_data['first_quarter_point_spread_2_way'][0]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_point_spread_2_way'][0]['betting_wager'] = "Spread";
                                        $prop_data['first_quarter_point_spread_2_way'][0]['row_line'] = 'home';
                                        $prop_data['first_quarter_point_spread_2_way'][0]['handicap'] = $prop_data['first_quarter_point_spread_2_way'][0]['handicap'];
                                    @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_point_spread_2_way'][0])}}','prop_bet')">
                                    ({{ $prop_data['first_quarter_point_spread_2_way'][0]['handicap'] }})
                                    {{ _preccedSign($prop_data['first_quarter_point_spread_2_way'][0]['odds'])}} </a>
                                @endif
                            </td>
                            <td data-th="Money Line" class=" ">
                                @if (empty($prop_data['first_quarter_money_line_2_way'][0]['odds']))
                                <div class="">
                                    <a href="#">
                                        OTB
                                    </a>
                                </div>
                                @else
                                <div class="">
                                    @php
                                        $prop_data['first_quarter_money_line_2_way'][0]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_money_line_2_way'][0]['odds']);
                                        $prop_data['first_quarter_money_line_2_way'][0]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_money_line_2_way'][0]['sub_type_name'] ="Money Line";
                                        $unique_id = $event_data->id."1st-quarter-66577-00-money-line";
                                        $prop_data['first_quarter_money_line_2_way'][0]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_money_line_2_way'][0]['betting_wager'] = "Money Line";
                                        $prop_data['first_quarter_money_line_2_way'][0]['row_line'] = 'home';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_money_line_2_way'][0])}}','prop_bet')">
                                        {{_preccedSign($prop_data['first_quarter_money_line_2_way'][0]['odds'])}}
                                    </a>
                                </div>
                                @endif

                            </td>
                            <td data-th="Total" class="">
                                @if (empty($prop_data['first_quarter_total_2_way'][0]['odds']))
                                <a href="#">
                                    OTB
                                </a>
                                @else
                                    @php
                                        $prop_data['first_quarter_total_2_way'][0]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_total_2_way'][0]['odds']);
                                        $prop_data['first_quarter_total_2_way'][0]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_total_2_way'][0]['sub_type_name'] ="Total";
                                        $unique_id = $event_data->id."1st-quarter-66563-00-total";
                                        $prop_data['first_quarter_total_2_way'][0]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_total_2_way'][0]['betting_wager'] = "Total";
                                        $prop_data['first_quarter_total_2_way'][0]['row_line'] = 'home';
                                        $prop_data['first_quarter_total_2_way'][0]['handicap'] =  $prop_data['first_quarter_total_2_way'][0]['handicap'];
                                    @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_total_2_way'][0])}}','prop_bet')">
                                    ({{ $prop_data['first_quarter_total_2_way'][0]['name'] }}
                                    ){{_preccedSign($prop_data['first_quarter_total_2_way'][0]['odds'])}}
                                </a>
                                @endif

                            </td>
                        </tr>
                        <tr class="mobilecss wordltable">

                            <td data-th="Player name" class="">
                                <!-- Each table heading name &  data-th wile be same -->

                                <div class="newtabls">
                                    <p>{{ $away_name }}</p>
                                </div>
                            </td>
                            <td data-th="Spread" class="">
                                @if (empty($prop_data['first_quarter_point_spread_2_way'][1]['odds']))
                                <a href="#">OTB </a>
                                @else
                                    @php
                                        $prop_data['first_quarter_point_spread_2_way'][1]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_point_spread_2_way'][1]['odds']);
                                        $prop_data['first_quarter_point_spread_2_way'][1]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_point_spread_2_way'][1]['sub_type_name'] ="Spread";
                                        $unique_id = $event_data->id."1st-quarter-66411-00-spread";
                                        $prop_data['first_quarter_point_spread_2_way'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_point_spread_2_way'][1]['betting_wager'] = "Spread";
                                        $prop_data['first_quarter_point_spread_2_way'][1]['row_line'] = 'away';
                                        $prop_data['first_quarter_point_spread_2_way'][1]['handicap'] = $prop_data['first_quarter_point_spread_2_way'][1]['handicap'];
                                    @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_point_spread_2_way'][1])}}','prop_bet')">
                                    ({{ $prop_data['first_quarter_point_spread_2_way'][1]['handicap'] }})
                                    {{_preccedSign($prop_data['first_quarter_point_spread_2_way'][1]['odds'])}} </a>
                                @endif

                            </td>
                            <td data-th="Money Line" class=" ">
                                @if (empty($prop_data['first_quarter_money_line_2_way'][1]['odds']))
                                <div class="">
                                    <a href="#">
                                        OTB
                                    </a>
                                </div>
                                @else
                                <div class="">
                                    @php
                                        $prop_data['first_quarter_money_line_2_way'][1]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_money_line_2_way'][1]['odds']);
                                        $prop_data['first_quarter_money_line_2_way'][1]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_money_line_2_way'][1]['sub_type_name'] ="Money Line";
                                        $unique_id = $event_data->id."1st-quarter-66599-01-money-line";
                                        $prop_data['first_quarter_money_line_2_way'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_money_line_2_way'][1]['betting_wager'] = "Money Line";
                                        $prop_data['first_quarter_money_line_2_way'][1]['row_line'] = 'away';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_money_line_2_way'][1])}}','prop_bet')">
                                        {{_preccedSign($prop_data['first_quarter_money_line_2_way'][1]['odds'])}}
                                    </a>
                                </div>
                                @endif

                            </td>
                            <td data-th="Total" class="">
                                @if (empty($prop_data['first_quarter_total_2_way'][1]['odds']))
                                <a href="#">
                                    OTB
                                </a>
                                @else
                                    @php
                                        $prop_data['first_quarter_total_2_way'][1]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_total_2_way'][1]['odds']);
                                        $prop_data['first_quarter_total_2_way'][1]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_total_2_way'][1]['sub_type_name'] ="Total";
                                        $unique_id = $event_data->id."1st-quarter-66536-01-total";
                                        $prop_data['first_quarter_total_2_way'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_total_2_way'][1]['betting_wager'] = "Total";
                                        $prop_data['first_quarter_total_2_way'][1]['row_line'] = 'away';
                                        $prop_data['first_quarter_total_2_way'][1]['handicap'] =  $prop_data['first_quarter_total_2_way'][1]['handicap'];
                                    @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_total_2_way'][1])}}','prop_bet')">
                                    ({{ $prop_data['first_quarter_total_2_way'][1]['name'] }}
                                    ){{ _preccedSign($prop_data['first_quarter_total_2_way'][1]['odds'])}}
                                </a>
                                @endif

                            </td>
                        </tr>

                    </tbody>
                </table>
                <!-- 1st Quarter -->
                @endif

                @elseif($prop_bet_details['prop_data']->sport_id == 18)

                @if (!empty($prop_data['first_half_spread']) && !empty($prop_data['first_half_money_line']) &&
                !empty($prop_data['first_half_totals']))
                <!-- 1st half -->
                    {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">1st half</h3>
                        <thead>
                        <tr>
                            <th>Player Name</th>
                            <th>Spread</th>
                            <th>Money Line</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="text-center">
                            <td>{{ $home_name }}</td>
                            <td data-th="Spread" class="">
                                @if (empty($prop_data['first_half_spread'][0]['odds']))
                                    <a href="#">OTB</a>
                                @else
                                    @php
                                        $prop_data['first_half_spread'][0]['odds'] =  _toAmericanDecimal($prop_data['first_half_spread'][0]['odds']);
                                        $prop_data['first_half_spread'][0]['team_name'] = "1st half";
                                        $prop_data['first_half_spread'][0]['sub_type_name'] ="Spread";
                                        $unique_id = $event_data->id."1st-half-005544-00-spread";
                                        $prop_data['first_half_spread'][0]['Unique_id'] = $unique_id;
                                        $prop_data['first_half_spread'][0]['betting_wager'] = "Spread";
                                        $prop_data['first_half_spread'][0]['row_line'] = 'home';
                                        $prop_data['first_half_spread'][0]['handicap'] =  $prop_data['first_half_spread'][0]['handicap'];
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_spread'][0])}}','prop_bet')">
                                        ({{ $prop_data['first_half_spread'][0]['handicap'] }})
                                        {{_preccedSign($prop_data['first_half_spread'][0]['odds'])}} </a>
                                @endif

                            </td>
                            <td data-th="Money Line" class=" ">
                                <div class="">
                                    @if (empty($prop_data['first_half_money_line'][0]['odds']))
                                        <a href="#">OTB</a>
                                    @else
                                        @php
                                            $prop_data['first_half_money_line'][0]['odds'] =  _toAmericanDecimal($prop_data['first_half_money_line'][0]['odds']);
                                            $prop_data['first_half_money_line'][0]['team_name'] = "1st half";
                                            $prop_data['first_half_money_line'][0]['sub_type_name'] ="Money Line";
                                            $unique_id = $event_data->id."1st-half-008833-00-money-line";
                                            $prop_data['first_half_money_line'][0]['Unique_id'] = $unique_id;
                                            $prop_data['first_half_money_line'][0]['betting_wager'] = "Money Line";
                                            $prop_data['first_half_money_line'][0]['row_line'] = 'home';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_money_line'][0])}}','prop_bet')">
                                            {{_preccedSign($prop_data['first_half_money_line'][0]['odds'])}}
                                        </a>
                                    @endif

                                </div>
                            </td>
                            <td data-th="Total" class="">
                                @if (empty($prop_data['first_half_totals'][0]['odds']))
                                    <a href="#">OTB</a>
                                @else
                                    @php
                                        $prop_data['first_half_totals'][0]['odds'] =  _toAmericanDecimal($prop_data['first_half_totals'][0]['odds']);
                                        $prop_data['first_half_totals'][0]['team_name'] = "1st half";
                                        $prop_data['first_half_totals'][0]['sub_type_name'] ="Total";
                                        $unique_id = $event_data->id."1st-half-002277-00-total";
                                        $prop_data['first_half_totals'][0]['Unique_id'] = $unique_id;
                                        $prop_data['first_half_totals'][0]['betting_wager'] = "Total";
                                        $prop_data['first_half_totals'][0]['row_line'] = 'home';
                                        $prop_data['first_half_totals'][0]['handicap'] =  $prop_data['first_half_totals'][0]['name'];
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_totals'][0])}}','prop_bet')">
                                        ({{ $prop_data['first_half_totals'][0]['header'] . $prop_data['first_half_totals'][0]['name'] }}
                                        ){{_preccedSign($prop_data['first_half_totals'][0]['odds'])}}
                                    </a>
                                @endif

                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ $away_name  }}
                            </td>
                            <td data-th="Spread" class="">
                                @if (empty($prop_data['first_half_spread'][1]['odds']))
                                    <a href="#">OTB</a>
                                @else
                                    @php
                                        $prop_data['first_half_spread'][1]['odds'] =  _toAmericanDecimal($prop_data['first_half_spread'][1]['odds']);
                                        $prop_data['first_half_spread'][1]['team_name'] = "1st half";
                                        $prop_data['first_half_spread'][1]['sub_type_name'] ="Spread";
                                        $unique_id = $event_data->id."1st-half-005599-01-spread";
                                        $prop_data['first_half_spread'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_half_totals'][1]['betting_wager'] = "Spread";
                                        $prop_data['first_half_totals'][1]['row_line'] = 'away';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_spread'][1])}}','prop_bet')">
                                        ({{ $prop_data['first_half_spread'][1]['handicap'] }})
                                        {{_preccedSign($prop_data['first_half_spread'][1]['odds'])}} </a>
                                @endif

                            </td>
                            <td data-th="Money Line" class=" ">
                                @if (empty($prop_data['first_half_money_line'][1]['odds']))
                                    <div><a href="#">OTB</a></div>
                                @else
                                    <div class="">
                                        @php
                                            $prop_data['first_half_money_line'][1]['odds'] =  _toAmericanDecimal($prop_data['first_half_money_line'][1]['odds']);
                                            $prop_data['first_half_money_line'][1]['team_name'] = "1st half";
                                            $prop_data['first_half_money_line'][1]['sub_type_name'] ="Money Line";
                                            $unique_id = $event_data->id."1st-half-008800-01-money-line";
                                            $prop_data['first_half_money_line'][1]['Unique_id'] = $unique_id;
                                            $prop_data['first_half_money_line'][1]['betting_wager'] = "Money Line";
                                            $prop_data['first_half_money_line'][1]['row_line'] = 'away';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_money_line'][1])}}','prop_bet')">
                                            {{_preccedSign($prop_data['first_half_money_line'][1]['odds'])}} </a>
                                    </div>
                                @endif

                            </td>
                            <td data-th="Total" class="">
                                @if(empty($prop_data['first_half_totals'][1]['odds']))
                                    <a >OTB</a>
                                @else
                                    @php
                                        $prop_data['first_half_totals'][1]['odds'] =  _toAmericanDecimal($prop_data['first_half_totals'][1]['odds']);
                                        $prop_data['first_half_totals'][1]['team_name'] = "1st half";
                                        $prop_data['first_half_totals'][1]['sub_type_name'] ="Total";
                                        $unique_id = $event_data->id."1st-half-008833-01-total";
                                        $prop_data['first_half_totals'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_half_totals'][1]['betting_wager'] = "Total";
                                        $prop_data['first_half_totals'][1]['row_line'] = 'away';
                                        $prop_data['first_half_totals'][1]['handicap'] =  $prop_data['first_half_totals'][1]['name'];
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_totals'][1])}}','prop_bet')">
                                        ({{ $prop_data['first_half_totals'][1]['header'] . $prop_data['first_half_totals'][1]['name'] }}
                                        ){{_preccedSign($prop_data['first_half_totals'][1]['odds'])}}
                                    </a>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    {{--mobile-view end--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767">1st half</h3>
                    <tbody>
                        <!-- table heading name -->

                        <tr>
                            <th> </th> <!-- Each table heading name &  data-th wile be same -->
                            <th>Spread</th>
                            <th>Money Line</th>
                            <th>Total</th>
                        </tr>
                        <!-- table single column & row list -->
                        <tr class="mobilecss wordltable">

                            <td data-th="Player name" class="">
                                <!-- Each table heading name &  data-th wile be same -->

                                <div class="newtabls">
                                    <p>{{ $home_name }}</p>
                                </div>
                            </td>
                            <td data-th="Spread" class="">
                                @if (empty($prop_data['first_half_spread'][0]['odds']))
                                <a href="#">OTB</a>
                                @else
                                    @php
                                        $prop_data['first_half_spread'][0]['odds'] =  _toAmericanDecimal($prop_data['first_half_spread'][0]['odds']);
                                        $prop_data['first_half_spread'][0]['team_name'] = "1st half";
                                        $prop_data['first_half_spread'][0]['sub_type_name'] ="Spread";
                                        $unique_id = $event_data->id."1st-half-005544-00-spread";
                                        $prop_data['first_half_spread'][0]['Unique_id'] = $unique_id;
                                        $prop_data['first_half_spread'][0]['betting_wager'] = "Spread";
                                        $prop_data['first_half_spread'][0]['row_line'] = 'home';
                                        $prop_data['first_half_spread'][0]['handicap'] =  $prop_data['first_half_spread'][0]['handicap'];
                                    @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_spread'][0])}}','prop_bet')">
                                    ({{ $prop_data['first_half_spread'][0]['handicap'] }})
                                    {{_preccedSign($prop_data['first_half_spread'][0]['odds'])}} </a>
                                @endif

                            </td>
                            <td data-th="Money Line" class=" ">
                                <div class="">
                                    @if (empty($prop_data['first_half_money_line'][0]['odds']))
                                    <a href="#">OTB</a>
                                    @else
                                        @php
                                            $prop_data['first_half_money_line'][0]['odds'] =  _toAmericanDecimal($prop_data['first_half_money_line'][0]['odds']);
                                            $prop_data['first_half_money_line'][0]['team_name'] = "1st half";
                                            $prop_data['first_half_money_line'][0]['sub_type_name'] ="Money Line";
                                            $unique_id = $event_data->id."1st-half-008833-00-money-line";
                                            $prop_data['first_half_money_line'][0]['Unique_id'] = $unique_id;
                                            $prop_data['first_half_money_line'][0]['betting_wager'] = "Money Line";
                                            $prop_data['first_half_money_line'][0]['row_line'] = 'home';
                                        @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_money_line'][0])}}','prop_bet')">
                                        {{_preccedSign($prop_data['first_half_money_line'][0]['odds'])}}
                                    </a>
                                    @endif

                                </div>
                            </td>
                            <td data-th="Total" class="">
                                @if (empty($prop_data['first_half_totals'][0]['odds']))
                                <a href="#">OTB</a>
                                @else
                                    @php
                                        $prop_data['first_half_totals'][0]['odds'] =  _toAmericanDecimal($prop_data['first_half_totals'][0]['odds']);
                                        $prop_data['first_half_totals'][0]['team_name'] = "1st half";
                                        $prop_data['first_half_totals'][0]['sub_type_name'] ="Total";
                                        $unique_id = $event_data->id."1st-half-002277-00-total";
                                        $prop_data['first_half_totals'][0]['Unique_id'] = $unique_id;
                                        $prop_data['first_half_totals'][0]['betting_wager'] = "Total";
                                        $prop_data['first_half_totals'][0]['row_line'] = 'home';
                                        $prop_data['first_half_totals'][0]['handicap'] =  $prop_data['first_half_totals'][0]['name'];
                                    @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_totals'][0])}}','prop_bet')">
                                    ({{ $prop_data['first_half_totals'][0]['header'] . $prop_data['first_half_totals'][0]['name'] }}
                                    ){{_preccedSign($prop_data['first_half_totals'][0]['odds'])}}
                                </a>
                                @endif

                            </td>
                        </tr>
                        <tr class="mobilecss wordltable">

                            <td data-th="Player name" class="">
                                <!-- Each table heading name &  data-th wile be same -->
                                <div class="newtabls">
                                    <p>{{ $away_name  }}</p>
                                </div>
                            </td>
                            <td data-th="Spread" class="">
                                @if (empty($prop_data['first_half_spread'][1]['odds']))
                                <a href="#">OTB</a>
                                @else
                                    @php
                                        $prop_data['first_half_spread'][1]['odds'] =  _toAmericanDecimal($prop_data['first_half_spread'][1]['odds']);
                                        $prop_data['first_half_spread'][1]['team_name'] = "1st half";
                                        $prop_data['first_half_spread'][1]['sub_type_name'] ="Spread";
                                        $unique_id = $event_data->id."1st-half-005599-01-spread";
                                        $prop_data['first_half_spread'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_half_totals'][1]['betting_wager'] = "Spread";
                                        $prop_data['first_half_totals'][1]['row_line'] = 'away';
                                    @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_spread'][1])}}','prop_bet')">
                                    ({{ $prop_data['first_half_spread'][1]['handicap'] }})
                                    {{_preccedSign($prop_data['first_half_spread'][1]['odds'])}} </a>
                                @endif

                            </td>
                            <td data-th="Money Line" class=" ">
                                @if (empty($prop_data['first_half_money_line'][1]['odds']))
                                <div><a href="#">OTB</a></div>
                                @else
                                <div class="">
                                    @php
                                        $prop_data['first_half_money_line'][1]['odds'] =  _toAmericanDecimal($prop_data['first_half_money_line'][1]['odds']);
                                        $prop_data['first_half_money_line'][1]['team_name'] = "1st half";
                                        $prop_data['first_half_money_line'][1]['sub_type_name'] ="Money Line";
                                        $unique_id = $event_data->id."1st-half-008800-01-money-line";
                                        $prop_data['first_half_money_line'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_half_money_line'][1]['betting_wager'] = "Money Line";
                                        $prop_data['first_half_money_line'][1]['row_line'] = 'away';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_money_line'][1])}}','prop_bet')">
                                        {{_preccedSign($prop_data['first_half_money_line'][1]['odds'])}} </a>
                                </div>
                                @endif

                            </td>
                            <td data-th="Total" class="">
                                @if(empty($prop_data['first_half_totals'][1]['odds']))
                                    <a >OTB</a>
                                    @else
                                    @php
                                        $prop_data['first_half_totals'][1]['odds'] =  _toAmericanDecimal($prop_data['first_half_totals'][1]['odds']);
                                        $prop_data['first_half_totals'][1]['team_name'] = "1st half";
                                        $prop_data['first_half_totals'][1]['sub_type_name'] ="Total";
                                        $unique_id = $event_data->id."1st-half-008833-01-total";
                                        $prop_data['first_half_totals'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_half_totals'][1]['betting_wager'] = "Total";
                                        $prop_data['first_half_totals'][1]['row_line'] = 'away';
                                        $prop_data['first_half_totals'][1]['handicap'] =  $prop_data['first_half_totals'][1]['name'];
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_half_totals'][1])}}','prop_bet')">
                                        ({{ $prop_data['first_half_totals'][1]['header'] . $prop_data['first_half_totals'][1]['name'] }}
                                        ){{_preccedSign($prop_data['first_half_totals'][1]['odds'])}}
                                    </a>
                                    @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- 1st half -->
                @endif

                @if (!empty($prop_data['first_quarter_spread']) && !empty($prop_data['first_quarter_money_line']) &&
                !empty($prop_data['first_quarter_totals']))
                <!-- 1st Quarter -->
                    {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">1st Quarter</h3>
                        <thead>
                        <tr>
                            <th>Player Name</th>
                            <th>Spread</th>
                            <th>Money Line</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="text-center">
                            <td>{{ $home_name }}</td>
                            <td data-th="Spread" class="">
                                @if (empty($prop_data['first_quarter_spread'][0]['odds']))
                                    <a href="#"> OTB </a>
                                @else
                                    @php
                                        $prop_data['first_quarter_spread'][0]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_spread'][0]['odds']);
                                        $prop_data['first_quarter_spread'][0]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_spread'][0]['sub_type_name'] ="Spread";
                                        $unique_id = $event_data->id."1st-quarter-46641-00-spread";
                                        $prop_data['first_quarter_spread'][0]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_spread'][0]['betting_wager'] = "Spread";
                                        $prop_data['first_quarter_spread'][0]['row_line'] = 'home';
                                        $prop_data['first_quarter_spread'][0]['handicap'] =  $prop_data['first_quarter_spread'][0]['handicap'];
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_spread'][0])}}','prop_bet')">
                                        ({{ $prop_data['first_quarter_spread'][0]['handicap'] }})
                                        {{_preccedSign($prop_data['first_quarter_spread'][0]['odds'])}}
                                    </a>
                                @endif
                            </td>
                            <td data-th="Money Line" class=" ">
                                @if (empty($prop_data['first_quarter_money_line'][0]['odds']))
                                    <div class="">
                                        <a href="#">
                                            OTB
                                        </a>
                                    </div>
                                @else
                                    <div class="">
                                        @php
                                            $prop_data['first_quarter_money_line'][0]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_money_line'][0]['odds']);
                                            $prop_data['first_quarter_money_line'][0]['team_name'] = "1st Quarter";
                                            $prop_data['first_quarter_money_line'][0]['sub_type_name'] ="Money Line";
                                            $unique_id = $event_data->id."1st-quarter-44041-00-money-line";
                                            $prop_data['first_quarter_money_line'][0]['Unique_id'] = $unique_id;
                                            $prop_data['first_quarter_money_line'][0]['betting_wager'] = "Money Line";
                                            $prop_data['first_quarter_money_line'][0]['row_line'] = 'home';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_money_line'][0])}}','prop_bet')">
                                            {{_preccedSign($prop_data['first_quarter_money_line'][0]['odds'])}}
                                        </a>
                                    </div>
                                @endif

                            </td>
                            <td data-th="Total" class="">
                                @if (empty($prop_data['first_quarter_totals'][0]['odds']))
                                    <a href="#">
                                        OTB
                                    </a>
                                @else
                                    @php
                                        $prop_data['first_quarter_totals'][0]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_totals'][0]['odds']);
                                        $prop_data['first_quarter_totals'][0]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_totals'][0]['sub_type_name'] ="Total";
                                        $unique_id = $event_data->id."1st-quarter-44963-00-total";
                                        $prop_data['first_quarter_totals'][0]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_totals'][0]['betting_wager'] = "Total";
                                        $prop_data['first_quarter_totals'][0]['row_line'] = 'home';
                                        $prop_data['first_quarter_totals'][0]['handicap'] =  $prop_data['first_quarter_totals'][0]['name'];
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_totals'][0])}}','prop_bet')">
                                        ({{ $prop_data['first_quarter_totals'][0]['header'] . $prop_data['first_quarter_totals'][0]['name'] }}
                                        ){{ _preccedSign($prop_data['first_quarter_totals'][0]['odds'])}}
                                    </a>
                                @endif

                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{ $away_name  }}
                            </td>
                            <td data-th="Spread" class="">
                                @if (empty($prop_data['first_quarter_spread'][1]['odds']))
                                    <a href="#">OTB </a>
                                @else
                                    @php
                                        $prop_data['first_quarter_spread'][1]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_spread'][1]['odds']);
                                        $prop_data['first_quarter_spread'][1]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_spread'][1]['sub_type_name'] ="Spread";
                                        $unique_id = $event_data->id."1st-quarter-46651-01-spread";
                                        $prop_data['first_quarter_spread'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_spread'][1]['betting_wager'] = "Spread";
                                        $prop_data['first_quarter_spread'][1]['row_line'] = 'away';
                                        $prop_data['first_quarter_spread'][1]['handicap'] =  $prop_data['first_quarter_spread'][1]['handicap'];
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_spread'][1])}}','prop_bet')">
                                        ({{ $prop_data['first_quarter_spread'][1]['handicap'] }})
                                        {{_preccedSign($prop_data['first_quarter_spread'][1]['odds'])}} </a>
                                @endif

                            </td>
                            <td data-th="Money Line" class=" ">
                                @if (empty($prop_data['first_quarter_money_line'][1]['odds']))
                                    <div class="">
                                        <a href="#">
                                            OTB
                                        </a>
                                    </div>
                                @else
                                    <div class="">
                                        @php
                                            $prop_data['first_quarter_money_line'][1]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_money_line'][1]['odds']);
                                            $prop_data['first_quarter_money_line'][1]['team_name'] = "1st Quarter";
                                            $prop_data['first_quarter_money_line'][1]['sub_type_name'] ="Money Line";
                                            $unique_id = $event_data->id."1st-quarter-44051-01-money-line";
                                            $prop_data['first_quarter_money_line'][1]['Unique_id'] = $unique_id;
                                            $prop_data['first_quarter_money_line'][1]['betting_wager'] = "Money Line";
                                            $prop_data['first_quarter_money_line'][1]['row_line'] = 'away';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_money_line'][1])}}','prop_bet')">
                                            {{_preccedSign($prop_data['first_quarter_money_line'][1]['odds'])}}
                                        </a>
                                    </div>
                                @endif
                            </td>
                            <td data-th="Total" class="">
                                @if (empty($prop_data['first_quarter_totals'][1]['odds']))
                                    <a href="#">
                                        OTB
                                    </a>
                                @else
                                    @php
                                        $prop_data['first_quarter_totals'][1]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_totals'][1]['odds']);
                                        $prop_data['first_quarter_totals'][1]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_totals'][1]['sub_type_name'] ="Total";
                                        $unique_id = $event_data->id."1st-quarter-44369-01-total";
                                        $prop_data['first_quarter_totals'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_totals'][1]['betting_wager'] = "Total";
                                        $prop_data['first_quarter_totals'][1]['row_line'] = 'away';
                                        $prop_data['first_quarter_totals'][1]['handicap'] =  $prop_data['first_quarter_totals'][1]['name'];
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_totals'][1])}}','prop_bet')">
                                        ({{ $prop_data['first_quarter_totals'][1]['header'] . $prop_data['first_quarter_totals'][1]['name'] }}
                                        ){{ _preccedSign($prop_data['first_quarter_totals'][1]['odds'])}}
                                    </a>
                                @endif

                            </td>
                        </tr>
                        </tbody>
                    </table>
                    {{--mobile-view end--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767">1st Quarter</h3>
                    <tbody>
                        <!-- table heading name -->

                        <tr>
                            <th> </th> <!-- Each table heading name &  data-th wile be same -->
                            <th>Spread</th>
                            <th>Money Line</th>
                            <th>Total</th>
                        </tr>
                        <!-- table single column & row list -->
                        <tr class="mobilecss wordltable">
                            <td data-th="Player name" class="">
                                <!-- Each table heading name &  data-th wile be same -->

                                <div class="newtabls">
                                    <p>{{ $home_name }}</p>
                                </div>
                            </td>
                            <td data-th="Spread" class="">
                                @if (empty($prop_data['first_quarter_spread'][0]['odds']))
                                <a href="#"> OTB </a>
                                @else
                                    @php
                                        $prop_data['first_quarter_spread'][0]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_spread'][0]['odds']);
                                        $prop_data['first_quarter_spread'][0]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_spread'][0]['sub_type_name'] ="Spread";
                                        $unique_id = $event_data->id."1st-quarter-46641-00-spread";
                                        $prop_data['first_quarter_spread'][0]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_spread'][0]['betting_wager'] = "Spread";
                                        $prop_data['first_quarter_spread'][0]['row_line'] = 'home';
                                        $prop_data['first_quarter_spread'][0]['handicap'] =  $prop_data['first_quarter_spread'][0]['handicap'];
                                    @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_spread'][0])}}','prop_bet')">
                                    ({{ $prop_data['first_quarter_spread'][0]['handicap'] }})
                                    {{_preccedSign($prop_data['first_quarter_spread'][0]['odds'])}}
                                </a>
                                @endif
                            </td>
                            <td data-th="Money Line" class=" ">
                                @if (empty($prop_data['first_quarter_money_line'][0]['odds']))
                                <div class="">
                                    <a href="#">
                                        OTB
                                    </a>
                                </div>
                                @else
                                <div class="">
                                    @php
                                        $prop_data['first_quarter_money_line'][0]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_money_line'][0]['odds']);
                                        $prop_data['first_quarter_money_line'][0]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_money_line'][0]['sub_type_name'] ="Money Line";
                                        $unique_id = $event_data->id."1st-quarter-44041-00-money-line";
                                        $prop_data['first_quarter_money_line'][0]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_money_line'][0]['betting_wager'] = "Money Line";
                                        $prop_data['first_quarter_money_line'][0]['row_line'] = 'home';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_money_line'][0])}}','prop_bet')">
                                        {{_preccedSign($prop_data['first_quarter_money_line'][0]['odds'])}}
                                    </a>
                                </div>
                                @endif

                            </td>
                            <td data-th="Total" class="">
                                @if (empty($prop_data['first_quarter_totals'][0]['odds']))
                                <a href="#">
                                    OTB
                                </a>
                                @else
                                    @php
                                        $prop_data['first_quarter_totals'][0]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_totals'][0]['odds']);
                                        $prop_data['first_quarter_totals'][0]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_totals'][0]['sub_type_name'] ="Total";
                                        $unique_id = $event_data->id."1st-quarter-44963-00-total";
                                        $prop_data['first_quarter_totals'][0]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_totals'][0]['betting_wager'] = "Total";
                                        $prop_data['first_quarter_totals'][0]['row_line'] = 'home';
                                        $prop_data['first_quarter_totals'][0]['handicap'] =  $prop_data['first_quarter_totals'][0]['name'];
                                    @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_totals'][0])}}','prop_bet')">
                                    ({{ $prop_data['first_quarter_totals'][0]['header'] . $prop_data['first_quarter_totals'][0]['name'] }}
                                    ){{ _preccedSign($prop_data['first_quarter_totals'][0]['odds'])}}
                                </a>
                                @endif

                            </td>
                        </tr>
                        <tr class="mobilecss wordltable">

                            <td data-th="Player name" class="">
                                <!-- Each table heading name &  data-th wile be same -->

                                <div class="newtabls">
                                    <p>{{ $away_name }}</p>
                                </div>
                            </td>
                            <td data-th="Spread" class="">
                                @if (empty($prop_data['first_quarter_spread'][1]['odds']))
                                <a href="#">OTB </a>
                                @else
                                    @php
                                        $prop_data['first_quarter_spread'][1]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_spread'][1]['odds']);
                                        $prop_data['first_quarter_spread'][1]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_spread'][1]['sub_type_name'] ="Spread";
                                        $unique_id = $event_data->id."1st-quarter-46651-01-spread";
                                        $prop_data['first_quarter_spread'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_spread'][1]['betting_wager'] = "Spread";
                                        $prop_data['first_quarter_spread'][1]['row_line'] = 'away';
                                        $prop_data['first_quarter_spread'][1]['handicap'] =  $prop_data['first_quarter_spread'][1]['handicap'];
                                    @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_spread'][1])}}','prop_bet')">
                                    ({{ $prop_data['first_quarter_spread'][1]['handicap'] }})
                                    {{_preccedSign($prop_data['first_quarter_spread'][1]['odds'])}} </a>
                                @endif

                            </td>
                            <td data-th="Money Line" class=" ">
                                @if (empty($prop_data['first_quarter_money_line'][1]['odds']))
                                <div class="">
                                    <a href="#">
                                        OTB
                                    </a>
                                </div>
                                @else
                                <div class="">
                                    @php
                                        $prop_data['first_quarter_money_line'][1]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_money_line'][1]['odds']);
                                        $prop_data['first_quarter_money_line'][1]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_money_line'][1]['sub_type_name'] ="Money Line";
                                        $unique_id = $event_data->id."1st-quarter-44051-01-money-line";
                                        $prop_data['first_quarter_money_line'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_money_line'][1]['betting_wager'] = "Money Line";
                                        $prop_data['first_quarter_money_line'][1]['row_line'] = 'away';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_money_line'][1])}}','prop_bet')">
                                        {{_preccedSign($prop_data['first_quarter_money_line'][1]['odds'])}}
                                    </a>
                                </div>
                                @endif
                            </td>
                            <td data-th="Total" class="">
                                @if (empty($prop_data['first_quarter_totals'][1]['odds']))
                                <a href="#">
                                    OTB
                                </a>
                                @else
                                    @php
                                        $prop_data['first_quarter_totals'][1]['odds'] =  _toAmericanDecimal($prop_data['first_quarter_totals'][1]['odds']);
                                        $prop_data['first_quarter_totals'][1]['team_name'] = "1st Quarter";
                                        $prop_data['first_quarter_totals'][1]['sub_type_name'] ="Total";
                                        $unique_id = $event_data->id."1st-quarter-44369-01-total";
                                        $prop_data['first_quarter_totals'][1]['Unique_id'] = $unique_id;
                                        $prop_data['first_quarter_totals'][1]['betting_wager'] = "Total";
                                        $prop_data['first_quarter_totals'][1]['row_line'] = 'away';
                                        $prop_data['first_quarter_totals'][1]['handicap'] =  $prop_data['first_quarter_totals'][1]['name'];
                                    @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['first_quarter_totals'][1])}}','prop_bet')">
                                    ({{ $prop_data['first_quarter_totals'][1]['header'] . $prop_data['first_quarter_totals'][1]['name'] }}
                                    ){{ _preccedSign($prop_data['first_quarter_totals'][1]['odds'])}}
                                </a>
                                @endif

                            </td>
                        </tr>

                    </tbody>
                </table>
                <!-- 1st Quarter -->
                @endif


                @if (!empty($prop_data['race_to_20_points']))
                <!-- race_to_20_points -->
                    {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">Race To 20 points</h3>
                        <thead>
                        <tr>
                            <th>{{ $home_name }}</th>
                            <th>{{ $away_name }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="text-center">
                            <td data-th="{{ $home_name }}" class=" ">
                                @if (empty($prop_data['race_to_20_points'][0]['odds']))
                                    <div class="">
                                        <a href="#"> OTB </a>
                                    </div>
                                @else
                                    <div class="">
                                        @php
                                            $prop_data['race_to_20_points'][0]['odds'] =  _toAmericanDecimal($prop_data['race_to_20_points'][0]['odds']);
                                            $prop_data['race_to_20_points'][0]['team_name'] = "Race To 20 points";
                                            $prop_data['race_to_20_points'][0]['sub_type_name'] = $home_name;
                                            $unique_id = $event_data->id."-race-to-20-points-50163-00".str_replace(['.', '_', ' ', '--','(',')'], '-',$prop_data['race_to_20_points'][0]['sub_type_name']);
                                            $prop_data['race_to_20_points'][0]['Unique_id'] = $unique_id;
                                            $prop_data['race_to_20_points'][0]['betting_wager'] = $home_name;
                                            $prop_data['race_to_20_points'][0]['row_line'] = 'home';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['race_to_20_points'][0])}}','prop_bet')">
                                            {{ _preccedSign($prop_data['race_to_20_points'][0]['odds']) }}
                                        </a>
                                    </div>
                                @endif
                            </td>
                            <td data-th="{{ $away_name }}" class="">
                                @if (empty($prop_data['race_to_20_points'][1]['odds']))
                                    <a href="#"> OTB </a>
                                @else
                                    @php
                                        $prop_data['race_to_20_points'][1]['odds'] =  _toAmericanDecimal($prop_data['race_to_20_points'][1]['odds']);
                                        $prop_data['race_to_20_points'][1]['team_name'] = "Race To 20 points";
                                        $prop_data['race_to_20_points'][1]['sub_type_name'] = $away_name;
                                        $unique_id = $event_data->id."-race-to-20-points-78785-01".str_replace(['.', '_', ' ', '--','(',')'], '-',$prop_data['race_to_20_points'][1]['sub_type_name']);
                                        $prop_data['race_to_20_points'][1]['Unique_id'] = $unique_id;
                                        $prop_data['race_to_20_points'][1]['betting_wager'] = $away_name;
                                        $prop_data['race_to_20_points'][1]['row_line'] = 'away';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['race_to_20_points'][1])}}','prop_bet')">
                                        {{_preccedSign($prop_data['race_to_20_points'][1]['odds'])}}
                                    </a>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    {{--mobile-view end--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767">Race To 20 points</h3>
                    <tbody class="rays">
                        <!-- table heading name -->

                        <tr>
                            <th>{{ $home_name }}</th>
                            <th>{{ $away_name }}</th>
                        </tr>

                        <!-- table single column & row list -->
                        <tr class="mobilecss wordltable">
                            <td data-th="{{ $home_name }}" class=" ">
                                @if (empty($prop_data['race_to_20_points'][0]['odds']))
                                <div class="">
                                    <a href="#"> OTB </a>
                                </div>
                                @else
                                <div class="">
                                    @php
                                        $prop_data['race_to_20_points'][0]['odds'] =  _toAmericanDecimal($prop_data['race_to_20_points'][0]['odds']);
                                        $prop_data['race_to_20_points'][0]['team_name'] = "Race To 20 points";
                                        $prop_data['race_to_20_points'][0]['sub_type_name'] = $home_name;
                                        $unique_id = $event_data->id."-race-to-20-points-50163-00".str_replace(['.', '_', ' ', '--','(',')'], '-',$prop_data['race_to_20_points'][0]['sub_type_name']);
                                        $prop_data['race_to_20_points'][0]['Unique_id'] = $unique_id;
                                        $prop_data['race_to_20_points'][0]['betting_wager'] = $home_name;
                                        $prop_data['race_to_20_points'][0]['row_line'] = 'home';
                                    @endphp
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['race_to_20_points'][0])}}','prop_bet')">
                                        {{ _preccedSign($prop_data['race_to_20_points'][0]['odds']) }}
                                    </a>
                                </div>
                                @endif
                            </td>
                            <td data-th="{{ $away_name }}" class="">
                                @if (empty($prop_data['race_to_20_points'][1]['odds']))
                                <a href="#"> OTB </a>
                                @else
                                    @php
                                        $prop_data['race_to_20_points'][1]['odds'] =  _toAmericanDecimal($prop_data['race_to_20_points'][1]['odds']);
                                        $prop_data['race_to_20_points'][1]['team_name'] = "Race To 20 points";
                                        $prop_data['race_to_20_points'][1]['sub_type_name'] = $away_name;
                                        $unique_id = $event_data->id."-race-to-20-points-78785-01".str_replace(['.', '_', ' ', '--','(',')'], '-',$prop_data['race_to_20_points'][1]['sub_type_name']);
                                        $prop_data['race_to_20_points'][1]['Unique_id'] = $unique_id;
                                        $prop_data['race_to_20_points'][1]['betting_wager'] = $away_name;
                                        $prop_data['race_to_20_points'][1]['row_line'] = 'away';
                                    @endphp
                                   <a id="{{$unique_id}}"
                                      onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($prop_data['race_to_20_points'][1])}}','prop_bet')">
                                       {{_preccedSign($prop_data['race_to_20_points'][1]['odds'])}}
                                </a>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
                @endif


                @if (!empty($prop_data['b_winning_margin']))
                <!-- Winning Margins -->
                    {{--mobile-view--}}
                    <table class="table prop-table prop-td-show-767">
                        <h3 style="width: 100%" class="tabletitle tabletitle-mobile prop-td-show-767">Winning Margins</h3>
                        <thead>
                        <tr>
                            <th></th>
                            <th>{{ $home_name }}</th>
                            <th>{{ $away_name }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php

                            $bWinningMargin = [];
                            foreach ($prop_data['b_winning_margin'] as $val) {
                            $bWinningMargin[$val['header']][] = [
                            'id' => $val['id'],
                            'header' => $val['header'],
                            'odds' => $val['odds'],
                            'name' => $val['name'],
                            ];
                            }

                            $rowWinningMargins = ($bWinningMargin[$home_name] >
                            $bWinningMargin[$away_name]) ?
                            $bWinningMargin[$home_name] : $bWinningMargin[$away_name];


                        @endphp

                        @foreach ($rowWinningMargins as $wk => $margin)
                            <!-- table single column & row list -->
                            <tr class="mobilecss wordltable">
                                <td data-th="" class=" ">
                                    <div class="">
                                        <a> {{$bWinningMargin[$home_name][$wk]['name']}}
                                        </a>
                                    </div>
                                </td>
                                <td data-th="{{$home_name}}" class=" ">
                                    @if (empty($bWinningMargin[$home_name][$wk]['odds']))
                                        <div class="">
                                            <a href="#"> OTB </a>
                                        </div>
                                    @else
                                        @php
                                            $bWinningMargin[$home_name][$wk]['odds'] =  _toAmericanDecimal($bWinningMargin[$home_name][$wk]['odds']);
                                            $bWinningMargin[$home_name][$wk]['team_name'] = "Winning Margins";
                                            $bWinningMargin[$home_name][$wk]['sub_type_name'] = $home_name;
                                            $unique_id = $event_data->id."-winning-margins-87876-".$wk."015".str_replace(['.', '_', ' ', '--','(',')'], '-',$bWinningMargin[$home_name][$wk]['sub_type_name']);
                                            $bWinningMargin[$home_name][$wk]['Unique_id'] = $unique_id;
                                            $bWinningMargin[$home_name][$wk]['betting_wager'] = $home_name;
                                            $bWinningMargin[$home_name][$wk]['row_line'] = 'home';
                                        @endphp
                                        <div class="">
                                            <a id="{{$unique_id}}"
                                               onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($bWinningMargin[$home_name][$wk])}}','prop_bet')">
                                                {{ _preccedSign($bWinningMargin[$home_name][$wk]['odds'])}}
                                            </a>
                                        </div>

                                    @endif
                                </td>
                                <td data-th="{{$away_name}}" class="">
                                    @if (empty($bWinningMargin[$away_name][$wk]['odds']))
                                        <a href="#"> OTB </a>
                                    @else
                                        @php
                                            $bWinningMargin[$away_name][$wk]['odds'] =  _toAmericanDecimal($bWinningMargin[$away_name][$wk]['odds']);
                                            $bWinningMargin[$away_name][$wk]['team_name'] = "Winning Margins";
                                            $bWinningMargin[$away_name][$wk]['sub_type_name'] = $away_name;
                                            $unique_id = $event_data->id."-winning-margins-64825-".$wk."016".str_replace(['.', '_', ' ', '--','(',')'], '-',$bWinningMargin[$away_name][$wk]['sub_type_name']);
                                            $bWinningMargin[$away_name][$wk]['Unique_id'] = $unique_id;
                                            $bWinningMargin[$away_name][$wk]['betting_wager'] = $away_name;
                                            $bWinningMargin[$away_name][$wk]['row_line'] = 'away';
                                        @endphp
                                        <a id="{{$unique_id}}"
                                           onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($bWinningMargin[$away_name][$wk])}}','prop_bet')">
                                            {{_preccedSign($bWinningMargin[$away_name][$wk]['odds'])}}
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{--mobile-view end--}}
                <table class="rwd-table newtables prop-td-hide-767">
                    <h3 class="tabletitle prop-td-hide-767">Winning Margins</h3>
                    <tbody class="rays">
                        <!-- table heading name -->

                        <tr>
                            <th></th>
                            <th>{{$home_name}}</th>
                            <th>{{$away_name}}</th>
                        </tr>

                        @php

                        $bWinningMargin = [];
                        foreach ($prop_data['b_winning_margin'] as $val) {
                        $bWinningMargin[$val['header']][] = [
                        'id' => $val['id'],
                        'header' => $val['header'],
                        'odds' => $val['odds'],
                        'name' => $val['name'],
                        ];
                        }

                        $rowWinningMargins = ($bWinningMargin[$home_name] >
                        $bWinningMargin[$away_name]) ?
                        $bWinningMargin[$home_name] : $bWinningMargin[$away_name];


                        @endphp

                        @foreach ($rowWinningMargins as $wk => $margin)
                        <!-- table single column & row list -->
                        <tr class="mobilecss wordltable">
                            <td data-th="" class=" ">
                                <div class="">
                                    <a> {{$bWinningMargin[$home_name][$wk]['name']}}
                                    </a>
                                </div>
                            </td>
                            <td data-th="{{$home_name}}" class=" ">
                                @if (empty($bWinningMargin[$home_name][$wk]['odds']))
                                <div class="">
                                    <a href="#"> OTB </a>
                                </div>
                                @else
                                    @php
                                        $bWinningMargin[$home_name][$wk]['odds'] =  _toAmericanDecimal($bWinningMargin[$home_name][$wk]['odds']);
                                        $bWinningMargin[$home_name][$wk]['team_name'] = "Winning Margins";
                                        $bWinningMargin[$home_name][$wk]['sub_type_name'] = $home_name;
                                        $unique_id = $event_data->id."-winning-margins-87876-".$wk."015".str_replace(['.', '_', ' ', '--','(',')'], '-',$bWinningMargin[$home_name][$wk]['sub_type_name']);
                                        $bWinningMargin[$home_name][$wk]['Unique_id'] = $unique_id;
                                        $bWinningMargin[$home_name][$wk]['betting_wager'] = $home_name;
                                        $bWinningMargin[$home_name][$wk]['row_line'] = 'home';
                                    @endphp
                                <div class="">
                                    <a id="{{$unique_id}}"
                                       onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($bWinningMargin[$home_name][$wk])}}','prop_bet')">
                                        {{ _preccedSign($bWinningMargin[$home_name][$wk]['odds'])}}
                                    </a>
                                </div>

                                @endif
                            </td>
                            <td data-th="{{$away_name}}" class="">
                                @if (empty($bWinningMargin[$away_name][$wk]['odds']))
                                <a href="#"> OTB </a>
                                @else
                                    @php
                                        $bWinningMargin[$away_name][$wk]['odds'] =  _toAmericanDecimal($bWinningMargin[$away_name][$wk]['odds']);
                                        $bWinningMargin[$away_name][$wk]['team_name'] = "Winning Margins";
                                        $bWinningMargin[$away_name][$wk]['sub_type_name'] = $away_name;
                                        $unique_id = $event_data->id."-winning-margins-64825-".$wk."016".str_replace(['.', '_', ' ', '--','(',')'], '-',$bWinningMargin[$away_name][$wk]['sub_type_name']);
                                        $bWinningMargin[$away_name][$wk]['Unique_id'] = $unique_id;
                                        $bWinningMargin[$away_name][$wk]['betting_wager'] = $away_name;
                                        $bWinningMargin[$away_name][$wk]['row_line'] = 'away';
                                    @endphp
                                <a id="{{$unique_id}}"
                                   onclick="Betslip.addToCart({{ json_encode($event_data) }},'{{ json_encode($bWinningMargin[$away_name][$wk])}}','prop_bet')">
                                    {{_preccedSign($bWinningMargin[$away_name][$wk]['odds'])}}
                                </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif

                @endif

            </div>
            <div class="col-3 mobilepostion">
                @include('frontend.bet_slip.slip')
            </div>
        </div>
    </div>
</section>
<!-- main body area start -->

<div class="modal" id="passwordModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Enter Your Password</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <input type="password" class="form-control" id="bet_confirm_password">
                <p id="passwordMessage" style="color: red"></p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" id="submit_confirm_password">Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="betNotice">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Bets data changed</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                Bets data has been changed, please review your bets
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal">Okay</button>
            </div>
        </div>
    </div>
</div>
@endsection
