@extends('layouts.frontend_layout')

@section('title')

@endsection

@section('content')

    <!-- Upcoming Events area start -->
    <section id="upcoming_events" class="upcoming-margin-top ">
        <div class="container">
            <div class="col-12  padding-0">
                <div class="row">
                    <div class="col-lg-12 col-xs-12 padding-0">
                        <div class="upcoming_title ">
                            <h2> Horse Racing </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Upcoming Events area end -->

<!-- main body area start -->
<section id="body_warapper">
    <div class="container">

        <div class="row" style="margin-top:30px">

                <div class="col-sm-9" id="horse-item">
                    <div class="TodayRace" style="margin-bottom:20px">
                        Today's Race
                    </div>
                    @foreach ($horsebettodaydata->tournament as $tournament)
                        @php
                            $num=0;
                            foreach($tournament->race as $race)
                            {
                                if($race->results[0]->horse[0]['name']=="") {
                                    $mostRecent =strtotime($race['datetime']);
                                    $racename=$race['name'];
                                    $num++;
                                }
                            }
                        @endphp
                        @php
                            if($num!==0)
                            {
                             foreach($tournament->race as $race)
                                    {
                                           if($race->results->horse[0]['name']=="")
                                                 {
                                                         $curDate=strtotime($race['datetime']);
                                                         if($curDate<$mostRecent)
                                                         {
                                                           $mostRecent=$curDate;
                                                           $racename=$race['name'];
                                                         }
                                                  }
                                          }
                                 $number=explode(" ",$racename)[1];
                                $racename=$tournament->race[intval($number)-1]['name'];
                                }
                              else
                                {
                                $racename="no result";
                                }
                        @endphp
                        @if($racename!=="no result")
                            <div class="horse-item-header">
                                <div class="row padding-horse-item-header" >
                                    <div class="col-sm-6 " style="text-align:left;"><span style="font-weight:bold">@php echo $tournament['name'];@endphp</span></div>
                                    <div class="col-sm-6" style="text-align:right;padding-right:0">
                                        <span> <i class="fa fa-minus" onclick="HorseBetslip.horsebettoggle(this)" ></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="horse-bet-content">
                                <div class="horse-item-preheader">
                                    <div class="row padding-horse-item-header">
                                        <div class="col-sm-6" style="text-align:left;color:#337AB7"><span>@php echo $racename  @endphp</span></div>
                                        <div class="col-sm-6" style="text-align:right;padding-right:0px"><span style="color:#808080"><b>Today </b> @php echo $tournament->race[intval($number)-1]['time'] @endphp</span></div>
                                    </div>
                                </div>
                                <div class="horse-item-body">
                                    <table class="horse-table">
                                        <tr>
                                            <th class="horse-th"><span style="font-weight:bold">P#</span></th>
                                            <th  class="horse-th"><span style="font-weight:bold">Horse & Jockey</span></th>
                                            <th  class="horse-th"><span style="font-weight:bold">ML</span></th>
                                            <th  class="horse-th"><select class="horse-betting-select">
                                                    <option><span style="font-weight:bold">Win/Place/Show</span></option>
                                                    @foreach($tournament->race[intval($number)-1]->wagers_available->wageravail  as $wager)
                                                        <option><span style="font-weight:bold">@php echo $wager['type'] @endphp</span></option>
                                                    @endforeach
                                                </select></th>
                                        </tr>
                                        @php $num=1; @endphp
                                        @foreach($tournament->race[intval($number)-1]->runners->horse as $horses)
                                            <tr>
                                                <td  class="horse-td"><div class="numberCircle@php echo $num @endphp">@php echo $horses['number'] @endphp</div></td>
                                                <td  class="horse-td"><div>@php echo $horses['name']@endphp</div>
                                                   <div style="margin-top:10px;color:#848080"><span >@php echo $horses['jockey']@endphp</span></div> </td>
                                                <td  class="horse-td">@php echo $horses->odds->bookmaker[0]['fractional']@endphp</td>
                                                <td  class="horse-td">
                                                    <button class="bet-btn" id="Horse_{{$tournament['id']}}_{{$tournament->race[intval($number)-1]['id']}}_{{$horses['id']}}_Win" onclick='HorseBetslip.Winsave(this,"{{$horses['number']}}","{{$tournament['name']}}","{{$tournament['id']}}","{{ $racename }}", "{{ $horses['name'] }}","{{$horses['id']}}",
                                                                                                            "{{$tournament->race[intval($number)-1]['datetime']}}","{{$tournament->race[intval($number)-1]['id']}}")'>W</button>
                                                    <button class="bet-btn"  id="Horse_{{$tournament['id']}}_{{$tournament->race[intval($number)-1]['id']}}_{{$horses['id']}}_Place" onclick='HorseBetslip.Placesave(this,"{{$horses['number']}}","{{$tournament['name']}}","{{$tournament['id']}}","{{ $racename }}", "{{ $horses['name'] }}","{{$horses['id']}}",
                                                            "{{$tournament->race[intval($number)-1]['datetime']}}","{{$tournament->race[intval($number)-1]['id']}}")'>P</button>
                                                    <button class="bet-btn"   id="Horse_{{$tournament['id']}}_{{$tournament->race[intval($number)-1]['id']}}_{{$horses['id']}}_Show"   onclick='HorseBetslip.Showsave(this,"{{$horses['number']}}","{{$tournament['name']}}","{{$tournament['id']}}","{{ $racename }}", "{{ $horses['name'] }}","{{$horses['id']}}",
                                                            "{{$tournament->race[intval($number)-1]['datetime']}}","{{$tournament->race[intval($number)-1]['id']}}")'>S</button>
                                                    <button class="bet-btn" id="Horse_{{$tournament['id']}}_{{$tournament->race[intval($number)-1]['id']}}_{{$horses['id']}}_W/P/S"  onclick='HorseBetslip.W_P_S(this,"{{$horses['number']}}","{{$tournament['name']}}","{{$tournament['id']}}","{{ $racename }}", "{{ $horses['name'] }}","{{$horses['id']}}",
                                                            "{{$tournament->race[intval($number)-1]['datetime']}}","{{$tournament->race[intval($number)-1]['id']}}")'>W/P/S</button>
                                                </td>
                                            </tr>
                                            @php $num++ @endphp
                                        @endforeach
                                    </table>
                                </div>
                                <div class="horse-item-footer" style="margin-bottom:10px;">
                                    <div class="row padding-horse-item-footer">
                                        <div class="col-sm-6">
                                        </div>
                                        <div class="col-sm-6" style="text-align:right">
                                            <button type="button" class="btn btn-horse-add-slip"  id="{{$tournament['id']}}_{{$tournament->race[intval($number)-1]['id']}}" disabled="true" onclick="HorseBetslip.addToHorseCart('{{$tournament['id']}}_{{$tournament->race[intval($number)-1]['id']}}')"><span style="font-size:20px">ADD TO BETSLIP</span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                    <div class="TodayRace" style="margin-bottom:20px;margin-top:20px">
                        Tomorrow's Race
                    </div>
                    @foreach ($horsebettomorrowdata->tournament as $tournament)

                        @php
                            $num=0;
                            foreach($tournament->race as $race)
                            {
                                if($race->results[0]->horse[0]['name']=="") {
                                    $mostRecent =strtotime($race['datetime']);
                                    $racename=$race['name'];
                                    $num++;
                                }
                            }
                        @endphp
                        @php
                            if($num!==0)
                            {
                             foreach($tournament->race as $race)
                                    {
                                           if($race->results->horse[0]['name']=="")
                                                 {
                                                         $curDate=strtotime($race['datetime']);
                                                         if($curDate<$mostRecent)
                                                         {
                                                           $mostRecent=$curDate;
                                                           $racename=$race['name'];
                                                         }
                                                  }
                                          }
                                 $number=explode(" ",$racename)[1];
                                $racename=$tournament->race[intval($number)-1]['name'];
                                }
                              else
                                {
                                $racename="no result";
                                }
                        @endphp
                        @if($racename!=="no result")
                            <div class="horse-item-header">
                                <div class="row padding-horse-item-header" >
                                    <div class="col-sm-6 " style="text-align:left"><span style="font-weight:bold">@php echo $tournament['name'];@endphp</span></div>
                                    <div class="col-sm-6" style="text-align:right;padding-right:0">
                                        <span> <i class="fa fa-minus" onclick="HorseBetslip.horsebettoggle(this)" ></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="horse-bet-content">
                                <div class="horse-item-preheader">
                                    <div class="row padding-horse-item-header">
                                        <div class="col-sm-6" style="text-align:left;color:#337AB7"><span>@php echo $racename  @endphp</span></div>
                                        <div class="col-sm-6" style="text-align:right;padding-right:0px"><span style="color:#808080"><b>Today </b> @php echo $tournament->race[intval($number)-1]['time'] @endphp</span></div>
                                    </div>
                                </div>
                                <div class="horse-item-body">
                                    <table class="horse-table">
                                        <tr>
                                            <th class="horse-th"><span style="font-weight:bold">P#</span></th>
                                            <th class="horse-th"><span style="font-weight:bold">Horse & Jockey</span></th>
                                            <th class="horse-th"><span style="font-weight:bold">ML</span></th>
                                            <th class="horse-th"><select class="horse-betting-select">
                                                    <option><span style="font-weight:bold">Win/Place/Show</span></option>
                                                    @foreach($tournament->race[intval($number)-1]->wagers_available->wageravail  as $wager)
                                                        <option><span style="font-weight:bold">@php echo $wager['type'] @endphp</span></option>
                                                    @endforeach
                                                </select></th>
                                        </tr>
                                        @php $num=1; @endphp
                                        @foreach($tournament->race[intval($number)-1]->runners->horse as $horses)
                                            <tr>
                                                <td class="horse-td"><div class="numberCircle@php echo $num  @endphp">@php echo $horses['number'] @endphp</div></td>
                                                <td class="horse-td"><div>@php echo $horses['name']@endphp</div>
                                                    <div style="margin-top:10px;color:#848080"><span >@php echo $horses['jockey']@endphp</span></div> </td>
                                                <td class="horse-td">@php echo $horses->odds->bookmaker[0]['fractional']@endphp</td>
                                                <td class="horse-td">
                                                    <button class="bet-btn" id="Horse_{{$tournament['id']}}_{{$tournament->race[intval($number)-1]['id']}}_{{$horses['id']}}_Win" onclick='HorseBetslip.Winsave(this,"{{$horses['number']}}","{{$tournament['name']}}","{{$tournament['id']}}","{{ $racename }}", "{{ $horses['name'] }}","{{$horses['id']}}",
                                                            "{{$tournament->race[intval($number)-1]['datetime']}}","{{$tournament->race[intval($number)-1]['id']}}")'>W</button>
                                                    <button class="bet-btn"  id="Horse_{{$tournament['id']}}_{{$tournament->race[intval($number)-1]['id']}}_{{$horses['id']}}_Place" onclick='HorseBetslip.Placesave(this,"{{$horses['number']}}","{{$tournament['name']}}","{{$tournament['id']}}","{{ $racename }}", "{{ $horses['name'] }}","{{$horses['id']}}",
                                                            "{{$tournament->race[intval($number)-1]['datetime']}}","{{$tournament->race[intval($number)-1]['id']}}")'>P</button>
                                                    <button class="bet-btn"   id="Horse_{{$tournament['id']}}_{{$tournament->race[intval($number)-1]['id']}}_{{$horses['id']}}_Show"   onclick='HorseBetslip.Showsave(this,"{{$horses['number']}}","{{$tournament['name']}}","{{$tournament['id']}}","{{ $racename }}", "{{ $horses['name'] }}","{{$horses['id']}}",
                                                            "{{$tournament->race[intval($number)-1]['datetime']}}","{{$tournament->race[intval($number)-1]['id']}}")'>S</button>
                                                    <button class="bet-btn" id="Horse_{{$tournament['id']}}_{{$tournament->race[intval($number)-1]['id']}}_{{$horses['id']}}_W/P/S"  onclick='HorseBetslip.W_P_S(this,"{{$horses['number']}}","{{$tournament['name']}}","{{$tournament['id']}}","{{ $racename }}", "{{ $horses['name'] }}","{{$horses['id']}}",
                                                            "{{$tournament->race[intval($number)-1]['datetime']}}","{{$tournament->race[intval($number)-1]['id']}}")'>W/P/S</button>
                                                </td>
                                            </tr>
                                            @php $num++ @endphp
                                        @endforeach
                                    </table>
                                </div>
                                <div class="horse-item-footer" style="margin-bottom:10px;">
                                    <div class="row padding-horse-item-footer">
                                        <div class="col-sm-6">
                                        </div>
                                        <div class="col-sm-6" style="text-align:right">
                                            <button type="button" class="btn btn-horse-add-slip"  id="{{$tournament['id']}}_{{$tournament->race[intval($number)-1]['id']}}" onclick="HorseBetslip.addToHorseCart('{{$tournament['id']}}_{{$tournament->race[intval($number)-1]['id']}}')"><span style="font-size:20px">ADD TO BETSLIP</span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
                <div class="modal" id="passwordModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Enter Your Password</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <input type="password" class="form-control" id="bet_confirm_password">
                            <p id="passwordMessage" style="color: red"></p>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-danger" id="submit_confirm_password">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
                <div class="col-sm-3">
                    @include('frontend.bet_slip.horse_slip')
                </div>
        </div>
    </div>
</section>
<!-- main body area start -->
@endsection
