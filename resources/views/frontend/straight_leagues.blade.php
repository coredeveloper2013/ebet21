@extends('layouts.frontend_layout')

@section('title')

@endsection

@section('content')

    <!-- main body area start -->
    <section id="body_warapper">
        <div class="container">
            <div class="row">
                <div class="col-9 padding-0 col-mobile">
                    <div id="league_bets">
                    @foreach ($data as $league_id => $league)
                        @if ($league->sport_id == 133 || $league->sport_id == 6 || $league->sport_id == 10)
                            @php
                                $sport_obj = null
                            @endphp
                            @if (!empty($league->league_data->results))
                                <!-- Upcoming Events area start -->
                                    <section id="upcoming_events">
                                        <div class="">
                                            <div class="row">
                                                <div class="col-lg-12 col-xs-12 padding-0">
                                                    <div class="upcoming_title">
                                                        <h2>{{$league->league_data->results[0]->LeagueName}}</h2>
                                                        <p>{{ _getHeadlineDate(strtotime($league->league_data->results[0]->Date)) }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <!-- Upcoming Events area end -->
                                    <div class="play_game_title">
                                        <h2>{{ $league->league_data->results[0]->Markets[0]->Name }}</h2>
                                    </div>
                                    @php
                                        $results_data_Total = $league;
                                        $results_data = $league->league_data->results[0]->Markets[0]->Results;
                                        unset($results_data_Total->league_data->results[0]->Markets[0]->Results);
                                        $bwing_event_info = $results_data_Total->league_data->results[0];
                                        $bwing_event_info->Date = _getDate(strtotime($bwing_event_info->Date));
                                        $bwing_event_info->Time = _getTime(strtotime($bwing_event_info->Date));
                                        usort($results_data, function ($a, $b) {
                                        return $a->Odds > $b->Odds;
                                        });
                                        $chunks = array_chunk($results_data, count($results_data) / 2, true);
                                    @endphp
                                    <table style="width:100%">
                                        <tr>
                                            <td>
                                                <table class="rwd-table newtables">
                                                    <tbody>
                                                    <!-- table heading name -->
                                                    <tr>
                                                        <th>Player name</th>
                                                        <!-- Each table heading name &  data-th wile be same -->
                                                        <th>Score</th>
                                                    </tr>
                                                    @foreach ($chunks[0] as $chunk0)
                                                        @php
                                                            $chunk0->Odds = _toAmericanDecimal($chunk0->Odds);
                                                            $chunk0->Unique_id = $chunk0->Id.'-'.str_replace(' ','',$chunk0->Name);
                                                            $chunk0->Unique_id = str_replace('.','-',$chunk0->Unique_id);
                                                        @endphp
                                                        <!-- table single column & row list -->
                                                        <tr class="mobilecss wordltable">
                                                            <td data-th="Player name" class="">
                                                                <!-- Each table heading name &  data-th wile be same -->
                                                                <div class="newtabls">
                                                                    <p>{{ $chunk0->Name }}</p>
                                                                </div>
                                                            </td>
                                                            <td data-th="Score" class="">
                                                                <a id="{{$chunk0->Unique_id}}"
                                                                   onclick="Betslip.addToCart({{json_encode($bwing_event_info)}},{{ json_encode($chunk0)}},'bwin')">
                                                                    {{ _preccedSign($chunk0->Odds) }} </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </td>
                                            <td>
                                                <table class="rwd-table newtables">
                                                    <tbody>
                                                    <!-- table heading name -->
                                                    <tr>
                                                        <th>Player name</th>
                                                        <!-- Each table heading name &  data-th wile be same -->
                                                        <th>Score</th>
                                                    </tr>
                                                    @foreach ($chunks[1] as $chunk1)
                                                        @php
                                                            $chunk1->Odds = _toAmericanDecimal($chunk1->Odds);
                                                            $chunk1->Unique_id = $chunk1->Id.'-'.str_replace(' ','',$chunk1->Name);
                                                            $chunk1->Unique_id = str_replace('.','-',$chunk1->Unique_id);
                                                        @endphp
                                                        <!-- table single column & row list -->
                                                        <tr class="mobilecss wordltable">
                                                            <td data-th="Player name" class="">
                                                                <!-- Each table heading name &  data-th wile be same -->
                                                                <div class="newtabls">
                                                                    <p>{{$chunk1->Name}}</p>
                                                                </div>
                                                            </td>
                                                            <td data-th="Score" class="">
                                                                <a id="{{$chunk1->Unique_id}}"
                                                                   onclick="Betslip.addToCart({{json_encode($bwing_event_info)}},{{ json_encode($chunk1)}},'bwin')">
                                                                    {{ _preccedSign($chunk1->Odds) }} </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                @else
                                    <div class="data-unavaiable">
                                        <h1> League Data unavaiable</h1>
                                    </div>
                                @endif
                            @else
                            <!-- Upcoming Events area start -->
                                <section id="upcoming_events">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-lg-12 col-xs-12 padding-0">
                                                <div class="upcoming_title">
                                                    <h2> {{ $league->league_data['league_data']['league_name'] }} </h2>
                                                    <p>{{  _getHeadlineDate(strtotime($league->league_data['league_data']['events'][0]['date'])) }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <!-- Upcoming Events area end -->

                                <div class="Live-bet-event-content">
                                    <div class="Live-bet-event-content-each">
                                        <div class="Live-bet-league-header rd-dark">
                                            <div class="_league_header_title _league_header_title_strt">Game</div>
                                            <div class="mobile-w-375">
                                                @php
                                                    $columnName = '';
                                                    if ($league->sport_id != 9 && $league->sport_id != 15 && $league->sport_id != 11 && $league->sport_id != 20 && $league->sport_id != 21)
                                                    {
                                                            if ( $league->sport_id == 17 ) {
                                                                echo '<div class="_league_header_spread">Puck <span class="d-sm-none">Line</span></div>';
                                                                $columnName = 'Puck Line';
                                                             }
                                                             else if ($league->sport_id == 16) {
                                                                    echo '<div class="_league_header_spread">Run <span class="d-sm-none">Line</span></div>';
                                                                    $columnName = 'Run Line';
                                                              }
                                                              else if($league->sport_id==151){
                                                                     echo '<div class="_league_header_spread">Spread</div>';
                                                                     $columnName = 'Maps Spread';
                                                              }
                                                              else {
                                                                   echo '<div class="_league_header_spread">Spread</div>';
                                                                   $columnName = 'Spread';
                                                                  }
                                                    }
                                                @endphp
                                                <div class="_league_header_spread @if($league->sport_id == 9 || $league->sport_id == 15 || $league->sport_id == 11 || $league->sport_id == 20 || $league->sport_id == 21) list-item-full @endif">Money <span class="d-sm-none">Line</span></div>
                                                @php

                                                   if ($league->sport_id != 9 && $league->sport_id != 15 && $league->sport_id != 11 && $league->sport_id != 20 && $league->sport_id != 21)
                                                        {
                                                                if ($league->sport_id == 17 || $league->sport_id == 13)
                                                            { echo '<div class="_league_header_spread">Game total</div>'; }
                                                            elseif ($league->sport_id == 151) {
                                                                echo '<div class="_league_header_spread">Moneyline (Map 1)</div>';
                                                            }
                                                            else { echo '<div class="_league_header_spread">Game total</div>'; }
                                                        }
                                                @endphp
                                            </div>
                                        </div>
                                        @foreach ($league->league_data['league_data']['events'] as $event_key => $event)
                                               @php
                                                   $event = (object) $event;
                                                   $event_time = strtotime($event->date.' '.$event->time);
                                               @endphp
                                            <div class="Live-bet-league-each">
                                                <div class="Live-bet-event-each">
                                                    <div class="_event_sections_mobile rd-dark">
                                                        <div class="w-100">
                                                            {{ _getTime($event_time) }} &nbsp;&nbsp;&nbsp;
                                                            {{ _getDate($event_time) }}
                                                            @if(isset($event->event_detail['propBets']) && !$event->event_detail['propBets'])
                                                                <form action="{{ url("/props-bet")}}" method="post">
                                                                    @csrf
                                                                    <input type="hidden" name="event_info"
                                                                           value="{{json_encode($event)}}">
                                                                    <input type="submit"
                                                                           class="_event_sections_button"
                                                                           value="Prop bets {{$event->event_detail['propBets'] }}">
                                                                    <i class="fa fa-fw fa-chevron-right _event_sections_button_icon"></i>
                                                                </form>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="_league_header_title">
                                                        <div class="_event_sections rd-dark @if($league->sport_id == 17 || $league->sport_id == 1 || $league->sport_id == 19) gm-sec-3 @endif">
                                                            <div class="w-100">

                                                                {{ _getTime($event_time) }} <br>
                                                                {{ _getDate($event_time) }} <br>
                                                                {{ $event->id }}
                                                                @if($league->sport_id == 12
                                                                || $league->sport_id == 17
                                                                ||  $league->sport_id == 18
                                                                ||  $league->sport_id == 1
                                                                ||  $league->sport_id == 16
                                                                ||  $league->sport_id == 13
                                                                ||  $league->sport_id == 14)
                                                                    <br>
                                                                    <br>
                                                                    <form action="{{ url("/props-bet")}}" method="post">
                                                                        @csrf
                                                                        <input type="hidden" name="event_info"
                                                                               value="{{json_encode($event)}}">
                                                                        <input type="hidden" name="sport_id"
                                                                               value="{{$league->sport_id}}">
                                                                        <input type="submit"
                                                                               class="_event_sections_button"
                                                                               value="Prop bets">
                                                                    </form>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="_event_titles @if($league->sport_id == 17 || $league->sport_id == 1 || $league->sport_id == 19) gm-sec-3 @endif">
                                                            <ul class="@if($league->sport_id == 17 || $league->sport_id == 1 || $league->sport_id == 19) gm-sec-3 @endif">
                                                                <li class="text-center">
                                                                    <span class="spn-title" title="Home">{{$event->home['name']}}</span>
                                                                </li>
                                                                <li class="text-center">
                                                                    <span class="spn-title" title="Away">{{$event->away['name']}}</span>
                                                                </li>
                                                                @if ($league->sport_id == 17 || $league->sport_id == 1 || $league->sport_id == 19 )
                                                                    <li class="text-center"><span class="spn-title">Draw</span></li>
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    @if ($league->sport_id != 9 && $league->sport_id != 15 && $league->sport_id != 11 && $league->sport_id != 20 && $league->sport_id != 21)
                                                        <div class="_league_header_spread">
                                                            <ul>
                                                                @if ($event->event_detail['spread']['home']['odd'] == "OTB" || $event->event_detail['spread']['home']['odd'] == "")
                                                                    <li class="no-event">OTB</li>
                                                                @else
                                                                    @php
                                                                        $spreadHome = _toAmericanDecimal($event->event_detail['spread']['home']['odd']);
                                                                        $home_hendicap  = $event->event_detail['spread']['home']['handicap'];
                                                                    @endphp
                                                                    <li onclick="Betslip.addToCart({{json_encode($event)}},{
                                                                        handicap : '{{$home_hendicap}}',
                                                                        odd      : '{{$spreadHome}}',
                                                                        column   : 'spread',
                                                                        columnName   : '{{$columnName}}',
                                                                        row_line : 'home',
                                                                        time     : '{{ _getTime($event_time) }}',
                                                                        date     : '{{ _getDate($event_time) }}'
                                                                        })" id="{{$event->id}}_spread_home">
                                                                        {{$home_hendicap. " ("._preccedSign($spreadHome). ")"}}
                                                                    </li>
                                                                @endif


                                                                @if ($event->event_detail['spread']['away']['odd'] == "OTB" || $event->event_detail['spread']['away']['odd']=="")
                                                                    <li class="no-event">OTB</li>
                                                                @else
                                                                    @php
                                                                        $spreadAway = _toAmericanDecimal($event->event_detail['spread']['away']['odd']);
                                                                        $away_handicap  = $event->event_detail['spread']['away']['handicap'];
                                                                        if ($home_hendicap<0) {
                                                                            if($away_handicap < 0)
                                                                            {
                                                                                $away_handicap = (-1 *  $away_handicap);
                                                                                $away_handicap = '+'.$away_handicap;
                                                                            }
                                                                        }
                                                                        else {
                                                                            $away_handicap = (-1 *  $away_handicap);
                                                                        }
                                                                    @endphp
                                                                    <li onclick="Betslip.addToCart({{json_encode($event)}},{
                                                                        handicap : '{{$away_handicap}}',
                                                                        odd      : '{{$spreadAway}}',
                                                                        column   : 'spread',
                                                                        columnName   : '{{$columnName}}',
                                                                        row_line : 'away',
                                                                        time     : '{{ _getTime($event_time) }}',
                                                                        date     : '{{ _getDate($event_time) }}'
                                                                        })" id="{{$event->id}}_spread_away">

                                                                        {{--@if ($league->sport_id == 17)--}}
                                                                            {{--@if(strlen($event->event_detail['spread']['away']['handicap']) > 5)--}}
                                                                                {{--{{"(".substr($event->event_detail['spread']['away']['handicap'], 0, 6).'...'. ") ". $spreadAway}}--}}
                                                                            {{--@else--}}
                                                                                {{--{{"(".$event->event_detail['spread']['away']['handicap']. ") ". $spreadAway}}--}}
                                                                            {{--@endif--}}
                                                                        {{--@else--}}
                                                                            {{$away_handicap. " ("._preccedSign($spreadAway). ")"}}
                                                                        {{--@endif--}}
                                                                    </li>
                                                                @endif


                                                                @if ($league->sport_id == 17 || $league->sport_id == 1 ||$league->sport_id == 19)
                                                                    <li class="no-event"> <a>&nbsp; </a> </li>
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    @endif
                                                    <div class="_league_header_mline @if($league->sport_id == 9 || $league->sport_id == 15 || $league->sport_id == 11 || $league->sport_id == 20 || $league->sport_id == 21) list-item-full @endif">
                                                        <ul>
                                                            @if ($event->event_detail['money']['home'] == "OTB" || $event->event_detail['money']['home']=="")
                                                                <li class="no-event">OTB</li>
                                                            @else
                                                                @php
                                                                    $moneyHome = _toAmericanDecimal($event->event_detail['money']['home']);
                                                                @endphp
                                                                <li onclick="Betslip.addToCart({{json_encode($event)}},{
                                                                    handicap : null,
                                                                    odd      : '{{$moneyHome}}',
                                                                    column   : 'money',
                                                                    columnName   : 'Money Line',
                                                                    row_line : 'home',
                                                                    time     : '{{ _getTime($event_time) }}',
                                                                    date     : '{{ _getDate($event_time) }}'
                                                                    })" id="{{$event->id}}_money_home">
                                                                    {{ _preccedSign($moneyHome) }}
                                                                </li>
                                                            @endif

                                                            @if ($event->event_detail['money']['away'] == "OTB" || $event->event_detail['money']['away']=="")
                                                                <li class="no-event">OTB</li>
                                                            @else
                                                                @php
                                                                    $moneyAway = _toAmericanDecimal($event->event_detail['money']['away']);
                                                                @endphp
                                                                <li onclick="Betslip.addToCart({{json_encode($event)}},{
                                                                    handicap : null,
                                                                    odd      : '{{$moneyAway}}',
                                                                    column   : 'money',
                                                                    columnName   : 'Money Line',
                                                                    row_line : 'away',
                                                                    time     : '{{ _getTime($event_time) }}',
                                                                    date     : '{{ _getDate($event_time) }}'
                                                                    })" id="{{$event->id}}_money_away">
                                                                    {{ _preccedSign($moneyAway)}}
                                                                </li>
                                                            @endif


                                                            @if ($league->sport_id == 17 || $league->sport_id == 1 || $league->sport_id == 19)
                                                                @if ($event->event_detail['draw'] == "OTB" || $event->event_detail['draw'] == "")
                                                                    <li class="no-event">OTB</li>
                                                                @else
                                                                    @php
                                                                        $draw = _toAmericanDecimal($event->event_detail['draw']);
                                                                    @endphp
                                                                    <li onclick="Betslip.addToCart({{json_encode($event)}},{
                                                                        handicap : null,
                                                                        odd      : '{{$draw}}',
                                                                        column   : 'money',
                                                                        columnName   : 'Money Line',
                                                                        row_line : 'draw',
                                                                        time     : '{{ _getTime($event_time) }}',
                                                                        date     : '{{ _getDate($event_time) }}'
                                                                        })" id="{{$event->id}}_money_draw">
                                                                        {{ _preccedSign($draw)  }}
                                                                    </li>
                                                                @endif

                                                            @endif
                                                        </ul>
                                                    </div>
                                                    @if ($league->sport_id != 9 && $league->sport_id != 15 && $league->sport_id != 11 && $league->sport_id != 20 && $league->sport_id != 21)
                                                        <div class="_league_header_total">
                                                            <ul>
                                                                @if ($event->event_detail['total']['home']['odd'] == "OTB" ||
                                                        $event->event_detail['total']['home']['odd'] == "")
                                                                    <li class="no-event">OTB</li>
                                                                @else
                                                                    @php
                                                                        $totalHome = _toAmericanDecimal($event->event_detail['total']['home']['odd']);
                                                                        $overunder = $event->event_detail['total']['home']['under_over'];
                                                                    @endphp
                                                                    <li onclick="Betslip.addToCart({{json_encode($event)}},{
                                                                        handicap :  '{{$overunder}}',
                                                                        odd      :  '{{$totalHome}}',
                                                                        column   :  'total',
                                                                        columnName :'Total',
                                                                        row_line :  'over',
                                                                        time     : '{{ _getTime($event_time) }}',
                                                                        date     : '{{ _getDate($event_time) }}'
                                                                        })" id="{{$event->id}}_total_over">
                                                                        @if($league->sport_id==151)
                                                                            {{_preccedSign($totalHome) }}
                                                                        @else
                                                                            {{$event->event_detail['total']['home']['under_over']." ("._preccedSign($totalHome).")" }}
                                                                        @endif
                                                                    </li>
                                                                @endif

                                                                @if ($event->event_detail['total']['away']['odd'] == "OTB" ||
                                                                $event->event_detail['total']['away']['odd'] == "")
                                                                    <li class="no-event">OTB</li>
                                                                @else
                                                                    @php
                                                                        $totalAway = _toAmericanDecimal($event->event_detail['total']['away']['odd']);
                                                                    @endphp
                                                                    <li onclick="Betslip.addToCart({{json_encode($event)}},{
                                                                        handicap : '{{$event->event_detail['total']['away']['under_over']}}',
                                                                        odd      : '{{$totalAway}}',
                                                                        column   : 'total',
                                                                        columnName :'Total',
                                                                        row_line : 'under',
                                                                        time     : '{{ _getTime($event_time) }}',
                                                                        date     : '{{ _getDate($event_time) }}'
                                                                        })" id="{{$event->id}}_total_under">
                                                                        @if($league->sport_id==151)
                                                                            {{_preccedSign($totalAway)}}
                                                                        @else
                                                                            {{$event->event_detail['total']['away']['under_over']." ("._preccedSign($totalAway).")" }}
                                                                        @endif
                                                                      </li>
                                                                @endif

                                                                @if ($league->sport_id == 17 || $league->sport_id == 1 || $league->sport_id == 19)
                                                                    <li class="no-event"> &nbsp; </li>
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <div id="event_details">

                    </div>
                </div>
                <div class="col-3  mobilepostion">
                    @include('frontend.bet_slip.slip')
                </div>
            </div>
        </div>
    </section>
    <!-- main body area start -->

    <div class="modal" id="passwordModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Enter Your Password</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="password" class="form-control" id="bet_confirm_password">
                    <p id="passwordMessage" style="color: red"></p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" id="submit_confirm_password">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="betNotice">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Bets data changed</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    Bets data has been changed, please review your bets
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" data-dismiss="modal">Okay</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_js')

    <script>
        // alert("hello world");
    </script>

@endsection
