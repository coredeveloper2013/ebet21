<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class HttpReferer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /* if(!isset($_SERVER['HTTP_REFERER']) && !isset($_SERVER['HTTP_CACHE_CONTROL'])) {
            Auth::logout();
            return redirect('/');
        } */
        return $next($request);
    }
}
