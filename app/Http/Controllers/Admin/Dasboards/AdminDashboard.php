<?php

namespace App\Http\Controllers\Admin\Dasboards;

use App\User;
use App\Models\AgentRate;
use App\Models\BettingDetail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Utility\DateTime\EbetDateTime;


class AdminDashboard
{

    public function __construct()
    { }

    public function weeklyActiveUsers()
    {
        $data = [];
        for($x=1; $x<=8; $x++) {
            $week = $this->getWeekMonSun(-$x);
            $sql = BettingDetail::where(\DB::raw('DATE(betting_details.created_at)'), '>=', $week->start)->where(\DB::raw('DATE(betting_details.created_at)'), '<=', $week->end);
            if (Auth::user()->role == 'agent' || Auth::user()->role == 'subagent') {
                $sql->join('assign_users', 'betting_details.user_id', '=', 'assign_users.user_id');
                $sql->where('assign_users.agent_id', Auth::user()->id);
            }
            $count = $sql->count('betting_details.id');

            $data['count'][] = $count;
            $data['week'][] = 'Week '.$x;
        }
        return $data;
    }

    public function salesGraphDashboard()
    {
        $rates = array();
        if (Auth::user()->role == 'admin') {
            $datas = \DB::select('call rateDataAdmin()');

            foreach ($datas as $data) {
               $month = date('M',strtotime($data->date));
               !isset($rates[$month]) ? $rates[$month] = 0 : '';
               $rates[$month] += ($data->rate * ($data->counts == 0 ? 1 : $data->counts));
            }
        } elseif (Auth::user()->role == 'agent' || Auth::user()->role == 'subagent') {
            $agentDatas = BettingDetail::select('betting_details.risk_amount','betting_details.created_at')
                ->join('assign_users', 'betting_details.user_id', '=', 'assign_users.user_id')
                ->where('assign_users.agent_id', Auth::user()->id)
                ->get();

            foreach ($agentDatas as $data) {
               $month = date('M',strtotime($data->created_at));
               !isset($rates[$month]) ? $rates[$month] = 0 : '';
               $rates[$month] += $data->risk_amount;
            }
        }
        return [
            array_keys($rates),
            array_values($rates)
        ];
    }

    public function currentWeekActiveUsers()
    {
        $datetime = new EbetDateTime();
        $lastWeek = $datetime->setTimeMil("last sunday midnight");
        $today    = $datetime->setTimeMil('today');
        $sql = BettingDetail::whereBetween(\DB::raw('DATE(betting_details.created_at)'), [$lastWeek, $today]);

        if (Auth::user()->role == 'agent' || Auth::user()->role == 'subagent') {
            $sql->join('assign_users', 'betting_details.user_id', '=', 'assign_users.user_id');
            $sql->where('assign_users.agent_id', Auth::user()->id);
        }
        return $sql->count(\DB::raw('DISTINCT(betting_details.user_id)'));
    }
    public function activeUsersLists()
    {
        $datetime = new EbetDateTime();
        $lastWeek = $datetime->setTimeMil("last sunday midnight");
        $today    = $datetime->setTimeMil('today');
        $sql = User::select('users.*', 'A.user_current_credit_limit', 'A.user_current_max_limit', 'A.user_current_pending', 'A.user_current_balance')
            ->join(\DB::raw("(SELECT A.user_id, A.user_current_credit_limit, A.user_current_max_limit, A.user_current_pending, A.user_current_balance
                FROM betting_details AS A
                WHERE A.id=(SELECT MAX(id) FROM betting_details WHERE DATE(created_at) between '".$lastWeek."' and '".$today."' AND user_id=A.user_id)) AS A"), 'users.id','=','A.user_id')
            ->where('users.role', 'user')
            ->orderBy('users.id', 'desc');

        if (Auth::user()->role == 'agent' || Auth::user()->role == 'subagent') {
            $sql->join('assign_users', 'A.user_id', '=', 'assign_users.user_id');
            $sql->where('assign_users.agent_id', Auth::user()->id);
        }

        return $sql->get();
    }

    public function todayRepresent()
    {
        $datetime = new EbetDateTime();
        $today    = $datetime->setTimeMil('today');
        $iBetDet1 = BettingDetail::where('result', 1)->where(\DB::raw('DATE(created_at)'), $today)->sum('betting_win');
        $iBetDet2 = BettingDetail::where('result', 2)->where('status', 2)->where(\DB::raw('DATE(created_at)'), $today)->sum('risk_amount');
        return ($iBetDet1 - $iBetDet2);
    }

    public function todayRepresentLists()
    {
        $todayUsers  =  User::select('users.*', 'B.winAmount', 'C.lossAmount')
       ->leftJoin(\DB::raw("(SELECT user_id, SUM(betting_win) AS winAmount FROM betting_details WHERE result='1' AND DATE(created_at)='".date("Y-m-d")."' GROUP BY user_id) AS B"), 'B.user_id', '=', 'users.id')
       ->leftJoin(\DB::raw("(SELECT user_id, SUM(risk_amount) AS lossAmount FROM betting_details WHERE result='2' AND status ='2' AND DATE(created_at)='".date("Y-m-d")."' GROUP BY user_id) AS C"), 'C.user_id', '=', 'users.id')
       ->where( function ($q) {
            $q->where('B.winAmount', '>', 0)
            ->orWhere('C.lossAmount', '>', 0);
        })
       ->get();
       return $todayUsers;
    }

    public function weeklyRepresent()
    {
        $datetime = new EbetDateTime();
        $today    = $datetime->setTimeMil('today');
        $lastWeekdate    = $datetime->setTimeMil('last sunday midnight');

        $sql = BettingDetail::where('betting_details.result', 1)->whereBetween(\DB::raw('DATE(betting_details.created_at)'), [$lastWeekdate, $today]);

        if (Auth::user()->role == 'agent' || Auth::user()->role == 'subagent') {
            $sql->join('assign_users', 'betting_details.user_id', '=', 'assign_users.user_id');
            $sql->where('assign_users.agent_id', Auth::user()->id);
        }
        $iBetDet1 = $sql->sum('betting_details.betting_win');

        $sql2 = BettingDetail::where('betting_details.result', 2)->where('betting_details.status', 2)->whereBetween(\DB::raw('DATE(betting_details.created_at)'), [$lastWeekdate, $today]);

        if (Auth::user()->role == 'agent' || Auth::user()->role == 'subagent') {
            $sql2->join('assign_users', 'betting_details.user_id', '=', 'assign_users.user_id');
            $sql2->where('assign_users.agent_id', Auth::user()->id);
        }
        $iBetDet2 = $sql2->sum('betting_details.risk_amount');

        return ($iBetDet1 - $iBetDet2);
    }
    public function weeklyRepresentLists()
    {
        $week1 = $this->getWeekMonSun(0);
        $sql  =  User::select('users.*', 'B.winAmount', 'C.lossAmount')
        ->leftJoin(\DB::raw("(SELECT user_id, SUM(betting_win) AS winAmount FROM betting_details WHERE result='1' AND DATE(created_at) between '".$week1->start."' and '".$week1->end."' GROUP BY user_id) AS B"), 'B.user_id', '=', 'users.id')
        ->leftJoin(\DB::raw("(SELECT user_id, SUM(risk_amount) AS lossAmount FROM betting_details WHERE result='2' AND status ='2' AND DATE(created_at) between '".$week1->start."' and '".$week1->end."' GROUP BY user_id) AS C"), 'C.user_id', '=', 'users.id')
        ->where( function ($q) {
             $q->where('B.winAmount', '>', 0)
             ->orWhere('C.lossAmount', '>', 0);
        });

        if (Auth::user()->role == 'agent' || Auth::user()->role == 'subagent') {
            $sql->join('assign_users', 'users.id', '=', 'assign_users.user_id');
            $sql->where('assign_users.agent_id', Auth::user()->id);
        }

        return $sql->get();
    }

    public function deletedWager()
    {
        return BettingDetail::where(\DB::raw('YEAR(deleted_at)'), date('Y'))->where(\DB::raw('MONTH(deleted_at)'), date('m'))->onlyTrashed()->count();
    }
    public function deletedWagerLists()
    {
        return BettingDetail::where(\DB::raw('YEAR(deleted_at)'), date('Y'))->where(\DB::raw('MONTH(deleted_at)'), date('m'))->onlyTrashed()->get();
    }

    public function getWeekMonSun($weekOffset) {
        $dt = new \DateTime();
        $dt->setIsoDate($dt->format('o'), $dt->format('W') + $weekOffset);
        return (object) [
            'start' => $dt->format('Y-m-d'),
            'end' => $dt->modify('+6 day')->format('Y-m-d'),
        ];
    }
    public function weeklyWager()
    {
        $week1 = $this->getWeekMonSun(0);
        if (Auth::user()->role == 'agent' || Auth::user()->role == 'subagent') {
            return AgentRate::whereBetween('create_date', [$week1->start, $week1->end])->where('user_id', Auth::user()->id)->sum('rate');
        } else {
            return AgentRate::whereBetween('create_date', [$week1->start, $week1->end])->sum('rate');
        }
    }

    public function weeklyWagerLists()
    {
        $week1 = $this->getWeekMonSun(0);
        if (Auth::user()->role == 'agent' || Auth::user()->role == 'subagent') {
            return User::select('users.id', 'users.username', 'users.name', 'B.total')->where('role', 'agent')->orderBy('users.id', 'desc')
            ->join(\DB::raw("(SELECT user_id, SUM(rate) AS total FROM agent_rates WHERE user_id= '".Auth::user()->id."' AND create_date between '".$week1->start."' and '".$week1->end."' GROUP BY user_id) AS B"), 'B.user_id', '=', 'users.id')
            ->get();
        } else {
            return User::select('users.id', 'users.username', 'users.name', 'B.total')->where('role', 'agent')->orderBy('users.id', 'desc')
            ->join(\DB::raw("(SELECT user_id, SUM(rate) AS total FROM agent_rates WHERE create_date between '".$week1->start."' and '".$week1->end."' GROUP BY user_id) AS B"), 'B.user_id', '=', 'users.id')
            ->get();
        }
    }

    public function allWager()
    {
        if (Auth::user()->role == 'agent' || Auth::user()->role == 'subagent') {
            return AgentRate::where('user_id', Auth::user()->id)->sum('rate');
        } else {
            return AgentRate::sum('rate');
        }
    }

    public function allWagerLists()
    {
        if (Auth::user()->role == 'agent' || Auth::user()->role == 'subagent') {
            return User::select('users.id', 'users.username', 'users.name', 'B.total')->where('role', 'agent')->orderBy('users.id', 'desc')
            ->join(\DB::raw("(SELECT user_id, SUM(rate) AS total FROM agent_rates WHERE user_id='".Auth::user()->id."' GROUP BY user_id) AS B"), 'B.user_id', '=', 'users.id')
            ->get();
        } else {
            return User::select('users.id', 'users.username', 'users.name', 'B.total')->where('role', 'agent')->orderBy('users.id', 'desc')
            ->join(\DB::raw("(SELECT user_id, SUM(rate) AS total FROM agent_rates  GROUP BY user_id) AS B"), 'B.user_id', '=', 'users.id')
            ->get();
        }
    }
}
