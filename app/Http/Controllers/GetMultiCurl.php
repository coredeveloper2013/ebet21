<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GetMultiCurl extends Controller
{
    private $leagueids = array();
    private $teamscorecurls = array();
    private $mh = null;
    private $requestArray = array();
    private $sport_ids = array();
    public function __construct($requestArray)
    {
        $this->requestArray = $requestArray;
        $this->mh = curl_multi_init();
        $this->curlInit();
    }
    public function curlInit()
    {
        $this->getSportFilter();
        $this->getLeagueLists();
        $this->getCurlRequested();
        $this->getLeaguewiseOdds();
    }
    public function getLeagueLists()
    {
        return $this->leagueids = array_unique($this->leagueids);
    }

    public function compareOdds()
    {
        foreach ($this->sport_ids as $key => $val)
        {
            foreach ($val as $k => $v)
            {
                if (property_exists($v,"old"))
                {
                    if (property_exists($v->old,"spread"))
                    {
                        if (property_exists($v->old->spread,'home'))
                        {
                             if (!empty($v->old->spread->home))
                             {
                                 return $v->old->spread->home != $v->new->spread->home ? true : false;
                             }
                        }
                        if (property_exists($v->old->spread,'away'))
                        {
                            if (!empty($v->old->spread->away))
                            {
                                return $v->old->spread->away != $v->new->spread->away ? true : false;
                            }
                        }
                    }
                    else if (property_exists($v->old,"run_line"))
                    {
                        if (property_exists($v->old->run_line,'home'))
                        {
                            if (!empty($v->old->run_line->home))
                            {
                                return $v->old->run_line->home != $v->new->spread->home ? true : false;
                            }
                        }
                        if (property_exists($v->old->run_line,'away'))
                        {
                            if (!empty($v->old->run_line->away))
                            {
                                return $v->old->run_line->away != $v->new->spread->away ? true : false;
                            }
                        }
                    }

                    if (property_exists($v->old,"money"))
                    {

                        if (property_exists($v->old->money,'home'))
                        {
                            if (!empty($v->old->money->home))
                            {
                                return $v->old->money->home != $v->new->money->home ? true : false;
                            }
                        }

                        if (property_exists($v->old->money,'away'))
                        {
                            if (!empty($v->old->money->away))
                            {
                                return $v->old->money->away != $v->new->money->away ? true : false;
                            }
                        }
                    }

                    if (property_exists($v->old,"total"))
                    {
                        if (property_exists($v->old->total,'home'))
                        {
                            if (!empty($v->old->total->home))
                            {
                                return $v->old->total->home != $v->new->total->home ? true : false;
                            }
                        }
                        else if (property_exists($v->old->total,'away'))
                        {
                            if (!empty($v->old->total->away))
                            {
                                return $v->old->total->away != $v->new->total->away ? true : false;
                            }
                        }
                    }

                    if (property_exists($v->old,"teamtotal"))
                    {
                        if (property_exists($v->old->teamtotal,'one'))
                        {
                            if (!empty($v->old->teamtotal->one))
                            {
                                return $v->old->teamtotal->one != $v->new->teamtotal->one ? true : false;
                            }
                        }

                        if (property_exists($v->old->teamtotal,'two'))
                        {
                            if (!empty($v->old->teamtotal->two))
                            {
                                return $v->old->teamtotal->two != $v->new->teamtotal->two ? true : false;
                            }
                        }

                        if (property_exists($v->old->teamtotal,'three'))
                        {
                            if (!empty($v->old->teamtotal->three))
                            {
                                return $v->old->teamtotal->three != $v->new->teamtotal->three ? true : false;
                            }
                        }

                        if (property_exists($v->old->teamtotal,'four'))
                        {
                            if (!empty($v->old->teamtotal->four))
                            {
                                return $v->old->teamtotal->four != $v->new->teamtotal->four ? true : false;
                            }
                        }
                    }

                    if (property_exists($v->old,"draw"))
                    {
                        if (!empty($v->old->draw))
                        {
                            return $v->old->draw != $v->new->draw ? true: false;
                        }
                    }
                }
            }
        }
        return false;
    }

    public function getCurlRequested()
    {

        foreach ($this->leagueids as $key => $val)
        {
            $fetchURL = 'https://api.betsapi.com/v1/bet365/prematch?token=26928-6HWCoyS7zhskxm&FI='.$val;
            $this->teamscorecurls[$val] = curl_init();
            curl_setopt($this->teamscorecurls[$val], CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($this->teamscorecurls[$val], CURLOPT_RETURNTRANSFER, true);
            curl_setopt($this->teamscorecurls[$val], CURLOPT_URL, $fetchURL);
            curl_multi_add_handle($this->mh, $this->teamscorecurls[$val]);
        }
        $index = null;
        do {
            curl_multi_exec($this->mh, $index);
        } while ($index > 0);

    }

    private function validateOdds($odd)
    {
        if (empty($odd) || $odd == "")
        {
            return -1;
        }
        return $this->toAmericanDecimal($odd);
    }

    private function getString($str)
    {
        preg_match('/spread|money|draw|teamtotal_2|teamtotal_1|teamtotal|total|run_line/', $str, $matches);
        if(count($matches) > 0)
        {
            return $matches[0];
        }
        return "nothing";
    }

    public function getSportFilter()
    {
         $sportids = [];
         foreach ($this->requestArray as $reqarray)
         {
             $sport = json_decode($reqarray->sport_league);
             array_push($sportids,$sport[0]);
             array_push($this->leagueids, $reqarray->even_id);
         }
         $unique_sport_ids = array_unique($sportids);

         foreach ($unique_sport_ids as $key => $val)
         {
             $this->sport_ids[$val] = array();
         }

        foreach ($this->requestArray as $reqarr)
        {
            $sport = json_decode($reqarr->sport_league);

            if (array_key_exists($sport[0], $this->sport_ids))
            {
                if (is_array($this->sport_ids[$sport[0]]))
                {
                          if (!array_key_exists($reqarr->even_id,$this->sport_ids[$sport[0]]))
                          {
                              $stdobj = new stdClass();
                              $stdobj->eventid = $reqarr->even_id;
                              $slug_wager = explode(',',$reqarr->slug_wager);
                              $wager  =  $slug_wager[0];
                              $slug   =  $slug_wager[1];
                              $mystr = $this->getString($wager);
                              $scores = intval($reqarr->scores);
                              if ($slug == "home")
                              {
                                  if ($mystr == "spread")
                                  {
                                      $stdobj->old->spread->home = $scores;
                                  }
                                  else if ($mystr == "money")
                                  {
                                      $stdobj->old->money->home = $scores;
                                  }
                                  else if ($mystr == "draw")
                                  {
                                      $stdobj->old->draw = $scores;
                                  }
                                  else if ($mystr == "total")
                                  {
                                      $stdobj->old->total->home = $scores;
                                  }
                                  else if ($mystr == "teamtotal")
                                  {
                                      $stdobj->old->teamtotal->one = $scores;
                                  }
                                  else if ($mystr == "teamtotal_1")
                                  {
                                      $stdobj->old->teamtotal->two = $scores;
                                  }
                                  else if ($mystr == "run_line")
                                  {
                                      $stdobj->old->run_line->home = $scores;
                                  }
                              }
                              else if ($slug == "away")
                              {
                                  if ($mystr == "spread")
                                  {
                                      $stdobj->old->spread->away = $scores;
                                  }
                                  else if ($mystr == "money")
                                  {
                                      $stdobj->old->money->away = $scores;
                                  }
                                  else if ($mystr == "draw")
                                  {
                                      $stdobj->old->draw = $scores;
                                  }
                                  else if ($mystr == "total")
                                  {
                                      $stdobj->old->total->away = $scores;
                                  }
                                  else if ($mystr == "teamtotal")
                                  {
                                      $stdobj->old->teamtotal->three = $scores;
                                  }
                                  else if ($mystr == "teamtotal_2")
                                  {
                                      $stdobj->old->teamtotal->four = $scores;
                                  }
                                  else if ($mystr == "run_line")
                                  {
                                      $stdobj->old->run_line->away = $scores;
                                  }

                              }
                              $this->sport_ids[$sport[0]][$reqarr->even_id] = $stdobj;
                          }
                          else
                          {
                              $objj = $this->sport_ids[$sport[0]][$reqarr->even_id];
                              $slug_wager = explode(',',$reqarr->slug_wager);
                              $wager  =  $slug_wager[0];
                              $slug   =  $slug_wager[1];
                              $mystr = $this->getString($wager);
                              $scores = intval($reqarr->scores);

                              if ($slug == "home")
                              {
                                      if ($mystr == "spread")
                                      {
                                          $objj->old->spread->home = $scores;
                                      }
                                      else if ($mystr == "run_line")
                                      {
                                          $objj->old->run_line->home = $scores;
                                      }
                                      else if ($mystr == "money")
                                      {
                                          $objj->old->money->home = $scores;
                                      }
                                      else if ($mystr == "draw")
                                      {
                                          $objj->old->draw = $scores;
                                      }
                                      else if ($mystr == "total")
                                      {
                                          $objj->old->total->home = $scores;
                                      }
                                      else if ($mystr == "teamtotal")
                                      {
                                          $objj->old->teamtotal->one = $scores;
                                      }
                                      else if ($mystr == "teamtotal_1")
                                      {
                                          $objj->old->teamtotal->two = $scores;
                                      }

                              }
                              else if ($slug == "away")
                              {
                                      if ($mystr == "spread")
                                      {
                                          $objj->old->spread->away = $scores;
                                      }
                                      else if ($mystr == "money")
                                      {
                                          $objj->old->money->away = $scores;
                                      }
                                      else if ($mystr == "draw")
                                      {
                                          $objj->old->draw = $scores;
                                      }
                                      else if ($mystr == "total")
                                      {
                                          $objj->old->total->away = $scores;
                                      }
                                      else if ($mystr == "teamtotal")
                                      {
                                          $objj->old->teamtotal->three = $scores;
                                      }
                                      else if ($mystr == "teamtotal_2")
                                      {
                                          $objj->old->teamtotal->four = $scores;
                                      }
                                      else if ($mystr == "run_line")
                                      {
                                          $objj->old->run_line->away = $scores;
                                      }
                              }

                          }
                }
                else
                {
                        $this->sport_ids[$sport[0]] = array();
                }
            }

        }

    }

    public function getSportLists()
    {
        $this->getLeaguewiseOdds();
        return $this->sport_ids;
    }

    public function getSportWiseEvents($sportid)
    {
        return $this->sport_ids[$sportid];
    }

    public function getLeagueContent($leagueid)
    {
        $event = curl_multi_getcontent($this->teamscorecurls[$leagueid]);
        return json_decode($event);
    }

    public function getLeaguewiseOdds()
    {

        foreach ($this->sport_ids as $key => $val)
        {
            if ($key == 1)
            {

                foreach ($val as $k => $v)
                {
                        $obj = $this->getLeagueContent($v->eventid);
                        $mainsp = $obj->results[0]->main->sp;
                        $valNew = $obj->results[0];
                        $spreadhome  =  empty($mainsp->handicap_result[0]->odds)
                            ? $mainsp->full_time_result[0]->odds
                            : $mainsp->handicap_result[0]->odds;
                        $v->new->spread->home = $this->validateOdds($spreadhome);
                        $spreadaway  = empty($mainsp->handicap_result[2]->odds)
                            ? $mainsp->full_time_result[2]->odds
                            : $mainsp->handicap_result[2]->odds;
                        $v->new->spread->away = $this->validateOdds($spreadaway);
                        $v->new->money->home  = $this->validateOdds($valNew->schedule->sp->main[0]->odds);
                        $v->new->money->away  = $this->validateOdds($valNew->schedule->sp->main[1]->odds);
                        $v->new->total->home  = $this->validateOdds($mainsp->goals_over_under[0]->odds);
                        $v->new->total->away  = $this->validateOdds($mainsp->goals_over_under[1]->odds);
                         $v->new->draw        =   $this->validateOdds($mainsp->full_time_result[1]->odds);
                }
            }
            else if ($key == 12)
            {

                foreach ($val as $k => $v)
                {
                    $obj = $this->getLeagueContent($v->eventid);
                    $mainsp = $obj->results[0]->main->sp;
                    $valNew = $obj->results[0];
                    $spreadhome  =  $mainsp->point_spread[0]->odds;
                    $v->new->spread->home = $this->validateOdds($spreadhome);
                    $spreadaway  = $mainsp->point_spread[1]->odds;
                    $v->new->spread->away = $this->validateOdds($spreadaway);
                    $v->new->money->home  = $this->validateOdds($mainsp->money_line[0]->odds);
                    $v->new->money->away  = $this->validateOdds($mainsp->money_line[1]->odds);
                    $v->new->total->home  = $this->validateOdds($mainsp->game_totals[0]->odds);
                    $v->new->total->away  = $this->validateOdds($mainsp->game_totals[1]->odds);
                }
            }
            else if ($key == 12 || $key == 18)
            {
                foreach ($val as $k => $v)
                {
                    $obj = $this->getLeagueContent($v->eventid);
                    $mainsp = $obj->results[0]->main->sp;
                    $valNew = $obj->results[0];
                    $spreadhome  =  $mainsp->point_spread[0]->odds;
                    $v->new->spread->home = $this->validateOdds($spreadhome);
                    $spreadaway  = $mainsp->point_spread[1]->odds;
                    $v->new->spread->away = $this->validateOdds($spreadaway);
                    $v->new->money->home  = $this->validateOdds($mainsp->money_line[0]->odds);
                    $v->new->money->away  = $this->validateOdds($mainsp->money_line[1]->odds);
                    $v->new->total->home  = $this->validateOdds($mainsp->game_totals[0]->odds);
                    $v->new->total->away  = $this->validateOdds($mainsp->game_totals[1]->odds);
                }
            }
            else if ($key == 16)
            {

                foreach ($val as $k => $v)
                {

                    $obj                  = $this->getLeagueContent($v->eventid);
                    $mainsp               = $obj->results[0]->main->sp;
                    $spreadhome           = $mainsp->run_line[0]->odds;
                    $v->new->spread->home = $this->validateOdds($spreadhome);
                    $spreadaway           = $mainsp->run_line[1]->odds;
                    $v->new->spread->away = $this->validateOdds($spreadaway);
                    $v->new->money->home  = $this->validateOdds($mainsp->money_line[0]->odds);
                    $v->new->money->away  = $this->validateOdds($mainsp->money_line[1]->odds);
                    $v->new->total->home  = $this->validateOdds($mainsp->game_totals[0]->odds);
                    $v->new->total->away  = $this->validateOdds($mainsp->game_totals[1]->odds);
                    $v->new->teamtotal->one = $this->validateOdds($mainsp->team_totals[0]->odds);
                    $v->new->teamtotal->two  = $this->validateOdds($mainsp->team_totals[1]->odds);
                    $v->new->teamtotal->three  = $this->validateOdds($mainsp->team_totals[2]->odds);
                    $v->new->teamtotal->four  = $this->validateOdds($mainsp->team_totals[3]->odds);
                }
            }
            else if ($key == 17)
            {
                foreach ($val as $k => $v)
                {
                    $obj                  = $this->getLeagueContent($v->eventid);
                    $mainsp               = $obj->results[0]->main->sp;
                    $spreadhome           =  $mainsp->puck_line[0]->odds;
                    $v->new->spread->home = $this->validateOdds($spreadhome);
                    $spreadaway           = $mainsp->puck_line[1]->odds;
                    $v->new->spread->away = $this->validateOdds($spreadaway);
                    $v->new->money->home  = $this->validateOdds($mainsp->money_line[0]->odds);
                    $v->new->money->away  = $this->validateOdds($mainsp->money_line[1]->odds);
                    $v->new->draw         = $this->validateOdds($mainsp->money_line_3_way[1]->odds);
                    $v->new->total->home  = $this->validateOdds($mainsp->game_totals[0]->odds);
                    $v->new->total->away  = $this->validateOdds($mainsp->game_totals[1]->odds);
                }
            }
            else if ($key == 9)
            {
                foreach ($val as $k => $v)
                {
                    $obj                  = $this->getLeagueContent($v->eventid);
                    $mainval              = $obj->results[0];
                    $v->new->money->home  = $this->validateOdds($mainval->schedule->sp->main[0]->odds);
                    $v->new->money->away  = $this->validateOdds($mainval->schedule->sp->main[1]->odds);
                }
            }
            else if ($key == 151)
            {

                foreach ($val as $k => $v)
                {
                    $obj                  = $this->getLeagueContent($v->eventid);
                    $mainsp               = $obj->results[0]->main->sp;
                    $v->new->money->home  = $this->validateOdds($mainsp->match_lines[0]->odds);
                    $v->new->money->away  = $this->validateOdds($mainsp->match_lines[3]->odds);
                }
            }
            else if ($key == 13)
            {

                foreach ($val as $k => $v)
                {
                    $obj                  = $this->getLeagueContent($v->eventid);
                    $mainsp               = $obj->results[0]->main->sp;
                    $valNew               = $obj->results[0];
                    $v->new->spread->home = $this->validateOdds($mainsp->set_betting[0]->odds);
                    $v->new->spread->away = $this->validateOdds($mainsp->set_betting[2]->odds);
                    $v->new->money->home  = $this->validateOdds($mainsp->to_win_match[0]->odds);
                    $v->new->money->away  = $this->validateOdds($mainsp->to_win_match[1]->odds);
                    $v->new->total->home  = $this->validateOdds($mainsp->total_games_2_way[0]->odds);
                    $v->new->total->away  = $this->validateOdds($mainsp->total_games_2_way[1]->odds);
                }
            }
        }

    }

    public function getSportLeague($sportid,$leagueid)
    {
        return $this->sport_ids[$sportid][$leagueid];
    }

}
