<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WagerDisable extends Model
{
    protected $fillable = [
        'sport_league','league_name','type',
    ];
}
