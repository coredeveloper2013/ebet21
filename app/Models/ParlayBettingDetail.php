<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParlayBettingDetail extends Model
{
    protected $table = "parlay_betting_details";
    protected $fillable = [
        'user_id','ticket_id', 'event_id', 'is_away', 'is_home', 'points', 'sport_league', 'sport_name', 'betting_condition', 'betting_condition_original','bet_extra_info','risk_amount',
        'parlay_win_amount', 'result', 'type', 'event_date', 'bet_type',
    ];
}
