<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = "notices";

    protected $fillable = [
        'user_id', 'random_id', 'sent_by', 'message', 'is_seen',
    ];
}
