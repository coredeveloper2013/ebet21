<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'assign_id', 'user_id', 'betting_id', 'amount', 'type', 'status', 'description', 'private_note', 'source',
    ];
}
