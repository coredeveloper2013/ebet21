<?php
namespace App\Ebet\Sports;

use App\Ebet\Sports\HomeAway;

class OddsData{
    private $spread;
    private $moneyLine;
    private $total;


    public function __construct()
    {
        $this->spread = new HomeAway;
        $this->moneyLine = new HomeAway;
        $this->total = new HomeAway;
    }

    public function getSpread()
    {
        return $this->spread;
    }
    public function getMoneyLine()
    {
        return $this->moneyLine;
    }
    public function getTotal()
    {
        return $this->total;
    }

}
