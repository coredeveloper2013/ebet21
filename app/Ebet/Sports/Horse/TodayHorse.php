<?php
/**
 * Created by PhpStorm.
 * User: RSCH1
 * Date: 12/10/2019
 * Time: 11:13 PM
 */
namespace App\Ebet\Sports\Horse;


class TodayHorse {

    public function __construct($url)
    {
        return $this->getHorsedata($url);
    }

    public function getHorsedata($url)
    {
        return $this->get_compressed($url);
    }


    public function get_compressed($url)
    {
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"Accept-Language: en-US,en;q=0.8rn" .
                    "Accept-Encoding: gzip,deflate,sdchrn" .
                    "Accept-Charset:UTF-8,*;q=0.5rn" .
                    "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:19.0) Gecko/20100101 Firefox/19.0 FirePHP/0.4rn"
            )
        );

        $context = stream_context_create($opts);
        $content = file_get_contents($url ,false,$context);
        foreach($http_response_header as $c => $h)
        {
            if(stristr($h, 'content-encoding') and stristr($h, 'gzip'))
            {
                $content = gzinflate( substr($content,10,-8) );
            }
        }
        return $content;
    }
}
