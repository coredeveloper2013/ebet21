<?php
namespace App\Ebet\Sports\Contributor;

use stdClass;
use App\Models\WagerDisable;
use Elasticsearch\ClientBuilder;
use App\Ebet\Sports\Straight\Mma;
use App\Ebet\Sports\Straight\Rugby;
use App\Ebet\Sports\Straight\Boxing;
use App\Ebet\Sports\Straight\Hockey;
use App\Ebet\Sports\Straight\Soccer;
use App\Ebet\Sports\Straight\Tennis;
use App\Ebet\Sports\Straight\Cricket;
use App\Ebet\Sports\Straight\Esports;
use App\Ebet\Sports\Straight\Baseball;
use App\Ebet\Sports\Straight\Football;
use App\Ebet\Sports\Straight\Handball;
use App\Ebet\Sports\Straight\Basketball;
use App\Ebet\Sports\Straight\Volleyball;
use App\Ebet\Sports\Straight\RugbyLeague;
use App\Ebet\Sports\Straight\StraightSport;


trait StraightBet
{

    private $teamscorecurls = array();
    private $league_details = array();

    private $elastic_sports = [
        '16' => "baseball",
        '17' => "hockey",
        '9'  => "mma",
        '13' => "tennis",
        '12' => "football",
        '18' => "basketball",
        '11' => "boxing",
        '14' => "volleyball",
        '15' => "cricket",
        '19' => "handball",
        '151' => "esports",
        '20' => "rugby",
        '21' => "rugby_league",
        '1' => "soccer",
    ];

    private $sports = [
        '16' => "Baseball",
        '17' => "Hockey",
        '9'  => "MMA",
        '13' => "Tennis",
        '12' => "Football",
        '151' => "E-Sports",
        '18' => "Basketball",
        '133' => "Golf",   //this is actually 13 to remove the conflict I used 133
        '6'  => "MotorSport",
        '10' => "Cycling",
        '11' => "Boxing",
        '14' => "volleyball",
        '15' => "Cricket",
        '19' => "Handball",
        '20' => "Rugby",
        '21' => "Rugby League",
        '1' => "Soccer",
    ];
    private $sport_images = [
        '16' => "/img/play-icon/Baseball.png",
        '17' => "/img/play-icon/Hockey.png",
        '1' => "/img/play-icon/Soccer.png",
        '9'  => "/img/play-icon/MMA.png",
        '13' => "/img/play-icon/Tennis.png",
        '12' => "/img/play-icon/Football.png",
        '18' => "/img/play-icon/Basketball.png",
        '133' => "/img/play-icon/Golf.png",   //this is actually 13 to remove the conflict I used 133
        '6'  => "/img/play-icon/Motorsports.png",
        '10' => "/img/play-icon/Cycling.png",
        '11' => "/img/play-icon/MMA.png",
        '14' => "/img/play-icon/MMA.png",
        '15' => "/img/play-icon/MMA.png",
        '19' => "/img/play-icon/MMA.png",
        '20' => "/img/play-icon/MMA.png",
        '21' => "/img/play-icon/MMA.png",
        '151' => "/img/play-icon/Esports.png"
    ];
    private  $multicurl = array();
    private $first_column = array();
    private $second_column = array();
    private $third_column = array();
    private $first_column_sports = [
        '13' => "Tennis",
        '17' => "Hockey",
        '9'  => "MMA",
        '16' => "Baseball",
        '15' => "Cricket",
    ];
    private $second_column_sports = [
        '12' => "Football",
        '151' => "E-Sports",
        '11'  => "Boxing",
        '14'  => "Volleyball",
        '19'  => "Handball",
        '20'  => "Rugby",
        '21' => "Rugby League",
    ];
    private $third_column_sports = [
        '18' => "Basketball",
        '1' => "Soccer",
        '133' => "Golf",
        '6' => "MotorSport",
        '10' => "Cycling",
    ];
    private $odds_multicurl  = array();

    public function storeSports()
    {
        $dataArr['12'] = new StraightSport(new Football('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=football_10',12));
        $dataArr['17'] = new StraightSport(new Hockey('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=hockey_10&bm=17,16,',17));
        $dataArr['14'] = new StraightSport(new Volleyball('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=volleyball_10',14));
        $dataArr['16'] = new StraightSport(new Baseball('http://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=baseball_10',16));
        $dataArr['15'] = new StraightSport(new Cricket('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=cricket_10',15));
        $dataArr['19'] = new StraightSport(new Handball('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=handball_10',19));
        $dataArr['13'] = new StraightSport(new Tennis('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=tennis_10',13));
        $dataArr['11'] = new StraightSport(new Boxing('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=boxing_10',11));
        $dataArr['9'] = new StraightSport(new Mma('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=mma_10',9));
        $dataArr['151'] = new StraightSport(new Esports('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=esports_10',151));
        $dataArr['18'] = new StraightSport(new Basketball('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=basket_10&bm=16,',18));
        $dataArr['1'] = new StraightSport(new Soccer('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=soccer_10&bm=56,16,',1));
        $dataArr['20'] = new StraightSport(new Rugby('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=rugby_10',20));
        $dataArr['21'] = new StraightSport(new RugbyLeague('https://www.goalserve.com/getfeed/4d5f7e372939425c2b7d08d76ad955ff/getodds/soccer?cat=rugbyleague_10',21));
        return $dataArr;
    }




    protected function getData()
    {
        $mh = curl_multi_init();
        foreach ($this->third_column_sports as $sport_key => $sport_value) {
            // URL from which data will be fetched
                $fetchURL = "https://api.betsapi.com/v1/bwin/prematch?token=28975-D9tsdZqqKnCdCy&sport_id={$sport_key}";
             if ($sport_key == 133) {
                $fetchURL = "https://api.betsapi.com/v1/bwin/prematch?token=28975-D9tsdZqqKnCdCy&sport_id=13";
            }
            $this->multicurl[$sport_key] = curl_init();
            curl_setopt($this->multicurl[$sport_key], CURLOPT_ENCODING, '');
            curl_setopt($this->multicurl[$sport_key], CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($this->multicurl[$sport_key], CURLOPT_RETURNTRANSFER, true);
            curl_setopt($this->multicurl[$sport_key], CURLOPT_URL, $fetchURL);
            curl_multi_add_handle($mh, $this->multicurl[$sport_key]);
        }
        $index = null;
        do {
            curl_multi_exec($mh, $index);
        } while ($index > 0);
    }

    public function getleagueData(array $arr)
    {

        $mh = curl_multi_init();
        foreach ($arr as $k => $v) {
            foreach ($v as $league_k => $league_v) {
                $fetchURL = "https://api.betsapi.com/v1/bet365/upcoming?sport_id={$k}&token=26928-6HWCoyS7zhskxm&league_id={$league_k}";
                if ($k == 133 || $k == 6 || $k == 10) {
                    $fetchURL = 'https://api.betsapi.com/v1/bwin/event?token=28975-D9tsdZqqKnCdCy&event_id=' . $league_k;
                }
                $this->odds_multicurl[$league_k] = curl_init();
                curl_setopt($this->odds_multicurl[$league_k], CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($this->odds_multicurl[$league_k], CURLOPT_RETURNTRANSFER, true);
                curl_setopt($this->odds_multicurl[$league_k], CURLOPT_URL, $fetchURL);
                curl_multi_add_handle($mh, $this->odds_multicurl[$league_k]);
            }
        }

        $index = null;
        do {
            curl_multi_exec($mh, $index);
        } while ($index > 0);
    }
    protected function resourceDistributionArray()
    {
        $sports = $this->getSportData();
        foreach($this->first_column_sports as $sport_key => $sport_value)
        {
                $sobj  = new stdClass();
                $sobj->game_name = $sport_value;
                $obj = $sports[$sport_key];
                $sports_league = [];
                $arr = [];
                 $arr = $obj['_source'][$this->elastic_sports[$sport_key].'_sports'];

                if(!empty($arr))
                {
                    foreach($arr as $ar)
                    {
                        $sports_league[$ar['league_id']] = $ar['league_name'];
                    }
                }

                $allleague = collect($sports_league)->map(function($item,$k)use($sport_key){
                    return $sport_key."_".$k;
                })->toArray();


               $alldd =  WagerDisable::whereIn('sport_league',$allleague)->get();
                $updateDb = [];
               foreach ($allleague as $key => $value) {
                   $exist = $alldd->search( function($item) use($value) {
                        return $item->sport_league == $value;
                    });
                    if(!$exist)
                    {
                        $updateDb[] =[
                            'sport_league' => $value,
                            'league_name' =>  $sports_league[$key],
                            'type' =>  "active",

                        ];
                    }
               }
                $dd =   WagerDisable::insert($updateDb);
                $sobj->game_data = $sports_league;
                $this->first_column[$sport_key] = $sobj;
        }
        foreach($this->second_column_sports as $sport_key => $sport_value)
        {
                $sobj  = new stdClass();
                $sobj->game_name = $sport_value;
                $obj = $sports[$sport_key];

                $sports_league = [];
                $arr = [];
                $arr = $obj['_source'][$this->elastic_sports[$sport_key].'_sports'];
                if(!empty($arr))
                {
                    foreach($arr as $ar)
                    {
                        $sports_league[$ar['league_id']] = $ar['league_name'];
                    }
                }

                $allleague = collect($sports_league)->map(function($item,$k)use($sport_key){
                    return $sport_key."_".$k;
                })->toArray();


               $alldd =  WagerDisable::whereIn('sport_league',$allleague)->get();
                $updateDb = [];
               foreach ($allleague as $key => $value) {
                   $exist = $alldd->search( function($item) use($value) {
                        return $item->sport_league == $value;
                    });
                    if(!$exist)
                    {
                        $updateDb[] =[
                            'sport_league' => $value,
                            'league_name' =>  $sports_league[$key],
                            'type' =>  "active",

                        ];
                    }
               }
                $dd =   WagerDisable::insert($updateDb);
                $sobj->game_data = $sports_league;
                $this->second_column[$sport_key] = $sobj;
        }
        foreach ($this->third_column_sports as $sport_key => $sport_value) {

                $sobj  = new stdClass();
                $sobj->game_name = $sport_value;
            if($sport_key == 18 || $sport_key == 1)
            {
                $obj = $sports[$sport_key];
                $sports_league = [];
                $arr = [];
                $arr = $obj['_source'][$this->elastic_sports[$sport_key].'_sports'];
                if(!empty($arr))
                {
                    foreach($arr as $ar)
                    {
                        $sports_league[$ar['league_id']] = $ar['league_name'];
                    }
                }
                $allleague = collect($sports_league)->map(function($item,$k)use($sport_key){
                    return $sport_key."_".$k;
                })->toArray();


               $alldd =  WagerDisable::whereIn('sport_league',$allleague)->get();
                $updateDb = [];
               foreach ($allleague as $key => $value) {
                   $exist = $alldd->search( function($item) use($value) {
                        return $item->sport_league == $value;
                    });
                    if(!$exist)
                    {
                        $updateDb[] =[
                            'sport_league' => $value,
                            'league_name' =>  $sports_league[$key],
                            'type' =>  "active",

                        ];
                    }
               }
                $dd =   WagerDisable::insert($updateDb);
                $sobj->game_data = $sports_league;
            }
            else{

                $getresponse =  curl_multi_getcontent($this->multicurl[$sport_key]);
                $obj = json_decode($getresponse);
                $sobj->game_data = $obj;

            }
            $this->third_column[$sport_key] = $sobj;
        }
    }

    public function getSportData()
    {
        $client = ClientBuilder::create()->build();
        foreach($this->elastic_sports as $sport_id => $sport)
        {
            $dataArr[$sport_id] = $client->get(['index'=> $sport,'id'=>'sport_'.$sport_id.'_'.$sport]);
        }
        return $dataArr;
    }
    public function getLeagueSport($league_id,$sport_obj)
    {
        foreach($sport_obj as $sport)
        {
            if($sport->league_id == $league_id)
              return $sport;
        }
        return null;
    }

}
