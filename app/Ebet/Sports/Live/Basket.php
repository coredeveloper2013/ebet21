<?php
namespace App\Ebet\Sports\Live;


use stdClass;
use App\Ebet\Sports\LiveSportAbstract;

class Basket extends LiveSportAbstract{


  protected $dataSet = [
    'spread'  => '1446',
    'money_line'  => '180032',
    'total'  => '1450',
  ];

   public function __construct($url)
   {
        parent::__construct($url);
   }

   public function getFormatted()
    {

        error_reporting(0);
        $allData = $this->getDataArr();
        $sortedbyleague = [];
        foreach($allData as $data)
        {
            $item  = new stdClass;
            $item->time = $data->info->secunds;
            $item->datetime = $data->info->start_date;
            $item->hr = $data->info->period;
            $item->id = $data->info->id;
            $team_names =  explode('vs',$data->info->name);
            $item->home = new stdClass;
            $item->home->name  =  $team_names[0];
            $item->away = new stdClass;
            $item->away->name =   $team_names[1];
            $item->ss =  new stdClass;
            $item->ss->home = $this->breakdownss($data->info->score)[0];
            $item->ss->away = $this->breakdownss($data->info->score)[1];
            $item->eventimers = $data->info->secunds;
            $item->league->name =  $data->info->league;
            foreach($data->sport_odd as $key => $odd)
            {
                if($key == $this->spread)
                {
                    foreach($odd->participants as $participant)
                    {
                        if($participant->short_name  == 'Home')
                        {
                            $item->odd->first = $this->numberFormatted($participant->value_us);
                            $item->odd->first_val =   $participant->handicap;
                            $item->home->id  =  $participant->id;
                        }
                        if($participant->short_name  == 'Away')
                        {
                            $item->odd->second = $this->numberFormatted($participant->value_us);
                            $item->odd->second_val = $participant->handicap;
                            $item->away->id  =  $participant->id;
                        }

                    }

                }


                if($key == $this->money_line)
                {
                    foreach($odd->participants as $participant)
                    {
                        if($participant->short_name  == 'Home')
                        {
                            $item->match->over->odd = $this->numberFormatted($participant->value_us);
                            $item->money_line->draw_line = 0;
                        }

                        if($participant->short_name  == 'Away')
                        {
                            $item->match->under->odd = $this->numberFormatted($participant->value_us);
                        }
                    }
                }

                if($key == $this->total)
                {
                    foreach($odd->participants as $participant)
                    {
                        if($participant->name  == 'Over')
                        {
                            $item->next->first  =  $this->numberFormatted($participant->value_us);
                            $item->over->handicap =   $participant->name.' '.$participant->handicap;
                        }
                        if($participant->name  == 'Under')
                        {
                            $item->next->second = $this->numberFormatted($participant->value_us);
                            $item->under->handicap =  $participant->name.' '.$participant->handicap;
                        }
                    }
                }
            }
            $tag = false;
            foreach ($sortedbyleague as  $sbl) {
                if ($sbl->even_name == $item->league->name) {
                    $tag = true;
                    array_push($sbl->results, $item);
                    break;
                }
            }
            if (!$tag) {
             $obj = new stdClass();
             $obj->even_id   = $item->id;
             $obj->even_name = $item->league->name;
             $obj->results  =  array();
             array_push($obj->results, $item);
             array_push($sortedbyleague, $obj);
            }

        }
        return $sortedbyleague;
    }
}
