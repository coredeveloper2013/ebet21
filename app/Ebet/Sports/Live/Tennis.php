<?php
namespace App\Ebet\Sports\Live;


use stdClass;
use App\Ebet\Sports\LiveSportAbstract;

class Tennis extends LiveSportAbstract{

  protected $spread = 'winner';
  protected $money_line = 'Set-1';
  protected $total = 'Set-2';

  protected $dataSet = [
    'winner'  => '1763',
    'Set-1'  => '130020',
    'Set-2'  => '130015',
  ];

   public function __construct($url)
   {
        parent::__construct($url);
   }


   public function dataTest()
   {
       dd($this->getDataArr());
   }



   public function getFormatted()
   {
       error_reporting(0);
       $allData = $this->getDataArr();
       $sortedbyleague = [];
       foreach($allData as $data)
       {
           $item  = new stdClass();
           $item->time = $data->info->secunds;
           $item->datetime = $data->info->start_date;
           $item->hr = 12;
           $ss = $this->breakdownss($data->info->score);
           $item->period =  $data->info->period;
           $item->ss =  new stdClass;
           $item->ss->home = $ss[0];
           $item->ss->away = $ss[1];
           $item->id = $data->info->id;
           $team_names =  explode('vs',$data->info->name);
           $item->home = new stdClass;
           $item->home->name  =  $team_names[0];
           $item->away = new stdClass;
           $item->away->name =   $team_names[1];
           $item->league->name =  $data->info->league;
           $item->eventimers = 15245852;
           foreach($data->sport_odd as $key => $odd)
           {
                //winner

               if($key == $this->spread)
               {
                   foreach($odd->participants as $participant)
                   {
                       if($participant->short_name  == 'Home')
                       {
                           $item->winner_set->home_odd = $this->numberFormatted($participant->value_us);
                           $item->home->id  =  $participant->id;
                       }
                       if($participant->short_name  == 'Away')
                       {
                           $item->winner_set->away_odd = $this->numberFormatted($participant->value_us);
                           $item->away->id =   $participant->id;
                       }

                   }

               }
                //Set-1

               if($key == $this->money_line)
               {
                   foreach($odd->participants as $participant)
                   {
                       if($participant->short_name  == 'Home')
                       {
                           $item->current_set->home_odd = $this->numberFormatted($participant->value_us);
                       }

                       if($participant->short_name  == 'Away')
                       {
                           $item->current_set->away_odd = $this->numberFormatted($participant->value_us);
                       }
                   }
               }
                 //set-2

               if($key == $this->total)
               {
                   foreach($odd->participants as $participant)
                   {
                       if($participant->short_name == "Home")
                       {
                           $item->current_set_two->home_odd  =  $this->numberFormatted($participant->value_us);
                       }
                       if($participant->short_name == "Away")
                       {
                           $item->current_set_two->away_odd   = $this->numberFormatted($participant->value_us);
                       }
                   }
               }
               $item->no_of_innings = 0;
           }

           //grouping respect to league_name


           $tag = false;
           foreach ($sortedbyleague as  $sbl) {
               if ($sbl->even_name == $item->league->name) {
                   $tag = true;
                   array_push($sbl->results, $item);
                   break;
               }
           }
           if (!$tag) {
            $obj = new stdClass();
            $obj->even_id   = $item->id;
            $obj->even_name = $item->league->name;
            $obj->results  =  array();
            array_push($obj->results, $item);
            array_push($sortedbyleague, $obj);
           }
       }
       return $sortedbyleague;
   }
}
