<?php
namespace App\Ebet\Sports\Live;

use App\Ebet\Sports\LiveSportAbstract;
use stdClass;

class LiveBetFormat
{

     private $liveSports;

     public function __construct(LiveSportAbstract $liveSports)
     {
        $this->liveSports = $liveSports;
     }

     public function getData()
     {
         return $this->liveSports->getFormatted();
     }

}



