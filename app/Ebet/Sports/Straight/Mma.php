<?php
namespace App\Ebet\Sports\Straight;

use stdClass;
use App\Ebet\Sports\SportAbstract;
use App\Ebet\Sports\Contributor\WagerColumn;

class Mma extends SportAbstract{
    use WagerColumn;

    public function __construct($url,$sport_id)
    {
        parent::__construct($url,$sport_id);
        $this->getOdds();
    }


    public function getOdds()
    {
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->matches))
                {
                    foreach($cat->matches->match as $match)
                    {
                        if(isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {
                                $item = $if_event;
                                $item->money_line = null;
                                if(isset($match->odds))
                                {
                                    foreach($match->odds->type as $type)
                                    {
                                        if(!empty($type->attributes()->value) && ($type->attributes()->value =='Home/Away' && $type->attributes()->id =='2'))
                                        {
                                            if(isset($type->bookmaker) && count($type->bookmaker->attributes()) > 0)
                                            {
                                                foreach($type->bookmaker as $bm)
                                                {
                                                    $extn = new stdClass;
                                                    $extn->sport_id = 9;
                                                    $this->moneyLine($item->event_odd,$bm,$extn);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }


                    }
                }
                else{
                    dd($cat['@attributes']);
                }
            }
        }
    }

    public function getItem()
    {
        return $this->leagues;
    }








}



