<?php
namespace App\Ebet\Sports\Straight;

use stdClass;
use App\Ebet\Sports\SportAbstract;
use App\Ebet\Sports\Contributor\WagerColumn;

class Tennis extends SportAbstract{

    use WagerColumn;

    public function __construct($url,$sport_id)
    {
        parent::__construct($url,$sport_id);
        $this->getOdds();
        $this->getPropsBet();
    }

    public function getOdds()
    {
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->matches))
                {
                    foreach($cat->matches->match as $match)
                    {
                        if(isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {

                                $item = $if_event;
                                if(isset($match->odds))
                                {
                                    foreach($match->odds->type as $type)
                                    {
                                        if(isset($type->attributes()->value) && ($type->attributes()->value =='Home/Away' && $type->attributes()->id =='2'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach($type->bookmaker as $bm){

                                                    $extn = new stdClass;
                                                    $extn->sport_id = 13;
                                                    $this->moneyLine($item->event_odd,$bm,$extn);

                                                  /*   if(isset($bookmaker) && count($bookmaker->attributes()) > 0){

                                                        if(($bookmaker->attributes()->name == 'bet365' && $bookmaker->attributes()->id == 16)
                                                        || (
                                                            $bookmaker->attributes()->name == 'BetFred'
                                                            &&
                                                            $bookmaker->attributes()->id == 116))
                                                            {
                                                            if(isset($bookmaker->odd))
                                                            {

                                                              $this->moneyLine($item->event_odd,$bookmaker);
                                                            }
                                                        }
                                                    } else {
                                                        if(($bookmaker->attributes()->name == 'bet365' && $bookmaker->attributes()->id == 16)
                                                            || ($bookmaker->attributes()->name == 'BetFred' && $bookmaker->attributes()->id == 116))
                                                        {
                                                            if(isset($bookmaker->odd))
                                                            {
                                                                $this->moneyLine($item->event_odd,$bookmaker);
                                                            }
                                                        }
                                                    } */
                                                }
                                            }
                                        }

                                       if(isset($type->attributes()->value) && ($type->attributes()->value =='Asian Handicap (1st Set)' && $type->attributes()->id =='22629'))
                                       {
                                           if(isset($type->bookmaker))
                                           {
                                                foreach($type->bookmaker as $bm){

                                                    if(isset($bm->handicap))
                                                    {
                                                        foreach($bm->handicap as $handicap)
                                                        {
                                                            $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                            $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                            $handicap->bm =  json_encode($bmrk);
                                                            $extn = new stdClass;
                                                            $extn->sport_id = 13;
                                                            $this->spread($item->event_odd,$handicap,$extn);
                                                        }
                                                    }

                                                 /*    if(($bookmaker->attributes()->name =='bet365' && $bookmaker->attributes()->id == 16)
                                                    || ($bookmaker->attributes()->name == 'BetFred' && $bookmaker->attributes()->id == 116))
                                                    {
                                                        if(isset($bookmaker->handicap))
                                                        {
                                                            foreach($bookmaker->handicap as $handicap)
                                                            {
                                                               $this->spread($item->event_odd,$handicap);
                                                            }
                                                        }
                                                    } */
                                                }
                                            }
                                        }
                                        //
                                        if(!empty($type->attributes()->value) && ($type->attributes()->value == 'Over/Under by Games in Match' && $type->attributes()->id =='22624'))
                                        {
                                            if(isset($type->bookmaker) && count($type->bookmaker->attributes()) > 0)
                                            {
                                                foreach($type->bookmaker as $bm){

                                                    if(isset($bm->total))
                                                    {
                                                        foreach($bm->total as $total)
                                                        {
                                                            $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                            $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                            $total->bm =  json_encode($bmrk);
                                                            $extn = new stdClass;
                                                            $extn->sport_id = 13;
                                                            $this->total($item->event_odd,$bm->total);
                                                        }
                                                    }

                                                  /*   if(($bookmaker->attributes()->name == 'bet365' && $bookmaker->attributes()->id == 16)
                                                    || ($bookmaker->attributes()->name == 'BetFred' && $bookmaker->attributes()->id == 116))
                                                    {
                                                        if(isset($bookmaker->total))
                                                        {
                                                           $this->total($item->event_odd,$bookmaker->total[0]);
                                                        }
                                                    } */

                                                }
                                           }
                                       }
                                    }
                                }
                            }
                        }


                    }
                }
                else{
                    dd($cat['@attributes']);
                }
            }
        }
    }

    public function getItem()
    {
        return $this->leagues;
    }

    public function getPropsBet(){
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->matches))
                {
                    foreach($cat->matches->match as $match)
                    {
                        if(isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {
                                $item = $if_event;
                                $item->prop_bets = [];
                                $item->prop_bets['first_set'] = [];  //13.1
                                $item->prop_bets['set_betting'] = [];  //13.22
                                $item->prop_bets['tie_break'] = [];    //13.23
                                $item->prop_bets['set_match'] = [];     //13.24

                                if(isset($match->odds))
                                {
                                    foreach($match->odds->type as $type)
                                    {
                                           //first set

                                           //spread

                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Asian Handicap (1st Set)' && $type->attributes()->id =='22629'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {
                                                    if(isset($bm->handicap))
                                                    {
                                                        foreach($bm->handicap as $handicap)
                                                        {
                                                            if($handicap->odd)
                                                            {
                                                                $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                                $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                                $handicap->bm =  json_encode($bmrk);
                                                                $extn = new stdClass;
                                                                $extn->sport_id = 13;
                                                                if($this->spread($item->prop_bets['first_set'],$handicap,$extn)) break 2;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //money line
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Home/Away (1st Set)' && $type->attributes()->id =='22628'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {

                                                    if(isset($bm->odd))
                                                    {
                                                        $extn = new stdClass;
                                                        $extn->sport_id = 13;
                                                        if($this->moneyLine($item->prop_bets['first_set'],$bm,$extn))
                                                        {
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //total
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Over/Under (1st Set)' && $type->attributes()->id =='22627'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {
                                                    if(isset($bm->total))
                                                    {
                                                        foreach($bm->total as $total)
                                                        {
                                                            $bmrk['name'] = isset($bm->attributes()->name)?$bm->attributes()->name:"";
                                                            $bmrk['id'] = isset($bm->attributes()->id)?$bm->attributes()->id:"";
                                                            $total->bm =  json_encode($bmrk);
                                                            $extn = new stdClass;
                                                            $extn->sport_id = 13;
                                                            if($this->tennisTotal($item->prop_bets['first_set'],$total,$extn)) break 2;
                                                        }
                                                    }
                                                }
                                            }
                                        }


                                         //set betting
                                         if(isset($type->attributes()->value) && ($type->attributes()->value == 'Set Betting' && $type->attributes()->id =='22632'))
                                         {
                                             if(isset($type->bookmaker))
                                             {
                                                 foreach ($type->bookmaker as $bm) {

                                                    if($bm->attributes()->name == 'bet365' && $bm->attributes()->id == 16)
                                                    {
                                                           $this->yesNo($item->prop_bets['set_betting'],$bm);
                                                    }
                                                 }

                                             }
                                         }

                                         //Tie-break
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Tie-break' && $type->attributes()->id =='22645'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {
                                                    if($bm->attributes()->name == 'bwin' && $bm->attributes()->id == 2)
                                                    {
                                                           $this->yesNo($item->prop_bets['tie_break'],$bm);

                                                    }
                                                }

                                            }
                                        }

                                         //set-match
                                        if(isset($type->attributes()->value) && ($type->attributes()->value == 'Set / Match' && $type->attributes()->id =='22647'))
                                        {
                                            if(isset($type->bookmaker))
                                            {
                                                foreach ($type->bookmaker as $bm) {
                                                    if($bm->attributes()->name == 'bet365' && $bm->attributes()->id == 16)
                                                    {
                                                           $this->yesNo($item->prop_bets['set_match'],$bm);
                                                    }
                                                }

                                            }
                                        }

                                    }
                                }

                            }
                        }
                    }
                }
                else{
                    dd($cat->attributes());
                }
            }
        }
    }


    public function tennisTotal(array &$arr,$odd_obj,$extn = null)
    {
        $item  = json_decode(json_encode($odd_obj),true);
        $bbm   =  isset($item['bm'])? json_decode($item['bm'],true): [];

        if(isset($item['odd']))
        {
            if(is_array($item['odd']))
            {
                foreach($item['odd'] as $total)
                {

                    if(isset($total['@attributes']))
                    {

                        if($total['@attributes']['name'] == 'Over')
                        {

                            $arr['total']['home']['odd']  =  $total['@attributes']['value'];
                        }

                        if($total['@attributes']['name'] == 'Under')
                        {
                            $arr['total']['away']['odd'] =  $total['@attributes']['value'];
                        }
                    }
                }
            }
        }

        if(isset($item['@attributes']))
        {
            if(isset($item['@attributes']['name']) && !empty($item['@attributes']['name']))
            {
                $arr['total']['home']['under_over'] = "Over ".$item['@attributes']['name'];
                $arr['total']['away']['under_over'] = "Under ".$item['@attributes']['name'];
            }
        }

            if(!isset($arr['total']['home']['odd'])) return;
            if(!isset($arr['total']['away']['odd'])) return;
            if($arr['total']['home']['odd'] =='' && $arr['total']['away']['odd'] == '') return;
            else{
                if(!empty($bbm))
                {
                    $arr['total']['bm']['name'] =  $bbm['name'];
                    $arr['total']['bm']['id']   =  $bbm['id'];
                }
                return true;
            }



    }


    public function yesNo(array &$arr,$odd_obj)
    {
        $item  =   json_decode( json_encode( $odd_obj),true);
        if(isset($item['odd']))
        {
           if(is_array($item['odd']))
           {
                foreach($item['odd'] as $moneyLine)
                {
                    if(isset($moneyLine['@attributes']))
                    {
                        $arr[$moneyLine['@attributes']['name']] = $moneyLine['@attributes']['value'];
                    }
                }
           }
        }
    }









}



