<?php
namespace App\Ebet\Results\Sports;

use App\Ebet\Results\BaseResult;

class Esport extends BaseResult
{
    public function __construct($url,$sport_id)
    {
        parent::__construct($url,$sport_id);
        $this->getAdditionalResult();
    }

    public function evaluateResult()
    {
        return $this->events;
    }


    public function getAdditionalResult()
    {
        foreach($this->dataArr->match as $cat)
        {

            if($this->ifLeague($cat->attributes()->league_id) !=null)
            {

                if(isset($cat->attributes()->id))
                {
                    $if_event = $this->ifEvent($cat->attributes()->id);
                    if($if_event != null)
                    {
                        $item = $if_event;
                        $item->first_quarter = "";
                        $item->second_quarter = "";
                        $item->third_quarter = "";
                        $item->fourth_quarter = "";
                        $item->overtime = "";
                        $item->event_info->status = ( (array) $cat->attributes()->status)[0];
                    }
                }
            }
        }
    }
}

