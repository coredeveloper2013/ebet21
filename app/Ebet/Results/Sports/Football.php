<?php
namespace App\Ebet\Results\Sports;

use App\Ebet\Results\BaseResult;

class Football extends BaseResult
{
    public function __construct($url,$sport_id)
    {
        parent::__construct($url,$sport_id);
        $this->getAdditionalResult();
    }

    public function evaluateResult()
    {
        return $this->events;
    }


    public function getAdditionalResult()
    {
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->match))
                {
                    foreach($cat->match as $match)
                    {

                        if(isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {


                                $item = $if_event;
                                $item->first_quarter = "";
                                $item->second_quarter = "";
                                $item->third_quarter = "";
                                $item->fourth_quarter = "";
                                $item->overtime = "";

                                $item->event_info->status = ( (array) $match->attributes()->status)[0];

                                if(isset($match->events))
                                {
                                    foreach ($match->events as $event) {


                                        if(isset($event->firstquarter))
                                        {
                                            $item->first_quarter = ( (array)  $event->firstquarter->attributes()->score)[0];
                                        }

                                        if(isset($event->secondquarter))
                                        {
                                            $item->second_quarter =  ( (array) $event->secondquarter->attributes()->score)[0];
                                        }

                                        if(isset($event->thirdquarter))
                                        {
                                            $item->third_quarter =  ( (array) $event->thirdquarter->attributes()->score)[0];
                                        }
                                        if(isset($event->fourthquarter))
                                        {
                                            $item->fourth_quarter =  ( (array) $event->fourthquarter->attributes()->score)[0];
                                        }
                                        if(isset($event->overtime))
                                        {
                                            $item->overtime =  ( (array) $event->overtime->attributes()->score)[0];
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }
    }
}

