<?php
namespace App\Ebet\Results\Sports;

use App\Ebet\Results\BaseResult;

class Tennis extends BaseResult
{
    public function __construct($url,$sport_id)
    {
        parent::__construct($url,$sport_id);
        $this->getAdditionalResult();
    }

    public function evaluateResult()
    {
        return $this->events;
    }
    public function getAdditionalResult()
    {
        foreach($this->dataArr->category as $cat)
        {
            if($this->ifLeague($cat->attributes()->id) !=null)
            {
                if(isset($cat->match))
                {
                    foreach($cat->match as $match)
                    {
                        if(isset($match->attributes()->id))
                        {
                            $if_event = $this->ifEvent($match->attributes()->id);
                            if($if_event != null)
                            {
                                $item = $if_event;
                                $item->first_quarter = "";
                                $item->second_quarter = "";
                                $item->third_quarter = "";
                                $item->fourth_quarter = "";
                                $item->fifth_quarter = "";
                                $item->overtime = "";
                                $item->event_info->status ='';
                                $item->event_info->status = ( (array) $match->attributes()->status)[0];

                                ( (array) $match->player[0]->attributes()->name)[0];

                                if(isset($match->player[0]))
                                {
                                    $event = $match->player[0];

                                    if(isset($event->attributes()->s1))
                                    {
                                        $item->first_quarter .= ( (array)  $event->attributes()->s1)[0];
                                    }

                                    if(isset($event->attributes()->s2))
                                    {
                                        $item->second_quarter .=  ( (array) $event->attributes()->s2)[0];
                                    }

                                    if(isset($event->attributes()->s3))
                                    {
                                        $item->third_quarter .=  ( (array) $event->attributes()->s3)[0];
                                    }
                                    if(isset($event->attributes()->s4))
                                    {
                                        $item->fourth_quarter .=  ( (array) $event->attributes()->s4)[0];
                                    }
                                    if(isset($event->attributes()->s5))
                                    {
                                        $item->fifth_quarter .=  ( (array) $event->attributes()->s5)[0];
                                    }
                                }

                                if(isset($match->player[1]))
                                {
                                    $event = $match->player[1];
                                    if(isset($event->attributes()->s1))
                                    {
                                        if(( (array)  $event->attributes()->s1)[0] !='')
                                        $item->first_quarter .= "-".( (array)  $event->attributes()->s1)[0];
                                    }

                                    if(isset($event->attributes()->s2))
                                    {
                                        if(( (array) $event->attributes()->s2)[0] !='')
                                        $item->second_quarter .= "-". ( (array) $event->attributes()->s2)[0];
                                    }

                                    if(isset($event->attributes()->s3))
                                    {
                                        if(( (array)  $event->attributes()->s3)[0] !='')
                                        $item->third_quarter .= "-". ( (array) $event->attributes()->s3)[0];
                                    }
                                    if(isset($event->attributes()->s4))
                                    {
                                        if(( (array)  $event->attributes()->s4)[0] !='')
                                        $item->fourth_quarter .= "-". ( (array) $event->attributes()->s4)[0];
                                    }
                                    if(isset($event->attributes()->s5))
                                    {
                                        if(( (array)  $event->attributes()->s5)[0] !='')
                                        $item->fifth_quarter .= "-". ( (array) $event->attributes()->s5)[0];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
