<?php
namespace App\Ebet\Db\Elastic;

use Elasticsearch\ClientBuilder;
use App\Ebet\Sports\Contributor\StraightBet;
use App\Ebet\Db\Elastic\Contributor\ElasticDataFilter;
use App\Ebet\Db\Elastic\Contributor\ElasticDataExtract;

class StoreStraightOdds {
    use StraightBet,ElasticDataFilter,ElasticDataExtract;

    private $sports_arr = [];
    private $leagues_Store = [];
    private $counter = 0;
     /**
      * Class constructor.
      */
     public function __construct()
     {
        $this->sports_arr = $this->storeSports();
        $this->_orderByLeague();
     }

     public function _orderByLeague()
     {
        foreach($this->sports_arr as $sprtKey => $sprt)
        {
           $sports_data =  $this->sports_arr[$sprtKey]->getLeagues();

           foreach($sports_data as $sport_data)
           {
                $this->counter++;
                $this->leagues_Store[$sprtKey.'_'.$sport_data->league_id] = $sport_data;
           }
        }
     }

    public function saveSports()
    {
        $client = ClientBuilder::create()->build();
        $arr = [];
        if(!empty($this->sports_arr))
        {
            foreach($this->sports_arr as $sprtKey => $sprt)
            {
                $params = [
                    'index' => $this->elastic_sports[$sprtKey],
                    'id'    => 'sport_'.$sprtKey."_".$this->elastic_sports[$sprtKey],
                    'body'  => [$this->elastic_sports[$sprtKey].'_sports' => json_decode( json_encode($this->sports_arr[$sprtKey]->getLeagues()), true)]
                ];
                $arr[] = $client->index($params);
            }
        }

        if(!empty($this->leagues_Store))
        {
            foreach($this->leagues_Store as $league_id => $data)
            {
                $league_params = [
                    'index' => 'league_wise_data',
                    'id'    => $league_id,
                    'body'  => ['league_data' => $data ]
                ];
                $arr[] = $client->index($league_params);
            }
        }

        print_r($arr);
    }
    /*
    Test function to fetch the data
    */
    public function getQueryResponse()
    {
        $client = ClientBuilder::create()->build();

        $params = [
            'index' => "mytestindex",
            'id'    => '123456',
            'body'  => ['sports' => 'Hello world']
        ];

      $client->index($params);


      $get_index = [

        'index' => "league_wise_data",
        'id'    => '17_3363'
      ];

        $response = $client->getSource($get_index);
         print_r($response);
    }

    public function __invoke()
    {
        $this->saveSports();
    }
}
