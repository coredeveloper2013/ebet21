<?php

namespace App\Providers;

use App\Utility\DateTime\DateTime;
use Illuminate\Support\ServiceProvider;

class UtilityServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
       /*  $this->app->bind('datetime', function () {
            return new DateTime();
        }); */
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
