<?php

use Illuminate\Database\Seeder;
use App\Models\BettingDetail;
class BettingDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(BettingDetail::class, 500)->create();
    }
}
