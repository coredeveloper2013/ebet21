<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => "test",
            'role' => "user",
            'name' => "test",
            'password' => bcrypt("12345678"),
        ]);

        User::create([
            'username' => "admin",
            'role' => "admin",
            'name' => "admin",
            'password' => bcrypt("12345678"),
        ]);
        User::create([
            'username' => "agent",
            'role' => "agent",
            'name' => "agent",
            'password' => bcrypt("12345678"),
        ]);

        for ($i = 1; $i <= 4; $i++) {
            User::create([
                'username' => "agent" . $i,
                'role' => "agent",
                'name' => "agent" . $i,
                'password' => bcrypt("12345678"),
            ]);
        }
        for ($i = 1; $i <= 25; $i++) {
            User::create([
                'username' => "test" . $i,
                'role' => "user",
                'name' => "test" . $i,
                'password' => bcrypt("12345678"),
            ]);
        }
    }
}
