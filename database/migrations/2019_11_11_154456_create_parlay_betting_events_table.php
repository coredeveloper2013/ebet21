<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParlayBettingEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parlay_betting_events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('event_id')->nullable();
            $table->integer('score_away')->nullable();
            $table->integer('score_home')->nullable();
            $table->integer('winner_away')->nullable();
            $table->integer('winner_home')->nullable();
            $table->text('score_away_by_period')->nullable();
            $table->text('score_home_by_period')->nullable();
            $table->text('event_status')->nullable();
            $table->string('betting_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parlay_betting_events');
    }
}
