<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParlayEventIdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parlay_event_ids', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->bigInteger('ticket_id');
            $table->string('event_ids')->nullable();
            $table->decimal('risk_amount', 10, 2);
            $table->decimal('parlay_win_amount', 10, 2);
            $table->string('result')->nullable()->comment('0:for pending,1:for win,2:for loss');
            $table->string('types')->nullable()->comment('1:for run line,2:for money line,3:for total runs	');
            $table->string('match_place')->nullable()->comment('1:is_away,0:is_home');
            $table->integer('free_play')->nullable();
            $table->string('betting_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parlay_event_ids');
    }
}
