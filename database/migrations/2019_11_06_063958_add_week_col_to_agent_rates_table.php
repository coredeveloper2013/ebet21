<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWeekColToAgentRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agent_rates', function (Blueprint $table) {
            $table->date('week_start')->nullable();
            $table->date('week_end')->nullable();
            $table->tinyInteger('week_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agent_rates', function (Blueprint $table) {
            $table->dropColumn('week_start');
            $table->dropColumn('week_end');
            $table->dropColumn('week_number');
        });
    }
}
