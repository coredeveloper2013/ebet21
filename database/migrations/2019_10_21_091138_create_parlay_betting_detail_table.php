<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParlayBettingDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parlay_betting_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('event_id');
            $table->integer('is_away');
            $table->integer('is_home');
            $table->string('points');
            $table->string('sport_league');
            $table->string('sport_name');
            $table->string('betting_condition');
            $table->string('betting_condition_original');
            $table->decimal('risk_amount', 10,2)->default(0);
            $table->decimal('parlay_win_amount', 10,2)->default(0);
            $table->integer('result')->default(0);
            $table->integer('type')->default(0);
            $table->dateTime('event_date');
            $table->string('bet_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parlay_betting_details');
    }
}
