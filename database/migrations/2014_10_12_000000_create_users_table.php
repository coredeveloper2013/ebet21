<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('role', ['admin', 'agent', 'subagent', 'user'])->default('user');
            $table->string('username', 100)->unique();
            $table->string('name');
            $table->string('image')->nullable();
            $table->string('email', 191)->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->float('coins')->default(0);
            $table->decimal('available_balance', 10,2)->default(0)->unsigned();
            $table->decimal('free_pay_balance', 10,2)->default(0)->unsigned();
            $table->decimal('credit_limit', 10,2)->default(0);
            $table->integer('betting')->default(0);
            $table->string('last_login', 64)->nullable();
            $table->timestamp('registered')->useCurrent();
            $table->string('lang')->nullable();
            $table->string('timezone')->default('Tokyo');
            $table->enum('sex', ['male', 'female'])->nullable();
            $table->text('bio')->nullable();
            $table->string('website')->nullable();
            $table->enum('notify', ['all', 'won', 'cancelled'])->nullable();
            $table->string('sendmail', 100)->nullable()->comment('0 = disabled, >0 is category id');
            $table->tinyInteger('remind')->nullable()->comment('remind user 30 mins b4 game ends');
            $table->tinyInteger('site_news')->nullable()->comment('0=disabled, 1=periodically,2=daily');
            $table->tinyInteger('game_digest')->nullable()->comment('receive news and updates');
            $table->enum('privacy_page', ['all', 'following', 'none'])->nullable();
            $table->enum('privacy_result', ['all', 'following', 'none'])->nullable();
            $table->decimal('rate', 10,2)->default(0);
            $table->decimal('agent_total', 10,2)->default(0);
            $table->string('referred_by')->nullable();
            $table->tinyInteger('status')->default(0)->comment('0-denied;1-approved;2-pending;3-highroller');
            $table->tinyInteger('is_casino')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
